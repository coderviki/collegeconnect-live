<?php

/**

 * The header for our theme

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package WordPress

 * @subpackage collegeconnect

 * @since 1.0

 * @version 1.0

 */



?>

<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js no-svg">

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="/wp-content/themes/collegeconnect/style.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--  <script src="http://collegeconnect.ph/js/bootstrap.min.js"></script> -->
<link rel="apple-touch-icon" sizes="57x57" href="/images/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
<link rel="manifest" href="/images/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/images/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


<?php wp_head(); ?>
<?php
error_reporting(0);
?>
</head>

<body>

<!-- Navbar (sit on top) -->

<div class="w3-top">

  <ul class="w3-navbar w3-white w3-card-2" id="myNavbar"> 

<a href="/" class="w3-wide" style="float:left;"><img style=padding:10px;height:80px;width:auto;" src="/images/CollegeConnect.png"></a>        

    <!-- Right-sided navbar links -->

    <li class="w3-right w3-hide-small desktop-menu">

      <a href="/about/">ABOUT</a>

      <a href="/colleges/">COLLEGES</a>

      <a href="/contact/"><i class="fa fa-envelope"></i> CONTACT</a>

      <a class="signup-btn" href="http://collegeconnect.ph/wp-login.php?action=register" style="background: orange;border-radius: 9px;color:white;"><strong><span style="">SIGN UP</span></strong></a>

    </li>

<!-- Hide right-floated links on small screens and replace them with a menu icon -->

    <!-- <li>

      <a href="javascript:void(0)" class="w3-right w3-hide-large w3-hide-medium" onclick="w3_open()">

        <i class="fa fa-bars w3-padding-right w3-padding-left"></i>

      </a>

    </li> -->

    <li class="mobile-menu-btn">

      <a href="#"><i class="fa fa-bars"></i> Menu</a>

    </li>



  </ul>

</div>

    <div class="nav-mobile-menu nav-mobile-menu-hide">

    <ul>

      <li><a href="/about/">ABOUT</a></li>

      <li><a href="/colleges/">COLLEGES</a></li>

      <li><a href="/contact/"><i class="fa fa-envelope"></i> CONTACT</a></li>

      <li><a class="signup-btn" href="http://collegeconnect.ph/wp-login.php?action=register" style="background: orange;border-radius: 9px;color:white;"><strong><span style="">SIGN UP</span></strong></a></li>

      </ul>

    </div>



<!-- Sidenav on small screens when clicking the menu icon -->

<!-- <nav class="w3-sidenav w3-black w3-card-2 w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="mySidenav">

  <a href="javascript:void(0)" onclick="w3_close()" class="w3-large w3-padding-16">Close ×</a>

  <a href="#about" onclick="w3_close()">ABOUT</a>

  <a href="#team" onclick="w3_close()">TEAM</a>

  <a href="#work" onclick="w3_close()">WORK</a>

  <a href="#pricing" onclick="w3_close()">PRICING</a>

  <a href="#contact" onclick="w3_close()">CONTACT</a>

</nav> -->