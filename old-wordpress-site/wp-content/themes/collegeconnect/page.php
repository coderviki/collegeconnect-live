<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
 
get_header(); ?>
<div id="main-content" class="main-content">
<div style="background:url(/images/collegeconnect_header.jpg);width:100%;height:300px;background-position:center;margin-top: .29%;text-align:center;">
<?php
$page_title = $wp_query->post->post_title;
echo "<div style='padding-top:8%;'><span style='color:#ffffff;'><h1>";
print_r($page_title);
echo "</h1></span></div>";
?>
</div>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
	while (have_posts()) : the_post(); 
					// Include the page content template.
					the_content();
endwhile;
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();