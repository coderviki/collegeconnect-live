<?php /* Template Name: Home Page Template */ ?>

<?php get_header(); ?>



<!-- Header with full-height image -->

<header class="bgimg-1 w3-display-container w3-grayscale-min" id="home">

  <div class="w3-display-left w3-padding-xxlarge w3-text-white">

    <span class="w3-jumbo w3-hide-small">Find the best college near you</span><br>

    <span class="w3-xxlarge w3-hide-large w3-hide-medium">Find the best college near you</span><br>

    <span class="w3-large">Hundreds of colleges to choose from</span>

    <p><?php echo do_shortcode('[sabai-directory-search-form page="shortcodes/shortcode-sabai-directory" no_cat="1"]'); ?></p>


  </div> 

  <div class="w3-display-bottomleft w3-padding-xxlarge w3-text-grey w3-large">

<!--    <a href="#" class="w3-hover-text-white"><i class="fa fa-facebook-official"></i></a>

    <a href="#" class="w3-hover-text-white"><i class="fa fa-flickr"></i></a>

    <a href="#" class="w3-hover-text-white"><i class="fa fa-instagram"></i></a>

    <a href="#" class="w3-hover-text-white"><i class="fa fa-twitter"></i></a>

    <a href="#" class="w3-hover-text-white"><i class="fa fa-linkedin"></i></a> -->

  </div>

</header>

<!-- <?php echo do_shortcode('[sabai-directory-photo-slider photo_size="thumbnail" sort="newest" slider_auto="0"]'); ?> -->

<!-- Courses -->

<div class="custom-section-courses">

<span style="text-align:center;"><h2>Top Courses in the Philippines</h2></span>

	<div class="container container-courses">
		<div class="information-main" style="text-align: center;">

<div class="boxes-courses"><a href="http://collegeconnect.ph/colleges/course/arts-and-humanities"><span><img src="/images/collegeconnect_arts-and-humanities-courses.png" alt="" width="50px"><br><br>Arts and Humanities</span></a></div>
<div class="boxes-courses"><a href="http://collegeconnect.ph/colleges/course/business-administration"><span><img src="/images/collegeconnect_business-administration-courses.png" alt="" width="50px"><br><br>Business Administration</span></a></div>
<div class="boxes-courses"><a href="http://collegeconnect.ph/colleges/course/education"><span><img src="/images/collegeconnect_education-courses.png" alt="" width="50px"><br><br>Education</span></a></div>
<div class="boxes-courses"><a href="http://collegeconnect.ph/colleges/course/communication"><span><img src="/images/collegeconnect_communications-courses.png" alt="" width="50px"><br><br>Communications</span></a></div>
<div class="boxes-courses"><a href="http://collegeconnect.ph/colleges/course/engineering"><span><img src="/images/collegeconnect_engineering-courses.png" alt="" width="50px"><br><br>Engineering</span></a></div>
<div class="boxes-courses"><a href="http://collegeconnect.ph/colleges/course/it"><span><img src="/images/collegeconnect_it-courses.png" alt="" width="50px"><br><br>IT</span></a></div>
<div class="boxes-courses"><a href="http://collegeconnect.ph/colleges/course/science-and-math"><span><img src="/images/collegeconnect_math-and-science-careers.png" alt="" width="50px"><br><br>Math and Science</span></a></div>
<div class="boxes-courses"><a href="http://collegeconnect.ph/colleges/course/social-and-behavioral-science"><span><img src="/images/collegeconnect_social-and-behavioral-sciences-courses.png" alt="" width="50px"><br><br>Social and Behavioral Science</span></a></div>
<div class="boxes-courses"><a href="http://collegeconnect.ph/colleges/course/tourism"><span><img src="/images/collegeconnect_tourism-courses.png" alt="" width="50px"><br><br>Tourism</span></a></div>

		  <div class="clearfix"> </div>

		</div>

	</div>

</div>

<!-- courses ended here -->

<!-- cities starts here -->

<div class="custom-section-cities">

<span style="text-align:center;color:black;"><h2>Top Cities To Study in Metro Manila</h2></span>

	<div class="container container-cities">
		<div class="information-main" style="text-align: center;">

<div class="boxes-cities"><a href="http://collegeconnect.ph/colleges?keywords=&address=manila"><span><img src="/images/collegeconnect_manila.jpg" alt="" width="250px"><span style="color:black;font-size: 15pt;font-weight: bold;">Manila</span></span></a></div>
<div class="boxes-cities"><a href="http://collegeconnect.ph/colleges?keywords=&address=quezon+city"><span><img src="/images/collegeconnect_quezon-city.JPG" alt="" width="250px"><span style="color:black;font-size: 15pt;font-weight: bold;">Quezon City</span></span></a></div>
<div class="boxes-cities"><a href="http://collegeconnect.ph/colleges?keywords=&address=makati"><span><img src="/images/collegeconnect_makati.jpg" alt="" width="250px"><span style="color:black;font-size: 15pt;font-weight: bold;">Makati</span></span></a></div>
<div class="boxes-cities"><a href="http://collegeconnect.ph/colleges?keywords=&address=pasig"><span><img src="/images/collegeconnect_pasig.jpg" alt="" width="250px"><span style="color:black;font-size: 15pt;font-weight: bold;">Pasig</span></span></a></div>
<div class="boxes-cities"><a href="http://collegeconnect.ph/colleges?keywords=&address=taguig"><span><img src="/images/collegeconnect_taguig.jpg" alt="" width="250px"><span style="color:black;font-size: 15pt;font-weight: bold;">Taguig</span></span></a></div>
		  <div class="clearfix"> </div>

		</div>

	</div>

</div>


<!--information start here-->

<div class="custom-section-why">

<span style="text-align:center;font-style:bold;"><h2>Why CollegeConnect?</h2></span>

	<div class="container">

		<div class="information-main">

			<div class="col-md-4 information-grid">

				<span class="info-icons hovicon effect-4 sub-b"><img src="/images/find-a-college.png" alt=""></span>

				<h4>Find a college faster</h4>

				<p>We help you find the best college quickly</p>

			</div>

			<div class="col-md-4 information-grid">

				<span class="info-icons hovicon effect-4 sub-b"><img src="/images/search-in-seconds.png" alt=""></span>

				<h4>Turn hours of searching into seconds</h4>

				<p>Discover what makes one college better than another using our website.</p>

			</div>

			<div class="col-md-4 information-grid">

				<span class="info-icons hovicon effect-4 sub-b"><img src="/images/access-info-faster.png" alt=""></span>

				<h4>Access information much faster</h4>

				<p>Spend less time finding the information you need.</p>

			</div>

		  <div class="clearfix"> </div>

		</div>

	</div>

</div>

<!--information end here-->





<div class="leaves">

	<div class="container">

		<div class="leaves-main wow zoomIn animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomIn;">

			<h1>Want To Make An Impact On Education?</h1>

			<h6>Show us what you have.</h6>

			<p>We are always looking for contributors to help us in our vision to make higher education better in the Philippines.</p>

		   <a href="/contact/" class="hvr-push">Discover More</a>

		</div>

	</div>

</div>



<?php get_footer(); ?>