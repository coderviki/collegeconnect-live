<?php
/* 
* Theme: PREMIUMPRESS CORE FRAMEWORK FILE
* Url: www.premiumpress.com
* Author: Mark Fail
*
* THIS FILE WILL BE UPDATED WITH EVERY UPDATE
* IF YOU WANT TO MODIFY THIS FILE, CREATE A CHILD THEME
*
* http://codex.wordpress.org/Child_Themes
*/
if (!defined('THEME_VERSION')) {	header('HTTP/1.0 403 Forbidden'); exit; }
?>

<?php global $post, $CORE, $userdata;

// CAN WE DISPLAY THE GOOGLE MAP BOX ?
if( get_post_meta($post->ID,'showgooglemap',true) == "yes"){
	$my_long  			= get_post_meta($post->ID,'map-log',true);
	$my_lat  			= get_post_meta($post->ID,'map-lat',true);
	$GOOGLEMAPADDRESS 	= 'https://www.google.com/maps/dir/'.str_replace(",","",str_replace(" ","+",get_post_meta($post->ID,'map_location',true)))."/".$my_lat.",".trim($my_long);
	if(isset($GLOBALS['CORE_THEME']['google_zoom'])){ $default_zoom = $GLOBALS['CORE_THEME']['google_zoom']; }else{ $default_zoom = 7; }
	if($default_zoom == ""){ $default_zoom = 7; }
}

 

ob_start();
?>
<a name="toplisting"></a>

<div class="panel default-listing panel-default">
[STICKER] <div class="pull-right dsocial">[D_SOCIAL size=16]</div>

<div class="panel-heading">
[TITLE]
</div>

<ol class="breadcrumb">
  <li>[FAVS]  </li>
  <li> [DATE] </li>
  <?php if(isset($GLOBALS['CORE_THEME']['feedback_enable']) && $GLOBALS['CORE_THEME']['feedback_enable'] == '1' && $userdata->ID != $post->post_author){ ?><li>[FEEDBACK]</li><?php } ?>
  
  <li  class="pull-right">[RATING small=1]</li>
 
  <li class="pull-right hidden-xs"><i class="fa fa-area-chart"></i> [hits] <?php echo $CORE->_e(array('single','19')); ?></li>
  <li class="pull-right hidden-xs">#[ID]</li>
  
</ol>
 
 
<div class="col-md-6">
	[IMAGES]
</div>
<div class="col-md-6"> 
	[EXCERPT] 
	
	[FIELDS smalllist=1]
	
	[THEMEEXTRA]
 	 
	<div class="clearfix"></div>	
</div>

<div class="clearfix"></div> 



<div class="board">

	<div class="board-inner">
    
	<ul class="nav nav-tabs" id="Tabs">
    
	<div class="liner"></div>
					
    <li class="active"><a href="#home" data-toggle="tab" title="<?php echo $CORE->_e(array('single','34')); ?>"><span class="round-tabs one"><i class="fa fa-file-text-o"></i></span></a></li>

    <li><a href="#t4" data-toggle="tab" title="<?php echo $CORE->_e(array('single','37')); ?>"> <span class="round-tabs two"><i class="fa fa-comments-o"></i></span> </a></li>
				  
    <li><a href="#messages" data-toggle="tab" title="<?php echo $CORE->_e(array('single','16')); ?>"><span class="round-tabs three"><i class="fa fa-bars"></i></span> </a></li>

    <li><a href="#settings" data-toggle="tab" title="<?php echo $CORE->_e(array('single','36')); ?>"><span class="round-tabs four"><i class="glyphicon glyphicon-comment"></i></span></a></li>
	
	<?php if(isset($GOOGLEMAPADDRESS)){ ?>
	
    <li><a href="#doner" data-toggle="tab" title="<?php echo $CORE->_e(array('button','52')); ?>" id="GoogleMapTab"><span class="round-tabs five"><i class="fa fa-map-marker"></i></span></a></li>
    
	<?php } ?>
   
    </ul></div>

	<div class="tab-content">
    
	<div class="tab-pane fade in active" id="home"> <h1>[TITLE]</h1> [CONTENT]</div>
    
	<div class="tab-pane fade" id="t4">[COMMENTS tab=0]</div>
    
	<div class="tab-pane fade" id="messages"><div class="well"><h3><?php echo $CORE->_e(array('single','16')); ?></h3> <hr /> [FIELDS] </div> </div>
    
	<div class="tab-pane fade" id="settings">[CONTACT style="2"]</div>
    
	<?php if(isset($GOOGLEMAPADDRESS)){ ?>
    
	<div class="tab-pane fade" id="doner">
	
		<div class="well">
		<a href="<?php echo $GOOGLEMAPADDRESS; ?>" target="_blank" class="btn btn-default pull-right">
		<?php echo $CORE->_e(array('button','56')); ?></a>
		<h3><?php echo $CORE->_e(array('add','67')); ?></h3>
		<hr />
		[GOOGLEMAP]	
		</div>
		<script>		
		jQuery( "#GoogleMapTab" ).click(function() {
		setTimeout(function () {google.maps.event.trigger(map, "resize"); map.setCenter(marker.getPosition());  }, 200);
		});
		</script>
	</div>
    <?php }else{ ?>    
    
	<style>.board .nav-tabs > li {width: 25%;}</style>
	
    <?php } ?>
	
	<div class="clearfix"></div>
	
	</div>
</div></div>

 

<script>
jQuery(function(){jQuery('a[title]').tooltip();});
</script>

<?php $SavedContent = ob_get_clean(); 
echo hook_item_cleanup($CORE->ITEM_CONTENT($post, hook_content_single_listing($SavedContent)));

?>