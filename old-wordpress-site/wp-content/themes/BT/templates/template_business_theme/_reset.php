<?php

add_action('hook_new_install','_newinstall');
function _newinstall(){ global $CORE, $wpdb;

 

// 4. CUSTOM SUBMISSION FIELDS
$submissionfields[0] = array(
"name" => "Website Link", 
"help" => "", 
"fieldtype" => "input", 
"values" => "", 
"taxonomy" => "", 
"key" => "url", 
"order" => "1", 
"required" => "yes", 
"ID" => "0", 
);
$submissionfields[1] = array(
"name" => "Phone", 
"help" => "", 
"fieldtype" => "input", 
"values" => "", 
"taxonomy" => "", 
"key" => "phone", 
"order" => "1", 
"required" => "yes", 
"ID" => "0", 
);
// SAVE ARRAY DATA		 
update_option( "submissionfields", $submissionfields);


// 5. DEFAULT TEMPLATE DATA
$GLOBALS['theme_defaults']['template'] 		= "template_business_theme";

// WEBSITE LOGO
//$GLOBALS['theme_defaults']['logo_url'] 	= THEME_URI."/templates/".$GLOBALS['theme_defaults']['template']."/img/logo.png";
$GLOBALS['theme_defaults']['logo_text1'] 	= "BUSINESS<span>DIRECTORY</span>";
$GLOBALS['theme_defaults']['logo_text2'] 	= "Your company slogan";
$GLOBALS['theme_defaults']['logo_icon'] 	= 0;

// EXTRA DEMO CONTENT
$GLOBALS['theme_defaults']['hdata'] = array(
"j1" => array(
"title1" => "Welcome to our directory website", 
"title2" => "Search and find what your looking for below;", 
"title3" => "You can customize this area and enter your own title text via the admin area of the theme.", 
"img" => get_template_directory_uri()."/templates/template_business_theme/img/bg1.png" ),


); 
 
// HOME PAGE OBJECT SETUP
$GLOBALS['theme_defaults']["homepage"]["widgetblock1"] = "basicsearch_0,new4_1,categoryblock_2,recentlisting_3";	
					
$GLOBALS['theme_defaults']['widgetobject']['basicsearch']['0'] = array(
'col1' => "<h1>Welcome to our directory website</h1><h2>Search and find what your looking for below;</h2>",
'col2' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tempus eleifend risus ut congue. Pellentesque nec lacus elit. Pellentesque convallis nisi ac augue pharetra eu tristique neque consequat. Mauris ornare tempor nulla, vel sagittis diam convallis eget.</p>",
'fullw' => "yes",
);
$GLOBALS['theme_defaults']['widgetobject']['new4']['1'] = array(
'text' => "<section class='visible-lg'><div class=\"wlt_object_head_4\"><div class=\"row\"><ul><li class=\"col-md-4 col-sm-4 col-xs-12\"><div class=\"b\">    <a href=\"#\" class=\"item-link\">    <img src=\"".THEME_URI."/templates/".$GLOBALS['theme_defaults']['template']."/img/demo/home1.jpg\"  alt=\"\" class=\"wp-image-2596\">    <div class=\"item-html\">    <h3><span>Add Listing</span>your business</h3>    <span class=\"btn1\">more</span>    </div>    </a></div></li><li class=\"col-md-4 col-sm-4 col-xs-12\"><div class=\"b\">    <a href=\"\" class=\"item-link\">    <img src=\"".THEME_URI."/templates/".$GLOBALS['theme_defaults']['template']."/img/demo/home2.jpg\"  alt=\"\" class=\"wp-image-2596\">    <div class=\"item-html\">     <h3><span>Contact Us</span> get in touch</h3>    <span class=\"btn1\">more</span>    </div>    </a></div></li><li class=\"col-md-4 col-sm-4 col-xs-12\"><div class=\"b\">    <a href=\"\" class=\"item-link\">    <img src=\"".THEME_URI."/templates/".$GLOBALS['theme_defaults']['template']."/img/demo/home3.jpg\"  alt=\"\" class=\"wp-image-2596\">    <div class=\"item-html\">     <h3><span>Our Blog</span> latest news</h3>    <span class=\"btn1\">more</span>    </div> </a></div></li> </ul></div></div></section>",
'fullw' => "yes",
);

$GLOBALS['theme_defaults']['widgetobject']['categoryblock']['2'] = array(
'title' => "Popular Directory Categories",
'btnview' => "yes",
'image' => "yes-side",
'desc' => "yes",
'iconsize' => "yes",
'catcount' => "yes",
'pcats' => "12",
'subcats' => "2",
'subcatcount' => "yes",
'subcatempty' => "yes",
'fullw' => "yes",
);
$GLOBALS['theme_defaults']['widgetobject']['recentlisting']['3'] = array(
'title' => "Recently Added Listings",
'query' => "orderby=rand&posts_per_page=8",
'style' => "grid",
'perrow' => "4",
'pagenav' => "no",
'fullw' => "yes",
);	

 
// CONTENT LAYOUT / SINGLE LAYOUT
$GLOBALS['theme_defaults']['content_layout'] = "listing-business";
$GLOBALS['theme_defaults']['single_layout'] = "listing-business";

// MENU LAYOUT
$GLOBALS['theme_defaults']['layout_menu'] = 2;

// HEADER LAYOUT
$GLOBALS['theme_defaults']['layout_header'] = 1;

// HEADER ACCOUNT DETAILS
$GLOBALS['theme_defaults']['header_accountdetails'] = 1;

 
$GLOBALS['theme_defaults']['itemcode'] 		= "[IMAGE hover=1 hover_content=5] [RATING]<div class='caption'><h1>[TITLE]</h1><div class='hidden_details'><ul><li>[ICON id='fa fa-group' fa=1] [hits] user views [ICON id='fa fa-comments' fa=1] [COMMENT_COUNT] reviews</li></ul> <hr />[EXCERPT size=200] <div class='clearfix'></div><hr />[BUTTON] <span class='right hidden-xs hidden-sm' style='padding-right:20px; line-height:40px;'>[FAVS]</span>[ICON id='fa fa-tags' fa=true] [CATEGORY] [TAGS] [DISTANCE]</div></div>"; 



update_option('wlt_reset_itemcode', $GLOBALS['theme_defaults']['itemcode']);


$GLOBALS['theme_defaults']['listingcode']		= '<div class="block"><div class="block-title"><h1><span>[TITLE]</span></h1></div><div class="block-content"> [IMAGES] <span class="right" style="margin-top:7px; margin-right:20px;">[CONTACT button=1] </span><ul class="nav nav-tabs navstyle1" id="Tabs"> <li class="active"><a href="#t1" data-toggle="tab">{Description}</a></li> <li><a href="#t2" data-toggle="tab">{Details}</a></li></ul><div class="tab-content"><div class="tab-pane active" id="t1">[TOOLBOX]<h5>[TITLE]</h5>[CONTENT] [GOOGLEMAP] </div><div class="tab-pane" id="t2">[FIELDS hide="map"]</div></div></div></div> [COMMENTS box=1]'; 

 
// 5. REINSTALL THE SAMPLE DATA CATEGORIES 
$new_cat_array = array(
"Automotive" => array ('Car Accessories','Car Dealers','Car Wash','Car Repairs','Car Rentals'),
"Business Services" => array ('Advertising','Employment Agencies','Careers Center','Legal Services'),
"Education" => array ('Schools','Collages','Universities','Library','Museum'),
"Food" => array ('Bakers','Resturants','Take Aways','McDonalds','Drive Throughts'),
"Health & Medicine" => array ('Walk Ins','Hospitals','Pharmacy','Drug Store'),
"IT Services" => array ('Website Designers','Hosting Services','Online Marketing','Advertising'),
"Shopping" => array ('Retail Stores','Furniture Stores','Home Stores','Warehouses','Markets'),
"Sports & Recreation" => array ('Extreme','Fishing','Golf','Hunting','Running'),
"Travel & Transport" => array ('Hotels','Motels','Bed &amp; Breakfast','Airport','Taxi Services'),
);
$cat_icons_small = array('','fa-car','fa-archive','fa-university','fa-coffee','fa-heart-o','fa-desktop','fa-cc-visa','fa-futbol-o','fa-bus');
 
$saved_cats_array = array(); $ff=1;
foreach($new_cat_array as $cat=>$catlist){
	if ( is_term( $cat , THEME_TAXONOMY ) ){	
		$term = get_term_by('slug', $cat, THEME_TAXONOMY);		 
		$nparent  = $term->term_id;
		$saved_cats_array[] = $term->term_id;	
	}else{
	
		$cat_id = wp_insert_term($cat, THEME_TAXONOMY, array('cat_name' => $cat, 'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tempus eleifend risus ut congue. Pellentesque nec lacus elit. Pellentesque convallis nisi ac augue pharetra eu tristique neque consequat. Mauris ornare tempor nulla, vel sagittis diam convallis eget.' ));
		if(!is_object($cat_id) && isset($cat_id['term_id'])){
		$saved_cats_array[] = $cat_id['term_id'];
		$nparent = $cat_id['term_id'];
		}else{
		$saved_cats_array[] = $cat_id->term_id;
		$nparent = $cat_id->term_id;
		}			
		// add in icon for this cat		
		$GLOBALS['theme_defaults']['category_icon_'.$nparent] = THEME_URI."/templates/".$GLOBALS['theme_defaults']['template']."/img/demo/cat_big_".$ff.".jpg";
		$GLOBALS['theme_defaults']['category_icon_small_'.$nparent] = $cat_icons_small[$ff];
		
		
		$ff++; 
	}
	/* SUB CATS */
	if(is_array($catlist)){
		foreach($catlist as $newcat){
		wp_insert_term($newcat, THEME_TAXONOMY, array('cat_name' => $newcat,'parent' => $nparent));
		}	
	}
}
// 6. INSTALL THE SAMPLE DATA LISTINGS
$posts_array = array(
"1" => array("name" =>"Example Listing 1"),
"2" => array("name" =>"Example Listing 2"),
"3" => array("name" =>"Example Listing 3"),
"4" => array("name" =>"Example Listing 4"),
"5" => array("name" =>"Example Listing 5"),
"6" => array("name" =>"Example Listing 6"),
"7" => array("name" =>"Example Listing 7"),
"8" => array("name" =>"Example Listing 8"),
"9" => array("name" =>"Example Listing 9")
);
$i = 1;
foreach($posts_array as $np){
 
	$my_post = array();
	$my_post['post_title'] 		= $np['name'];
	$my_post['post_content'] 	= "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tempus eleifend risus ut congue. Pellentesque nec lacus elit. Pellentesque convallis nisi ac augue pharetra eu tristique neque consequat. Mauris ornare tempor nulla, vel sagittis diam convallis eget.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tempus eleifend risus ut congue. Pellentesque nec lacus elit. Pellentesque convallis nisi ac augue pharetra eu tristique neque consequat. Mauris ornare tempor nulla, vel sagittis diam convallis eget.</p><blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p><small>Someone famous <cite title='Source Title'>Source Title</cite></small>
</blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tempus eleifend risus ut congue. Pellentesque nec lacus elit. Pellentesque convallis nisi ac augue pharetra eu tristique neque consequat. Mauris ornare tempor nulla, vel sagittis diam convallis eget.</p><dl class='dl-horizontal'>
				<dt>Description lists</dt>
				<dd>A description list is perfect for defining terms.</dd>
				<dt>Euismod</dt>
				<dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
				<dd>Donec id elit non mi porta gravida at eget metus.</dd>
				<dt>Malesuada porta</dt>
				<dd>Etiam porta sem malesuada magna mollis euismod.</dd>
				<dt>Felis euismod semper eget lacinia</dt>
				<dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
			  </dl>";
	$my_post['post_type'] 		= THEME_TAXONOMY."_type";
	$my_post['post_status'] 	= "publish";
	$my_post['post_category'] 	= "";
	$my_post['tags_input'] 		= "";
	$POSTID 					= wp_insert_post( $my_post );	
 	
	add_post_meta($POSTID, "phone", "123-444-555-6666");
	add_post_meta($POSTID, "url", "http://premiumpress.com");
	add_post_meta($POSTID, "image", get_template_directory_uri()."/framework/img/demo/noimage.png");	
	add_post_meta($POSTID, "featured", "no");
	// UPDATE CAT LIST
	wp_set_post_terms( $POSTID, $saved_cats_array, THEME_TAXONOMY );
$i++;		
}



// TURN ON GALLEYRY MAP
$GLOBALS['theme_defaults']['default_gallery_map'] = 1;

 

} 

?>