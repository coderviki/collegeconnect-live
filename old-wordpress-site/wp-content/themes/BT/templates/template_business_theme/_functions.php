<?php

// ADD IN DIRECTORY GLOBAL
define('WLT_BUSINESS',true);

// TURN OFF DEFAULT PRICE SEARCH
define('DEFAULTS_PRICE_SEARCH',false);


// GOOGLE FONTS
function load_fonts() {
	if(!defined('WLT_CHILDTHEME')){
	wp_register_style('googleFonts', 'http://fonts.googleapis.com/css?family=Roboto:300,400,500,700');
	wp_enqueue_style( 'googleFonts');
	}
}    
add_action('wp_print_styles', 'load_fonts');


add_action('hook_admin_2_homeedit','_hook_admin_2_homeedit');
function _hook_admin_2_homeedit($c){

$new = array(
 

't1' => array(
	"n" => "Image Block 1", 
	"data" => array(
		"title" 	=> array( "t" => "Title Text <br><small>(use span tags to seperate text)</small>", "d" => "<span>Add Listing</span>your business" ),
		"img" 	=> array( "t" => "Image (400px/95px) ","type" => "upload", "d" => THEME_URI."/templates/".$GLOBALS['CORE_THEME']['template']."/img/home1.jpg"  ),
 		"link" 	=> array( "t" => "Link", "d" => "http://"  ),
 		
	),
),

't2' => array(
	"n" => "Image Block 2", 
	"data" => array(
		"title" 	=> array( "t" => "Title Text <br><small>(use span tags to seperate text)</small>", "d" => "<span>Contact Us</span> get in touch"  ),
		"img" 	=> array( "t" => "Image (400px/95px)","type" => "upload", "d" => THEME_URI."/templates/".$GLOBALS['CORE_THEME']['template']."/img/home2.jpg"  ),
 		"link" 	=> array( "t" => "Link", "d" => "http://"  ),
 		
	),
),

't3' => array(
	"n" => "Image Block 3", 
	"data" => array(
		"title" 	=> array( "t" => "Title Text <br><small>(use span tags to seperate text)</small>",  "d" => "<span>Our Blog</span> latest news" ),
		"img" 	=> array( "t" => "Image (400px/95px)","type" => "upload", "d" => THEME_URI."/templates/".$GLOBALS['CORE_THEME']['template']."/img/home3.jpg"  ),
 		"link" 	=> array( "t" => "Link", "d" => "http://"  ),
 		
	),
),

);

$c = array_merge($c, $new);

 
return $c;
}
 
?>