<?php
/* 
* Theme: PREMIUMPRESS CORE FRAMEWORK FILE
* Url: www.premiumpress.com
* Author: Mark Fail
*
* THIS FILE WILL BE UPDATED WITH EVERY UPDATE
* IF YOU WANT TO MODIFY THIS FILE, CREATE A CHILD THEME
*
* http://codex.wordpress.org/Child_Themes
*/
if (!defined('THEME_VERSION')) {	header('HTTP/1.0 403 Forbidden'); exit; }

global $CORE;
 

// GET HOME PAGE CUSTOM DATA
$HDATA = $GLOBALS['CORE_THEME']['hdata'];

// MAP DEFAULTS
$defaults = $CORE->wlt_google_getdefaults(); 
 
// HEADER
get_header($CORE->pageswitch()); 

?> 

<?php if(!isset($GLOBALS['CORE_THEME']['home_section1']) || (isset($GLOBALS['CORE_THEME']['home_section1']) && $GLOBALS['CORE_THEME']['home_section1'] == '1' ) ){  ?>

<?php if(isset($GLOBALS['CORE_THEME']['homesliderid']) && $GLOBALS['CORE_THEME']['homesliderid'] != "" && $GLOBALS['CORE_THEME']['homeslider'] == 1 ){  

echo do_shortcode(stripslashes("[rev_slider ".$GLOBALS['CORE_THEME']['homesliderid']."]")); 

}else{ ?>

<div class="BasicSearchBox" <?php if(isset($HDATA['j1']['img']) && strlen($HDATA['j1']['img']) > 1){ echo 'style="background: url(\''.stripslashes($HDATA['j1']['img']).'\');"'; } ?>><div class="container-fluid"><div class="row">

<h1><?php echo stripslashes($HDATA['j1']['title1']); ?></h1>
            
<h2><?php echo stripslashes($HDATA['j1']['title2']); ?></h2>
            
<div class="well">    
				<form method="get" action="<?php echo get_home_url(); ?>/" class="clearfix">
				<div class="col-md-5">        
					<input type="text" value="" placeholder="<?php echo $CORE->_e(array('homepage','6','flag_noedit')); ?>" class="hsk form-control" name="s" />        
				</div>
				<div class="col-md-5">        
					<select name="cat1" class="form-control"><option value=""><?php echo $CORE->_e(array('button','44','flag_noedit')); ?></option><?php echo $CORE->CategoryList(array(0,false,0,THEME_TAXONOMY)); ?></select> 
				</div>
				<div class="col-md-2">
				<button class="btn btn-primary"><?php echo $CORE->_e(array('button','11')); ?></button>     
				</div>
				</form>
</div>

<div class="hidden-xs"><?php echo wpautop(stripslashes($HDATA['j1']['title3'])); ?></div>

             
</div></div></div>


<?php } ?>

<?php } ?>


<?php if(!isset($GLOBALS['CORE_THEME']['home_section2']) || (isset($GLOBALS['CORE_THEME']['home_section2']) && $GLOBALS['CORE_THEME']['home_section2'] == '1' ) ){  ?>
 
<div class="wlt_object_head_4 hidden-xs">
<div class="row">

    <ul>
    
        <li class="col-md-4 col-sm-12 col-xs-12">
            <div class="b">
                <a href="<?php echo stripslashes($HDATA['t1']['link']); ?>" class="item-link">
                    <img src="<?php if(isset($HDATA['t1']['img']) && strlen($HDATA['t1']['img']) > 1){ echo $HDATA['t1']['img']; }else{ echo THEME_URI."/templates/".$GLOBALS['CORE_THEME']['template']."/img/home1.jpg"; } ?>"  alt="home" class="wp-image-2596">
                    <div class="item-html">
                        <h3><?php if(isset($HDATA['t1']['title']) && strlen($HDATA['t1']['title']) > 1){ echo stripslashes($HDATA['t1']['title']); }else{ ?><span>Add Listing</span>your business<?php } ?></h3>
                        <span class="btn1">more</span>
                    </div>
                </a>
            </div>
        </li>
        
        <li class="col-md-4 col-sm-12 col-xs-12">
        <div class="b">
        <a href="<?php echo stripslashes($HDATA['t2']['link']); ?>" class="item-link">
            <img src="<?php if(isset($HDATA['t2']['img']) && strlen($HDATA['t2']['img']) > 1){ echo $HDATA['t2']['img']; }else{ echo THEME_URI."/templates/".$GLOBALS['CORE_THEME']['template']."/img/home2.jpg"; } ?>"  alt="home" class="wp-image-2596">
            <div class="item-html">
                <h3><?php if(isset($HDATA['t2']['title']) && strlen($HDATA['t2']['title']) > 1){ echo stripslashes($HDATA['t2']['title']); }else{ ?><span>Contact Us</span> get in touch<?php } ?></h3>
                <span class="btn1">more</span>
            </div>
        </a>
        </div>
        </li>
        <li class="col-md-4 col-sm-12 col-xs-12"><div class="b">
        <a href="<?php echo stripslashes($HDATA['t3']['link']); ?>" class="item-link">
            <img src="<?php if(isset($HDATA['t3']['img']) && strlen($HDATA['t3']['img']) > 1){ echo $HDATA['t3']['img']; }else{ echo THEME_URI."/templates/".$GLOBALS['CORE_THEME']['template']."/img/home1.jpg"; } ?>"  alt="home" class="wp-image-2596">
            <div class="item-html">
                <h3><?php if(isset($HDATA['t3']['title']) && strlen($HDATA['t3']['title']) > 1){ echo stripslashes($HDATA['t3']['title']); }else{ ?><span>Our Blog</span> latest news<?php } ?></h3>
                <span class="btn1">more</span>
            </div>
        </a>
        
        </div>
        
        </li>
    
    </ul>

</div>
</div>
 
<?php } ?>





<?php if(!isset($GLOBALS['CORE_THEME']['home_section3']) || (isset($GLOBALS['CORE_THEME']['home_section3']) && $GLOBALS['CORE_THEME']['home_section3'] == '1' ) ){  ?>

<div class="catstyle5 cols<?php echo $GLOBALS['CORE_THEME']['layout_columns']['homepage']; ?>" style="margin-bottom:20px;">
 
<ul>
<?php
		$i = 1; $n = 1;
		$args = array(
			  'taxonomy'     => THEME_TAXONOMY,
			  'orderby'      => 'count',
			  'order'		=> 'desc',
			  'show_count'   => 0,
			  'pad_counts'   => 1,
			  'hierarchical' => 0,
			  'title_li'     => '',
			  'hide_empty'   => 0,
			 
		);
$categories = get_categories($args);

foreach ($categories as $category) { 

		// HIDE PARENT
		if($category->parent != 0){ continue; }
		
		// IMAGE
		if(isset($category->term_id) && isset($GLOBALS['CORE_THEME']['category_icon_'.$category->term_id])   ){
		$image = $GLOBALS['CORE_THEME']['category_icon_'.$category->term_id];
		}else{
		$image = get_template_directory_uri()."/framework/img/demo/noimage.png";
		}
		
		// LINK 
		$link = get_term_link($category);

?>

<li class="col-xs-12 col-md-4 col-sm-6 col-md-4box0">
						
    <div class="media">
    <a class="pull-left hidden-xs" href="<?php echo $link; ?>">
        <img class="media-object cimg<?php echo $i; ?>" alt="<?php echo $category->name; ?>" src="<?php echo $image; ?>">
    </a>
    
    <div class="media-body">
    
    <h4 class="media-heading"><a href="<?php echo $link; ?>"><?php echo $category->name; ?></a></h4>
   
   <small><?php echo substr($category->description,0,50); ?></small>
 
    
     
    </div>
    </div>
    
</li>
<?php
 
$i++;
}

?>
</ul>
<div class="clearfix"></div>
 
</div>
<?php } ?>
                


<?php if(!isset($GLOBALS['CORE_THEME']['home_section4']) || (isset($GLOBALS['CORE_THEME']['home_section4']) && $GLOBALS['CORE_THEME']['home_section4'] == '1' ) ){  ?>

<?php $GLOBALS['item_class_size'] = "col-md-3  col-sm-3 ";  ?>
<div class="row wlt_search_results grid_style">
        <?php echo do_shortcode('[LISTINGS dataonly=1 show=8 custom="new"]'); ?> 
</div>

<?php } ?>       
        
        
<?php get_footer($CORE->pageswitch()); ?>