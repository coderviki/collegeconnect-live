<?php
$dir_map_api=get_option('_dir_map_api');
if($dir_map_api==""){$dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';}
?>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $dir_map_api;?>'></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

                  
          <div class="profile-content">	  
			  
			   <span class="caption-subject"> <?php _e('New Listing','ivdirectories'); ?></span>
			   <hr>

                  <div class="portlet-body">                    

                      
					  <?php
						global $wpdb;
						// Check Max\
						$package_id=get_user_meta($current_user->ID,'iv_directories_package_id',true);
						$max=get_post_meta($package_id, 'iv_directories_package_max_post_no', true);
						if($max==""){
									$user_role= $current_user->roles[0];
									if(isset($current_user->roles[0]) and $current_user->roles[0]=='administrator'){
										$max=999999;
									}

						}
							$sql="SELECT * FROM $wpdb->posts WHERE post_type = 'iv_directories_pack'  and post_status='draft' ";
							$membership_pack = $wpdb->get_results($sql);
							$total_package = count($membership_pack);
							if(sizeof($membership_pack)<1){ 
								$max=999999;
							
							}	
						$default_post_type = array();
						$postkey= array();
						 $post_set=get_option('_iv_directory_url_postype' );									
							
						if($post_set!=""){ 
								$default_fields=get_option('_iv_directory_url_postype' );
						}else{															
								$default_fields['university']='University';
								$default_fields['college']='College';
								$default_fields['school']='School';															
						}
						if(sizeof($post_set)<1){																
								$default_fields['university']='University';
								$default_fields['college']='College';
								$default_fields['school']='School';		
						 }	
						 foreach($default_fields as $key => $value)
							{
							  $postkey[] = $key;
							}
						$post_type = join("','",$postkey);	

						$sql="SELECT count(*) as total FROM $wpdb->posts WHERE post_type IN ('".$post_type ."' )  and post_author='".$current_user->ID."' ";
						$all_post = $wpdb->get_row($sql);
						$my_post_count=$all_post->total;

						if ( $my_post_count>=$max or !current_user_can('edit_posts') )  {
								$iv_redirect = get_option( '_iv_directories_profile_page');
								$reg_page= get_permalink( $iv_redirect);
							?>
							<?php _e('Please Upgrade Your Account','ivdirectories'); ?>
							 <a href="<?php echo $reg_page.'?&profile=level'; ?>" title="Upgarde"><b><?php _e('Here','ivdirectories'); ?> </b></a>
							<?php _e('To Add More Post.','ivdirectories'); ?>


						<?php
						}else{

					?>

						<div class="row">
							<div class="col-md-12">


							<form action="" id="new_post" name="new_post"  method="POST" role="form">
								<div class=" form-group">
									<label for="text" class=" control-label"><?php _e('Listing Type','ivdirectories'); ?></label>
									<div class="">
										
										<?php
														
												$cpt = array();
												$cpt_set=get_option('_iv_directory_url_postype' );
													
													
												if($cpt_set!=""){ 
														$cpt=get_option('_iv_directory_url_postype' );
												}else{															
														$cpt['university']='University';
														$cpt['college']='College';
														$cpt['school']='School';	
																														
												}
												if(sizeof($cpt_set)<1){																
														$cpt['university']='University';
														$cpt['college']='College';
														$cpt['school']='School';		
												 }															
												$i=1;$old_select='';
													echo "<select id='cpt_page' name='cpt_page' class='form-control'>";
												foreach ( $cpt as $field_key => $field_value ) {		
													 echo "<option value='{$field_key}' ".($old_select==$field_key ? 'selected':'').">{$field_value}</option>";
												}
													echo "</select>";
											?>				
									</div>									
								</div>
								<div class=" form-group">
									<label for="text" class=" control-label"><?php _e('Name/Title','ivdirectories'); ?></label>
									<div class="  ">
										<input type="text" class="form-control-solid" name="title" id="title" value="" placeholder="<?php _e('Enter Name Here','ivdirectories'); ?>">
									</div>
								</div>

								<div class="form-group">

										<div class=" ">
												<?php
													$settings_a = array(
														'textarea_rows' =>8,
														'editor_class' => 'form-control-solid'
														);

													$editor_id = 'new_post_content';
													wp_editor( '', $editor_id,$settings_a );
													?>

										</div>

								</div>
								<div class=" row form-group ">
									<label for="text" class=" col-md-5 control-label"><?php _e('Logo','ivdirectories'); ?>  </label>

										<div class="col-md-4" id="logo_image_div">
											<a  href="javascript:void(0);" onclick="logo_post_image('logo_image_div');"  >
											<?php  echo '<img width="100px" src="'. wp_iv_directories_URLPATH.'assets/images/image-add-icon.png">'; ?>
											</a>
										</div>

										<input type="hidden" name="logo_image_id" id="logo_image_id" value="">

										<div class="col-md-3" id="logo_image_edit">
											<button type="button" onclick="logo_post_image('logo_image_div');"  class="btn btn-xs green-haze"><?php _e('Add','ivdirectories'); ?> </button>

										</div>
								</div>
								<div class=" row form-group ">
									<label for="text" class=" col-md-5 control-label"><?php _e('Feature Image','ivdirectories'); ?>  </label>

										<div class="col-md-4" id="post_image_div">
											<a  href="javascript:void(0);" onclick="edit_post_image('post_image_div');"  >
											<?php  echo '<img width="100px" src="'. wp_iv_directories_URLPATH.'assets/images/image-add-icon.png">'; ?>
											</a>
										</div>

										<input type="hidden" name="feature_image_id" id="feature_image_id" value="">

										<div class="col-md-3" id="post_image_edit">
											<button type="button" onclick="edit_post_image('post_image_div');"  class="btn btn-xs green-haze"><?php _e('Add','ivdirectories'); ?> </button>

										</div>
								</div>


								<div class=" row form-group ">
									<label for="text" class=" col-md-5 control-label"><?php _e('Image Gallery','ivdirectories'); ?>
										<button type="button" onclick="edit_gallery_image('gallery_image_div');"  class="btn btn-xs green-haze"><?php _e('Add Images','ivdirectories'); ?></button>
									 </label>
								</div>
								<div class=" row form-group ">
											<!--
										<div class="col-md-12" id="gallery_image_div">

											<a  href="javascript:void(0);" onclick="edit_gallery_image('gallery_image_div');"  >
											<?php  echo '<img src="'. wp_iv_directories_URLPATH.'assets/images/gallery_icon.png">'; ?>
											</a>

										</div>
											-->
										<input type="hidden" name="gallery_image_ids" id="gallery_image_ids" value="">

										<div class="col-md-12" id="gallery_image_div">
										</div>
								</div>



								<div class="clearfix"></div>
								<div class=" row form-group ">
									<label for="text" class=" col-md-12 control-label"><?php _e('Post Status','ivdirectories'); ?>  </label>

										<div class="col-md-12" id="">
										<select name="post_status" id="post_status"  class="form-control-solid">
											<?php
												$dir_approve_publish =get_option('_dir_approve_publish');
												if($dir_approve_publish!='yes'){?>
													<option value="publish"><?php _e('Publish','ivdirectories'); ?></option>
												<?php
												}else{ ?>
													<option value="pending"><?php _e('Pending Review','ivdirectories'); ?></option>
												<?php
												}
											?>
											<option value="draft"><?php _e('Draft','ivdirectories'); ?></option>

										</select>


										</div>

								</div>

								
								<div class="clearfix"></div>
								<div class=" row form-group">
									<label for="text" class=" col-md-12 control-label"><?php _e('Category','ivdirectories'); ?></label>
									<div class=" col-md-12 " id="select_cpt">

								<?php
								$selected='';
								foreach ( $cpt as $field_key => $field_value ) {
									
									echo '<select name="postcats[]" class="form-control-solid " multiple="multiple" size="8">';
									echo'	<option selected="'.$selected.'" value="">'.__('Choose a category','ivdirectories').'</option>';

										$selected='';
										if( isset($_POST['submit'])){
											$selected = $_POST['postcats'];
										}
											//directories
											$taxonomy = $field_key.'-category';
											$args = array(
												'orderby'           => 'name',
												'order'             => 'ASC',
												'hide_empty'        => false,
												'exclude'           => array(),
												'exclude_tree'      => array(),
												'include'           => array(),
												'number'            => '',
												'fields'            => 'all',
												'slug'              => '',
												'parent'            => '0',
												'hierarchical'      => true,
												'child_of'          => 0,
												'childless'         => false,
												'get'               => '',

											);
								$terms = get_terms($taxonomy,$args); // Get all terms of a taxonomy
								if ( $terms && !is_wp_error( $terms ) ) :
									$i=0;
									foreach ( $terms as $term_parent ) {  ?>


											<?php 

											echo '<option  value="'.$term_parent->slug.'" '.($selected==$term_parent->slug?'selected':'' ).'><strong>'.$term_parent->name.'<strong></option>';
											?>
												<?php

												$args2 = array(
													'type'                     => $field_key,
													'parent'                   => $term_parent->term_id,
													'orderby'                  => 'name',
													'order'                    => 'ASC',
													'hide_empty'               => 0,
													'hierarchical'             => 1,
													'exclude'                  => '',
													'include'                  => '',
													'number'                   => '',
													'taxonomy'                 => $field_key.'-category',
													'pad_counts'               => false

												);
												$categories = get_categories( $args2 );
												if ( $categories && !is_wp_error( $categories ) ) :


													foreach ( $categories as $term ) {
														echo '<option  value="'.$term->slug.'" '.($selected==$term->slug?'selected':'' ).'>--'.$term->name.'</option>';
													}

												endif;
												
										$i++;
									}
								endif;
									echo '</select>';
									
									break;
								}	
								?>
									</div>

								</div>





						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('Address','ivdirectories'); ?></label>
							<div class=" ">
								<input type="text" class="form-control-solid" name="address" id="address" value="" placeholder="<?php _e('Enter here Here','ivdirectories'); ?>">
							</div>
							<input type="hidden" id="latitude" name="latitude" placeholder="Latitude" value="" >
							<input type="hidden" id="longitude" name="longitude" placeholder="Longitude"  value="" >                                
                                <input type="hidden" id="state" name="state" />
                             
                                <input type="hidden" id="country" name="country" />
						</div>
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('City','ivdirectories'); ?></label>							
							<div class=" "> 
								<input type="text" class="form-control-solid" name="city" id="city" value="" placeholder="<?php _e('Enter city ','ivdirectories'); ?>">
						</div>
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('Zipcode','ivdirectories'); ?></label>							
							<div class=" "> 
								<input type="text" class="form-control-solid" name="postcode" id="postcode" value="" placeholder="<?php _e('Enter Zipcode ','ivdirectories'); ?>">
						</div>
						
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('Address Map','ivdirectories'); ?></label>
							<div class=" ">
									<div  id="map-canvas"  style="width:100%;height:300px;"></div>

								<script type="text/javascript">
								var geocoder;
								jQuery(document).ready(function($) {
									var map;
									var marker;

									 geocoder = new google.maps.Geocoder();


									function geocodePosition(pos) {
									  geocoder.geocode({
									    latLng: pos
									  }, function(responses) {
									    if (responses && responses.length > 0) {
									      updateMarkerAddress(responses[0].formatted_address);
									    } else {
									      updateMarkerAddress('Cannot determine address at this location.');
									    }
									  });
									}

									function updateMarkerPosition(latLng) {
									  jQuery('#latitude').val(latLng.lat());
									  jQuery('#longitude').val(latLng.lng());
										//console.log(latLng);
										codeLatLng(latLng.lat(), latLng.lng());
									}

									function updateMarkerAddress(str) {
									  jQuery('#address').val(str);
									}

									function initialize() {

									  var latlng = new google.maps.LatLng(40.748817, -73.985428);
									  var mapOptions = {
									    zoom: 2,
									    center: latlng
									  }

									  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

									  geocoder = new google.maps.Geocoder();

									  marker = new google.maps.Marker({
									  	position: latlng,
									    map: map,
									    draggable: true
									  });

									  // Add dragging event listeners.
									  google.maps.event.addListener(marker, 'dragstart', function() {
									    updateMarkerAddress('Please Wait Dragging...');
									  });

									  google.maps.event.addListener(marker, 'drag', function() {
									    updateMarkerPosition(marker.getPosition());
									  });

									  google.maps.event.addListener(marker, 'dragend', function() {
									    geocodePosition(marker.getPosition());
									  });

									}

									google.maps.event.addDomListener(window, 'load', initialize);

									jQuery(document).ready(function() {

									  initialize();

									  jQuery(function() {
									   	var input = document.getElementById('address');
											var autocomplete = new google.maps.places.Autocomplete(input);
										google.maps.event.addListener(autocomplete, 'place_changed', function () {
											var place = autocomplete.getPlace();
											//document.getElementById('city2').value = place.name;
											document.getElementById('latitude').value = place.geometry.location.lat();
											document.getElementById('longitude').value = place.geometry.location.lng();
											
											var location = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
											codeLatLng(place.geometry.location.lat(), place.geometry.location.lng());
											
									        marker.setPosition(location);
									        map.setZoom(16);
									        map.setCenter(location);
											
										  });	
									  });

									  //Add listener to marker for reverse geocoding
									  google.maps.event.addListener(marker, 'drag', function() {
									    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
									      if (status == google.maps.GeocoderStatus.OK) {
									        if (results[0]) {

									          jQuery('#address').val(results[0].formatted_address);
									          jQuery('#latitude').val(marker.getPosition().lat());
									          jQuery('#longitude').val(marker.getPosition().lng());
									        }
									      }
									    });
									  });

									});

								});
								// For city country , zip
								function codeLatLng(lat, lng) {
									var city;
									var postcode;
									var state;
									var country;
									var latlng = new google.maps.LatLng(lat, lng);
									geocoder.geocode({'latLng': latlng}, function(results, status) {
									  if (status == google.maps.GeocoderStatus.OK) {
									  //console.log(results)
										if (results[1]) {

										//find country name
											 for (var i=0; i<results[0].address_components.length; i++) {
											for (var b=0;b<results[0].address_components[i].types.length;b++) {

												//there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
												if (results[0].address_components[i].types[b] == "locality") {
													//this is the object you are looking for
													city= results[0].address_components[i];		
													//break;
												}
												if (results[0].address_components[i].types[b] == "country") {
													country= results[0].address_components[i];
												}
												if (results[0].address_components[i].types[b] == "postal_code") {													
													postcode= results[0].address_components[i];													
												}	
											}
										}
										//city data
										jQuery('#address').val(results[0].formatted_address);
										jQuery('#city').val(city.long_name);
										jQuery('#postcode').val(postcode.long_name);
										jQuery('#country').val(country.long_name);
										//alert(city.short_name + " " + city.long_name)


										} else {

										}
									  } else {

									  }
									});
								  }

						    </script>
							
							</div>
						</div>


						<div class="clearfix"></div>

					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
									  <?php _e('Speciality(s)','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
									  <?php _e('[ Edit ]','ivdirectories'); ?>
									</a>
								  </h4>
								</div>
								<div id="collapseEight" class="panel-collapse collapse">
								  <div class="panel-body">
									<div class=" form-group">

											<div class=" ">
											<?php
											$Specialities =__('Amazing Compass,Library,Residential Hostel,Industry focus learning,Flexible learning,Knowledge transfer,Teaching skills','ivdirectories');

											$field_set=get_option('iv_listing_Specialities' );
											if($field_set!=""){
													$Specialities=get_option('iv_listing_Specialities' );
											}


										$i=1;

										$Specialities_fields= explode(",",$Specialities);
										foreach ( $Specialities_fields as $field_value ) { ?>
												<div class="col-md-4">
													<label class="form-group"> <input type="checkbox" name="Specialities_arr[]" id="Specialities_arr[]" value="<?php echo $field_value; ?>"> <?php echo $field_value; ?> </label>
												</div>



										<?php
										}
										?>
										</div>
									</div>
								  </div>
								</div>
					  </div>
						<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseEight1">
									  <?php _e('Amenities/Tags','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
									  <?php _e('[ Edit ]','ivdirectories'); ?> 
									</a>
								  </h4>
								</div>
								<div id="collapseEight1" class="panel-collapse collapse">
								  <div class="panel-body">
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Select Tags','ivdirectories'); ?></label>
											<div id="iv_tags"> 
												
												<?php
												foreach ( $cpt as $field_key => $field_value ) {										
													$args2 = array(
														'type'                     => $field_key,
														//'parent'                   => $term_parent->term_id,
														'orderby'                  => 'name',
														'order'                    => 'ASC',
														'hide_empty'               => 0,
														'hierarchical'             => 1,
														'exclude'                  => '',
														'include'                  => '',
														'number'                   => '',
														'taxonomy'                 => $field_key.'_tag',
														'pad_counts'               => false

													);
													$categories = get_categories( $args2 );												
													if ( $categories && !is_wp_error( $categories ) ) :


														foreach ( $categories as $term ) {
															//echo '<option  value="'.$term->slug.'" '.($selected==$term->slug?'selected':'' ).'>--'.$term->name.'</option>';
															?>
															<div class="col-md-4">
															 <label class="form-group"> <input type="checkbox" name="tag_arr[]" id="tag_arr[]" value="<?php echo $term->slug; ?>"> <?php echo $term->name; ?> </label>  
															</div>
														<?php	
														}

													endif;																
														$i++;													
													break;
												}	
												?>
												
											</div>																
									</div>
									<div class="clearfix"></div>
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Add New Amenities/Tags','ivdirectories'); ?></label>						
											<div class="  "> 
												<input type="text" class="form-control" name="new_tag" id="new_tag" value="" placeholder="<?php _e('Enter New Tags: Separate tags with commas','ivdirectories'); ?>">
											</div>																
									</div>	
									
									
																		
								  </div>
								</div>
					  </div>
					

					<div class="clearfix"></div>

					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									  <?php _e('Contact Info','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									  <?php _e('[ Edit ]','ivdirectories'); ?>
									</a>
								  </h4>
								</div>
								<div id="collapseFour" class="panel-collapse collapse">
								  <div class="panel-body">
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Phone','ivdirectories'); ?></label>
											<div class="  ">
												<input type="text" class="form-control-solid" name="phone" id="phone" value="" placeholder="<?php _e('Enter Phone Number','ivdirectories'); ?>">
											</div>
									</div>
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Fax','ivdirectories'); ?></label>

											<div class="  ">
												<input type="text" class="form-control-solid" name="fax" id="fax" value="" placeholder="<?php _e('Enter Fax Number','ivdirectories'); ?>">
											</div>
									</div>
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Email Address','ivdirectories'); ?></label>

											<div class="  ">
												<input type="text" class="form-control-solid" name="contact-email" id="contact-email" value="" placeholder="<?php _e('Enter Email Address','ivdirectories'); ?>">
											</div>
									</div>
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Web Site','ivdirectories'); ?></label>

											<div class="  ">
												<input type="text" class="form-control-solid" name="contact_web" id="contact_web" value="" placeholder="<?php _e('Enter Web Site','ivdirectories'); ?>">
											</div>
									</div>


								  </div>
								</div>
					  </div>
							<div class="clearfix"></div>
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapsethirt3">
									  <?php _e('Documents','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapsethirt3">
									   <?php _e('[ Edit ]','ivdirectories'); ?>
									</a>
								  </h4>
								</div>
								<div id="collapsethirt3" class="panel-collapse collapse">
								  <div class="panel-body">									 
									  <div id="docs">
										 <div id="doc">
													<div class=" form-group">
														<label for="text" class=" control-label"><?php _e('Doc Title','ivdirectories'); ?></label>

														<div class="  ">
															<input type="text" class="form-control-solid" name="doc_title[]" id="doc_title[]" value="" placeholder="<?php _e('Enter Doc title','ivdirectories'); ?>">
														</div>
													</div>													
													<div class=" form-group " style="margin-top:10px">
													<label for="text" class=" col-md-4 control-label"><?php _e('Add PDF Doc ','ivdirectories'); ?>  </label>

													<div class="col-md-8" id="doc_image_div">
														<a  href="javascript:void(0);" onclick="doc_post_image(this);"  >
														<?php  echo '<img width="80px" src="'. wp_iv_directories_URLPATH.'assets/images/document_add.png">'; ?>
														</a>
													</div>
												</div>
										</div>
									</div>
									<div class=" row  form-group ">
										<div class="col-md-12" >
										<button type="button" onclick="add_doc_field();"  class="btn btn-xs green-haze"><?php _e('Add More','ivdirectories'); ?></button>
										</div>
									</div>



										

								  </div>
								</div>
					  </div>


					<div class="clearfix"></div>
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapsethirty2">
									  <?php _e('Awards','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapsethirty2">
									   <?php _e('[ Edit ]','ivdirectories'); ?>
									</a>
								  </h4>
								</div>
								<div id="collapsethirty2" class="panel-collapse collapse">
								  <div class="panel-body">
									  <?php
										// video, event ,  award
										if($this->check_write_access('award')){
										?>
									  <div id="awards">
										 <div id="award">
													<div class=" form-group">
														<label for="text" class=" control-label"><?php _e('Award Title','ivdirectories'); ?></label>

														<div class="  ">
															<input type="text" class="form-control-solid" name="award_title[]" id="award_title[]" value="" placeholder="<?php _e('Enter award title','ivdirectories'); ?>">
														</div>
													</div>
													<div class=" form-group">
														<label for="text" class=" control-label"><?php _e('Award Description','ivdirectories'); ?></label>

														<div class="  ">
															<input type="text" class="form-control-solid" name="award_description[]" id="award_description[]" value="" placeholder="<?php _e('Enter Award Description','ivdirectories'); ?>">
														</div>
													</div>
													<div class=" form-group">
														<label for="text" class=" control-label"><?php _e('Year(s) for which award was received','ivdirectories'); ?></label>

														<div class="  ">
															<input type="text" class="form-control-solid" name="award_year[]" id="award_year[]" value="" placeholder="<?php _e('Enter Award Year','ivdirectories'); ?>">
														</div>
													</div>
													<div class=" form-group " style="margin-top:10px">
													<label for="text" class=" col-md-5 control-label"><?php _e('Award Image','ivdirectories'); ?>  </label>

													<div class="col-md-4" id="award_image_div">
														<a  href="javascript:void(0);" onclick="award_post_image(this);"  >
														<?php  echo '<img width="100px" src="'. wp_iv_directories_URLPATH.'assets/images/image-add-icon.png">'; ?>
														</a>
													</div>
												</div>
										</div>
									</div>
									<div class=" row  form-group ">
										<div class="col-md-12" >
										<button type="button" onclick="add_award_field();"  class="btn btn-xs green-haze"><?php _e('Add More','ivdirectories'); ?></button>
										</div>
									</div>



										<?php
										}else{
												_e('Please upgrade your account to add Award ','ivdirectories');
										}
										?>

								  </div>
								</div>
					  </div>



				
					<div class="clearfix"></div>
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									  <?php _e('Videos','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									   <?php _e('[ Edit ]','ivdirectories'); ?>
									</a>
								  </h4>
								</div>
								<div id="collapseThree" class="panel-collapse collapse">
								  <div class="panel-body">
									  <?php
											// video, event , doctor , booking
										 if($this->check_write_access('video')){

										?>
										<div class=" form-group">

												<label for="text" class=" control-label"><?php _e('Youtube','ivdirectories'); ?></label>

												<div class="  ">
													<input type="text" class="form-control-solid" name="youtube" id="youtube" value="" placeholder="<?php _e('Enter Youtube video ID, e.g : bU1QPtOZQZU ','ivdirectories'); ?>">
												</div>
										</div>
										<div class=" form-group">
												<label for="text" class=" control-label"><?php _e('vimeo','ivdirectories'); ?></label>

												<div class="  ">
													<input type="text" class="form-control-solid" name="vimeo" id="vimeo" value="" placeholder="<?php _e('Enter vimeo ID, e.g : 134173961','ivdirectories'); ?>">
												</div>
										</div>
										<?php
										}else{
												_e('Please upgrade your account to add video ','ivdirectories');
										}
										?>

								  </div>
								</div>
					  </div>


					<div class="clearfix"></div>
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
									  <?php _e('Social Profiles','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
									   <?php _e('[ Edit ]','ivdirectories'); ?>
									</a>
								  </h4>
								</div>
								<div id="collapseFive" class="panel-collapse collapse">
								  <div class="panel-body">

										<div class="form-group">
										<label class="control-label">FaceBook</label>
										<input type="text" name="facebook" id="facebook" value="" class="form-control-solid"/>
									  </div>
									  <div class="form-group">
										<label class="control-label">Linkedin</label>
										<input type="text" name="linkedin" id="linkedin" value="" class="form-control-solid"/>
									  </div>
									  <div class="form-group">
										<label class="control-label">Twitter</label>
										<input type="text" name="twitter" id="twitter" value="" class="form-control-solid"/>
									  </div>
									  <div class="form-group">
										<label class="control-label">Google+ </label>
										<input type="text" name="gplus" id="gplus" value=""  class="form-control-solid"/>
									  </div>
									  
									   <div class="form-group">
										<label class="control-label">Pinterest </label>
										<input type="text" name="pinterest" id="pinterest" value=""  class="form-control-solid"/>
									  </div>
									  
									   <div class="form-group">
										<label class="control-label">Instagram </label>
										<input type="text" name="instagram" id="instagram" value=""  class="form-control-solid"/>
									  </div>
									
									

								  </div>
								</div>
					  </div>


					<div class="clearfix"></div>
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									  <?php _e('Additional Info','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									 <?php _e('[ Edit ]','ivdirectories'); ?>
									</a>
								  </h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse">
								  <div class="panel-body">
										<?php


											$default_fields = array();
												$field_set=get_option('iv_directories_fields' );
											if($field_set!=""){
													$default_fields=get_option('iv_directories_fields' );
											}else{
													$default_fields['Totalenrollment']='Total enrollment';
													$default_fields['Undergraduate_graduate_ratio']='Undergraduate/graduate ratio ';
													$default_fields['No_of_international_students']='No. of international students ';
													$default_fields['pers_of international_students']=' % of international students';
													$default_fields['no_of_faculty']='No. of faculty';
													$default_fields['Student_faculty_ratio']='Student faculty ratio';
													$default_fields['no_of_staff']='No. of staff';
													$default_fields['Annual_revenue ']='Annual revenue';
											}
											if(sizeof($field_set)<1){
													$default_fields['Totalenrollment']='Total enrollment';
													$default_fields['Undergraduate_graduate_ratio']='Undergraduate/graduate ratio ';
													$default_fields['No_of_international_students']='No. of international students ';
													$default_fields['pers_of international_students']=' % of international students';
													$default_fields['no_of_faculty']='No. of faculty';
													$default_fields['Student_faculty_ratio']='Student faculty ratio';
													$default_fields['no_of_staff']='No. of staff';
													$default_fields['Annual_revenue ']='Annual revenue';
											 }


										$i=1;
										foreach ( $default_fields as $field_key => $field_value ) { ?>
												 <div class="form-group">
													<label class="control-label"><?php echo _e($field_value, 'wp_iv_directories'); ?></label>
													<input type="text" placeholder="<?php echo 'Enter '.$field_value;?>" name="<?php echo $field_key;?>" id="<?php echo $field_key;?>"  class="form-control-solid" value=""/>
												  </div>

										<?php
										}
										?>

								  </div>
								</div>
					  </div>


						  <div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									  <?php _e('Working Time','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									  <?php _e('[ Edit ]','ivdirectories'); ?>
									</a>
								  </h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse">
								  <div class="panel-body">
									<div id="day_field_div">
										<div class=" row form-group " id="day-row1" >
											<div class=" col-md-4">
											<select name="day_name[]" id="day_name[]" class="form-control-solid">
											<option value=""></option>
											<option value="<?php _e('Monday','ivdirectories'); ?> "> <?php _e('Monday','ivdirectories'); ?>  </option> 
											<option value="<?php _e('Tuesday','ivdirectories'); ?>"><?php _e('Tuesday','ivdirectories'); ?></option> 
											<option value="<?php _e('Wednesday','ivdirectories'); ?>"><?php _e('Wednesday','ivdirectories'); ?></option> 
											<option value="<?php _e('Thursday','ivdirectories'); ?>"><?php _e('Thursday','ivdirectories'); ?></option> 
											<option value="<?php _e('Friday','ivdirectories'); ?>"><?php _e('Friday','ivdirectories'); ?></option> 
											<option value="<?php _e('Saturday','ivdirectories'); ?>"><?php _e('Saturday','ivdirectories'); ?></option> 
											<option value="<?php _e('Sunday','ivdirectories'); ?>"><?php _e('Sunday','ivdirectories'); ?></option> 
											</select>
											</div>
											<div  class=" col-md-4">
											<input type="text" placeholder="<?php _e('12:00 AM','ivdirectories'); ?> " name="day_value1[]" id="day_value1[]"  class="form-control" />
											
											</div>
											<div  class="col-md-4">

												<input type="text" placeholder="<?php _e('12:00 AM','ivdirectories'); ?> " name="day_value2[]" id="day_value2[]"  class="form-control" />
											</div>

										</div>
									</div>

									<div class=" row  form-group ">
										<div class="col-md-12" >
										<button type="button" onclick="add_day_field();"  class="btn btn-xs green-haze"><?php _e('Add More','ivdirectories'); ?></button>
										</div>
									</div>
								  </div>
								</div>
					  </div>
					<div class="clearfix"></div>
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
									  <?php _e('Event','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
									  <?php _e('[ Edit ]','ivdirectories'); ?>
									</a>
								  </h4>
								</div>
								<div id="collapseSix" class="panel-collapse collapse">
								  <div class="panel-body">
										<?php
											// video, event , doctor , booking
										 if($this->check_write_access('event')){

										?>
										<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Event Title','ivdirectories'); ?></label>
												<input type="text" class="form-control-solid" name="event-title" id="event-title" value="" placeholder="<?php _e('Enter Title Here','ivdirectories'); ?>">
										</div>
										<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Event Detail','ivdirectories'); ?></label>
												<?php
													$settings_a = array(
														'textarea_rows' =>10,
														'editor_class' => 'form-control-solid'
														);
													$content_client ='';//$content;//get_option( 'iv_directories_signup_email');
													$editor_id = 'event-detail';
													//wp_editor( $content_client, $editor_id,$settings_a );

													?>
												<textarea id="event-detail" name="event-detail"  rows="4" class="form-control-solid" >  </textarea>

										</div>
										<div class=" row form-group " style="margin-top:10px">
											<label for="text" class=" col-md-5 control-label"><?php _e('Event Image','ivdirectories'); ?>  </label>

											<div class="col-md-4" id="event_image_div">
												<a  href="javascript:void(0);" onclick="event_post_image('event_image_div');"  >
												<?php  echo '<img width="100px" src="'. wp_iv_directories_URLPATH.'assets/images/image-add-icon.png">'; ?>
												</a>
											</div>

											<input type="hidden" name="event_image_id" id="event_image_id" value="">

											<div class="col-md-3" id="event_image_edit">
												<button type="button" onclick="event_post_image('event_image_div');"  class="btn btn-xs green-haze"><?php _e('Add','ivdirectories'); ?></button>

											</div>
										</div>
										<?php
										}else{
												_e('Please upgrade your account to add event ','ivdirectories');
										}
										?>
								  </div>

								</div>
					  </div>



								<div class="margiv-top-10">
								    <div class="" id="update_message"></div>
									<input type="hidden" name="user_post_id" id="user_post_id" value="<?php //echo $curr_post_id; ?>">
								    <button type="button" onclick="iv_save_post();"  class="btn green-haze"><?php _e('Save Post','ivdirectories'); ?></button>

		                        </div>

							</form>
						  </div>
						</div>
			<?php

				} // for Role



			?>


					 </div>                
                </div>
                
                
              </div>
            </div>
          <!-- END PROFILE CONTENT -->


	 <script>
function iv_save_post (){
	tinyMCE.triggerSave();
	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	var loader_image = '<img src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
				jQuery('#update_message').html(loader_image);
				var search_params={
					"action"  : 	"iv_directories_save_listing",
					"form_data":	jQuery("#new_post").serialize(),
				};
				jQuery.ajax({
					url : ajaxurl,
					dataType : "json",
					type : "post",
					data : search_params,
					success : function(response){
						if(response.code=='success'){
								var url = "<?php echo get_permalink(); ?>?&profile=all-post";
								jQuery(location).attr('href',url);
						}
					}
				});

	}
function add_day_field(){
	var main_opening_div =jQuery('#day-row1').html();
	jQuery('#day_field_div').append('<div class="clearfix"></div><div class=" row form-group" >'+main_opening_div+'</div>');
}

function add_award_field(){
	var main_award_div =jQuery('#award').html();
	jQuery('#awards').append('<div class="clearfix"></div><hr>'+main_award_div+'');
}
function add_doc_field(){
	var main_award_div =jQuery('#doc').html();
	jQuery('#docs').append('<div class="clearfix"></div><hr>'+main_award_div+'');
}

function  remove_post_image	(profile_image_id){
	jQuery('#'+profile_image_id).html('');
	jQuery('#feature_image_id').val('');
	jQuery('#post_image_edit').html('<button type="button" onclick="edit_post_image(\'post_image_div\');"  class="btn btn-xs green-haze">Add</button>');
}
function  remove_event_image	(profile_image_id){
	jQuery('#'+profile_image_id).html('');
	jQuery('#event_image_id').val('');
	jQuery('#event_image_edit').html('<button type="button" onclick="event_post_image(\'event_image_div\');"  class="btn btn-xs green-haze">Add</button>');
}

 function edit_post_image(profile_image_id){
				var image_gallery_frame;
               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: '<?php _e( 'Set Feature Image ', 'ivdirectories' ); ?>',
                    button: {
                        text: '<?php _e( 'Set Feature Image', 'ivdirectories' ); ?>',
                    },
                    multiple: false,
                    displayUserSettings: true,
                });
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {
							jQuery('#'+profile_image_id).html('<img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'">');
							jQuery('#feature_image_id').val(attachment.id );
							jQuery('#post_image_edit').html('<button type="button" onclick="edit_post_image(\'post_image_div\');"  class="btn btn-xs green-haze">Edit</button> &nbsp;<button type="button" onclick="remove_post_image(\'post_image_div\');"  class="btn btn-xs green-haze">Remove</button>');
						}
					});
                });
				image_gallery_frame.open();
	}
 function logo_post_image(profile_image_id){
				var image_gallery_frame;
               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: '<?php _e( 'Set Logo Image ', 'ivdirectories' ); ?>',
                    button: {
                        text: '<?php _e( 'Set Logo Image', 'ivdirectories' ); ?>',
                    },
                    multiple: false,
                    displayUserSettings: true,
                });
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {
							jQuery('#'+profile_image_id).html('<img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'">');
							jQuery('#logo_image_id').val(attachment.id );
							jQuery('#logo_image_edit').html('<button type="button" onclick="edit_post_image(\'post_image_div\');"  class="btn btn-xs green-haze">Edit</button> &nbsp;<button type="button" onclick="remove_post_image(\'post_image_div\');"  class="btn btn-xs green-haze">Remove</button>');
						}
					});
                });
				image_gallery_frame.open();
	}

function award_post_image(awardthis){
				var image_gallery_frame;
               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: '<?php _e( 'Set award Image ', 'ivdirectories' ); ?>',
                    button: {
                        text: '<?php _e( 'Set award Image', 'ivdirectories' ); ?>',
                    },
                    multiple: false,
                    displayUserSettings: true,
                });
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {

							jQuery(awardthis).html('<img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'"><input type="hidden" name="award_image_id[]" id="award_image_id[]" value="'+attachment.id+'">');


						}
					});
                });
				image_gallery_frame.open();
	}
function doc_post_image(awardthis){
				var image_gallery_frame;
               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: '<?php _e( 'Set Doc PDF ', 'ivdirectories' ); ?>',
                    button: {
                        text: '<?php _e( 'Set Doc PDF', 'ivdirectories' ); ?>',
                    },
                    multiple: false,
                    displayUserSettings: true,
                });
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                       
                        if ( attachment.id ) {

							jQuery(awardthis).html('<img  width="60px" class="img-responsive"  src="<?php echo wp_iv_directories_URLPATH.'assets/images/PDF1.png';?>">'+attachment.filename+'<input type="hidden" name="doc_image_id[]" id="doc_image_id[]" value="'+attachment.id+'">');


						}
					});
                });
				image_gallery_frame.open();
	}	
function event_post_image(profile_image_id){
				var image_gallery_frame;
               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: '<?php _e( 'Set Event Image ', 'ivdirectories' ); ?>',
                    button: {
                        text: '<?php _e( 'Set Event Image', 'ivdirectories' ); ?>',
                    },
                    multiple: false,
                    displayUserSettings: true,
                });
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {
							jQuery('#'+profile_image_id).html('<img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'">');
							jQuery('#event_image_id').val(attachment.id );
							jQuery('#event_image_edit').html('<button type="button" onclick="event_post_image(\'event_image_div\');"  class="btn btn-xs green-haze">Edit</button> &nbsp;<button type="button" onclick="remove_event_image(\'event_image_div\');"  class="btn btn-xs green-haze">Remove</button>');
						}
					});
                });
				image_gallery_frame.open();
	}

 function edit_gallery_image(profile_image_id){

				var image_gallery_frame;
				var hidden_field_image_ids = jQuery('#gallery_image_ids').val();
               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: '<?php _e( 'Gallery Images ', 'ivdirectories' ); ?>',
                    button: {
                        text: '<?php _e( 'Gallery Images', 'ivdirectories' ); ?>',
                    },
                    multiple: true,
                    displayUserSettings: true,
                });
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        console.log(attachment);
                        if ( attachment.id ) {
							jQuery('#'+profile_image_id).append('<div id="gallery_image_div'+attachment.id+'" class="col-md-3"><img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'"><button type="button" onclick="remove_gallery_image(\'gallery_image_div'+attachment.id+'\', '+attachment.id+');"  class="btn btn-xs btn-danger">Remove</button> </div>');
							hidden_field_image_ids=hidden_field_image_ids+','+attachment.id ;
							jQuery('#gallery_image_ids').val(hidden_field_image_ids);
						}
					});

                });
				image_gallery_frame.open();
 }
function  remove_gallery_image(img_remove_div,rid){
	var hidden_field_image_ids = jQuery('#gallery_image_ids').val();
	hidden_field_image_ids =hidden_field_image_ids.replace(rid, '');
	jQuery('#'+img_remove_div).remove();
	jQuery('#gallery_image_ids').val(hidden_field_image_ids);
	//jQuery('#gallery_gallery_edit').html('');

}

jQuery(function() {
		jQuery('#cpt_page').on('change', function (e) {
			var optionSelected = jQuery("option:selected", this);
			var valueSelected = this.value;		
						
				var ajaxurl = '<?php echo admin_url( 'admin-ajax.php');  ?>';	
				var search_params={
					"action"  : 	"iv_directories_cpt_change",		
					"select_data":	valueSelected, 									
				};		
				jQuery.ajax({					
					url : ajaxurl,					 
					dataType : "json",
					type : "post",
					data : search_params,
					success : function(response){ 											
						jQuery('#select_cpt').html(response.msg );
						jQuery('#iv_tags').html(response.tags );
												
					}
				});						
				

		});
});
	
</script>

