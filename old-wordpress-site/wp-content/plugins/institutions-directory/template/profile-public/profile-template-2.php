<?php
wp_enqueue_style('wp-iv_directories-piblic-11', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap.css');
wp_enqueue_script('iv_directories-piblic-12', wp_iv_directories_URLPATH . 'admin/files/js/bootstrap.min.js');

wp_enqueue_style('iv_directories-style-64', wp_iv_directories_URLPATH . 'assets/cube/css/cubeportfolio.min.css');

wp_enqueue_style('user-public-profile-style', wp_iv_directories_URLPATH .'admin/files/css/user-public-profile.css', array(), $ver = false, $media = 'all');

//wp_enqueue_media();
$display_name='';
$email='';
$user_id=1;
if(isset($_REQUEST['id'])){
  $author_name= $_REQUEST['id'];
  $user = get_user_by( 'slug', $author_name );
  if(isset($user->ID)){
    $user_id=$user->ID;
    $display_name=$user->display_name;
    $email=$user->user_email;
  }
}else{
 global $current_user;

 $user_id=$current_user->ID;
 $display_name=$current_user->display_name;
 $email=$current_user->user_email;
 if($user_id==0){
  $user_id=1;
}
}
  //print_r($current_user);
$iv_profile_pic_url=get_user_meta($user_id, 'iv_profile_pic_url',true);
$default_post_type = array();
	$postkey= array();
	 $post_set=get_option('_iv_directory_url_postype' );									
		
	if($post_set!=""){ 
			$default_fields=get_option('_iv_directory_url_postype' );
	}else{															
			$default_fields['university']='University';
      $default_fields['college']='College';
      $default_fields['school']='School';														
	}
	if(sizeof($post_set)<1){																
			$default_fields['university']='University';
      $default_fields['college']='College';
      $default_fields['school']='School';	
	 }	
	 foreach($default_fields as $key => $value)
		{
		  $postkey[] = $key;
		}
	$post_type = join("','",$postkey);	
	
	$color_setting=get_option('_dir_color');	
if($color_setting==""){$color_setting='#0099fe';}
$color_setting=str_replace('#','',$color_setting);
					
?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<style>
  @media (min-width: 767px) {
  .narrow-width.public-profile-content .col-md-4.col-sm-4,
  .narrow-width.public-profile-content .col-md-8.col-sm-8,
  .narrow-width.public-profile-content .col-md-6 {
    width: 100% !important;
  }
}
#profile-template-5 .active-li{
  border: 1px solid #<?php echo $color_setting; ?>;
  background:#<?php echo $color_setting; ?>
}
</style>

<!-- Bootstrap -->


<div id="profile-template-5" class="bootstrap-wrapper around-separetor public-profile-content">
  <div class="wrapper">
    <div class="row margin-top-10">
      <div class="col-md-4 col-sm-4">
        <div class="profile-sidebar">
          <div class="portlet light profile-sidebar-portlet">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic text-center">
              <?php
              if($iv_profile_pic_url!=''){ ?>
              <img src="<?php echo $iv_profile_pic_url; ?>">
              <?php
            }else{
              echo'	 <img src="'. wp_iv_directories_URLPATH.'assets/images/Blank-Profile.jpg" class="agent">';
            }
            ?>
          </div>
          <!-- END SIDEBAR USERPIC -->
          <!-- SIDEBAR USER TITLE -->
          <div class="profile-usertitle">
            <div class="profile-usertitle-name">
             <?php
             $name_display=get_user_meta($user_id,'first_name',true).' '.get_user_meta($user_id,'last_name',true);
             echo (trim($name_display)!=""? $name_display : $display_name );?>

           </div>
           <div class="profile-usertitle-job">
            <?php echo get_user_meta($user_id,'occupation',true); ?>
          </div>
        </div>

      </div>
      <!-- END PORTLET MAIN -->
      <!-- PORTLET MAIN -->
      <div class="portlet portlet0 light">
        <!-- STAT -->

        <!-- END STAT -->
        <div>
          <h4 class="profile-desc-title"><?php _e('About','ivdirectories'); ?>     <?php
           $name_display=get_user_meta($user_id,'first_name',true).' '.get_user_meta($user_id,'last_name',true);
           echo (trim($name_display)!=""? $name_display : $display_name );?>
         </h4>
         <span class="profile-desc-text"> <?php echo get_user_meta($user_id,'description',true); ?> </span>
         <?php
         if( get_user_meta($user_id,'hide_phone',true)==''){ ?>
         <div class="margin-top-20 profile-desc-text">


          <strong class="desc-title"><i class="fa fa-phone"></i><?php esc_html_e('Phone:','ivdirectories'); ?> </strong><?php echo ''. get_user_meta($user_id,'phone',true); ?>
        </div>
        <?php
      }
      if( get_user_meta($user_id,'hide_mobile',true)==''){ ?>
      <div class="margin-top-20 profile-desc-text">


        <strong class="desc-title"><i class="fa fa-mobile"></i><?php esc_html_e('Mobile:','ivdirectories'); ?> </strong><?php echo ''. get_user_meta($user_id,'mobile',true); ?>
      </div>
      <?php
    }

    if( get_user_meta($user_id,'hide_email',true)==''){ ?>
        <div class="margin-top-20 profile-desc-text"
        ><strong class="desc-title small-icon"><i class="fa fa-envelope"></i><?php esc_html_e('Email:','ivdirectories'); ?> </strong>
        <a href="mailto:<?php echo $email; ?>">

          <?php echo $email; ?>
        </a>
      </div>
<?php
}
?>
  <div class="margin-top-20 profile-desc-text"
        ><strong class="desc-title"><i class="fa fa-globe"></i><?php esc_html_e('Website:','ivdirectories'); ?> </strong>
        <a href="http://<?php  echo get_user_meta($user_id,'web_site',true); ?>">
        <?php  echo get_user_meta($user_id,'web_site',true);  ?>
        </a>
      </div>


<h4 class="profile-desc-title margin-top-40"><?php esc_html_e('Social Profile','ivdirectories'); ?> </h4>

<div class="social-info">
    <div class="profile-desc-link">

      <a href="http://www.twitter.com/<?php  echo get_user_meta($user_id,'twitter',true);  ?>/"><i class="fa fa-twitter-square"></i><?php  //echo get_user_meta($user_id,'twitter',true);  ?></a>
    </div>
    <div class="profile-desc-link">

      <a href="http://www.facebook.com/<?php  echo get_user_meta($user_id,'facebook',true);  ?>/"><i class="fa fa-facebook-square"></i><?php  //echo get_user_meta($user_id,'facebook',true);  ?></a>
    </div>

    <div class="profile-desc-link">

      <a href="http://www.plus.google.com/<?php  echo get_user_meta($user_id,'gplus',true);  ?>/"><i class="fa fa-google-plus-square"></i><?php  //echo get_user_meta($user_id,'gplus',true);  ?></a>
    </div>
  </div>

</div>
</div>
<!-- END PORTLET MAIN -->
</div>

</div>
<div class="col-md-8 col-sm-8">
  <div class="profile-content">
    <div class="portlet-title tabbable-line clearfix">
      <div class="caption caption-md">
        <i class="icon-globe theme-font hide"></i>
        <span class="caption-subject font-blue-madison bold uppercase"><?php _e('User Post','ivdirectories'); ?> </span>
      </div>
    </div>
    <div class="portlet-body">
      <div class="tab-content">
        <!-- PERSONAL INFO TAB -->
        <div class="tab-pane active" id="tab_1_1">
          <div class="main row padding-left-10 text-center">
           <?php
           global $wpdb;
           $iv_post='directories';
           $per_page=8;
           $row_strat=0;$row_end=$per_page;
           $current_page=0 ;
           if(isset($_REQUEST['cpage']) and $_REQUEST['cpage']!=1 ){
            $current_page=$_REQUEST['cpage']; $row_strat =($current_page-1)*$per_page;
            $row_end=$per_page;
          }
          $sql="SELECT * FROM $wpdb->posts WHERE post_type IN ('".$post_type."' ) and post_author='".$user_id."' and post_status IN ('publish') ORDER BY  ID DESC  limit ".$row_strat.", ".$row_end." ";
          $authpr_post = $wpdb->get_results($sql);
          $total_post=count($authpr_post);
							//echo'$total_package.....'.$total_package;
          if(sizeof($total_post)>0){
            $i=0;
            foreach ( $authpr_post as $row )
              {?>
            <div class="col-md-6 text-left">
              <div class="view view-tenth ">
                <?php
                if ( get_the_post_thumbnail( $row->ID, 'thumbnail' )!="" ) {

							//the_post_thumbnail('post-thumbnail', array( 'class' => "home-img"));
                  echo get_the_post_thumbnail( $row->ID, 'medium',array( 'class' => "home-img") );
                }else{ 
                $feature_img= wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";
                ?>
                <img src="<?php echo $feature_img;?>">
                	
                <?php
				}
              ?>

              <div class="mask">
                <h4> <?php echo $row->post_title;?></h4>
                <p><a href="<?php echo get_permalink( $row->ID ); ?>"><i class="fa fa-link"></i></a></p>
              </div>
            </div>
            <div class="view-details">


            <h3 class="post-onprofile-header text-left"><a href="<?php echo get_permalink( $row->ID ); ?>" class="post-list-header"><?php echo substr($row->post_title,0,22);?></a></h3>
            <p class="post-onprofile text-left">
              <?php
                                             // $content=the_excerpt();
              $content_2 =  $row->post_content;

             // echo substr(strip_tags($content_2) ,0,75); ?></p>
              <p class="date">
                <?php _e('Post on :','ivdirectories'); ?>
                <?php echo date('d M Y',strtotime($row->post_date)); ?>
              </p>
              </div>
            </div>
            <?php
          }
        }
        ?>
      </div>

    </div>
    <!-- END PERSONAL INFO TAB -->
    <div class="center"><?php
    
    
      $sql2="SELECT * FROM $wpdb->posts WHERE post_type IN ('".$post_type."' ) and post_author='".$user_id."' and post_status IN ('publish') ";
      $authpr_post2 = $wpdb->get_results($sql2);
      $total_post=count($authpr_post2);
      $total_page= $total_post/$per_page;
      $total_page=ceil( $total_page);
      if($total_page>1){
        $current_page =($current_page==''? '1': $current_page );
        echo ' <ul class="iv-pagination">';
        for($i=1;$i<= $total_page;$i++){
          echo '<li class="'.($i==$current_page  ? 'active-li': '').' list-pagi"><a href="'.get_permalink().'?&profile=all-post&cpage='.$i.'"> '.$i.'</a></li>';
        }
        echo'</ul>';
      }
      ?>
    </div>
  </div>

</div>

</div>
</div>
</div>
</div>
</div>


<script>
  (function($){
    jQuery(window).load(function() {
      jQuery('.home-img').find('img').each(function() {
        var imgClass = (this.width / this.height > 1) ? 'wide' : 'tall';
        jQuery(this).addClass(imgClass);
      })

    })
  });

  jQuery(document).ready(function($) {
    var getWidth = jQuery('.bootstrap-wrapper').width();
    if (getWidth < 780) {
      jQuery('.bootstrap-wrapper').addClass('narrow-width');
    } else {
      jQuery('.bootstrap-wrapper').removeClass('narrow-width');
    }
  });
</script>

