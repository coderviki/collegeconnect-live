
<?php
$current_post_type='';
if(isset($atts['post_type'])){
		$current_post_type = $atts['post_type'];
}
$main_class = new wp_iv_directories;

wp_enqueue_style('iv_directories-style-1109', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap.css');
wp_enqueue_style('iv_directories-style-11265', wp_iv_directories_URLPATH . 'admin/files/css/search-form.css');
wp_enqueue_style('iv_directories-css-queryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css');
wp_enqueue_script('iv_directories-jqueryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
require(wp_iv_directories_DIR .'/admin/files/css/color_style.php');
?>
<div class="bootstrap-wrapper  ">
 	<div id="content">	
				
		<div id="top-search" class=" navbar-default navbar search-form-nav" style="width:100%;">
			<div class=" navbar-collapse text-center" >
				<form class="form-inline advanced-serach" method="POST" action="<?php echo get_post_type_archive_link($current_post_type);?>"  onkeypress="return event.keyCode != 13;">

					<div class="input-field">
						<?php
						$dir_search_keyword=get_option('_dir_search_keyword');	
						if($dir_search_keyword==""){$dir_search_keyword='yes';}	
						if($dir_search_keyword=='yes'){
						?>	
						<div class="form-group" style="margin-top:8px">
							<input type="text" class="form-control-solid " id="keyword" name="keyword"  placeholder="<?php _e( 'Keyword', 'ivdirectories' ); ?>" value="">
							<?php $pos = $main_class->get_unique_keyword_values('keyword',$current_post_type);
									
								?>
								<script>									
									jQuery(function() {
									var availableTags = [ "<?php echo  implode('","',$pos); ?>" ];
									jQuery( "#keyword" ).autocomplete({source: availableTags});
								  });
								  
								</script>
						</div>						
						<?php
						}
						$dir_search_city=get_option('_dir_search_city');	
						if($dir_search_city==""){$dir_search_city='yes';}	
						if($dir_search_city=='yes'){
						// City
						$args_citys = array(
							'post_type'  => $current_post_type,
							'posts_per_page' => -1,
							'meta_query' => array(
								array(
									'key'     => 'city',	
									'orderby' => 'meta_value', 
									'order' => 'ASC',		
								),
								
							),
						);
						$citys = new WP_Query( $args_citys );	
						$citys_all = $citys->posts;
						$get_cityies =array();
						foreach ( $citys_all as $term ) {
							$new_city="";
							$new_city=get_post_meta($term->ID,'city',true);
							if (!in_array($new_city, $get_cityies)) {
								$get_cityies[]=$new_city;
							
							}	
						}	

					// City
					
						
						?>	
						
						<div class="form-group" >
							<select name="dir_city"  id="dir_city" class="form-control-solid" style="margin-top:8px">
								<option   value=""><?php esc_html_e('Choose a City','ivdirectories'); ?></option>	
								<?php	
										$selected_city= (isset($_REQUEST['dir_city'])?$_REQUEST['dir_city']:'' );					
										if(count($get_cityies)) {									  
										  foreach($get_cityies as $row1) {
											  if($row1!=''){													  
											  ?>
											<option   value="<?php echo $row1; ?>" ><?php echo $row1; ?></option>
											<?php
											}
												
											}
										  
										} 
											
										?>												
							</select>
						</div>
						<?php
							}
						?>
						
						<?php
						$dir_search_country=get_option('_dir_search_country');	
						if($dir_search_country==""){$dir_search_country='yes';}	
						if($dir_search_country=='yes'){
						// Country
							$args_country = array(
								'post_type'  => $current_post_type,
								'posts_per_page' => -1,
								'meta_query' => array(
									array(
										'key'     => 'country',	
										'orderby' => 'meta_value', 
										'order' => 'ASC',		
									),
									
								),
							);
							$country = new WP_Query( $args_country );	
							$country_all = $country->posts;
							$get_country =array();
							foreach ( $country_all as $term ) {
								$new_country="";
								$new_country=get_post_meta($term->ID,'country',true);
								if (!in_array($new_country, $get_country)) {
									$get_country[]=$new_country;
								
								}	
							}	

							//Country
							?>
						<div class="form-group" >
							<select name="dir_country"  id="dir_country" class="form-control-solid" style="margin-top:8px">
								<option   value=""><?php esc_html_e('Choose a Country','ivdirectories'); ?></option>	
								<?php	$selected_country= (isset($_REQUEST['dir_country'])?$_REQUEST['dir_country']:'' );						
										if(count($get_country)) {									  
										  foreach($get_country as $row1) {
											  if($row1!=''){													  
											  ?>
											<option   value="<?php echo $row1; ?>" ><?php echo $row1; ?></option>
											<?php
											}
												
											}
										  
										} 
											
										?>												
							</select>
						</div>
						<?php
							}
						?>
						
						<?php
						$dir_search_tag=get_option('_dir_search_tag');	
						if($dir_search_tag==""){$dir_search_tag='yes';}	
						if($dir_search_tag=='yes'){
						?>		
						<div class="form-group" style="margin-top:8px">
							<?php
							$selected= (isset($_REQUEST[$current_post_type.'_tag'])?$_REQUEST[$current_post_type.'_tag']:'' );
							echo '<select name="'.$current_post_type.'_tag" class="form-control-solid">';
							echo'	<option selected="'.$selected.'" value="">'.__('Any Tag','ivdirectories').'</option>';
							$args3 = array(
									'type'                     => $current_post_type,
									//'parent'                   => $term_parent->term_id,
									'orderby'                  => 'name',
									'order'                    => 'ASC',
									'hide_empty'               => 1,
									'hierarchical'             => 1,
									'exclude'                  => '',
									'include'                  => '',
									'number'                   => '',
									'taxonomy'                 => $current_post_type.'_tag',
									'pad_counts'               => false

									);
								$p_tags = get_categories( $args3 );
								if ( $p_tags && !is_wp_error( $p_tags ) ) :


									foreach ( $p_tags as $term ) {
										echo '<option  value="'.$term->slug.'" '.($selected==$term->slug?'selected':'' ).'>'.$term->name.'</option>';
									}

								endif;
								echo '</select>';
								?>
						</div>
						<?php
						}
						?>  
						<?php
						$dir_search_category=get_option('_dir_search_category');	
						if($dir_search_category==""){$dir_search_category='yes';}	
						if($dir_search_category=='yes'){
						?>		
						<div class="form-group" style="margin-top:8px">
							<?php
							$selected= '';
							echo '<select name="'.$current_post_type.'-category" class="form-control-solid">';
							echo'	<option selected="'.$selected.'" value="">'.__('Any Category','ivdirectories').'</option>';


							if( isset($_POST['submit'])){
								$selected = $_POST[$current_post_type.'-category'];
							}
											//directories
							$taxonomy = $current_post_type.'-category';
							$args = array(
								'orderby'           => 'name',
								'order'             => 'ASC',
								'hide_empty'        => true,
								'exclude'           => array(),
								'exclude_tree'      => array(),
								'include'           => array(),
								'number'            => '',
								'fields'            => 'all',
								'slug'              => '',
								'parent'            => '0',
								'hierarchical'      => true,
								'child_of'          => 0,
								'childless'         => false,
								'get'               => '',

								);
								$terms = get_terms($taxonomy,$args); // Get all terms of a taxonomy
								if ( $terms && !is_wp_error( $terms ) ) :
									$i=0;
								foreach ( $terms as $term_parent ) {  ?>


								<?php

								echo '<option  value="'.$term_parent->slug.'" '.($selected==$term_parent->slug?'selected':'' ).'><strong>'.$term_parent->name.'<strong></option>';
								?>
								<?php

								$args2 = array(
									'type'                     => $current_post_type,
									'parent'                   => $term_parent->term_id,
									'orderby'                  => 'name',
									'order'                    => 'ASC',
									'hide_empty'               => 1,
									'hierarchical'             => 1,
									'exclude'                  => '',
									'include'                  => '',
									'number'                   => '',
									'taxonomy'                 => $current_post_type.'-category',
									'pad_counts'               => false

									);
								$categories = get_categories( $args2 );
								if ( $categories && !is_wp_error( $categories ) ) :


									foreach ( $categories as $term ) {
										echo '<option  value="'.$term->slug.'" '.($selected==$term->slug?'selected':'' ).'>-'.$term->name.'</option>';
									}

									endif;
									?>


									<?php
									$i++;
								}
								endif;
								echo '</select>';
								?>
							</div>
						<?php
						}
						?>	
						<?php
						 $_dir_search_zipcode=get_option('_dir_search_zipcode');	
						if($_dir_search_zipcode==""){$dir_search_location='yes';}	
						if($_dir_search_zipcode=='yes'){
							$zipcode=(isset($_REQUEST['zipcode'])?$_REQUEST['zipcode']:'' )
						?>		
						<div class="form-group" style="margin-top:8px">
								<input type="text" class="form-control-solid " id="zipcode" name="zipcode"  placeholder="<?php _e( 'Zipcode', 'ivdirectories' ); ?>"
								value="">
								
								<?php $pos = $main_class->get_unique_post_meta_values('postcode',$current_post_type);
									//print_r($pos);
								?>
								<script>
								  jQuery(function() {
									var availableTags = [ "<?php echo  implode('","',$pos); ?>" ];
									jQuery( "#zipcode" ).autocomplete({source: availableTags});
								  });
								</script>
								
						</div>	
						<?php
						}
						?>
						<?php
						$dir_search_location=get_option('_dir_search_location');	
						if($dir_search_location==""){$dir_search_location='yes';}	
						if($dir_search_location=='yes'){
						?>	
							<div class="form-group" style="margin-top:8px">
								<input type="text" class="form-control-solid " id="address" name="address"  placeholder="<?php _e( 'Location', 'ivdirectories' ); ?>"
								value="">
								<input type="hidden" id="latitude" name="latitude" placeholder="Latitude" value="" >
								<input type="hidden" id="longitude" name="longitude" placeholder="Longitude"  value="">
							</div>
							<?php
						}	
							$dir_f_search_radius=get_option('_dir_f_search_radius');	
							if($dir_f_search_radius==""){$dir_f_search_radius='yes';}	
							if($dir_f_search_radius=='yes'){
							
							
							$dir_search_redius=get_option('_dir_search_redius');
							if($dir_search_redius==""){$dir_search_redius='Km';}
							$radius='50';
							?>
							<div class="form-group range-input" style="margin-top:8px">
								<?php _e( 'Radius', 'ivdirectories' ); ?>: <span id="rvalue"><?php echo $radius;?></span><?php echo ' '.$dir_search_redius; ?>
								<div class="range range-success">
									<input type="range" name="range" id="range" min="1" max="1000" value="<?php echo $radius;?>" onchange="range.value=value">
									<input type="hidden" name="range_value" id="range_value" value="<?php echo $radius; ?>" >
								</div>
							</div>
						<?php
							}
						
						?>


							<div class="form-group search" style="margin-top:8px">
								<button type="submit" id="submit" name="submit"  class="btn-new btn-custom-search "><?php _e('Search','ivdirectories'); ?> </button>
							</div>
						</div>
					
					</form>

				</div>
			</div>

	</div>	
 </div>
<?php
$dir_map_api=get_option('_dir_map_api');	
if($dir_map_api==""){$dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';}	
?>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $dir_map_api;?>'></script>
<script>

function initialize_address() {
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
			google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            //document.getElementById('city2').value = place.name;
            document.getElementById('latitude').value = place.geometry.location.lat();
            document.getElementById('longitude').value = place.geometry.location.lng();
        });
    }
google.maps.event.addDomListener(window, 'load', initialize_address);
</script>
