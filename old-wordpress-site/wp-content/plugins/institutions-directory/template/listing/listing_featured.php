<?php
wp_enqueue_style('iv_directories-style-1109', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap.css');
wp_enqueue_style('iv_directories-style-64', wp_iv_directories_URLPATH . 'assets/cube/css/cubeportfolio.css');
wp_enqueue_style('iv_directories-style-11222', wp_iv_directories_URLPATH . 'admin/files/css/listing-style.css');
require(wp_iv_directories_DIR .'/admin/files/css/color_style.php');

$feature_post_ids =array();
if(isset($atts['post_ids'])){
		$feature_post_ids = explode(",", $atts['post_ids']);
}
$current_post_type='';$post_type='';
if(isset($atts['post_type'])){
	$post_type =  $atts['post_type'];
	$current_post_type=$post_type;
}
if($post_type!=''){
	$args = array(
			'post_type' => $current_post_type, // enter your custom post type
			'post__in'=>$feature_post_ids,
			'post_status' => 'publish',
			//'fields' => 'all',
			//'orderby' => 'ASC',
			'posts_per_page'=> '-1',  // overrides posts per page in theme settings	
			);
	if(isset($atts['category'] )){
		$postcats = get_query_var($current_post_type.'-'.$atts['category']);
		$args[$current_post_type.'-'.$atts['category']]=$postcats;
		$selected=$postcats;
		$args['posts_per_page']='9999';
	}

	
	$filter_query = new WP_Query( $args );
	
		$div_id=rand(1, 500);
		
		if ( $filter_query->have_posts() ) : ?>
		
			<div id="js-grid-meet-the-<?php echo $div_id;?>" class="cbp cbp-l-grid-team" >		
		
				<?php
				while ( $filter_query->have_posts() ) : $filter_query->the_post();
				$id = get_the_ID();	
				$feature_img='';
				if(has_post_thumbnail()){
					$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'medium' );
					if($feature_image[0]!=""){
						$feature_img =$feature_image[0];
					}
				}else{
					$feature_img= wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";

				}
				$currentCategory=wp_get_object_terms( $id, $current_post_type.'-category');
				$cat_name = (isset($currentCategory[0]->name)?$currentCategory[0]->name:'' );	
				
				?>
				<div class="cbp-item  ">
						<a href="<?php echo get_permalink($id); ?>" class="cbp-caption " rel="nofollow">
							<div class="cbp-caption-defaultWrap">
								<div class="image-container" style="background: url('<?php echo esc_attr($feature_img);?>') center center no-repeat; background-size: cover;">
								</div>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignCenter">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-text"><?php _e('VIEW DETAIL', 'ivdirectories' ); ?></div>
									</div>
								</div>
							</div>
						</a>
						<a href="<?php echo get_permalink($id); ?>" class="cbp-l-grid-team-name" ><?php echo substr(get_the_title($id),0,28); ?></a>
						<div class="cbp-l-grid-team-position"><?php echo $cat_name.'&nbsp;'; ?></div>
					</div>
	<?php
				endwhile;
				?>
				</div>
	<?php			
		endif;	
		

	?>


	<?php
	wp_enqueue_script('iv_directories-ar-script-203', wp_iv_directories_URLPATH . 'assets/cube/js/jquery.cubeportfolio.min.js');
	/*
   $handle = 'cubeportfolio.min.js';
   $list = 'enqueued';
     if (wp_script_is( $handle, $list )) {
       return;
     } else {
       wp_register_script( 'cubeportfolio.min.js', wp_iv_directories_URLPATH . 'assets/cube/js/jquery.cubeportfolio.min.js');
       wp_enqueue_script( 'cubeportfolio.min.js' );
     }	
	*/
	?>	
	<script>
	function loadBackupScript(callback) {
			var script;
			
			script = document.createElement('script');
			script.onload = callback;  
			script.src = '<?php echo wp_iv_directories_URLPATH . 'assets/cube/js/jquery.cubeportfolio.min.js';?>';
			document.head.appendChild(script);
		}

	loadBackupScript(function() { 		
		
			setTimeout(function(){
			(function($, window, document, undefined) {
				'use strict';

				// init cubeportfolio
				jQuery('#js-grid-meet-the-<?php echo $div_id;?>').cubeportfolio({
					defaultFilter: '*',
					filters: '#js-filters-meet-the-<?php echo $div_id;?>',
					layoutMode: 'grid',       
					animationType: 'sequentially',
					gapHorizontal: 50,
					gapVertical: 40,
					gridAdjustment: 'responsive',
					mediaQueries: [{
						width: 1500,
						cols: 5
					}, {
						width: 1100,
						cols: 4
					}, {
						width: 800,
						cols: 4
					}, {
						width: 480,
						cols: 2
					}, {
						width: 320,
						cols: 1
					}],
					caption: 'fadeIn',
					displayType: 'lazyLoading',
					displayTypeSpeed: 100,

					// singlePage popup
					singlePageDelegate: '.cbp-singlePage',
					singlePageDeeplinking: true,
					singlePageStickyNavigation: true,
					singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
					singlePageCallback: function(url, element) {
						// to update singlePage content use the following method: this.updateSinglePage(yourContent)
						var t = this;

						$.ajax({
								url: url,
								type: 'GET',
								dataType: 'html',
								timeout: 10000
							})
							.done(function(result) {
								t.updateSinglePage(result);
							})
							.fail(function() {
								t.updateSinglePage('AJAX Error! Please refresh the page!');
							});
					},
				});
			})(jQuery, window, document);
					
				},1000);
		
		
	 });	
		 
		 	


	</script>	
<?php
}
?>	

