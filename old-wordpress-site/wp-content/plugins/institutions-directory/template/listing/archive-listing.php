<?php
//get_header();
global $post,$wpdb,$tag;
wp_enqueue_style('iv_directories-style-1109', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap.css');
wp_enqueue_style('iv_directories-style-64', wp_iv_directories_URLPATH . 'assets/cube/css/cubeportfolio.css');
wp_enqueue_style('iv_directories-style-11222', wp_iv_directories_URLPATH . 'admin/files/css/listing-style.css');
wp_enqueue_style('iv_directories-css-queryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css');
wp_enqueue_style('iv_directories-style-11265', wp_iv_directories_URLPATH . 'admin/files/css/search-form.css');
wp_enqueue_script('iv_directories-script-12', wp_iv_directories_URLPATH . 'admin/files/js/markerclusterer.js');
wp_enqueue_script('iv_directories-jqueryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');

require(wp_iv_directories_DIR .'/admin/files/css/color_style.php');

$current_post_type= get_post_type();

if(isset($atts['post_type'])){	
	$current_post_type =  $atts['post_type'];	
}


$ins_lat='37.4419';
$ins_lng='-122.1419';
$zipcode='';
$search_show=0;
$map_show=0;
$category_one_shortcode='no';
$search_button_show='yes';
$dir_searchbar_show=get_option('_dir_searchbar_show');
if($dir_searchbar_show=="yes"){$search_show=1;}
$dir_map_show=get_option('_dir_map_show');
if($dir_map_show=="yes"){$map_show=1;}
$search_button_show=get_option('_search_button_show');
if($search_button_show==""){$search_button_show='yes';}


$dirs_data =array();
$tag_arr= array();
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
		'post_type' => $current_post_type, // enter your custom post type
		'paged' => $paged,
		'post_status' => 'publish',
		//'fields' => 'all',
		//'orderby' => 'ASC',
		//'posts_per_page'=> '2',  // overrides posts per page in theme settings
		);

	// Add new shortcode only category
$dir_listing_sort=get_option('_dir_listing_sort');	
if($dir_listing_sort=='ASC'){
	$args['orderby']='title';
	$args['order']='ASC';
}
if($dir_listing_sort=='DESC'){
	$args['orderby']='title';
	$args['order']='DESC';
}
$lat='';$long='';$keyword_post='';$address='';$postcats ='';$selected='';

if(get_query_var($current_post_type.'_tag')!=''){
	$post_tag = get_query_var($current_post_type.'_tag');
	$args[$current_post_type.'_tag']=$post_tag;
	$selected_tag=$post_tag;
	$args['posts_per_page']='9999';
}
if( isset($_POST[$current_post_type.'_tag'])){
	if($_POST[$current_post_type.'_tag']!=''){
		$postcats = $_POST[$current_post_type.'_tag'];
		$args[$current_post_type.'_tag']=$postcats;
		$selected=$postcats;
		$args['posts_per_page']='9999';
	}
}

if(get_query_var($current_post_type.'-category')!=''){
	$postcats = get_query_var($current_post_type.'-category');
	$args[$current_post_type.'-category']=$postcats;
	$selected=$postcats;
	$args['posts_per_page']='9999';

}

if( isset($_POST[$current_post_type.'-category'])){
	if($_POST[$current_post_type.'-category']!=''){
		$postcats = $_POST[$current_post_type.'-category'];
		$args[$current_post_type.'-category']=$postcats;
		$selected=$postcats;
		$args['posts_per_page']='9999';
	}
}


$radius=get_option('_iv_radius');
if( isset($_POST['range_value'])){
	$radius = $_POST['range_value'];
}
if($radius==''){$radius='50';}

if( isset($_POST['address'])){
	if($_POST['address']!=""){
		$lat =  $_POST['latitude'];
		$long = $_POST['longitude'];
		$address=trim($_POST['address']);
		if($lat=='' || $long==''){
			$latitude='';$longitude='';

			$prepAddr = str_replace(' ','+',$address);
			$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
			$output= json_decode($geocode);
			if(isset( $output->results[0]->geometry->location->lat)){
				$latitude = $output->results[0]->geometry->location->lat;
			}
			if(isset($output->results[0]->geometry->location->lng)){
				$longitude = $output->results[0]->geometry->location->lng;
			}
			$lat=$latitude;
			$long=$longitude;
			$args['distance']='50';
		}else{
			$args['distance']=$radius;
		}

		$args['lat']=$lat;
		$args['lng']=$long;

		$args['posts_per_page']='9999';
	}
}
if( isset($_POST['keyword'])){
	if($_POST['keyword']!=""){
		$args['s']= $_POST['keyword'];
		$keyword_post=$_POST['keyword'];
		$args['posts_per_page']='9999';
	}
}
if( isset($tag)){
	if($tag!=""){
		if(!isset($_POST['keyword'])){
			$args['tag']= $tag;
		}
	}
}


// Meta Query***********************
$city_mq ='';
if(isset($_REQUEST['dir_city']) AND $_REQUEST['dir_city']!=''){							
		$city_mq = array(
		'relation' => 'AND',
			array(
				'key'     => 'city',
				'value'   => $_REQUEST['dir_city'],
				'compare' => 'LIKE'
			),
		);
}
$country_mq='';
if(isset($_REQUEST['dir_country']) AND $_REQUEST['dir_country']!=''){	
	$country_mq = array(
		'relation' => 'AND',
			array(
				'key'     => 'country',
				'value'   => $_REQUEST['dir_country'],
				'compare' => 'LIKE'
			),
		);
}
$zip_mq='';
if(isset($_REQUEST['zipcode']) AND $_REQUEST['zipcode']!=''){	
	$zip_mq = array(
		'relation' => 'AND',
			array(
				'key'     => 'postcode',
				'value'   => $_REQUEST['zipcode'],
				'compare' => 'LIKE'
			),
		);
}

$args['meta_query'] = array(
	$city_mq, $country_mq, $zip_mq,
);


$dir_load_listing_all=get_option('_dir_load_listing_all');
if($dir_load_listing_all==""){$dir_load_listing_all='yes';}
if($dir_load_listing_all=='yes'){$args['posts_per_page']='9999';}

$the_query = new WP_GeoQuery( $args );

$main_class = new wp_iv_directories;

?>

	<div class="bootstrap-wrapper ">
		<div id="content">
		<!-- Map**************-->
		<div id="top-map" style="<?php echo ($map_show==1 ? '': 'display: none;'); ?>">
			<div id="map"  style=" height: 380px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden;"> </div>
		</div>


		<div id="top-search" class=" navbar-default navbar search-form-nav" style="<?php echo ($search_show==1 ? '': 'display: none;'); ?>width:100%;">
			<div class=" navbar-collapse text-center" >
				<form class="form-inline advanced-serach" method="POST"  onkeypress="return event.keyCode != 13;">

					<div class="input-field">
						<?php
						$dir_search_keyword=get_option('_dir_search_keyword');	
						if($dir_search_keyword==""){$dir_search_keyword='yes';}	
						if($dir_search_keyword=='yes'){
						?>	
						<div class="form-group" style="margin-top:8px">
							<input type="text" class="form-control-solid " id="keyword" name="keyword"  placeholder="<?php _e( 'Keyword', 'ivdirectories' ); ?>" value="<?php echo $keyword_post; ?>">
							<?php $pos = $main_class->get_unique_keyword_values('keyword',$current_post_type);
									
								?>
								<script>									
									jQuery(function() {
									var availableTags = [ "<?php echo  implode('","',$pos); ?>" ];
									jQuery( "#keyword" ).autocomplete({source: availableTags});
								  });
								  
								</script>
						</div>						
						<?php
						}
						$dir_search_city=get_option('_dir_search_city');	
						if($dir_search_city==""){$dir_search_city='yes';}	
						if($dir_search_city=='yes'){
						// City
						$args_citys = array(
							'post_type'  => $current_post_type,
							'posts_per_page' => -1,
							'meta_query' => array(
								array(
									'key'     => 'city',	
									'orderby' => 'meta_value', 
									'order' => 'ASC',		
								),
								
							),
						);
						$citys = new WP_Query( $args_citys );	
						$citys_all = $citys->posts;
						$get_cityies =array();
						foreach ( $citys_all as $term ) {
							$new_city="";
							$new_city=get_post_meta($term->ID,'city',true);
							if (!in_array($new_city, $get_cityies)) {
								$get_cityies[]=$new_city;
							
							}	
						}	

					// City
					
						
						?>	
						
						<div class="form-group" >
							<select name="dir_city"  id="dir_city" class="form-control-solid" style="margin-top:8px">
								<option   value=""><?php esc_html_e('Choose a City','ivdirectories'); ?></option>	
								<?php	
										$selected_city= (isset($_REQUEST['dir_city'])?$_REQUEST['dir_city']:'' );					
										if(count($get_cityies)) {									  
										  foreach($get_cityies as $row1) {
											  if($row1!=''){													  
											  ?>
											<option   value="<?php echo $row1; ?>" <?php echo ($selected_city==$row1?'selected':''); ?>><?php echo $row1; ?></option>
											<?php
											}
												
											}
										  
										} 
											
										?>												
							</select>
						</div>
						<?php
							}
						?>
						
						<?php
						$dir_search_country=get_option('_dir_search_country');	
						if($dir_search_country==""){$dir_search_country='yes';}	
						if($dir_search_country=='yes'){
						// Country
							$args_country = array(
								'post_type'  => $current_post_type,
								'posts_per_page' => -1,
								'meta_query' => array(
									array(
										'key'     => 'country',	
										'orderby' => 'meta_value', 
										'order' => 'ASC',		
									),
									
								),
							);
							$country = new WP_Query( $args_country );	
							$country_all = $country->posts;
							$get_country =array();
							foreach ( $country_all as $term ) {
								$new_country="";
								$new_country=get_post_meta($term->ID,'country',true);
								if (!in_array($new_country, $get_country)) {
									$get_country[]=$new_country;
								
								}	
							}	

							//Country
							?>
						<div class="form-group" >
							<select name="dir_country"  id="dir_country" class="form-control-solid" style="margin-top:8px">
								<option   value=""><?php esc_html_e('Choose a Country','ivdirectories'); ?></option>	
								<?php	$selected_country= (isset($_REQUEST['dir_country'])?$_REQUEST['dir_country']:'' );						
										if(count($get_country)) {									  
										  foreach($get_country as $row1) {
											  if($row1!=''){													  
											  ?>
											<option   value="<?php echo $row1; ?>" <?php echo ($selected_country==$row1?'selected':''); ?> ><?php echo $row1; ?></option>
											<?php
											}
												
											}
										  
										} 
											
										?>												
							</select>
						</div>
						<?php
							}
						?>
						
						<?php
						$dir_search_tag=get_option('_dir_search_tag');	
						if($dir_search_tag==""){$dir_search_tag='yes';}	
						if($dir_search_tag=='yes'){
						?>		
						<div class="form-group" style="margin-top:8px">
							<?php
							$selected= (isset($_REQUEST[$current_post_type.'_tag'])?$_REQUEST[$current_post_type.'_tag']:'' );
							echo '<select name="'.$current_post_type.'_tag" class="form-control-solid">';
							echo'	<option selected="'.$selected.'" value="">'.__('Any Tag','ivdirectories').'</option>';
							$args3 = array(
									'type'                     => $current_post_type,
									//'parent'                   => $term_parent->term_id,
									'orderby'                  => 'name',
									'order'                    => 'ASC',
									'hide_empty'               => 1,
									'hierarchical'             => 1,
									'exclude'                  => '',
									'include'                  => '',
									'number'                   => '',
									'taxonomy'                 => $current_post_type.'_tag',
									'pad_counts'               => false

									);
								$p_tags = get_categories( $args3 );
								if ( $p_tags && !is_wp_error( $p_tags ) ) :


									foreach ( $p_tags as $term ) {
										echo '<option  value="'.$term->slug.'" '.($selected==$term->slug?'selected':'' ).'>'.$term->name.'</option>';
									}

								endif;
								echo '</select>';
								?>
						</div>
						<?php
						}
						?>  
						<?php
						$dir_search_category=get_option('_dir_search_category');	
						if($dir_search_category==""){$dir_search_category='yes';}	
						if($dir_search_category=='yes'){
						?>		
						<div class="form-group" style="margin-top:8px">
							<?php
							$selected= '';
							echo '<select name="'.$current_post_type.'-category" class="form-control-solid">';
							echo'	<option selected="'.$selected.'" value="">'.__('Any Category','ivdirectories').'</option>';


							if( isset($_POST['submit'])){
								$selected = $_POST[$current_post_type.'-category'];
							}
											//directories
							$taxonomy = $current_post_type.'-category';
							$args = array(
								'orderby'           => 'name',
								'order'             => 'ASC',
								'hide_empty'        => true,
								'exclude'           => array(),
								'exclude_tree'      => array(),
								'include'           => array(),
								'number'            => '',
								'fields'            => 'all',
								'slug'              => '',
								'parent'            => '0',
								'hierarchical'      => true,
								'child_of'          => 0,
								'childless'         => false,
								'get'               => '',

								);
								$terms = get_terms($taxonomy,$args); // Get all terms of a taxonomy
								if ( $terms && !is_wp_error( $terms ) ) :
									$i=0;
								foreach ( $terms as $term_parent ) {  ?>


								<?php

								echo '<option  value="'.$term_parent->slug.'" '.($selected==$term_parent->slug?'selected':'' ).'><strong>'.$term_parent->name.'<strong></option>';
								?>
								<?php

								$args2 = array(
									'type'                     => $current_post_type,
									'parent'                   => $term_parent->term_id,
									'orderby'                  => 'name',
									'order'                    => 'ASC',
									'hide_empty'               => 1,
									'hierarchical'             => 1,
									'exclude'                  => '',
									'include'                  => '',
									'number'                   => '',
									'taxonomy'                 => $current_post_type.'-category',
									'pad_counts'               => false

									);
								$categories = get_categories( $args2 );
								if ( $categories && !is_wp_error( $categories ) ) :


									foreach ( $categories as $term ) {
										echo '<option  value="'.$term->slug.'" '.($selected==$term->slug?'selected':'' ).'>-'.$term->name.'</option>';
									}

									endif;
									?>


									<?php
									$i++;
								}
								endif;
								echo '</select>';
								?>
							</div>
						<?php
						}
						?>	
						<?php
						 $_dir_search_zipcode=get_option('_dir_search_zipcode');	
						if($_dir_search_zipcode==""){$dir_search_location='yes';}	
						if($_dir_search_zipcode=='yes'){
							$zipcode=(isset($_REQUEST['zipcode'])?$_REQUEST['zipcode']:'' )
						?>		
						<div class="form-group" style="margin-top:8px">
								<input type="text" class="form-control-solid " id="zipcode" name="zipcode"  placeholder="<?php _e( 'Zipcode', 'ivdirectories' ); ?>"
								value="<?php echo trim($zipcode); ?>">
								
								<?php $pos = $main_class->get_unique_post_meta_values('postcode',$current_post_type);
									//print_r($pos);
								?>
								<script>
								  jQuery(function() {
									var availableTags = [ "<?php echo  implode('","',$pos); ?>" ];
									jQuery( "#zipcode" ).autocomplete({source: availableTags});
								  });
								</script>
								
						</div>	
						<?php
						}
						?>
						<?php
						$dir_search_location=get_option('_dir_search_location');	
						if($dir_search_location==""){$dir_search_location='yes';}	
						if($dir_search_location=='yes'){
						?>	
							<div class="form-group" style="margin-top:8px">
								<input type="text" class="form-control-solid " id="address" name="address"  placeholder="<?php _e( 'Location', 'ivdirectories' ); ?>"
								value="<?php echo trim($address); ?>">
								<input type="hidden" id="latitude" name="latitude" placeholder="Latitude" value="<?php echo $lat; ?>" >
								<input type="hidden" id="longitude" name="longitude" placeholder="Longitude"  value="<?php echo $long; ?>">
							</div>
							<?php
						}	
							$dir_f_search_radius=get_option('_dir_f_search_radius');	
							if($dir_f_search_radius==""){$dir_f_search_radius='yes';}	
							if($dir_f_search_radius=='yes'){
							
							
							$dir_search_redius=get_option('_dir_search_redius');
							if($dir_search_redius==""){$dir_search_redius='Km';}
							?>
							<div class="form-group range-input" style="margin-top:8px">
								<?php _e( 'Radius', 'ivdirectories' ); ?>: <span id="rvalue"><?php echo $radius;?></span><?php echo ' '.$dir_search_redius; ?>
								<div class="range range-success">
									<input type="range" name="range" id="range" min="1" max="1000" value="<?php echo $radius;?>" onchange="range.value=value">
									<input type="hidden" name="range_value" id="range_value" value="<?php echo $radius; ?>" >
								</div>
							</div>
						<?php
							}
						
						?>


							<div class="form-group search" style="margin-top:8px">
								<button type="submit" id="submit" name="submit"  class="btn-new btn-custom-search "><?php _e('Search','ivdirectories'); ?> </button>
							</div>
						</div>
					
					</form>

				</div>
			</div>

		</div>
		<div class="clearfix" style="margin-top:20px">
			<div class="listing-filter-content">
			<?php

				if($search_button_show=='yes'){
					?>
					<div id="" class="cbp-l-filters-button cbp-l-filters-right">
						<div id="search_toggle_div" class="cbp-filter-item" onclick="toggle_top_search('top-search');" ><i class="fa fa-search listing-padding-right"></i><?php _e('Advance Search', 'ivdirectories' ); ?></div>
						<div  id="map_toggle_div"  class="cbp-filter-item" onclick="toggle_top_search('top-map');"><i class="fa fa-globe listing-padding-right"></i><?php _e('Map', 'ivdirectories' ); ?></div>
					</div>
					<?php
				}
				?>
			</div>

		</div>	
		<?php
		$dir_top_filter=get_option('_dir_top_filter');
		if($dir_top_filter==""){$dir_top_filter='yes';}
		if($dir_top_filter=='yes'){
			if ( $the_query->have_posts() ){
		?>
			<div class="clearfix" style="margin-top:20px">
			<div class="listing-filter-content">

			
			<?php
			if($category_one_shortcode=='no'){
				?>
				<div id="js-filters-meet-the-team" class="cbp-l-filters-button cbp-l-filters-left" >
					<?php

					if($postcats==''){	?>

					<div data-filter="*" class="cbp-filter-item">
						<?php _e('Show All', 'ivdirectories' ); ?>
					</div>
					<?php

					$argscat = array(
						'type'                     => $current_post_type,
						'orderby'                  => 'name',
						'order'                    => 'ASC',
						'hide_empty'               => true,
						'hierarchical'             => 1,
						'exclude'                  => '',
						'include'                  => '',
						'number'                   => '',
						'taxonomy'                 => $current_post_type.'-category',
						'pad_counts'               => false

						);
					$categories = get_categories( $argscat );

					if ( $categories && !is_wp_error( $categories ) ) :
						foreach ( $categories as $term ) {
							if($dir_load_listing_all=='yes'){
								echo '<div data-filter=".'.$term->slug.'" class="cbp-filter-item"> '.$term->name.' <div class="cbp-filter-counter"></div></div>';
							}else{
								?>
								<div data-filter="" class="cbp-filter-item"><a href="<?php echo get_post_type_archive_link( $current_post_type).'?&'.$current_post_type.'-category='.$term->slug ; ?>">
									<?php echo $term->name; ?>
								</a>
							</div>
							<?php
						}
					}
					endif;
				}
				if($postcats!=''){ ?>
				<div data-filter="" class="cbp-filter-item"><a href="<?php echo get_post_type_archive_link( $current_post_type) ; ?>">
					<?php _e('Show All', 'ivdirectories' ); ?>
				</a>
			</div>
			<?php
			echo '<div data-filter=".'.$postcats.'"  class="cbp-filter-item-active cbp-filter-item"> '.ucwords(str_replace('-',' ',$postcats)).' <div class="cbp-filter-counter"></div></div>';


		}

		?>
	</div>
	<?php
		} // For one category short code
		?>


		</div>
			
			<?php
			}
		}	
			?>

	



	<div id="js-grid-meet-the-team" class="cbp cbp-l-grid-team" >
		<?php
		$i=1;
		if ( $the_query->have_posts() ) :

			while ( $the_query->have_posts() ) : $the_query->the_post();
			$id = get_the_ID();

			$gallery_ids=get_post_meta($id ,'image_gallery_ids',true);
			$gallery_ids_array = array_filter(explode(",", $gallery_ids));

			$dir_data['link']=get_post_permalink();
			$dir_data['title']=$post->post_title;
			$dir_data['lat']=get_post_meta($id,'latitude',true);
			$dir_data['lng']=get_post_meta($id,'longitude',true);
			
			if(get_post_meta($id,'latitude',true)!=''){$ins_lat=get_post_meta($id,'latitude',true);}
			if(get_post_meta($id,'longitude',true)!=''){$ins_lng=get_post_meta($id,'longitude',true);}
			 
			$dir_data['address']=get_post_meta($id,'address',true);
			$dir_data['image']= '';
			$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'thumbnail' );
			if($feature_image[0]!=""){
						//$dir_data['image']= '<img class=" img-responsive" src="'. $feature_image[0].'">';
				$dir_data['image']=  $feature_image[0];
			}
			$dir_data['marker_icon']=wp_iv_directories_URLPATH."/assets/images/map-marker/map-marker.png";
			$currentCategoryId='';
			$terms =get_the_terms($id, $current_post_type."-category");
			if($terms!=""){
				foreach ($terms as $termid) {
					if(isset($termid->term_id)){
						$currentCategoryId= $termid->term_id;
					}
				}
			}
			$marker = get_option('_cat_map_marker_'.$currentCategoryId,true);
			if($marker!=''){
					$image_attributes = wp_get_attachment_image_src( $marker ); // returns an array
					if( $image_attributes ) {

						$dir_data['marker_icon']= $image_attributes[0];
					}
				}
				array_push( $dirs_data, $dir_data );
				$feature_img='';
				if(has_post_thumbnail()){
					$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'medium' );
					if($feature_image[0]!=""){
						$feature_img =$feature_image[0];
					}
				}else{
					$feature_img= wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";

				}

				$currentCategory=wp_get_object_terms( $id, $current_post_type.'-category');
				$cat_link='';$cat_name='';$cat_slug='';
				if ( ! empty( $currentCategory ) ) {
					if(isset($currentCategory[0]->slug)){					
						for($i=0;$i<20;$i++){
							if(isset($currentCategory[$i]->slug)){
								$cat_slug=$cat_slug.' '.(isset($currentCategory[$i]->slug) ? $currentCategory[$i]->slug :'');
							}
						
						}
						
						$cat_name = $currentCategory[0]->name;
						$cat_link= get_term_link($currentCategory[0], $current_post_type.'-category');
					}
				}	
				?>
				<div class="cbp-item <?php echo $cat_slug; ?> ">
					<a href="<?php echo get_post_permalink(); ?>" class="cbp-caption " >
						<div class="cbp-caption-defaultWrap">
							<div class="image-container" style="background: url('<?php echo esc_attr($feature_img);?>') center center no-repeat; background-size: cover;">
							</div>
						</div>
						<div class="cbp-caption-activeWrap">
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body">
									<div class="cbp-l-caption-text"><?php _e('VIEW DETAIL', 'ivdirectories' ); ?></div>
								</div>
							</div>
						</div>
					</a>
					<a href="<?php echo get_post_permalink(); ?>" class="cbp-l-grid-team-name" ><?php echo substr($post->post_title,0,28); ?></a>
					<div class="cbp-l-grid-team-position"><?php echo $cat_name.'&nbsp;'; ?></div>
				</div>
				<?php
				$i++;

				endwhile;
				$dirs_json ='';
				if(!empty($dirs_data)){
					$dirs_json =json_encode($dirs_data);
				}

				?>


			<?php else :
			$dirs_json='';
			?>

			<?php _e( 'Sorry, no posts matched your criteria.' ); ?>

		<?php endif; ?>



	</div>
	
	<div style="text-align:center; margin-top:30px">
				<?php
				if ( !$the_query->have_posts() ){
					esc_html_e( 'Sorry, no data matched your criteria.','ivdirectories' );
				}
				?>
		</div>		
						
				<!--
					paging plugin
					https://wordpress.org/plugins/wp-pagenavi/screenshots/
				-->
				<?php if (function_exists('wp_pagenavi')) : ?>
					<div style="text-align:center; margin-top:30px">

						<?php wp_pagenavi( array( 'query' => $the_query ) ); ?>
					</div>
				<?php else: ?>
					<br/>
					<br/>
					<div class="cbp-l-filters-left nav-next"><span class="cbp-l-grid-team-name;"><?php previous_posts_link( ''.__( ' Newer Entries', 'ivdirectories' ).'' ); ?></span></div>
					<div class="cbp-l-filters-right nav-previous" ><span class="cbp-l-grid-team-name"><?php next_posts_link( ''.__( ' Older Entries ', 'ivdirectories' ).'' ); ?></span></div>


					<br/>
					<br/>
				<?php endif; ?>

				<!--END .navigation-links-->
	</div>
</div>
			
			<?php
			//wp_enqueue_script('iv_directories-ar-script-23', wp_iv_directories_URLPATH . 'assets/cube/js/jquery.cubeportfolio.min.js');
			$handle = 'cubeportfolio.min.js';
		   $list = 'enqueued';
			 if (wp_script_is( $handle, $list )) {
			   return;
			 } else {
			   wp_register_script( 'cubeportfolio.min.js', wp_iv_directories_URLPATH . 'assets/cube/js/jquery.cubeportfolio.min.js');
			   wp_enqueue_script( 'cubeportfolio.min.js' );
			 }	
			 
			wp_enqueue_script('iv_directories-ar-script-102', wp_iv_directories_URLPATH . 'assets/cube/js/meet-team.js');
			?>

			<?php
			$dir_map_api=get_option('_dir_map_api');
			if($dir_map_api==""){$dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';}
			$dir_map_zoom=get_option('_dir_map_zoom');	
			if($dir_map_zoom==""){$dir_map_zoom='7';}	
			?>
			<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $dir_map_api;?>'></script>
			<script type="text/javascript">
				function initialize() {
					var center = new google.maps.LatLng('<?php echo $ins_lat; ?>', '<?php echo $ins_lng; ?>');
					//var center = new google.maps.LatLng(49, 2.56);
					var map = new google.maps.Map(document.getElementById('map'), {
						zoom: <?php echo $dir_map_zoom; ?>,
						center: center,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					var markers = [];
					var infowindow = new google.maps.InfoWindow();
					var dirs ='';
					var min = .999999;
					var max = 1.000002;
					<?php echo ($dirs_json!=''? 'var dirs ='.$dirs_json:''); ?>;
					if(dirs!=''){
						for (i = 0; i < dirs.length; i++) {
						//for(var i=0;i<5;i++){
							 var new_lat= dirs[i].lat  * (Math.random() * (max - min) + min);
							 var new_lng= dirs[i].lng  * (Math.random() * (max - min) + min);
							 
							 var latLng = new google.maps.LatLng(new_lat,new_lng);							 
							//var latLng = new google.maps.LatLng(dirs[i].lat,dirs[i].lng);
							var marker = new google.maps.Marker({
								position: latLng,
								map: map,
								icon: dirs[i].marker_icon,
							});
							markers.push(marker);
							google.maps.event.addListener(marker, 'click', (function(marker, i) {
								return function() {
											//infowindow.setContent('<div id="map-marker-info " ><a href="'+dirs[i].link +'">'+dirs[i].image+'<h5>'+ dirs[i].title //+'</h5><span class="address">'+dirs[i].address+'</span></a></div>');
											infowindow.setContent('<div id="map-marker-info" style="overflow: auto; cursor: default; clear: both; position: relative; border-radius: 4px; padding: 15px; border-color: rgb(255, 255, 255); border-style: solid; background-color: rgb(255, 255, 255); border-width: 1px; width: 275px; height: 130px;"><div style="overflow: hidden;" class="map-marker-info"><a  style="text-decoration: none;" href="'+dirs[i].link +'">	<span style="background-image: url('+dirs[i].image+')" class="list-cover has-image"></span><span class="address"><strong>'+dirs[i].title +'</strong></span> <span class="address" style="margin-top:15px">'+dirs[i].address+'</span></a></div></div>');
											infowindow.open(map, marker);
										}
									})(marker, i));
}
}
var markerCluster = new MarkerClusterer(map, markers);

}
function cs_toggle_street_view(btn) {
	var toggle = panorama.getVisible();
	if (toggle == false) {
		if(btn == 'streetview'){
			panorama.setVisible(true);
		}
	} else {
		if(btn == 'mapview'){
			panorama.setVisible(false);
		}
	}
}
google.maps.event.addDomListener(window, 'load', initialize);

//google.maps.event.trigger(map, 'resize');
jQuery("#search_toggle_div").on('click', function(e) {
	setTimeout(function(){
		initialize();
		google.maps.event.trigger(map, 'resize');
	},500)
});
jQuery("#map_toggle_div").on('click', function(e) {
	setTimeout(function(){
		initialize();
		google.maps.event.trigger(map, 'resize');
	},500)
});



function initialize_address() {
	var input = document.getElementById('address');
	var autocomplete = new google.maps.places.Autocomplete(input);
	google.maps.event.addListener(autocomplete, 'place_changed', function () {
		var place = autocomplete.getPlace();
		//document.getElementById('city2').value = place.name;
		document.getElementById('latitude').value = place.geometry.location.lat();
		document.getElementById('longitude').value = place.geometry.location.lng();
	  });
}
google.maps.event.addDomListener(window, 'load', initialize_address);
				jQuery('input[name="range"]').on("change", function() {
		//jQuery(this).next().html(jQuery(this).val() + '%');
		jQuery('#rvalue').html(jQuery(this).val());
		jQuery('#range_value').val(jQuery(this).val());
		//console.log(jQuery(this).val());
	});

function toggle_top_map(divId) {
	jQuery("#"+divId).toggle('slow');


}
function toggle_top_search(divId) {

	jQuery("#"+divId).toggle('slow');
	initialize();
	google.maps.event.trigger(map, 'resize');

}


</script>
<?php
//get_footer();
?>			
