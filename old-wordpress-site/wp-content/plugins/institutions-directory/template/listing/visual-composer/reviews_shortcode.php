<?php

global $post, $comments;	
$id=$post->ID;
?>
<div class="col-md-12 ">
<ul class="cbp-l-project-details-list stars">
<?php
$i=1;$default_fields='';
$field_set=get_option('iv_cpt-1_fields_review' );
if($field_set!=""){
		$default_fields=get_option('iv_cpt-1_fields_review' );
}else{
	$default_fields['Compass']=esc_html__('Compass','ivdirectories');
	$default_fields['Library']=esc_html__('Library','ivdirectories');
	$default_fields['Teacher']=esc_html__('Teacher','ivdirectories');
	$default_fields['Cost']=esc_html__('Cost','ivdirectories');
	$default_fields['Teaching']=esc_html__('Teaching','ivdirectories');
	$default_fields['Learning']=esc_html__('Learning','ivdirectories');

}
if(sizeof($default_fields)>0){
	foreach ( $default_fields as $field_key => $field_value ) {
		$field_value_trim=trim($field_value);
		$old_rating= get_post_meta($id,$field_key.'_rating',true);
		$key_total_count= get_post_meta($id,$field_key.'_count',true);	
		if($key_total_count<1){$key_total_count=1;}
		$old_rating=$old_rating/$key_total_count;
		?>
		<!-- <li><strong><?php echo $field_value_trim; ?></strong> -->
		 <li class="row">
			<h5 class="title col-xs-6"><?php echo $field_value_trim; ?></h5>
			 <span class="col-xs-6">
		 
			  <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;"  onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','1')" >
			 <i  id="<?php echo $field_key ?>_1" class="uourating fa fa-star<?php echo($old_rating>=1 ? '':'-o'); ?>"></i></a>

			 <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;"  onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','2')" >
			 <i id="<?php echo $field_key ?>_2"  class="uourating fa fa-star<?php echo($old_rating>=2 ? '':'-o'); ?>"></i></a>

			 <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;"  onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','3')" >
			 <i id="<?php echo $field_key ?>_3"  class="uourating fa fa-star<?php echo($old_rating>=3 ? '':'-o'); ?>"></i></a>

			 <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;" onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','4')" >
			 <i id="<?php echo $field_key ?>_4"  class="uourating fa fa-star<?php echo($old_rating>=4 ? '':'-o'); ?>"></i></a>

			 <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;" onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','5')" >
			 <i id="<?php echo $field_key ?>_5" class="uourating fa fa-star<?php echo($old_rating>=5 ? '':'-o'); ?>"></i></a>
			 </span>
		 
		 </li>
		<?php

		}
}		
	?>
</ul>
</div>



