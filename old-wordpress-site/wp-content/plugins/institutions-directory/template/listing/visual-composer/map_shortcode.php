<?php

global $post;	
$id=$post->ID;
?>
<div class="col-md-12 ">
<?php
	$lat=get_post_meta($id,'latitude',true);
	$lng=get_post_meta($id,'longitude',true);

	// Get latlng from address* START********
	$dir_lat=$lat;
	$dir_lng=$lng;
	$address = get_post_meta($id,'address',true);
	if($address!=''){
			if($dir_lat=='' || $dir_lng==''){
				$latitude='';$longitude='';

				$prepAddr = str_replace(' ','+',$address);
				$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
				$output= json_decode($geocode);
				if(isset( $output->results[0]->geometry->location->lat)){
					$latitude = $output->results[0]->geometry->location->lat;
				}
				if(isset($output->results[0]->geometry->location->lng)){
					$longitude = $output->results[0]->geometry->location->lng;
				}
				if($latitude!=''){
					update_post_meta($id,'latitude',$latitude);
				}
				if($longitude!=''){
					update_post_meta($id,'longitude',$longitude);
				}
				$lat=$latitude;
				$lng=$longitude;
			}
	}
?>

<iframe width="100%" height="325" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=<?php echo $address; ?>&amp;ie=UTF8&amp;&amp;output=embed"></iframe>
</div>
