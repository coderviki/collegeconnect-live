<?php

global $post;	
$id=$post->ID;
?>
<div class="col-md-12 ">
	<a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php _e('Share On Facebook','ivdirectories'); ?>" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();  ?>"><i class="fa fa-facebook-square fa-2x"></i></a>

	<a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php _e('Share On Twitter','ivdirectories'); ?>" href="https://twitter.com/home?status=<?php the_permalink();  ?>"><i class="fa fa-twitter fa-2x"></i></a>

	<a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php _e('Share On linkedin','ivdirectories'); ?>" href="https://www.linkedin.com/shareArticle?mini=true&url=test&title=<?php echo $post->post_title; ?>&summary=&source="><i class="fa fa-linkedin-square fa-2x"></i></a>

	<a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php _e('Share On google+','ivdirectories'); ?>" href="https://plus.google.com/share?url=<?php the_permalink();  ?>"><i class="fa fa-google-plus-square fa-2x"></i></a>
</div>





