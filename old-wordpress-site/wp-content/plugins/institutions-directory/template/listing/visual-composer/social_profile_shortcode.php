<?php

global $post;	
$id=$post->ID;
?>
<div class="col-md-12 ">
<ul class="cbp-l-project-details-list">
	<li>

			<?php
			if(get_post_meta($id,'facebook',true)!=""){ ?>
				<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('Facebook Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'facebook',true);?>" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a>
			<?php
			}
			?>
			<?php
			if(get_post_meta($id,'twitter',true)!=""){ ?>
				<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('Twitter Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'twitter',true);?>" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
			<?php
			}
			?>
			<?php
			if(get_post_meta($id,'linkedin',true)!=""){ ?>
				<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('linkedin Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'linkedin',true);?>" target="_blank"><i class="fa fa-linkedin-square fa-2x"></i></a>
			<?php
			}
			?>
			<?php
			if(get_post_meta($id,'gplus',true)!=""){ ?>
				<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('google+ Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'gplus',true);?>" target="_blank"><i class="fa fa-google-plus-square fa-2x"></i></a>
			<?php
			}
			if(get_post_meta($id,'pinterest',true)!=""){ ?>
				<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('Pinterest Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'pinterest',true);?>" target="_blank"><i class="fa fa-pinterest-square fa-2x"></i></a>
			<?php
			}
			if(get_post_meta($id,'instagram',true)!=""){ ?>
				<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('Instagram Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'instagram',true);?>" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
			<?php
			}
			
		 ?>
		 </li>
</ul> 

</div>
