<?php

global $post;	
$id=$post->ID;
?>
<?php
								
		$lat=get_post_meta($id,'latitude',true);
		$lng=get_post_meta($id,'longitude',true);
	?>
<div class="col-md-12 ">
<ul class="cbp-l-project-details-list">
	<li><strong><?php _e('Location','ivdirectories'); ?></strong>
	<div class="tooltipsingle">
			<?php
			
				echo '<a  style="text-decoration: none;"  class= "tooltipsingle" href="http://maps.google.com/maps?saddr=Current+Location&amp;daddr='.get_post_meta($id,'latitude',true).'%2C'.get_post_meta($id,'longitude',true).'" target="_blank"">'.get_post_meta($id,'address',true).'</a>';
			?>
					
				<span class="tooltiptext"><?php _e('Click address for directions','ivdirectories'); ?> </span>
		</div>
	</li>
	  <li><strong><?php _e('Phone','ivdirectories'); ?></strong>
		<?php echo '<a style="text-decoration: none;" href="tel:'.get_post_meta($id,'phone',true).'">'.get_post_meta($id,'phone',true).'</a>' ;?>
	</li>
	  <li><strong><?php _e('Fax','ivdirectories'); ?></strong>
				<?php echo get_post_meta($id,'fax',true).'&nbsp;';?>			</li>
	  <li><strong><?php _e('Email','ivdirectories'); ?></strong>
				<?php echo get_post_meta($id,'contact-email',true).'&nbsp;';?>					</li>
	  <li><strong><?php _e('Web Site','ivdirectories'); ?></strong>
			<?php echo '<a style="text-decoration: none;" href="'. get_post_meta($id,'contact_web',true).'" target="_blank"">'. get_post_meta($id,'contact_web',true).'&nbsp; </a>';?>
	 </li>
	
	 
	</ul> 
</div>


