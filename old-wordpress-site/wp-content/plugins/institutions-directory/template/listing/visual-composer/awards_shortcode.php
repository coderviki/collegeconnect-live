<?php
global $post;	
$id=$post->ID;
?>
<div class="col-md-12 ">
<?php
   for($i=0;$i<20;$i++){
	   if(get_post_meta($id,'_award_title_'.$i,true)!='' || get_post_meta($id,'_award_description_'.$i,true) || get_post_meta($id,'_award_year_'.$i,true)|| get_post_meta($id,'_award_image_id_'.$i,true) ){?>
		
		   <div class="cbp-l-inline">
				<div class="cbp-l-inline-left">
					<?php
						if(get_post_meta($id,'_award_image_id_'.$i,true)!=''){?>
							<img src="<?php echo wp_get_attachment_url( get_post_meta($id,'_award_image_id_'.$i,true) ); ?> " >
						<?php
						}

					?>

				</div>
				<div class="cbp-l-inline-right-hd">
					<div class="cbp-l-award-title"><?php echo get_post_meta($id,'_award_title_'.$i,true); ?></div>
					<div class="cbp-l-inline-subtitle"><?php echo get_post_meta($id,'_award_year_'.$i,true); ?></div>
					<div class="cbp-l-inline-desc">
							<?php echo get_post_meta($id,'_award_description_'.$i,true); ?>
					</div>
				</div>
			</div>

		<?php
		}
	}
	?>
</div>


