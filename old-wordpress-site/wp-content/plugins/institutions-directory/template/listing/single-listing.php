<?php
get_header();
global $post,$wpdb;

	 $id = get_the_ID();
	 $post_id_1 = get_post($id);
	$post_id_1->post_title;
if (!defined('wp_iv_directories_URLPATH'))define('wp_iv_directories_URLPATH', trailingslashit(WP_PLUGIN_URL . '/' . plugin_basename(dirname(__FILE__))));
$wp_iv_directories_URLPATH=wp_iv_directories_URLPATH;

wp_enqueue_style('wp-iv_directories-myaccount-style-11', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap.css');
wp_enqueue_style('iv_directories-style-71', wp_iv_directories_URLPATH . 'assets/cube/css/cubeportfolio.css');
wp_enqueue_style('iv_directories-style-10', wp_iv_directories_URLPATH . 'admin/files/css/single-cpt.css');

require(wp_iv_directories_DIR .'/admin/files/css/color_style.php');
?>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<?php

$wp_directory= new wp_iv_directories();
 while ( have_posts() ) : the_post();

	if(has_post_thumbnail()){
		$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' );
		if($feature_image[0]!=""){
			$feature_img =$feature_image[0];
		}
	}else{
		$feature_img= wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";

	}
	/*
	$currentCategory=wp_get_object_terms( $id, 'listing-category');
	$cat_link='';$cat_name='';$cat_slug='';
	if(isset($currentCategory[0]->slug)){
		$cat_slug = $currentCategory[0]->slug;
		$cat_name = $currentCategory[0]->name;
		$cat_link= get_term_link($currentCategory[0], 'listing-category');
	}
	*/
	$template_option='1';
	if(get_option('cpt_page_'.get_post_type())>0){
		$template_option='0';
	}
	if(get_option('cpt_page_'.get_post_type())==''){
		$template_option='1';
	}

	?>



	<?php
	if($template_option=='1'){
	?>
	<div class="bootstrap-wrapper ">
	<div class="row">
		<div class="cbp-l-project-title col-md-12">
				<?php
					echo '<h3>'.$post_id_1->post_title.'</h3>';
				 ?>

		</div>
	</div>

	<div class="row">
			 <div class="col-md-9 col-md-push-3 ">				
					
								<?php
									//get_template_part( 'content', 'single' );

									$gallery_ids=get_post_meta($id ,'image_gallery_ids',true);
									$gallery_ids_array = array_filter(explode(",", $gallery_ids));
									$i=1;
									
									if(sizeof($gallery_ids_array)>0){
										?>
										<div class="cbp-slider">						
										<ul class="cbp-slider-wrap">
												<?php
												foreach($gallery_ids_array as $slide){
														if($slide!=''){ ?>
															
														 <li class="cbp-slider-item">
															<img src="<?php echo wp_get_attachment_url( $slide ); ?> " >
														 </li>
														<?php
														$i++;
														}
													}	
													?>
												</ul>
											</div>											
										<?php	
										}
									if(sizeof($gallery_ids_array)<1){ ?>
										 <div class="content">
										<?php	if(has_post_thumbnail($id)){												
													echo get_the_post_thumbnail($id, 'large');												
											}else{												?>
												 
												<img class="img-responsive"  src="<?php echo  wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";?>">
												 
											<?php
											}
											?>
										</div>	
								<?php			
									}
								?>
						

				 <div class="content">
				 		<div class="title-content">
				 			<div class="cposttype-title"><h5><?php _e('About Us','ivdirectories'); ?></h5></div>
				 				<span id="fav_dir<?php echo $id; ?>" style="float: right;">
											<?php
												$user_ID = get_current_user_id();
												if($user_ID>0){
													$my_favorite = get_post_meta($id,'_favorites',true);
													$all_users = explode(",", $my_favorite);
													if (in_array($user_ID, $all_users)) { ?>
														<a style="text-decoration: none;" data-toggle="tooltip" data-placement="bottom" title="<?php _e('Added to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_unfavorite('<?php echo $id; ?>')" >
														<span class="hide-sm"><i class="fa fa-heart  red-heart fa-lg"></i>&nbsp;&nbsp; </span></a>
													<?php
													}else{ ?>
														<a style="text-decoration: none;" data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_favorite('<?php echo $id; ?>')" >
														<span class="hide-sm"><i class="fa fa-heart fa-lg"></i>&nbsp;&nbsp; </span>
														</a>
													<?php
													}
												}else{ ?>
														<a style="text-decoration: none;" data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_favorite('<?php echo $id; ?>')" >
														<span class="hide-sm"><i class="fa fa-heart fa-lg"></i>&nbsp;&nbsp; </span>
														</a>
											<?php
												}
											?>
								</span>

				 		</div>

				 		<div class="conten-desc">

						<?php
								if($wp_directory->check_reading_access('description',$id)){
							?>

								<div class="cbp-l-project-desc-text">
								<?php
										$content = apply_filters( 'the_content', get_the_content() );
										$content = str_replace( ']]>', ']]&gt;', $content );
										echo  $content;

								?>
								</div>
							<div class="cbp-l-project-desc-text">
								<?php
								$i=1;
								$field_set=get_option('iv_directories_fields' );
								if($field_set!=""){
										$default_fields=get_option('iv_directories_fields' );
								}else{
										$default_fields['Totalenrollment']='Total enrollment';
										$default_fields['Undergraduate_graduate_ratio']='Undergraduate/graduate ratio ';
										$default_fields['No_of_international_students']='No. of international students ';
										$default_fields['pers_of international_students']=' % of international students';
										$default_fields['no_of_faculty']='No. of faculty';
										$default_fields['Student_faculty_ratio']='Student faculty ratio';
										$default_fields['no_of_staff']='No. of staff';
										$default_fields['Annual_revenue ']='Annual revenue';
								}
								if(sizeof($default_fields)>0){ 	?>
									<ul class="cbp-l-project-adi-list">
									<?php
									foreach ( $default_fields as $field_key => $field_value ) {
										$field_value_trim=trim($field_value);
										?>
										 <li><?php echo _e($field_value_trim, 'wp_iv_directories'); ?>
											<?php echo ' : '.get_post_meta($id,$field_key,true); ?>
										</li>
									<?php
									}
									?>
								</ul>
								<?php
								}
								?>
							</div>
						<?php
						}else{
								echo get_option('_iv_visibility_login_message');

							}
						?>

						</div>




				</div> <!-- End About US-->

						<?php
						$Specialitiess = get_post_meta($id,'Specialities',true);
						$Specialitiess_arr=array_filter(explode(",",$Specialitiess));
						if(sizeof($Specialitiess_arr)>0){?>
							
							<div class="content Specialitiess-list">
							<div class="title-content">
								<div class="cposttype-title"><h5><?php _e('Specialitiess','ivdirectories'); ?></h5>
								</div>
							</div>

							<div class="conten-desc specialist-list">
							<?php
								$Specialitiess = get_post_meta($id,'Specialities',true);
								$Specialitiess_arr= explode(",",$Specialitiess);
								if(sizeof($Specialitiess_arr)>0){?>
									<ul class="cbp-l-project-details-list">
									<?php
									foreach($Specialitiess_arr as $sp_one){
											if(trim($sp_one)!=''){
											?>
										<li><?php echo $sp_one;?><span style="float: right;" ></span></li>
									<?php
										}
									}?>
									</ul>
								<?php
								}
								?>
							</div>
					</div>
						
					<?php
					}
					
					$dir_tag_show=get_option('_dir_tag_show');	
					if($dir_tag_show==""){$dir_tag_show='yes';}
					if($dir_tag_show=='yes'){
					?>
						<div class="content ">
							<div class="title-content">
								<div class="cposttype-title"><h5><?php _e('Tags','ivdirectories'); ?></h5>
								</div>
							</div>

							<div class="conten-desc tag-list">
							<?php 
													$post_type=get_post_type();
												
													echo '<ul class="tags">';
													$tags_all= wp_get_object_terms( $id,  $post_type.'_tag');												
													foreach ( $tags_all as $term ) {	
														
														echo'<li><a href="'.get_tag_link($term->term_id) .'">'.$term->name.'</a></li>';
														//echo'<li><a href="#">'.$one_tag->name.'</a></li>';																
																
													 }					
												
													echo'</ul>';
									
									
								  ?>
							</div>
					</div>	
			
					<?php
				}	
				
				
			if(trim(get_post_meta($id,'_award_title_0',true))!=''){ ?>
					<div class="content">
						<div class="title-content">
							<div class="cposttype-title"><h5><?php _e('Awards','ivdirectories'); ?></h5></div>
						</div>

						<div class="conten-desc award-content">

							<div class="cbp-l-project-desc-text">
								<?php
								if($wp_directory->check_reading_access('award',$id)){
								?>
								<?php
								   for($i=0;$i<20;$i++){
									   if(get_post_meta($id,'_award_title_'.$i,true)!='' || get_post_meta($id,'_award_description_'.$i,true) || get_post_meta($id,'_award_year_'.$i,true)|| get_post_meta($id,'_award_image_id_'.$i,true) ){?>

										   <div class="cbp-l-inline">
												<div class="cbp-l-inline-left">
													<?php
														if(get_post_meta($id,'_award_image_id_'.$i,true)!=''){?>
															<img src="<?php echo wp_get_attachment_url( get_post_meta($id,'_award_image_id_'.$i,true) ); ?> " >
														<?php
														}

													?>

												</div>
												<div class="cbp-l-inline-right-hd">
													<div class="cbp-l-award-title"><?php echo get_post_meta($id,'_award_title_'.$i,true); ?></div>
													<div class="cbp-l-inline-subtitle"><?php echo get_post_meta($id,'_award_year_'.$i,true); ?></div>
													<div class="cbp-l-inline-desc">
															<?php echo get_post_meta($id,'_award_description_'.$i,true); ?>
													</div>
												</div>
											</div>

										<?php
										}
									}
								}else{
								echo get_option('_iv_visibility_login_message');

							}
							?>
							</div>
						</div>

				</div>
			<?php
			}
			
			if(trim(get_post_meta($id,'_doc_title_0',true))!=''){ ?>
					<div class="content">
						<div class="title-content">
							<div class="cposttype-title"><h5><?php _e('Documents','ivdirectories'); ?></h5></div>
						</div>

						<div class="conten-desc award-content">

							<div class="cbp-l-project-desc-text">
								<?php
								if($wp_directory->check_reading_access('documents',$id)){
								?>
								
								<?php
								   for($i=0;$i<30;$i++){
									   if(get_post_meta($id,'_doc_title_'.$i,true)!='' ||  get_post_meta($id,'_doc_id_'.$i,true) ){?>

										   <div class="cbp-l-inline">
												<div class="col-md-2">
													<?php
														if(get_post_meta($id,'_doc_id_'.$i,true)!=''){?>
															<a target="_blank"  href="<?php echo wp_get_attachment_url( get_post_meta($id,'_doc_id_'.$i,true) ); ?>">
																<img style="width:60px" src="<?php echo wp_iv_directories_URLPATH.'assets/images/PDF1.png'; ?> " >																
															</a>
														<?php
														}
													?>
												</div>												
												<div class="col-md-10">
														<a target="_blank"  href="<?php echo wp_get_attachment_url( get_post_meta($id,'_doc_id_'.$i,true) ); ?>">
														<?php echo get_post_meta($id,'_doc_title_'.$i,true); ?>
														</a>
												</div>													
											</div>

										<?php
										}
									}
								}else{
								echo get_option('_iv_visibility_login_message');

							}
							?>
							</div>
						</div>

				</div>
			<?php
			}
			?>
			
			
			
			
			
				<div class="">
			<?php
				if($wp_directory->check_reading_access('video',$id)){
				 $video_vimeo_id= get_post_meta($id,'vimeo',true);
				 $video_youtube_id=get_post_meta($id,'youtube',true);
				if($video_vimeo_id!='' || $video_youtube_id!=''){
				?>
				<div class="content">
					<div class="title-content">
								<div class="cposttype-title"><h5><?php _e('Video','ivdirectories'); ?></h5>
								</div>

							<div class="conten-desc">
								<?php
									 $v=0;
									 $video_vimeo_id= get_post_meta($id,'vimeo',true);
									 if($video_vimeo_id!=""){ $v=$v+1; ?>
											<iframe src="https://player.vimeo.com/video/<?php echo $video_vimeo_id; ?>" width="100%" height="315px" frameborder="0"></iframe>
									<?php
									 }
									?>
									<br/>
									<?php
									 $video_youtube_id=get_post_meta($id,'youtube',true);
									 if($video_youtube_id!=""){
											echo($v==1?'<hr>':'');
										 ?>

											<iframe width="100%" height="315px" src="https://www.youtube.com/embed/<?php echo $video_youtube_id; ?>" frameborder="0" allowfullscreen></iframe>
									<?php
									 }
									?>
							</div>
				</div>
				</div>
				<?php
					}
				}
				?>

		</div>
		
		<div class="content">					
							<?php
							if($wp_directory->check_reading_access('event')){
							?>
								<?php

							 if(trim(get_post_meta($id,'event_title',true))!='' || trim(get_post_meta($id,'event_detail',true))!=''  || trim(get_post_meta($id,'_event_image_id',true))!=''  ){?>
							 	<div class="title-content">
							 		<div class="cposttype-title"><h5><?php _e('Event','ivdirectories'); ?></h5>
							 		</div>
							 	</div>
							 	<div class="conten-desc award-content">

									<div class="cbp-l-project-desc-text">


											   <div class="cbp-l-inline">
													<div class="cbp-l-inline-left">
														<?php
															if(get_post_meta($id,'_event_image_id',true)!=''){?>
																<img src="<?php echo wp_get_attachment_url( get_post_meta($id,'_event_image_id',true) ); ?> " >
															<?php
															}

														?>

													</div>
													<div class="cbp-l-inline-right-hd">
														<div class="cbp-l-award-title"><?php echo get_post_meta($id,'event_title',true); ?></div>
														<div class="cbp-l-inline-desc">
																<?php echo get_post_meta($id,'event_detail',true); ?>
														</div>
													</div>
												</div>


								</div>
								</div>

								<?php
										}

								  ?>

								<?php
							}
							?>
				
		</div>
		<?php
		$dir_comments_show=get_option('_dir_comments_show');	
		if($dir_comments_show==""){$dir_comments_show='no';}
		
		if($dir_comments_show=='yes'){
		?>
		<div class="content col-md-12">
				<div class="title-content">
					<div class="cposttype-title"><h5><?php _e('Comments','ivdirectories'); ?></h5>
					</div>
				</div>
			
			<div class=" ">
			 <?php comments_template(); ?>
			 </div>
		</div>	
		<?php
		}
		?>

		</div> <!-- end col-md-9  -->
		 <div class="col-md-3 col-md-pull-9">
		 	<div class="medicaldirectory-sidebar">
			 	<?php
				$logo_image_id=get_post_meta($id ,'logo_image_id',true);
				if($logo_image_id>0){?>
					<img style="vertical-align:middle"  src="<?php echo wp_get_attachment_url( $logo_image_id ); ?> " >
				<?php
				}?>


						<div class="sidebar-content">

							
							<div class="cbp-l-project-details-title "><span><?php _e('Contact Info','ivdirectories'); ?></span>
							</div>
								<?php
								if($wp_directory->check_reading_access('contact info',$id)){
									$lat=get_post_meta($id,'latitude',true);
									$lng=get_post_meta($id,'longitude',true);
								?>
							
							<ul class="cbp-l-project-details-list">
								<li><strong><?php _e('Location','ivdirectories'); ?></strong>
								<div class="tooltipsingle">
									<?php
									
										echo '<a  style="text-decoration: none;"  class= "tooltipsingle" href="http://maps.google.com/maps?saddr=Current+Location&amp;daddr='.get_post_meta($id,'latitude',true).'%2C'.get_post_meta($id,'longitude',true).'" target="_blank"">'.get_post_meta($id,'address',true).'</a>';
									?>
											
										<span class="tooltiptext"><?php _e('Click address for directions','ivdirectories'); ?> </span>
								</div>
								
								
								</li>
								  <li><strong><?php _e('Phone','ivdirectories'); ?></strong>
									<?php echo '<a style="text-decoration: none;" href="tel:'.get_post_meta($id,'phone',true).'">'.get_post_meta($id,'phone',true).'</a>' ;?>
								</li>
								  <li><strong><?php _e('Fax','ivdirectories'); ?></strong>
											<?php echo get_post_meta($id,'fax',true).'&nbsp;';?>			</li>
								  <li><strong><?php _e('Email','ivdirectories'); ?></strong>
											<?php echo get_post_meta($id,'contact-email',true).'&nbsp;';?>					</li>
								  <li><strong><?php _e('Web Site','ivdirectories'); ?></strong>
										<?php echo '<a style="text-decoration: none;" href="'. get_post_meta($id,'contact_web',true).'" target="_blank"">'. get_post_meta($id,'contact_web',true).'&nbsp; </a>';?>
								 </li>
								 <li><strong><?php _e('Social Profile','ivdirectories'); ?></strong>

								 		<?php
								 		if(get_post_meta($id,'facebook',true)!=""){ ?>
								 			<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('Facebook Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'facebook',true);?>" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a>
								 		<?php
								 		}
								 		?>
								 		<?php
								 		if(get_post_meta($id,'twitter',true)!=""){ ?>
								 			<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('Twitter Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'twitter',true);?>" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
								 		<?php
								 		}
								 		?>
								 		<?php
								 		if(get_post_meta($id,'linkedin',true)!=""){ ?>
								 			<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('linkedin Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'linkedin',true);?>" target="_blank"><i class="fa fa-linkedin-square fa-2x"></i></a>
								 		<?php
								 		}
								 		?>
								 		<?php
								 		if(get_post_meta($id,'gplus',true)!=""){ ?>
								 			<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('google+ Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'gplus',true);?>" target="_blank"><i class="fa fa-google-plus-square fa-2x"></i></a>
								 		<?php
								 		}
								 		if(get_post_meta($id,'pinterest',true)!=""){ ?>
								 			<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('Pinterest Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'pinterest',true);?>" target="_blank"><i class="fa fa-pinterest-square fa-2x"></i></a>
								 		<?php
								 		}
								 		if(get_post_meta($id,'instagram',true)!=""){ ?>
								 			<a data-toggle="tooltip" data-placement="bottom" class="icon-blue"  title="<?php _e('Instagram Profile','ivdirectories'); ?>" href="<?php echo get_post_meta($id,'instagram',true);?>" target="_blank"><i class="fa fa-instagram-square fa-2x"></i></a>
								 		<?php
								 		}
								 	}
								 ?>
								 </li>
						
								 <li><strong><?php _e('Share','ivdirectories'); ?></strong>
								 			<a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php _e('Share On Facebook','ivdirectories'); ?>" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();  ?>"><i class="fa fa-facebook-square fa-2x"></i></a>

								 			<a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php _e('Share On Twitter','ivdirectories'); ?>" href="https://twitter.com/home?status=<?php the_permalink();  ?>"><i class="fa fa-twitter fa-2x"></i></a>

								 			<a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php _e('Share On linkedin','ivdirectories'); ?>" href="https://www.linkedin.com/shareArticle?mini=true&url=test&title=<?php echo $post_id_1->post_title; ?>&summary=&source="><i class="fa fa-linkedin-square fa-2x"></i></a>

								 			<a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php _e('Share On google+','ivdirectories'); ?>" href="https://plus.google.com/share?url=<?php the_permalink();  ?>"><i class="fa fa-google-plus-square fa-2x"></i></a>

								 	<?php
								 	 if(get_post_meta($id,'facebook',true)!='' || get_post_meta($id,'twitter',true)!='' || get_post_meta($id,'linkedin',true)!=''|| get_post_meta($id,'gplus',true)!='' ){

								 	?>
								 </li>
							</ul>
						<?php
						}else{
							echo get_option('_iv_visibility_login_message');

						}
						?>






				</div>

				<div class="sidebar-content">
				
					<div class="cbp-l-project-details-title "><span><?php _e('Location Map','ivdirectories'); ?></span>
							
						</div>
				</div>

					<div class="cbp-l-project-desc-text">
							<?php
								$lat=get_post_meta($id,'latitude',true);
								$lng=get_post_meta($id,'longitude',true);

								// Get latlng from address* START********
								$dir_lat=$lat;
								$dir_lng=$lng;
								$address = get_post_meta($id,'address',true);
								if($address!=''){
										if($dir_lat=='' || $dir_lng==''){
											$latitude='';$longitude='';

											$prepAddr = str_replace(' ','+',$address);
											$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
											$output= json_decode($geocode);
											if(isset( $output->results[0]->geometry->location->lat)){
												$latitude = $output->results[0]->geometry->location->lat;
											}
											if(isset($output->results[0]->geometry->location->lng)){
												$longitude = $output->results[0]->geometry->location->lng;
											}
											if($latitude!=''){
												update_post_meta($id,'latitude',$latitude);
											}
											if($longitude!=''){
												update_post_meta($id,'longitude',$longitude);
											}
											$lat=$latitude;
											$lng=$longitude;
										}
								}
							?>

							<iframe width="100%" height="325" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=<?php echo $address; ?>&amp;ie=UTF8&amp;&amp;output=embed"></iframe><br />
						</div>



				<div class="sidebar-content">
					<?php
					$openin_days =get_post_meta($id ,'_opening_time',true);
					if($openin_days!=''){
					if(sizeof($openin_days)>0){?>

							<div class="cbp-l-project-details-title "><span><?php _e('Opening Time','ivdirectories'); ?></span></div>
							

							<ul class="cbp-l-project-details-list">
						<?php
														

						$openin_days =get_post_meta($id ,'_opening_time',true);		

						 if(!is_array($openin_days)){	
							$openin_days_final = array();
							$daysArr = explode( ',', $openin_days );
							foreach( $daysArr as $val ){
							  $tmp = explode( '|', $val );
							  $openin_days_final[ $tmp[0] ] = $tmp[1].'|'.$tmp[2];
							}
							$openin_days=$openin_days_final;
						 } 
							foreach($openin_days as $key => $item){
								$day_time = explode("|", $item);
								?>
								 <li><strong><?php echo $key; ?></strong><?php echo $day_time[0].' - '.$day_time[1];  ?></li>
								<?php

								}
							?>
							   </ul>
						<?php
					} }
				 ?>
				</div>
				<div class="sidebar-content ">
				<div class="cbp-l-project-details-title "><span><?php esc_html_e('Reviews','ivdirectories'); ?></span></div>
										
					<ul class="cbp-l-project-details-list ">
					<?php
					$i=1;$default_fields='';
					$field_set=get_option('iv_cpt-1_fields_review' );
					if($field_set!=""){
							$default_fields=get_option('iv_cpt-1_fields_review' );
					}else{
						$default_fields['Compass']=esc_html__('Compass','ivdirectories');
						$default_fields['Library']=esc_html__('Library','ivdirectories');
						$default_fields['Teacher']=esc_html__('Teacher','ivdirectories');
						$default_fields['Cost']=esc_html__('Cost','ivdirectories');
						$default_fields['Teaching']=esc_html__('Teaching','ivdirectories');
						$default_fields['Learning']=esc_html__('Learning','ivdirectories');

					}
					if(sizeof($default_fields)>0){
						foreach ( $default_fields as $field_key => $field_value ) {
							$field_value_trim=trim($field_value);
							$old_rating= get_post_meta($id,$field_key.'_rating',true);
							$key_total_count= get_post_meta($id,$field_key.'_count',true);	
							if($key_total_count<1){$key_total_count=1;}
							$old_rating=$old_rating/$key_total_count;
							?>
							 <li><strong><?php echo $field_value_trim; ?></strong>
								  <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;"  onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','1')" >
								 <i  id="<?php echo $field_key ?>_1" class="uourating fa fa-star<?php echo($old_rating>=1 ? '':'-o'); ?>"></i></a>

								 <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;"  onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','2')" >
								 <i id="<?php echo $field_key ?>_2"  class="uourating fa fa-star<?php echo($old_rating>=2 ? '':'-o'); ?>"></i></a>

								 <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;"  onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','3')" >
								 <i id="<?php echo $field_key ?>_3"  class="uourating fa fa-star<?php echo($old_rating>=3 ? '':'-o'); ?>"></i></a>

								 <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;" onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','4')" >
								 <i id="<?php echo $field_key ?>_4"  class="uourating fa fa-star<?php echo($old_rating>=4 ? '':'-o'); ?>"></i></a>

								 <a  title="<?php esc_html_e('Submit Rating','ivdirectories'); ?>" href="javascript:;" onclick="save_rating('<?php echo $id; ?>','<?php echo $field_key; ?>','5')" >
								 <i id="<?php echo $field_key ?>_5" class="uourating fa fa-star<?php echo($old_rating>=5 ? '':'-o'); ?>"></i></a>
							 
							 
							 </li>
							<?php

							}
					}		
						?>
					</ul>
						
				</div>



					<?php
				$dir_contact_show=get_option('_dir_contact_show');
				if($dir_contact_show==''){$dir_contact_show='yes';}
				if($dir_contact_show=='yes'){

				?>


					<div class="sidebar-content">
						<div class="cbp-l-project-details-title "><span><?php _e('Contact Us','ivdirectories'); ?></span></div>
							<?php
								if($wp_directory->check_reading_access('contact us',$id)){
							?>
								<form action="" id="message-pop" name="message-pop"  method="POST" role="form">
								<div class="cbp-l-grid-projects-desc">
									<input id="subject" name ="subject" type="text" placeholder="<?php _e( 'Enter Subject', 'ivdirectories' ); ?>" class="form-control-solid">
								</div>
								<div class="cbp-l-grid-projects-desc">
									<input name ="email_address" id="email_address" type="text" placeholder="<?php _e( 'Enter Email', 'ivdirectories' ); ?>" class="form-control-solid">
								</div>
								<div class="cbp-l-grid-projects-desc">
									<textarea name="message-content" id="message-content"  class="form-control-solid"  cols="54" rows="4" title="Please Enter Message"  placeholder="<?php _e( 'Enter Message', 'ivdirectories' ); ?>"  ></textarea>
								</div>
								 <input type="hidden" name="dir_id" id="dir_id" value="<?php echo $id; ?>">
								  <a onclick="send_message_iv();" class="btn-new btn-custom full-width"><?php _e( 'Send Message', 'ivdirectories' ); ?></a>
									<div id="update_message_popup"></div>
								</form>
							<?php
							}else{
									echo get_option('_iv_visibility_login_message');

							}
							?>

					</div>
					<?php
					}
				?>




		</div>






				 <?php
				$dir_claim_show=get_option('_dir_claim_show');
				if($dir_claim_show==""){$dir_claim_show='yes';}
				if($dir_claim_show=="yes"){
				if(get_post_meta($id,'iv_listing_approve',true)!='yes'){
				?>

				<div class="medicaldirectory-sidebar">
					<div class="sidebar-content  claims">
			 			<div class="cbp-2-project-details">

					<div class="cbp-l-project-details-title "><span><?php _e('Claim The Listing','ivdirectories'); ?></span>
					</div>
				 <form action="" id="message-claim" name="message-claim"  method="POST" role="form">
						<div class="cbp-l-grid-projects-desc">
							<input id="subject" name ="subject" type="text" placeholder="<?php _e( 'Enter Subject', 'ivdirectories' ); ?>" Value="<?php _e('Claim The Listing', 'ivdirectories' ); ?>" class="form-control-solid">
						</div>
						<div class="cbp-l-grid-projects-desc">
							<textarea name="message-content" id="message-content"  class="form-control-solid"  cols="56" rows="4" title="Please Enter Message"  placeholder="<?php _e( 'Enter Message', 'ivdirectories' ); ?>"  ></textarea>
						</div>
						 <input type="hidden" name="dir_id" id="dir_id" value="<?php echo $id; ?>">
						  <a onclick="send_message_claim();" class="btn-new btn-custom full-width"><?php _e( 'Submit Claim', 'ivdirectories' ); ?></a>
							<div id="update_message_claim"></div>

					</form>

			<?php
				}
			}
			?>


		</div>
		</div>
		</div>



		</div> <!-- end col-md-3 -->

	</div>

</div>
<?php
	}else{
		
		$post_id = get_option('cpt_page_'.get_post_type());// example post id
		$post_content = get_post($post_id);
		$content = $post_content->post_content;
		echo do_shortcode( $content );//executing shortcodes

	
	}

?>






<?php
endwhile;

$dir_slider_auto=get_option('_dir_slider_auto');	
if($dir_slider_auto==""){$dir_slider_auto='no';}

//wp_enqueue_script('iv_directories-ar-script-23', wp_iv_directories_URLPATH . 'assets/cube/js/jquery.cubeportfolio.min.js');
//wp_enqueue_script('iv_directories-ar-script-24', wp_iv_directories_URLPATH . 'assets/cube/js/slider-main.js');
//wp_enqueue_script('iv_directories-ar-script-102', wp_iv_directories_URLPATH . 'assets/cube/js/meet-team.js');
?>


<script type="text/javascript" src="<?php echo $wp_iv_directories_URLPATH; ?>assets/cube/js/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="<?php echo $wp_iv_directories_URLPATH; ?>assets/cube/js/meet-team.js"></script>



<script type="text/javascript">

function send_message_iv(){
		var formc = jQuery("#message-pop");
		if (jQuery.trim(jQuery("#message-content",formc).val()) == "") {
                  alert("Please put your message");
        } else {
			var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
			var loader_image = '<img style="width:60px" src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
				jQuery('#update_message_popup').html(loader_image);
				var search_params={
					"action"  : 	"iv_directories_message_send",
					"form_data":	jQuery("#message-pop").serialize(),
				};
				jQuery.ajax({
					url : ajaxurl,
					dataType : "json",
					type : "post",
					data : search_params,
					success : function(response){
						jQuery('#update_message_popup').html(response.msg );
						jQuery("#message-pop").trigger('reset');
					}
				});
		}
	}
function send_message_claim(){

		var isLogged ="<?php echo get_current_user_id();?>";
        if (isLogged=="0") {
                alert("<?php _e('Please login to Claim The Listing', 'ivdirectories' ); ?>");
        } else {

			var form = jQuery("#message-claim");
			if (jQuery.trim(jQuery("#message-content", form).val()) == "") {
                  alert("Please put your message");
			} else {
				var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
				var loader_image = '<img style="width:60px"  src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
				jQuery('#update_message_claim').html(loader_image);
				var search_params={
					"action"  : 	"iv_directories_claim_send",
					"form_data":	jQuery("#message-claim").serialize(),
				};
				jQuery.ajax({
					url : ajaxurl,
					dataType : "json",
					type : "post",
					data : search_params,
					success : function(response){
						jQuery('#update_message_claim').html('   '+response.msg );
						jQuery("#message-claim").trigger('reset');

					}
				});
			}
		}

	}
function save_favorite(id) {

		  var isLogged ="<?php echo get_current_user_id();?>";

                if (isLogged=="0") {
                     alert("<?php _e('Please login to add favorite','ivdirectories'); ?>");
                } else {

						var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
						var search_params={
							"action"  : 	"iv_directories_save_favorite",
							"data": "id=" + id,
						};

						jQuery.ajax({
							url : ajaxurl,
							dataType : "json",
							type : "post",
							data : search_params,
							success : function(response){
								jQuery("#fav_dir"+id).html('<a data-toggle="tooltip" data-placement="bottom" title="<?php _e('Added to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_unfavorite('+id+')" ><span class="hide-sm"><i class="fa fa-heart  red-heart fa-lg"></i>&nbsp;&nbsp; </span></a>');


							}
						});

				}

    }
	function save_unfavorite(id) {
		  var isLogged ="<?php echo get_current_user_id();?>";

                if (isLogged=="0") {
                     alert("<?php _e('Please login to remove favorite','ivdirectories'); ?>");
                } else {

						var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
						var search_params={
							"action"  : 	"iv_directories_save_un_favorite",
							"data": "id=" + id,
						};

						jQuery.ajax({
							url : ajaxurl,
							dataType : "json",
							type : "post",
							data : search_params,
							success : function(response){
								jQuery("#fav_dir"+id).html('<a data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_favorite('+id+')" ><span class="hide-sm"><i class="fa fa-heart fa-lg "></i>&nbsp;&nbsp; </span></a>');


							}
						});

				}

    }

(function($, window, document, undefined) {
    var slider = $('.cbp-slider');
    slider.find('.cbp-slider-item').addClass('cbp-item');
    slider.cubeportfolio({
        layoutMode: 'slider',
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1,
            cols: 1
        }],
        gapHorizontal: 0,
        gapVertical: 0,
        caption: '',
        auto:<?php echo ($dir_slider_auto=='yes'? 'true':'false'); ?>,
    });
})(jQuery, window, document);
(function($, window, document, undefined) {
    'use strict';
    // init cubeportfolio
    var singlePage = jQuery('#js-singlePage-container').children('div');
    jQuery('#js-grid-slider-projects').cubeportfolio({
        layoutMode: 'slider',
        drag: true,
        auto: false,
        autoTimeout: 4000,
        autoPauseOnHover: true,
        showNavigation: true,
        showPagination: true,
        rewindNav: false,
        scrollByPage: false,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 5
        }, {
            width: 1100,
            cols: 4
        }, {
            width: 800,
            cols: 3
        }, {
            width: 480,
            cols: 2
        }, {
            width: 320,
            cols: 1
        }],
        gapHorizontal: 0,
        gapVertical: 25,
        caption: 'overlayBottomReveal',
        displayType: 'lazyLoading',
        displayTypeSpeed: 100,
        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',
        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageAnimation: 'fade',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var indexElement = $(element).parents('.cbp-item').index(),
                item = singlePage.eq(indexElement);
            this.updateSinglePage(item.html());},
    });
})(jQuery, window, document);

function save_rating(post_id,rating_text,rating_value) {
		
		  
             var isLogged ="<?php echo get_current_user_id();?>";

                if (isLogged=="0") {
                     alert("<?php _e('Please login to remove favorite','ivdirectories'); ?>");                    
               
                } else { 
						
						var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';							
						var search_params={
							"action"  : 	"iv_directories_save_rating",	
							"data": "id=" + post_id+"&rating_text="+rating_text+"&rating_value="+rating_value,
						};
						
						jQuery.ajax({					
							url : ajaxurl,					 
							dataType : "json",
							type : "post",
							data : search_params,
							success : function(response){ 
								var ii=0;
								for(ii=0; ii<=5; ii++){									
									jQuery("#"+rating_text+"_"+ii).removeClass("fa-star");
									jQuery("#"+rating_text+"_"+ii).addClass("fa-star-o");									
								}	
								
								for(ii=0; ii<=rating_value; ii++){
									jQuery("#"+rating_text+"_"+ii).removeClass("fa-star-o");
									jQuery("#"+rating_text+"_"+ii).removeClass("fa-star");
									jQuery("#"+rating_text+"_"+ii).addClass("fa-star");	
								}	
								 
								
							}
						});
						
				}  
				
    }
</script>



<?php


get_footer();
?>
