<style>
.bs-callout {
    margin: 20px 0;
    padding: 15px 30px 15px 15px;
    border-left: 5px solid #eee;
}
.bs-callout-info {
    background-color: #E4F1FE;
    border-color: #22A7F0;
}
.html-active .switch-html, .tmce-active .switch-tmce {
	height: 28px!important;
	}
	.wp-switch-editor {
		height: 28px!important;
	}
</style>	
	

		<h3  class=""><?php _e('Directory Setting','ivdirectories');  ?><small></small>	
		</h3>
	
		<br/>
		<div id="update_message"> </div>		 
					
			<form class="form-horizontal" role="form"  name='directory_settings' id='directory_settings'>											
					<?php
				
					$new_badge_day=get_option('_iv_new_badge_day');
					//$bid_start_amount=get_option('_bid_start_amount');
					$dir_approve_publish =get_option('_dir_approve_publish');
					$dir_archive=get_option('_dir_archive_page');	
					if($dir_approve_publish==""){$dir_approve_publish='no';}	
					$dir_claim_show=get_option('_dir_claim_show');	
					if($dir_claim_show==""){$dir_claim_show='yes';}
					
					$search_button_show=get_option('_search_button_show');	
					if($search_button_show==""){$search_button_show='yes';}
					
					$dir_searchbar_show=get_option('_dir_searchbar_show');	
					if($dir_searchbar_show==""){$dir_searchbar_show='no';}
					
					$dir_map_show=get_option('_dir_map_show');	
					if($dir_map_show==""){$dir_map_show='no';}
					
					$dir_social_show=get_option('_dir_social_show');	
					if($dir_social_show==""){$dir_social_show='yes';}				
					
					
					$dir_contact_show=get_option('_dir_contact_show');	
					if($dir_contact_show==""){$dir_contact_show='yes';}
					
					
					$dir_load_listing_all=get_option('_dir_load_listing_all');	
					if($dir_load_listing_all==""){$dir_load_listing_all='yes';}
					
					$dir_load_doctor_all=get_option('_dir_load_doctor_all');	
					if($dir_load_doctor_all==""){$dir_load_doctor_all='yes';}
					
					$dir_comments_show=get_option('_dir_comments_show');	
					if($dir_comments_show==""){$dir_comments_show='no';}
					
					$dir_slider_auto=get_option('_dir_slider_auto');	
					if($dir_slider_auto==""){$dir_slider_auto='no';}
					
					$dir_tag_show=get_option('_dir_tag_show');	
					if($dir_tag_show==""){$dir_tag_show='no';}
					
				
					?>
					
					<h4><?php _e('Archive Page','ivdirectories');  ?> </h4>
					<hr>	
					<?php
					$dir_listing_sort=get_option('_dir_listing_sort');	
					if($dir_listing_sort==""){$dir_listing_sort='date';}
					?>
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('listings Order','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_listing_sort" id="dir_listing_sort" value='date' <?php echo ($dir_listing_sort=='date' ? 'checked':'' ); ?> ><?php _e('Published Date','medico');  ?>
							</label>	
						</div>
						<div class="col-md-2">	
							<label>											
							<input type="radio"  name="dir_listing_sort" id="dir_listing_sort" value='ASC' <?php echo ($dir_listing_sort=='ASC' ? 'checked':'' );  ?> > <?php _e('Alphabetically A-Z','medico');  ?>
							</label>
						</div>
						<div class="col-md-2">	
							<label>											
							<input type="radio"  name="dir_listing_sort" id="dir_listing_sort" value='DESC' <?php echo ($dir_listing_sort=='DESC' ? 'checked':'' );  ?> > <?php _e('Alphabetically Z-A','medico');  ?>
							</label>
						</div>
							
					</div>
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('All listings','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_load_listing_all" id="dir_load_listing_all" value='yes' <?php echo ($dir_load_listing_all=='yes' ? 'checked':'' ); ?> ><?php _e('Single Page [No Paging]','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_load_listing_all" id="dir_load_listing_all" value='no' <?php echo ($dir_load_listing_all=='no' ? 'checked':'' );  ?> > <?php _e('Listing with paging','medico');  ?>
							</label>
						</div>	
					</div>
					
				
					
						<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Listing Page toggle button[Search + Map] ','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="search_button_show" id="search_button_show" value='yes' <?php echo ($search_button_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="search_button_show" id="search_button_show" value='no' <?php echo ($search_button_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide','medico');  ?>
							</label>
						</div>	
					</div>
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Listing Page Top Map','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_map_show" id="dir_map_show" value='yes' <?php echo ($dir_map_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show  Top Map','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_map_show" id="dir_map_show" value='no' <?php echo ($dir_map_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Top Map','medico');  ?>
							</label>
						</div>	
					</div>
					<?php
					$dir_top_filter=get_option('_dir_top_filter');
					if($dir_top_filter==""){$dir_top_filter='yes';}
					?>
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Listing Filter option','medico');  ?></label>
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_top_filter" id="dir_top_filter" value='yes' <?php echo ($dir_top_filter=='yes' ? 'checked':'' ); ?> ><?php _e('Show Filter','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_top_filter" id="dir_top_filter" value='no' <?php echo ($dir_top_filter=='no' ? 'checked':'' );  ?> > <?php _e('Hide Filter','medico');  ?>
							</label>
						</div>	
					</div>	
										
										
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Listing Page Search Bar','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_searchbar_show" id="dir_searchbar_show" value='yes' <?php echo ($dir_searchbar_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show  Search Bar','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_searchbar_show" id="dir_searchbar_show" value='no' <?php echo ($dir_searchbar_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Search Bar','medico');  ?>
							</label>
						</div>	
					</div>
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Map Zoom','medico');  ?></label>
					<?php
						
						$dir_map_zoom=get_option('_dir_map_zoom');	
						if($dir_map_zoom==""){$dir_map_zoom='7';}	
					?>
						<div class="col-md-2">
							<label>												
							<input  type="input" name="dir_map_zoom" id="dir_map_zoom" value='<?php echo $dir_map_zoom; ?>' >
							</label>	
						</div>
						<div class="col-md-2">
							<label>												
							<?php _e('20 is more Zoom, 1 is less zoom','medico');  ?> 
							</label>	
						</div>
							
					</div>
					
					
					
					
					<h4><?php _e('Single Page','ivdirectories');  ?> </h4>
					<hr>	
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Social Profile','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_social_show" id="dir_social_show" value='yes' <?php echo ($dir_social_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Social Profile','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_social_show" id="dir_social_show" value='no' <?php echo ($dir_social_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Social Profile','medico');  ?>
							</label>
						</div>	
					</div>
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Slider Autoplay','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_slider_auto" id="dir_slider_auto" value='yes' <?php echo ($dir_slider_auto=='yes' ? 'checked':'' ); ?> ><?php _e('Slider Autoplay True','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_slider_auto" id="dir_slider_auto" value='no' <?php echo ($dir_slider_auto=='no' ? 'checked':'' );  ?> > <?php _e('Slider Autoplay False','medico');  ?>
							</label>
						</div>	
					</div>
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Tags','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_tag_show" id="dir_tag_show" value='yes' <?php echo ($dir_tag_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Tags','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_tag_show" id="dir_tag_show" value='no' <?php echo ($dir_tag_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Tags','medico');  ?>
							</label>
						</div>	
					</div>	
															
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Contact Us','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_contact_show" id="dir_contact_show" value='yes' <?php echo ($dir_contact_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Contact Us','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_contact_show" id="dir_contact_show" value='no' <?php echo ($dir_contact_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Contact Us','medico');  ?>
							</label>
						</div>	
					</div>

					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Claim','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_claim_show" id="dir_claim_show" value='yes' <?php echo ($dir_claim_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Claim','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_claim_show" id="dir_claim_show" value='no' <?php echo ($dir_claim_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Claim','medico');  ?>
							</label>
						</div>	
					</div>
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Comments','medico');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_comments_show" id="dir_comments_show" value='yes' <?php echo ($dir_comments_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Comments','medico');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_comments_show" id="dir_comments_show" value='no' <?php echo ($dir_comments_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Comments','medico');  ?>
							</label>
						</div>	
					</div>
					
					
					
					
					<h4><?php _e('Search Options','ivdirectories');  ?> </h4>
					<hr>
					<?php
					$dir_search_keyword=get_option('_dir_search_keyword');	
					if($dir_search_keyword==""){$dir_search_keyword='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Keyword','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_keyword" id="dir_search_keyword" value='yes' <?php echo ($dir_search_keyword=='yes' ? 'checked':'' ); ?> > Show Keyword  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_keyword" id="dir_search_keyword" value='no' <?php echo ($dir_search_keyword=='no' ? 'checked':'' );  ?> > Hide Keyword  
							</label>
						</div>	
					</div>
					
					<?php
					$dir_search_city=get_option('_dir_search_city');	
					if($dir_search_city==""){$dir_search_city='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('City','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_city" id="dir_search_city" value='yes' <?php echo ($dir_search_city=='yes' ? 'checked':'' ); ?> > Show City  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_city" id="dir_search_city" value='no' <?php echo ($dir_search_city=='no' ? 'checked':'' );  ?> > Hide City   
							</label>
						</div>	
					</div>
					
					<?php
					$dir_search_country=get_option('_dir_search_country');	
					if($dir_search_country==""){$dir_search_country='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Country','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_country" id="dir_search_country" value='yes' <?php echo ($dir_search_country=='yes' ? 'checked':'' ); ?> > Show Country  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_country" id="dir_search_country" value='no' <?php echo ($dir_search_country=='no' ? 'checked':'' );  ?> > Hide Country   
							</label>
						</div>	
					</div>
					
					
					<?php
					$dir_search_tag=get_option('_dir_search_tag');	
					if($dir_search_tag==""){$dir_search_tag='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Tags','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_tag" id="dir_search_tag" value='yes' <?php echo ($dir_search_tag=='yes' ? 'checked':'' ); ?> > Show Tags  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_tag" id="dir_search_tag" value='no' <?php echo ($dir_search_tag=='no' ? 'checked':'' );  ?> > Hide Tags   
							</label>
						</div>	
					</div>
					
					<?php
					$dir_search_category=get_option('_dir_search_category');	
					if($dir_search_category==""){$dir_search_category='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Category','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_category" id="dir_search_category" value='yes' <?php echo ($dir_search_category=='yes' ? 'checked':'' ); ?> > Show Category  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_category" id="dir_search_category" value='no' <?php echo ($dir_search_category=='no' ? 'checked':'' );  ?> > Hide Category   
							</label>
						</div>	
					</div>
					
					<?php
					 $dir_search_location=get_option('_dir_search_location');	
					if($dir_search_location==""){$dir_search_location='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Location','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_location" id="dir_search_location" value='yes' <?php echo ($dir_search_location=='yes' ? 'checked':'' ); ?> > Show Location  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_location" id="dir_search_location" value='no' <?php echo ($dir_search_location=='no' ? 'checked':'' );  ?> > Hide Location   
							</label>
						</div>	
					</div>
					
					<?php
					$dir_f_search_radius=get_option('_dir_f_search_radius');	
					if($dir_f_search_radius==""){$dir_f_search_radius='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Radius','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_f_search_radius" id="dir_f_search_radius" value='yes' <?php echo ($dir_f_search_radius=='yes' ? 'checked':'' ); ?> > Show Radius  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_f_search_radius" id="dir_f_search_radius" value='no' <?php echo ($dir_f_search_radius=='no' ? 'checked':'' );  ?> > Hide Radius   
							</label>
						</div>	
					</div>
					<?php
					$dir_search_zipcode=get_option('_dir_search_zipcode');	
					if($dir_search_zipcode==""){$dir_search_zipcode='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Zipcode','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_zipcode" id="dir_search_zipcode" value='yes' <?php echo ($dir_search_zipcode=='yes' ? 'checked':'' ); ?> > Show Zipcode  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_zipcode" id="dir_search_zipcode" value='no' <?php echo ($dir_search_zipcode=='no' ? 'checked':'' );  ?> > Hide Zipcode   
							</label>
						</div>	
					</div>
					
					
					
					<h4><?php _e('Other Options','ivdirectories');  ?> </h4>
					<hr>
					
					
					<?php
					$dir_color=get_option('_dir_color');	
					if($dir_color==""){$dir_color='#0099fe';}
					$dir_color=str_replace('#','',$dir_color);
					?>
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Plugin Color','ivdirectories');  ?></label>
					
					<div class="col-md-6">
							<label>												
							<input type="color" name="dir_color" id="dir_color" value='#<?php echo $dir_color;?>' > 
							</label>	
						</div>
						
					</div>
					
							
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Directory Publish By User','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_approve_publish" id="dir_approve_publish" value='yes' <?php echo ($dir_approve_publish=='yes' ? 'checked':'' ); ?> > Admin Will Approve  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_approve_publish" id="dir_approve_publish" value='no' <?php echo ($dir_approve_publish=='no' ? 'checked':'' );  ?> > User Can Publish
							</label>
						</div>	
					</div>
					
					<?php
					$dir_search_redius=get_option('_dir_search_redius');	
					if($dir_search_redius==""){$dir_search_redius='Km';}	
					?>
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Directory Radius','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_redius" id="dir_search_redius" value='Km' <?php echo ($dir_search_redius=='Km' ? 'checked':'' ); ?> > Km  
							</label>	
						</div>
						<div class="col-md-2">	
							<label>											
							<input type="radio"  name="dir_search_redius" id="dir_search_redius" value='Miles' <?php echo ($dir_search_redius=='Miles' ? 'checked':'' );  ?> > Miles
							</label>
						</div>	
					</div>
					
							<?php
					$dir_map_api=get_option('_dir_map_api');	
					if($dir_map_api==""){$dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';}	
					?>
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Google Map API Key','medical-directory');  ?></label>
					
							<div class="col-md-5">																		
								<input class="col-md-12" type="text" name="dir_map_api" id="dir_map_api" value='<?php echo $dir_map_api; ?>' >						
								
						</div>
						<div class="col-md-4">
							<label>												
							 <b> <a href="https://developers.google.com/maps/documentation/geocoding/get-api-key">Get your API key here </a></b>
							
							</label>	
						</div>				
					</div>
					
				
					
					
				<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Cron Job URL','ivdirectories');  ?>						 
					
					</label>
					
						<div class="col-md-6">
							<label>												
							 <b> <a href="<?php echo admin_url('admin-ajax.php'); ?>?action=iv_directories_cron_job"><?php echo admin_url('admin-ajax.php'); ?>?action=iv_directories_cron_job </a></b>
							
							</label>	
						</div>
						<div class="col-md-3">
							Cron JOB Detail : Hide Listing( Package setting),Subscription Remainder email.
						</div>		
							
					</div>
					
					
				
					<div class="form-group">
					<label  class="col-md-3 control-label"> </label>
					<div class="col-md-8">
						
						<button type="button" onclick="return  iv_update_dir_setting();" class="btn btn-success">Update</button>
					</div>
				</div>
						
			</form>
								

	
<script>

function iv_update_dir_setting(){
var search_params={
		"action"  : 	"iv_update_dir_setting",	
		"form_data":	jQuery("#directory_settings").serialize(), 
	};
	jQuery.ajax({					
		url : ajaxurl,					 
		dataType : "json",
		type : "post",
		data : search_params,
		success : function(response){
			jQuery('#update_message').html('<div class="alert alert-info alert-dismissable"><a class="panel-close close" data-dismiss="alert">x</a>'+response.msg +'.</div>');
			
		}
	})

}

	

</script>
