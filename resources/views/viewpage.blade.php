<!DOCTYPE html>

<html>

<head>

    <title>{{ $page->title }} | CollegeConnect.ph</title>

    <meta charset="utf-8">

    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}"/>  

    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}"/>  



    <script type="text/javascript" src="{{asset('js/count.js')}}"></script>     



</head>

<body>

    <div class="container-fluid">

     @include ('header')

  

    <div class="container layer1">

        <div class="row">

            <div class="post-content">

                <div class="post-container">

                    <h1>{{ $page->title }}</h1>

                    <p style="color:#888;">{{ date ('M-d-Y',strtotime($page->created_at)) }}</p>

                    <p><img src="{{asset('thumbnails/'.$page->thumbnail)}}" class="responsive"></p>

                    <p><?php echo html_entity_decode($page->content); ?></p>

                </div>

            </div>

        </div>

    </div>

           

                                

            @include ('footer')

        </div>

    </body>

</html>

