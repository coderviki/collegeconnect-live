<!DOCTYPE html>
<html>
<head>	
<title>CollegeConnect.ph | Colleges and Universities in the Philippines
</title>	
<meta charset="utf-8">        
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">        
<meta name="viewport" content="width=device-width, initial-scale=1">        
<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">        
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js">
</script>        
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">
</script>        
<script type="text/javascript" src="{{asset('js/ion.rangeSlider.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}"/>         
<link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}"/>         
<link rel="stylesheet" type="text/css" href="{{asset('css/ion.rangeSlider.min.css')}}"/> 
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.skinHTML5.min.css">        
<script type="text/javascript" src="{{asset('js/count.js')}}">
</script>  
</head>
<body>
		@include('header')	
<!-- if there is search query -->	
@if(isset($_REQUEST['searchItem']) && $_REQUEST['searchItem']<>'')	
<div class="container-fluid clear schools-page">		
<div class="row" style="width:100%;">			
<div class="col-md-12">				
<!--Adverts Placement-->			
</div>		
</div>		
<div class="row" style="width:100%;">		
<div class="school-wrapper" style="margin-left: 8.33333333%;height: 200px;">
              <!-- Tab links -->
            <div class="tab">
                <button class="tablinks" id="defaultOpen" onclick="openCity(event, 'Search by Course')">Search by Course</button>
                <button class="tablinks" onclick="openCity(event, 'Search by College')">Search by College</button>
            </div>

            <!-- Tab content -->
            <div style="width:100%;height: 130px;" id="Search by Course" class="tabcontent form-group col-md-6">
                <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                  <input style="width:25%;float:left;" class="form-control" type="text" name="searchItem" placeholder="School Course" required>
                  <input style="width:30%;float:left;" class="form-control" type="text" name="searchLocation" placeholder="Location" >
                   <div style="width:20%;" class="form-control form-group col-md-4">
                    <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
                        <option selected="selected" value="bachelorsdegree">Bachelors Degree</option>
                        <option value="mastersdegree">Masters Degree</option>
                        <option value="technicalvocational">Technical/Vocational</option>
                        <option value="certificateshortcourse">Certificate/Short Course</option>
                    </select>
                    </div>
               <div style="width:20%;background-color:transparent !important;border:0px;padding: 5px !important;" class="form-group col-md-2">
                    <input type="submit" class="btn btn-search" name="search" value="Search Courses">
                </div> 
                </form>
            </div>

            <div id="Search by College" style="width:100%;height: 130px;" class="tabcontent form-group col-md-6">
                <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                  <input style="width:25%;float:left;" class="form-control" type="text" name="searchItem" placeholder="School Name" required>
                  <input style="width:30%;float:left;" class="form-control" type="text" name="searchLocation" placeholder="Location">
                   <div style="width:20%;float:left;" class="form-control form-group col-md-4">
                    <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
                        <option selected="selected" value="schools">Schools</option>
                    </select>
                    </div>
                <div style="width:20%;float:left;background-color:transparent !important;border:0px;padding: 5px !important;" class="form-group col-md-2">
                    <input type="submit" class="btn btn-search" name="search" value="Search Colleges">
                </div>
                </form>
            </div>
</div>
<div class="col-md-2 col-md-offset-1 filter hide-me">				
<div class="filter-title">					
<h2>
<i class="fa fa-filter">
</i> Filter
</h2>				
</div>				
<div class="criteria-div">					
<form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">                        
<!--<div class="criteria">                            
<p class="criteria-title">School Type
</p>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="university">Public</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="polytechnic"> Polytechnic</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="monotechnic"> Monotechnic</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="college of education"> College of Education</label>                       
</div>       -->                                   
<div class="criteria">                            
<p class="criteria-title">Tuition Fees Range <a href="javascript:void(0);" onclick="$('input[name=FeesRange]').attr('checked',false);
" >clear</a></p>                       
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 10000") checked @endif value="0 - 10000">Under 10,000
</label>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "10000 - 20000") checked @endif value="10000 - 20000">10,000-20,000
</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "20000 - 30000") checked @endif value="20000 - 30000">20,000-30,000
</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "30000 - 40000") checked @endif value="30000 - 40000">30,000-40,000
</label>
<br/>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "40000 - 50000") checked @endif value="40000 - 50000">40,000-50,000
</label>
<br/>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "50000 - 100000") checked @endif value="50000 - 100000">50,000+
</label>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 100000") checked @endif value="0 - 100000">Any college (0-100,000)
</label>
</div>
<div class="criteria">                            
<p class="criteria-title">Tuition: Sort By</p>                            
<select name="sortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
    <option value="HightoLow" @if(isset($returnData['sortby']) && $returnData['sortby'] == "HightoLow")  @endif>Price High to Low</option>
    <option value="LowtoHigh" @if(isset($returnData['sortby']) && $returnData['sortby'] == "LowtoHigh")  @endif>Price Low to High</option>
</select>
</div>
<div class="criteria">                            
<p class="criteria-title">Philippines Academic Ranking: Sort By</p>                            
<select name="rankingsortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow")  @endif>Ranking High to Low</option>
    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh")  @endif>Ranking Low to High</option>
</select>
</div>
<div class="criteria">                            
<p class="criteria-title">Ownership</p>
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="Center of Excellence Award">Center of Excellence Award</label>
<br/>
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="ABET Accreditation">ABET Accreditation</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="federal">Public</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="private">Private</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="state">State</label>                       
</div>                                               
<input type="hidden" name="searchItem" value="{{$searchItem}}">                                            
<input type="hidden" name="criteria" value="{{ $criteria }}">                        
<input type="submit" class="btn btn-primary" value="Apply Filter">                     
</form>        				
</div>			
</div>			
<!--Show on mobile-->			
<div class="col-md-2 col-md-offset-1 filter hideShowDesktop" style="margin: 10px;">                
<div class="filter-title" >                    
<a href="#menu" data-toggle="collapse">
<h2 style="border:2px solid #3399ff;" class="btn btn-default dropdown-toggle">
<i class="fa fa-filter">
</i> Filter 
<span class="caret">
</span>
</h2>
</a>                
</div>                
<div class="criteria-div collapse"  id="menu">                    
<form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">                        
<!--<div class="criteria">                            
<p class="criteria-title">School Type
</p>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="university">Public</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="polytechnic"> Polytechnic</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="monotechnic"> Monotechnic</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="college of education"> College of Education</label>                       
</div>       -->                                   
<div class="criteria">                            
<p class="criteria-title">Tuition Fees Range <a href="javascript:void(0);" onclick="$('input[name=FeesRange]').attr('checked',false);
" >clear</a></p>                       
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 10000") checked @endif value="0 - 10000">Under 10,000
</label>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "10000 - 20000") checked @endif value="10000 - 20000">10,000-20,000
</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "20000 - 30000") checked @endif value="20000 - 30000">20,000-30,000
</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "30000 - 40000") checked @endif value="30000 - 40000">30,000-40,000
</label>
<br/>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "40000 - 50000") checked @endif value="40000 - 50000">40,000-50,000
</label>
<br/>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "50000 - 100000") checked @endif value="50000 - 100000">50,000+
</label>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 100000") checked @endif value="0 - 100000">Any college (0-100,000)
</label>
</div>
<div class="criteria">                            
<p class="criteria-title">Tuition: Sort By</p>                            
<select name="sortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
    <option value="HightoLow" @if(isset($returnData['sortby']) && $returnData['sortby'] == "HightoLow")  @endif>Price High to Low</option>
    <option value="LowtoHigh" @if(isset($returnData['sortby']) && $returnData['sortby'] == "LowtoHigh")  @endif>Price Low to High</option>
</select>
</div>
<div class="criteria">                            
<p class="criteria-title">Philippines Academic Ranking: Sort By</p>                            
<select name="rankingsortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow")  @endif>Ranking High to Low</option>
    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh")  @endif>Ranking Low to High</option>
</select>
</div>
<div class="criteria">                            
<p class="criteria-title">Ownership</p>
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="Center of Excellence Award">Center of Excellence Award</label>
<br/>
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="ABET Accreditation">ABET Accreditation</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="federal">Public</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="private">Private</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="state">State</label>                       
</div>                                              
<input type="hidden" name="searchItem" value="{{$searchItem}}">                                              
<input type="hidden" name="criteria" value="{{ $criteria }}">                                
<input type="submit" class="btn btn-primary" value="Apply Filter">                     
</form>                
</div>            
</div>            
<!--END OF FILTER SHOW OR HIDE-->			
<div class="col-md-7 school-listing">				
<h2><span style="color:orange;"> {{$count}} </span> Schools were found for {{$criteria}} in <span style="color:orange;"> {{$searchItem}}</span> in <span style="color:orange;">{{$searchLocation}}</span></h2>			
<div class="school">				@if(isset($msg))				
<div class="alert alert-danger">                    
<ul>                     	 
<li>
<h3>{{ $msg }}</h3>
</li>                    
</ul>                
</div>				@else				

@foreach($result as $sch)				
<div class="school-wrapper col-md-12">					
<div class="school-left col-md-4">						
<div class="school-name">							
<div class="school-icon">								
<img src="{{asset('images/'.$sch->logo)}}">							
</div>							
<div class="school-title">								
<a href="{{$sch->url}}">{{strtoupper($sch->name)}}</a>
<br>								
<span>{{$sch->location}}</span> 						
</div>							
<div class="school-image">								
<img src="{{asset('images/'.$sch->thumbnail)}}">							
</div>						
</div>					
</div>					
<div class="school-right col-md-8">												
<strong>Metro Manila Ranking:</strong> @if($sch->ranking_metro_manila)
    {{$sch->ranking_metro_manila}}
@else
<span>N/A</span>
@endif
<strong>Overall Ranking:</strong> @if($sch->ranking_philippines)
    {{$sch->ranking_philippines}}
@else
<span>N/A</span>
@endif    			
<p>{!! $sch->describtion !!}</p>						
<div class="score-card">							
<div class="score-div">								
<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Year Founded"></i>								
<p>Type</p>								
<p class="score">@if($sch->type)
    {{$sch->type}}
@else
<span>N/A</span>
@endif</p>							
</div>							
<div class="score-div">								
<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Nigeria"></i>								
<p>Entrance Exam</p>								
<p class="score">@if($sch->entrance_exam)
    {{$sch->entrance_exam}}
@else
<span>N/A</span>
@endif</p>							
</div>							
<div class="score-div">								
<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Africa">
</i>								
<p>Religious Affiliation</p>								
<p class="score">@if($sch->religious_affiliation)
    {{$sch->religious_affiliation}}
@else
<span>N/A</span>
@endif</p>							
</div>		
<div class="score-div">                             
<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Africa">
</i>                                
<p>Term Structure</p>                             
<p class="score">@if($sch->term_structure)
    {{$sch->term_structure}}
@else
<span>N/A</span>
@endif</p>                          
</div> 
<div class="score-div">                             
<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Africa">
</i>                                
<p>School Starts: </p>                             
<p class="score">@if($sch->school_year)
    {{$sch->school_year}}
@else
<span>N/A</span>
@endif</p>                          
</div>           					
<div class="score-div">								
<i class="fa fa-money" data-toggle="tooltip" data-placement="top" title="Average Fees in Nigerian Naira"></i>								
<p>Fees</p>								
<p class="score">PHP {{$sch->fees}}</p>							
</div>						
</div>					
</div>				
</div>
<div class="school-listing-action"> 
<a href="">Apply</a>                            
<a href="">View Courses</a>                     
</div>  
				@endforeach				@endif			
</div>			
</div>						
<div class="col-md-2 ads">							
</div>		
</div>		
</div>	
<!-- if search query is empty -->	@else		
<div class="container-fluid clear schools-page">		
<div class="row">			
<div class="col-md-12">				
<!--Adverts Placement-->			
</div>		
</div>		
<div class="row" style="width:100%;">

<div class="school-wrapper" style="margin-left: 8.33333333%;height: 200px;">
             <!-- Tab links -->
            <div class="tab">
                <button class="tablinks" id="defaultOpen" onclick="openCity(event, 'Search by Course')">Search by Course</button>
                <button class="tablinks" onclick="openCity(event, 'Search by College')">Search by College</button>
            </div>

            <!-- Tab content -->
            <div style="width:100%;height: 130px;" id="Search by Course" class="tabcontent form-group col-md-6">
                <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                  <input style="width:25%;float:left;" class="form-control" type="text" name="searchItem" placeholder="School Course" required>
                  <input style="width:30%;float:left;" class="form-control" type="text" name="searchLocation" placeholder="Location" >
                   <div style="width:20%;" class="form-control form-group col-md-4">
                    <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
                        <option selected="selected" value="bachelorsdegree">Bachelors Degree</option>
                        <option value="mastersdegree">Masters Degree</option>
                        <option value="technicalvocational">Technical/Vocational</option>
                        <option value="certificateshortcourse">Certificate/Short Course</option>
                    </select>
                    </div>
               <div style="width:20%;background-color:transparent !important;border:0px;padding: 5px !important;" class="form-group col-md-2">
                    <input type="submit" class="btn btn-search" name="search" value="Search Courses">
                </div> 
                </form>
            </div>

            <div id="Search by College" style="width:100%;height: 130px;" class="tabcontent form-group col-md-6">
                <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                  <input style="width:25%;float:left;" class="form-control" type="text" name="searchItem" placeholder="School Name" required>
                  <input style="width:30%;float:left;" class="form-control" type="text" name="searchLocation" placeholder="Location">
                   <div style="width:20%;float:left;" class="form-control form-group col-md-4">
                    <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
                        <option selected="selected" value="schools">Schools</option>
                    </select>
                    </div>
                <div style="width:20%;float:left;background-color:transparent !important;border:0px;padding: 5px !important;" class="form-group col-md-2">
                    <input type="submit" class="btn btn-search" name="search" value="Search Colleges">
                </div>
                </form>
            </div>
</div>

<div class="col-md-2 col-md-offset-1 filter hide-me">				
<div class="filter-title">					
<h2><i class="fa fa-filter"></i> Filter</h2>				
</div>				
<div class="criteria-div">					
<form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">                        
<!--<div class="criteria">                            
<p class="criteria-title">School Type
</p>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="university">Public</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="polytechnic"> Polytechnic</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="monotechnic"> Monotechnic</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="college of education"> College of Education</label>                       
</div>       -->                                   
<div class="criteria">                            
<p class="criteria-title">Tuition Fees Range <a href="javascript:void(0);" onclick="$('input[name=FeesRange]').attr('checked',false);
" >clear</a></p>                       
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 10000") checked @endif value="0 - 10000">Under 10,000
</label>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "10000 - 20000") checked @endif value="10000 - 20000">10,000-20,000
</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "20000 - 30000") checked @endif value="20000 - 30000">20,000-30,000
</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "30000 - 40000") checked @endif value="30000 - 40000">30,000-40,000
</label>
<br/>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "40000 - 50000") checked @endif value="40000 - 50000">40,000-50,000
</label>
<br/>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "50000 - 100000") checked @endif value="50000 - 100000">50,000+
</label>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 100000") checked @endif value="0 - 100000">Any college (0-100,000)
</label>
</div>
<div class="criteria">                            
<p class="criteria-title">Tuition: Sort By</p>                            
<select name="sortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
    <option value="HightoLow" @if(isset($returnData['sortby']) && $returnData['sortby'] == "HightoLow")  @endif>Price High to Low</option>
    <option value="LowtoHigh" @if(isset($returnData['sortby']) && $returnData['sortby'] == "LowtoHigh")  @endif>Price Low to High</option>
</select>
</div>
<div class="criteria">                            
<p class="criteria-title">Philippines Academic Ranking: Sort By</p>                            
<select name="rankingsortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow")  @endif>Ranking High to Low</option>
    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh")  @endif>Ranking Low to High</option>
</select>
</div>
<div class="criteria">                            
<p class="criteria-title">Ownership</p> 
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="Center of Excellence Award">Center of Excellence Award</label>
<br/>
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="federal">ABET Accreditation</label>
<br/>                              
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="ABET Accreditation">Public</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="private">Private</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="state">State</label>                       
</div>                                              
<input type="hidden" name="searchItem" value="{{$searchItem}}">                                            
<input type="hidden" name="criteria" value="{{ $criteria }}">                        
<input type="submit" class="btn btn-primary" value="Apply Filter">                     
</form>       		
</div>			
</div>			
<!--Show on mobile-->			
<div class="col-md-2 col-md-offset-1 filter hideShowDesktop" style="margin: 10px;">                
<div class="filter-title" >                    
<a href="#menu" data-toggle="collapse">
<h2 style="border:2px solid #3399ff;" class="btn btn-default dropdown-toggle">
<i class="fa fa-filter"></i> Filter 
<span class="caret">
</span>
</h2>
</a>                
</div>                
<div class="criteria-div collapse"  id="menu">                    
<form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">                        
<!--<div class="criteria">                            
<p class="criteria-title">School Type
</p>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="university">Public</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="polytechnic"> Polytechnic</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="monotechnic"> Monotechnic</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="School_type" value="college of education"> College of Education</label>                       
</div>       -->                                   
<div class="criteria">                            
<p class="criteria-title">Tuition Fees Range <a href="javascript:void(0);" onclick="$('input[name=FeesRange]').attr('checked',false);
" >clear</a></p>                       
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 10000") checked @endif value="0 - 10000">Under 10,000
</label>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "10000 - 20000") checked @endif value="10000 - 20000">10,000-20,000
</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "20000 - 30000") checked @endif value="20000 - 30000">20,000-30,000
</label>
<br/>                            
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "30000 - 40000") checked @endif value="30000 - 40000">30,000-40,000
</label>
<br/>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "40000 - 50000") checked @endif value="40000 - 50000">40,000-50,000
</label>
<br/>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "50000 - 100000") checked @endif value="50000 - 100000">50,000+
</label>
<label style="font-weight:normal;">
<input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 100000") checked @endif value="0 - 100000">Any college (0-100,000)
</label>
</div>
<div class="criteria">                            
<p class="criteria-title">Tuition: Sort By</p>                            
<select name="sortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
    <option value="HightoLow" @if(isset($returnData['sortby']) && $returnData['sortby'] == "HightoLow") selected @endif>Price High to Low</option>
    <option value="LowtoHigh" @if(isset($returnData['sortby']) && $returnData['sortby'] == "LowtoHigh") selected @endif>Price Low to High</option>
</select>
</div>
<div class="criteria">                            
<p class="criteria-title">Philippines Academic Ranking: Sort By</p>                            
<select name="rankingsortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow") selected @endif>Ranking High to Low</option>
    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh") selected @endif>Ranking Low to High</option>
</select>
</div>
<div class="criteria">                            
<p class="criteria-title">Ownership</p>
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="Center of Excellence Award">Center of Excellence Award</label>
<br/>
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="ABET Accreditation">ABET Accreditation</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="federal">Public</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="private">Private</label>
<br/>                            
<label style="font-weight:normal;">
<input type="checkbox" name="ownership[]" value="state">State</label>                       
</div>                                              
<input type="hidden" name="searchItem" value="{{$searchItem}}">                                        
<input type="hidden" name="criteria" value="{{ $criteria }}">                        
<input type="submit" class="btn btn-primary" value="Apply Filter">                     
</form>                
</div>            
</div>            
<!--END OF FILTER SHOW OR HIDE-->			
<div class="col-md-7 school-listing">				
<h2>SCHOOLS</h2>								
<div class="school">				@if(isset($msg))				
<h3 style="text-align:center;">{{$msg}} 
</h3>
				@else
                				@foreach($result as $sch)				
<div class="school-wrapper col-md-12">					
<div class="school-left col-md-4">						
<div class="school-name">							
<div class="school-icon">								
<img src="{{asset('images/'.$sch->logo)}}">							
</div>							
<div class="school-title">								
<a href="{{$sch->url}}">{{strtoupper($sch->name)}}</a>
<br>								
<span>{{$sch->location}}</span>							
</div>							
<div class="school-image">								
<img src="{{asset('images/'.$sch->thumbnail)}}">							
</div>						
</div>					
</div>					
<div class="school-right col-md-8">                                             
<strong>Metro Manila Ranking:</strong> @if($sch->ranking_metro_manila)
    {{$sch->ranking_metro_manila}}
@else
<span>N/A</span>
@endif
<strong>Overall Ranking:</strong> @if($sch->ranking_philippines)
    {{$sch->ranking_philippines}}
@else
<span>N/A</span>
@endif                  
<p>{!! $sch->describtion !!}</p>                        
<div class="score-card">                            
<div class="score-div">                             
<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Year Founded"></i>                              
<p>Type</p>                             
<p class="score">@if($sch->type)
    {{$sch->type}}
@else
<span>N/A</span>
@endif</p>                          
</div>                          
<div class="score-div">                             
<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Nigeria"></i>                               
<p>Entrance Exam</p>                                
<p class="score">@if($sch->entrance_exam)
    {{$sch->entrance_exam}}
@else
<span>N/A</span>
@endif</p>                          
</div>                          
<div class="score-div">                             
<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Africa">
</i>                                
<p>Religious Affiliation</p>                                
<p class="score">@if($sch->religious_affiliation)
    {{$sch->religious_affiliation}}
@else
<span>N/A</span>
@endif</p>                          
</div>      
<div class="score-div">                             
<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Africa">
</i>                                
<p>Term Structure</p>                             
<p class="score">@if($sch->term_structure)
    {{$sch->term_structure}}
@else
<span>N/A</span>
@endif</p>                          
</div> 
<div class="score-div">                             
<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Africa">
</i>                                
<p>School Starts: </p>                             
<p class="score">@if($sch->school_year)
    {{$sch->school_year}}
@else
<span>N/A</span>
@endif</p>                          
</div>                              
<div class="score-div">                             
<i class="fa fa-money" data-toggle="tooltip" data-placement="top" title="Average Fees in Nigerian Naira"></i>                               
<p>Fees</p>                           
<p class="score">PHP {{$sch->fees}}</p>                         
</div>  					
</div>					
</div>				
</div>
<div class="school-listing-action"> 
<a href="">Apply</a>                            
<a href="">View Courses</a>                     
</div>  
				@endforeach
                                @if(!isset($_REQUEST['fees'])) {{
$result->links()
}}
                                                                				@endif
    @endif
</div>			
</div>						
<div class="col-md-2 ads">							
</div>		
</div>		
</div>
	@endif	@include ('footer')
<script type="text/javascript">

    function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
} 
</script>   
                                                <script type="text/javascript">
            // Get the element with id="defaultOpen" and click on it
                document.getElementById("defaultOpen").click();
                </script>
</body>
</html>