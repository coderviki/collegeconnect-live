<!DOCTYPE html>
<html>
<head>
    <title>Find the Best Colleges in the Philippines | CollegeConnect.ph</title>
    <meta name="description" content="Having a hard time finding the best college or university for the course you want?  Look no further!  Find the best college near you in the Philippines for FREE!  Open to everyone.">
      @include ('meta')


      <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '127852161036059'); 
fbq('track', 'PageView');
fbq('track', 'CompleteRegistration');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=127852161036059&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
    <div class="container-fluid">
     @include ('header')

     <?php
     $redirectUrl = null;
     $searchItem = null;
     $searchLocation = null;
     if(isset($_GET['search'])){
        $searchItem = $_GET['searchItem'];
        $searchLocation = $_GET['searchLocation'];
        $redirect = $_GET['criteria'];
        switch ($redirect) {
           case 'schools':
           echo '<meta http-equiv="refresh" content="00000000;url=/schools?searchItem='.$searchItem.'$searchLocation='.$searchLocation.'">';
           break;
           case 'articles':
           echo '<meta http-equiv="refresh" content="0;url=/articles?searchItem='.$searchItem.'">';
           break;
           case 'courses':
           echo '<meta http-equiv="refresh" content="0;url=/courses?searchItem='.$searchItem.' ?searchLocation='.$searchLocation.'">';
           break;
           default:
           $redirectUrl = "";
           break;
       }
   }
   ?>
   <div class="container-fluid main-masthead">
    <div class="header-top">
        <div class="col-md-12">
            <h1 class="text-center" style="font-family: Montserrat,arial;text-shadow: 2px 2px 4px #000000;
    color: #ffffff;">Search for the Best College Near You!</h1>
        </div>
    </div>
    <div class="container"> 
        <div class="row col-md-12 sform">
    
             <!-- Tab links -->
			<div class="tab">
  				<button class="tablinks" id="defaultOpen" onclick="openCity(event, 'Search by Course')">Search by Course</button>
  				<button class="tablinks" onclick="openCity(event, 'Search by College')">Search by College</button>
			</div>

			<!-- Tab content -->
			<div style="width:100%;height: 130px;" id="Search by Course" class="tabcontent form-group col-md-6">
                <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                <div class="col-25">
 				  <input class="form-control" type="text" name="searchItem" placeholder="eg: Marketing" required>
                </div>
         <!--       <div class="col-30">
 				  <input class="form-control" type="text" name="searchLocation" placeholder="Location" >
                </div> -->
 				   <div class="col-md-4 col-20">
                    <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
						<option selected="selected" value="bachelorsdegree">Bachelors Degree</option>
                        <option value="mastersdegree">Masters Degree</option>
                        <option value="technicalvocational">Technical/Vocational</option>
                        <option value="certificateshortcourse">Certificate/Short Course</option>
					</select>
                	</div>
               <div class="form-group col-md-2 form-button">
                    <input type="submit" class="btn btn-search" name="search" value="Search Courses">
                </div> 
                </form>
			</div>

			<div id="Search by College" style="width:100%;height: 130px;" class="tabcontent form-group col-md-6">
                <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                <div class="col-25">
  				  <input class="form-control" type="text" name="searchItem" placeholder="eg: University of the Philippines" required>
                </div>
            <!--    <div class="col-30">
 				  <input class="form-control" type="text" name="searchLocation" placeholder="Location">
                </div> -->
 				   <div class="col-md-4 col-20">
                    <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
                        <option selected="selected" value="schools">Schools</option>
					</select>
                	</div>
                <div class="form-group col-md-2 form-button">
                    <input type="submit" class="btn btn-search" name="search" value="Search Colleges">
                </div>
                </form>
			</div>
                     <!--   <form action="{{ action("searchController@postsearch") }}" method="get" role="form" class="form-inline">
                <div class="form-group col-md-6">
                    <input class="form-control" type="text" name="searchItem" placeholder="School Name or Course" required>
                </div>
                <div class="form-group col-md-6">
                    <input class="form-control" type="text" name="searchLocation" placeholder="Location" required>
                </div>
                <div class="form-group col-md-4">
                    <select class="form-control" name="criteria" required>
                        <option value="" >Select Criteria</option>
                        <option value="schools">Schools</option>
                        <option selected="selected" value="courses">Courses</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="submit" class="btn btn-search" name="search">
                </div>
            </form> -->
        </div>
        <div class="row col-md-12 home-links">
        <h3 class="text-center" style="font-family: Montserrat,arial;text-shadow: 2px 2px 1px #000000;
    color: #ffffff;">For Working Professionals, Transferees, and High School Graduates</h1><br>
       <a href="/register">Sign Up for FREE!</a> <a href="/schools"><i class="fa fa-book"></i> View Schools</a>
       <!--<a href=""><i class="fa fa-pencil-square"></i>
        Exam</a>
        <a href=""><i class="fa fa-forumbee"></i>
            Forum</a>-->
        </div>
    </div>
    </div>
                <div class="container-fluid layer2">
                <div class="container">
                    <div>
                        <div class="col-md-12">
                                        <div class="container-fluid" style="margin: 10px;">
                                        <div class="container">
                                            <div>
                                                <h3 class="text-center" style="font-family: Montserrat,arial;color: #000000;font-weight: bolder;">
                                                    Search. Filter. Choose.</h3><br>
    <h4 class="text-center" style="font-family: Montserrat,arial;color: #000000;">We make it easier for you to choose the right college.</h4>
                                          <!--      <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-book icon-custom"></span>
                                                    <p class="count">200+</p>
                                                    <p class="count-label">Colleges and Universities</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-book icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Courses</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <i class="fa fa-book icon-custom"></i> 
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Articles</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-user icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Users</p>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    <div class="container layer1">
        <div class="row">
            <!-- <div class="col-md-4 latest">
                <div class="latest-wrapper">
                    <h2 class="title">Latest News</h2>
                    <div class="content">
                        <a href="" class="latest-heading">Top 15 Mistakes to Avoid in Choosing a College</a>
                        <img style="padding: 5px;width:100%" src="{{asset('images/test-image.jpg')}}" class="img-responsive">
                        <p class="latest-body">Afraid of choosing the wrong college? Avoid these 15 common blunders students often make in considers their college options.</p>
                        <div class="stats">
                            <span><i class="fa fa-eye"></i>
                                20</span>
                                <span><i class="fa fa-commenting"></i>
                                    20</span>
                                </div>
                            </div>

                        </div>
                    </div>-->
                    <div class="top-schools">
                        <h2 class="title">Top Performing Schools</h2>
                        @foreach($sch as $sch)
                        <div class="sch">
                            <div class="sch-content">
                                <div style="min-height:107px;">
                                    <img style="width:42px;padding:5px;" src="{{asset('images/'.$sch->logo)}}"><span style="font-size: 14pt;font-weight:bolder;float:right;">{{$sch->name}}</span><br><span style="font-size: 9pt;font-weight:bolder;float:right;color:grey;">{{$sch->location}}</span>
                                </div>
                                <div>
                                    <br>
                                    <img style="width:100%;" src="{{asset('images/'.$sch->thumbnail)}}"><br><br>
                                    <div class="school-listing-action"> 
                                        <a href="{{$sch->url}}">View Profile</a>
                                </div>         
                                    </div>  
                                    
                            </div>
                        </div>
                        @endforeach
                    </div>
                <!--    <div class="col-md-4 top-courses">
                        <h2 class="title">Popular Courses</h2>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square red-pos">1<sup>st</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">2<sup>nd</sup></span>

                        </div> -->
              <!--          <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">3<sup>rd</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">4<sup>th</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">5<sup>th</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">6<sup>th</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">7<sup>th</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">8<sup>th</sup></span>

                        </div>-->
                    </div>

                </div>
            </div>
            <div class="container-fluid layer2">
                <div class="container">
                    <div>
                        <div class="col-md-12">
                                        <div class="container-fluid" style="margin: 10px;">
                                        <div class="container">
                                            <div style="width:100%;">
                                                <div class="why-choose-us-left">
                                                <img src="https://collegeconnect.ph/images/why-choose-us.png">
                                                </div>
                                                <div class="why-choose-us-right">
                                                <h3 class="text-center" style="font-family: Montserrat,arial;color: #000000;font-weight: bolder;">Why use CollegeConnect?</h3><br>
                                                    <ul>
                                                        <li>Find schools near you that you may not have heard of.</li>
                                                        <li>Spend less time searching for the right school.</li>
                                                        <li>Narrow down your search results by things that matter such as board exam ranking, degree type, and location.</li>
                                                        <li>We are for everyone (not just high school graduates)</li>
                                                    </ul>
                                                </div>
                                          <!--      <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-book icon-custom"></span>
                                                    <p class="count">200+</p>
                                                    <p class="count-label">Colleges and Universities</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-book icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Courses</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <i class="fa fa-book icon-custom"></i> 
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Articles</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-user icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Users</p>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <div class="container-fluid layer2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title text-center">Articles</h1>
                                    @foreach($post as $post)
                                    <div class="col-md-4">
                                        <div class="content">
                                        <a href="{{ $post->url }}" class="latest-heading">{{substr(($post->title),0,88)}}..</a>
                                            <img style="padding: 5px; width:100%; height:218px;" src="{{asset('thumbnails/'.$post->thumbnail)}}" class="img-responsive">
                                        </div>
                                    </div>
                                   @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                      <!--              <div class="container-fluid tag-cloud">
                                        <div class="container ">
                                            <div class="row">
                                                <ul>
                                                    <li class="l2"><a href="">Applying for a postgraduate degree</a></li>
                                                    <li class="l1"><a href="">Making an Application </a></li>
                                                    <li class="l2"><a href="">Visas and Immigration</a></li>
                                                    <li class="l3"><a href="">Boarding Schools</a></li>
                                                    <li class="l1"><a href="">Ranking </a></li>
                                                    <li class="l2"><a href="">Choose a Subject</a></li>
                                                    <li class="l2"><a href="">Living in the UK </a></li>
                                                    <li class="l1"><a href="">Random</a></li>
                                                    <li class="l2"><a href="">Study in the UK</a></li>
                                                    <li class="l3"><a href="">University News</a></li>
                                                    <li class="l1"><a href="">Stories</a></li>
                                                    <li class="l2"><a href="">Living in the UK</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> -->

                                    @include ('footer')
                                </div>
                                   <script type="text/javascript">
    function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
} 
</script>   
                                   				<script type="text/javascript">
			// Get the element with id="defaultOpen" and click on it
				document.getElementById("defaultOpen").click();
				</script>
                            </body>
                            </html>
