            <footer class="container-fluid">

        <!--    <div class="container">

                <div class="row">

                    <div class="col-md-3">

                        <h2>SchoolDir</h2>

                        <ul>

                            <li><a href="">Link 1</a></li>

                            <li><a href="">Link 2</a></li>

                            <li><a href="">Link 3</a></li>

                            <li><a href="">Link 4</a></li>

                        </ul>

                    </div>

                    <div class="col-md-3">

                        <h2>Our Services</h2>

                        <ul>

                            <li><a href="">Link 1</a></li>

                            <li><a href="">Link 2</a></li>

                            <li><a href="">Link 3</a></li>

                            <li><a href="">Link 4</a></li>

                        </ul>

                    </div>

                    <div class="col-md-3">

                        <h2>Quick Links</h2>

                        <ul>

                            <li><a href="">Link 1</a></li>

                            <li><a href="">Link 2</a></li>

                            <li><a href="">Link 3</a></li>

                            <li><a href="">Link 4</a></li> 

                        </ul>

                    </div>

                    <div class="col-md-3">

                        <h2>Connections</h2>

                        <div class="icons-footer">

                            <a href=""><i class="fa fa-facebook-square"></i></a>

                            <a href=""><i class="fa fa-twitter-square"></i></a>

                            <a href=""><i class="fa fa-youtube-square"></i></a>

                            <a href=""><i class="fa fa-google-plus-square"></i></a>

                            <a href=""><i class="fa fa-linkedin-square"></i></a>

                        </div> 

                        @if(Session::has('message'))

                        <div id="message" style="background-color: #cfe5fa; padding:5px; color: #484848; margin:2px; text-align: center;">{{ Session::get('message') }}</div>

                        @endif

                        <form class="form-inline" id="subscribe" role="form" action="subscribe">

                        <div class="form-group">

                            <input class="form-control" name="email" type="email" placeholder="Valid Email Address" required>

                        </div>

                        <div class="form-group">

                            <input class="btn btn-warning" type="submit" value="Subscribe">

                        </div>

                    </form> -->

                    <!--

                    <script>

                    var form = document.getElementById('subscribe');

                    var request = XMLHttpRequest();



                    form.addEventListener('submit',function(e){

                        e.preventDefault();

                        var formdata = new formDate(form);

                        request.open('post', '/subscribe');

                        request.addEventListener("load", transferComplete);

                        request.send(formdata)

                    }); 

                    function transferComplete(data)

                    {

                        response JSON.parse(data.currentTarget.response);

                        if(response.sucess)

                        {

                            document.getElementById('message').innerHTML="Subscribed Successful";

                        }

                    }

                    </script> -->

     <!--               </div>

                </div>

            </div> -->

            <div class="row last-footer">

                    <div class="col-md-12">

                        <p class="text-center">&copy; {{date('Y')}} CollegeConnect.ph | <a href="">Terms and Conditions</a> | <a href="">Privacy Policy</a></p>

                        <p class="text-center">CollegeConnect.ph is an online marketplace that helps Filipinos and foreigners find the best colleges and universities in the Philippines.  CollegeConnect.ph believes that anyone, regardless of age, can and should be able to access high quality education.</p>

                    </div>

                </div>

        </footer>