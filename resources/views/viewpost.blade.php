<!DOCTYPE html>

<html>

<head>
    <title>{{ $post->title }} | CollegeConnect.ph</title>
 @include ('meta')   
</head>

<body>

    <div class="container-fluid">

     @include ('header')

  

    <div class="container layer1">

        <div class="row">

            <div class="post-content">

                <div class="post-container">

                    <h1>{{ $post->title }}</h1>

                    <p style="color:#888;">{{ date ('M-d-Y',strtotime($post->created_at)) }}</p>

           <!--         <p><img src="{{asset('thumbnails/'.$post->thumbnail)}}" class="responsive"></p> -->

                    <p><?php echo html_entity_decode($post->content); ?></p>

                </div>

            </div>

        </div>

    </div>

           

                                

            @include ('footer')

        </div>

    </body>

</html>

