<!DOCTYPE html>

<html>

<head>
    <title>SchoolDir</title>
@include ('meta')   
</head>

<body>

    <div class="container-fluid">

     @include ('header')

  

    <div class="container layer1">

        <div class="row">

            <div class="login-container">

                <div class="reg-content">

                    <h2 style="text-align:center;">SchoolDir-LOGIN</h2>

                    <form class="form-horizontal" role="form" method="post" action="{{ action("membersController@loginuser") }}">

                            <div class="" style="background-color:#fff; margin:30px 0px 0px 20px;">

                                

                                <label for="email">Email: </label>

                                <div class="input-group">

                                    <div class="input-group-addon">

                                      <i class="fa fa-envelope-o"></i>

                                    </div>

                                <input class="form-control input" name="email" value="" type="email" required/>

                                </div>



                                <label for="password">password: </label>

                                <div class="input-group">

                                    <div class="input-group-addon">

                                      <i class="fa fa-key"></i>

                                    </div>

                                <input class="form-control input" name="password" value="" type="password" required/>

                                </div><br/>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <input type="submit" class="btn btn-primary" name="Submit" value="LOGIN" />

                            </div>

                        </form>

                </div>

            </div>

        </div>

    </div>

           

                                    <div class="container-fluid" style="margin: 10px;">

                                        <div class="container">

                                            <div class="row">

                                                <div class="col-sm-3 col-xs-6 count-div">

                                                    <span class="glyphicon glyphicon-book icon-custom"></span>

                                                    <p class="count">1000</p>

                                                    <p class="count-label">Schools</p>

                                                </div>

                                                <div class="col-sm-3 col-xs-6 count-div">

                                                    <span class="glyphicon glyphicon-book icon-custom"></span>

                                                    <p class="count">1000</p>

                                                    <p class="count-label">Courses</p>

                                                </div>

                                                <div class="col-sm-3 col-xs-6 count-div">

                                                    <i class="fa fa-book icon-custom"></i> 

                                                    <p class="count">1000</p>

                                                    <p class="count-label">Articles</p>

                                                </div>

                                                <div class="col-sm-3 col-xs-6 count-div">

                                                    <span class="glyphicon glyphicon-user icon-custom"></span>

                                                    <p class="count">1000</p>

                                                    <p class="count-label">Users</p>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    @include ('footer')

                                </div>

                                

                            </body>

                            </html>

