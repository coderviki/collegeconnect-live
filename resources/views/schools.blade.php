<!DOCTYPE html>
<html>
    <head>	
        <title>CollegeConnect.ph | Colleges and Universities in the Philippines</title>	
@include ('meta')   
<link href="css/mtree.css" rel="stylesheet">
<style>
.mtree-demo .mtree {
  background: #EEE;
  margin: 20px auto;
  max-width: 320px;
  border-radius: 3px;
}

.mtree-skin-selector {
  text-align: center;
  background: #EEE;
  padding: 10px 0 15px;
}

.mtree-skin-selector li {
  display: inline-block;
  float: none;
}

.mtree-skin-selector button {
  padding: 5px 10px;
  margin-bottom: 1px;
  background: #BBB;
}

.mtree-skin-selector button:hover { background: #999; }

.mtree-skin-selector button.active {
  background: #999;
  font-weight: bold;
}

.mtree-skin-selector button.csl.active { background: #FFC000; }
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    </head>
    <body>
        @include('header')	
        <!-- if there is search query -->	
        @if(isset($_REQUEST['searchItem']) && $_REQUEST['searchItem']<>'')
        <div class="container-fluid clear schools-page">		
            <div class="row" style="width:100%;">			
                <div class="col-md-12">				
                    <!--Adverts Placement-->			
                </div>		
            </div>		
            <div class="row" style="width:100%;">		
                <div class="school-wrapper-searchbox">
                    <!-- Tab links -->
                    <div class="tab">
                        <button class="tablinks" id="defaultOpen" onclick="opensearchLocation(event, 'Search by Course')">Search by Course</button>
                        <button class="tablinks" onclick="opensearchLocation(event, 'Search by College')">Search by College</button>
                    </div>

                    <!-- Tab content -->
                    <div style="width:100%;/*height: 130px;*/" id="Search by Course" class="tabcontent form-group col-md-6">
                        <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                            <div class="col-25">
                            <input class="form-control" type="text" name="searchItem" placeholder="eg: Marketing" required>
                             </div>
                        <!--   <div class="col-30">
                            <input class="form-control" type="text" name="searchLocation" placeholder="eg: Manila" >
                            </div> -->
                            <div class="col-md-4 col-20">
                                <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
                                    <option selected="selected" value="bachelorsdegree">Bachelors Degree</option>
                                    <option value="mastersdegree">Masters Degree</option>
                                    <option value="technicalvocational">Technical/Vocational</option>
                                    <option value="certificateshortcourse">Certificate/Short Course</option>
                                </select>
                            </div>
                            <div style="width:20%;background-color:transparent !important;border:0px;padding: 5px !important;" class="form-group col-md-2 form-button">
                                <input type="submit" class="btn btn-search" name="search" value="Search Courses">
                            </div> 
                        </form>
                    </div>

                    <div id="Search by College" style="width:100%;height: 130px;" class="tabcontent form-group col-md-6">
                        <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                            <div class="col-25">
                            <input class="form-control" type="text" name="searchItem" placeholder="eg: University of the Philippines" required>
                            </div>
                    <!--    <div class="col-30">
                            <input class="form-control" type="text" name="searchLocation" placeholder="eg: Manila">
                            </div> -->
                            <div class="col-md-4 col-20">
                                <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
                                    <option selected="selected" value="schools">Schools</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2 form-button">
                                <input type="submit" class="btn btn-search" name="search" value="Search Colleges">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-1 filter hide-me">				
                    <div class="filter-title">					
                        <h2>
                            <i class="fa fa-filter">
                            </i> Filter
                        </h2>				
                    </div>				
                    <div class="criteria-div">					
                        <form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">                        
                            <!--<div class="criteria">                            
                            <p class="criteria-title">School Type
                            </p>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="university">Public</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="polytechnic"> Polytechnic</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="monotechnic"> Monotechnic</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="college of education"> College of Education</label>                       
                            </div>       -->                                   
                            <div class="criteria">                            
                                <p class="criteria-title">Tuition Fees Range <a href="{{ URL::to('/') }}/schools">clear</a></p>                       
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 10000") checked @endif value="0 - 10000">Under 10,000
                                </label>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "10000 - 20000") checked @endif value="10000 - 20000">10,000-20,000
                                </label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "20000 - 30000") checked @endif value="20000 - 30000">20,000-30,000
                                </label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "30000 - 40000") checked @endif value="30000 - 40000">30,000-40,000
                                </label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "40000 - 50000") checked @endif value="40000 - 50000">40,000-50,000
                                </label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "50000 - 100000") checked @endif value="50000 - 100000">50,000+
                                </label>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 100000") checked @endif value="0 - 100000">Any college (0-100,000)
                                </label>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Tuition: Sort By</p>                            
                                <select name="sortby" class="tusortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
                                    <option value="">--Select--</option>
                                    <option value="HightoLow" @if(isset($returnData['sortby']) && $returnData['sortby'] == "HightoLow") selected  @endif>Price High to Low</option>
                                    <option value="LowtoHigh" @if(isset($returnData['sortby']) && $returnData['sortby'] == "LowtoHigh") selected @endif>Price Low to High</option>
                                </select>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Philippines Board Exam Ranking: Sort By <br><span style="font-size:8pt;">(some schools may not have board exam ranking)</span></p>                            
                                <select name="rankingsortby" class="rnsortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
                                    <option value="">--Select--</option>
                                    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh") selected @endif>Ranking - Lowest to Highest</option>
                                    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow") selected @endif>Ranking - Highest to Lowest</option>
                                </select>
                            </div>
                            <div class="criteria">
                            <p class="criteria-title">Location</p>
                           <input class="form-control" type="text" name="searchLocation" placeholder="eg: Manila" id="location" @if(isset($searchLocation) && !empty($searchLocation)) value="{{$searchLocation}}" @endif>
                           <strong>Popular Locations</strong><br>
                           <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Manila">Manila</label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Bonifacio Global City">Bonifacio Global City</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Makati">Makati</label>
                                <br/>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Status</p> 
                                <label style="font-weight:normal;">
                                    <input type="radio" name="centers_of_excellence" value="{{$searchItem}}">Center of Excellence Recognition</label>
                                <br/>
                          <!--     <label style="font-weight:normal;">
                                   <input type="checkbox" name="type" value="ABET">ABET (International) Accreditation</label>
                                <br/>                    -->          
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="Public">Public</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="Private">Private</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="State">State</label>                       
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Term</p> 
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Semester">Semester</label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Quarter">Quarter</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Trimester">Trimester</label>
                                <br/>                                                   
                            </div>                                             
                            <input type="hidden" name="searchItem" value="{{$searchItem}}">
                            <!--<input type="hidden" name="searchLocation" @if(isset($searchLocation) && !empty($searchLocation)) value="{{$searchLocation}}" @endif>-->
                            <input type="hidden" name="criteria" value="{{ $criteria }}">                        
                            <input type="submit" class="btn btn-primary" value="Apply Filter">                     
                        </form>        				
                    </div>			
                </div>			
                <!--Show on mobile-->			
                <div class="col-md-2 col-md-offset-1 filter hideShowDesktop" style="margin: 10px;">                
                    <div class="filter-title" >                    
                        <a href="#menu" data-toggle="collapse">
                            <h2 style="border:2px solid #3399ff;" class="btn btn-default dropdown-toggle">
                                <i class="fa fa-filter">
                                </i> Filter 
                                <span class="caret">
                                </span>
                            </h2>
                        </a>                
                    </div>                
                    <div class="criteria-div collapse"  id="menu">                    
                        <form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">                        
                            <!--<div class="criteria">                            
                            <p class="criteria-title">School Type
                            </p>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="university">Public</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="polytechnic"> Polytechnic</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="monotechnic"> Monotechnic</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="college of education"> College of Education</label>                       
                            </div>       -->                                   
                            <div class="criteria">                            
                                <p class="criteria-title">Tuition Fees Range <a href="{{ URL::to('/') }}/schools" >clear</a></p>                       
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 10000") checked @endif value="0 - 10000">Under 10,000
                                </label>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "10000 - 20000") checked @endif value="10000 - 20000">10,000-20,000
                                </label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "20000 - 30000") checked @endif value="20000 - 30000">20,000-30,000
                                </label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "30000 - 40000") checked @endif value="30000 - 40000">30,000-40,000
                                </label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "40000 - 50000") checked @endif value="40000 - 50000">40,000-50,000
                                </label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "50000 - 100000") checked @endif value="50000 - 100000">50,000+
                                </label>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 100000") checked @endif value="0 - 100000">Any college (0-100,000)
                                </label>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Tuition: Sort By</p>                            
                                <select name="sortby" class="tusortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
                                    <option value="">--Select--</option>
                                    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh") selected @endif>Ranking - Lowest to Highest</option>
                                    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow") selected @endif>Ranking - Highest to Lowest</option>
                                </select>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Philippines Board Exam Ranking: Sort By <br><span style="font-size:8pt;">(some schools may not have board exam ranking)</span></p>                            
                                <select name="rankingsortby" class="rnsortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
                                    <option value="">--Select--</option>
                                    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh") selected @endif>Ranking - Lowest to Highest</option>
                                    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow") selected @endif>Ranking - Highest to Lowest</option>
                                </select>
                            </div>
                            <div class="criteria">
                            <p class="criteria-title">Location</p>
                           <input class="form-control" type="text" name="searchLocation" placeholder="eg: Manila" id="location" @if(isset($searchLocation) && !empty($searchLocation)) value="{{$searchLocation}}" @endif>
                           <strong>Popular Locations</strong><br>
                           <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Manila">Manila</label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Bonifacio Global City">Bonifacio Global City</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Makati">Makati</label>
                                <br/>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Status</p> 
                                <label style="font-weight:normal;">
                                    <input type="radio" name="centers_of_excellence" value="{{$searchItem}}">Center of Excellence Recognition</label>
                                <br/>
                  <!--             <label style="font-weight:normal;">
                                    <input type="checkbox" name="type" value="ABET">ABET (International) Accreditation</label>
                                <br/>                 -->           
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="Public">Public</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="Private">Private</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="State">State</label>                       
                            </div>
                           <div class="criteria">                            
                                <p class="criteria-title">Term</p> 
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Semester">Semester</label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Quarter">Quarter</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Trimester">Trimester</label>
                                <br/>                                                   
                            </div>                                          
                            <input type="hidden" name="searchItem" value="{{$searchItem}}">
                            <!--<input type="hidden" name="searchLocation" @if(isset($searchLocation) && !empty($searchLocation)) value="{{$searchLocation}}" @endif>-->
                            <input type="hidden" name="criteria" value="{{ $criteria }}">                                
                            <input type="submit" class="btn btn-primary" value="Apply Filter">                     
                        </form>                
                    </div>            
                </div>            
                <!--END OF FILTER SHOW OR HIDE-->			
                <div class="col-md-7 school-listing">				
                    <h2><span style="color:orange;"> {{$count}} </span> Schools were found for {{$criteria}} in <span style="color:orange;"> {{$searchItem}}</span></h2>			
                    <div class="school">				@if(isset($msg))				
                        <div class="alert alert-danger">                    
                            <ul>                     	 
                                <li>
                                    <h3>{{ $msg }}</h3>
                                </li>                    
                            </ul>                
                        </div>				@else				

                        @foreach($result as $sch)				
                        <div class="school-wrapper-content col-md-12">					
                            <div class="school-left col-md-4">						
                                <div class="school-name">							
                                    <div class="school-icon">								
                                        <img src="{{asset('images/'.$sch->logo)}}">							
                                    </div>							
                                    <div class="school-title">								
                                        <a href="{{$sch->url}}">{{strtoupper($sch->name)}}</a>
                                        <br>								
                                        <span>{{$sch->location}}</span> 						
                                    </div>							
                                    <div class="school-image">								
                                        <img src="{{asset('images/'.$sch->thumbnail)}}">						
                                    </div><br>
                                    <div class="school-listing-action"> 
                                    <a href="{{$sch->url}}">View Profile</a>                                     
                                    </div>						
                                </div>					
                            </div>					
                            <div class="school-right col-md-8">												
                                <strong>Board Exam Ranking (Metro Manila):</strong> @if($sch->ranking_metro_manila)
                                {{$sch->ranking_metro_manila}}
                                @else
                                <span>N/A</span>
                                @endif
                                <strong>Board Exam Ranking (Overall):</strong> @if($sch->ranking_philippines)
                                {{$sch->ranking_philippines}}
                                @else
                                <span>N/A</span>
                                @endif    			
                                <p>{{ str_limit($sch->describtion, 300) }};
                                </p>						
                                <div class="score-card">							
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title=""></i> <span class="score"><strong>Type:</strong> @if($sch->type)
                                            {{$sch->type}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>							
                                    <div class="score-div">								
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title=""></i> <span class="score"><strong>Entrance Exam:</strong> @if($sch->entrance_exam)
                                            {{$sch->entrance_exam}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>							
                                    </div>							
                                    <div class="score-div">								
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="">
                                        </i> <span class="score"><strong>Religious Affiliation:</strong> @if($sch->religious_affiliation)
                                            {{$sch->religious_affiliation}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>							
                                    </div>		
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="">
                                        </i> <span class="score"><strong>Term Structure:</strong> @if($sch->term_structure)
                                            {{$sch->term_structure}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div> 
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="">
                                        </i> <span class="score"><strong>School Starts:</strong> @if($sch->school_year)
                                            {{$sch->school_year}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>           					
                                    <div class="score-div">								
                                        <i class="fa fa-money" data-toggle="tooltip" data-placement="top" title=""></i><span class="score"><strong>Fees:</strong> PHP {{$sch->fees}}</span>	
                                    </div>						
                                </div>					
                            </div>				
                        </div>  
                        @endforeach				@endif			
                    </div>			
                </div>						
                <div class="col-md-2 ads">							
                </div>		
            </div>		
        </div>	
        <!-- if search query is empty -->	@else		
        <div class="container-fluid clear schools-page">		
            <div class="row">			
                <div class="col-md-12">				
                    <!--Adverts Placement-->			
                </div>		
            </div>		
            <div class="row" style="width:100%;">

                <div class="school-wrapper-searchbox">
                    <!-- Tab links -->
                    <div class="tab">
                        <button class="tablinks" id="defaultOpen" onclick="opensearchLocation(event, 'Search by Course')">Search by Course</button>
                        <button class="tablinks" onclick="opensearchLocation(event, 'Search by College')">Search by College</button>
                    </div>

                    <!-- Tab content -->
                    <div style="width:100%;height: 130px;" id="Search by Course" class="tabcontent form-group col-md-6">
                        <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                            <div class="col-25">
                            <input class="form-control" type="text" name="searchItem" placeholder="eg: Marketing" required>
                            </div>
                         <!--  <div class="col-30">
                            <input class="form-control" type="text" name="searchLocation" placeholder="eg: Manila" >
                            </div> -->
                            <div  class="col-md-4  col-20">
                                <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
                                    <option selected="selected" value="bachelorsdegree">Bachelors Degree</option>
                                    <option value="mastersdegree">Masters Degree</option>
                                    <option value="technicalvocational">Technical/Vocational</option>
                                    <option value="certificateshortcourse">Certificate/Short Course</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2 form-button">
                                <input type="submit" class="btn btn-search" name="search" value="Search Courses">
                            </div> 
                        </form>
                    </div>

                    <div id="Search by College" style="width:100%;height: 130px;" class="tabcontent form-group col-md-6">
                        <form action="{{ action("searchController@postsearch") }}" method="get" role="form" style="padding-top: 3%;" class="form-inline">
                            <div class="col-25">
                            <input class="form-control" type="text" name="searchItem" placeholder="eg: University of the Philippines" required>
                            </div>
                    <!--   <div class="col-30">
                            <input class="form-control" type="text" name="searchLocation" placeholder="eg: Manila">
                            </div> -->
                            <div class="col-md-4 col-20">
                                <select style="height: 40px;background: #e6e6e6;width:100%;border: 0px solid #ccc;" class="" name="criteria" required>
                                    <option selected="selected" value="schools">Schools</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2 form-button">
                                <input type="submit" class="btn btn-search" name="search" value="Search Colleges">
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-md-2 col-md-offset-1 filter hide-me">
                    <div class="criteria-div">
                        <strong>Filter by Location</strong>

                        <ul class="mtree transit">

                          <li><a href="#">Metro Manila</a>
                            <ul>
                              <li><a href="#">Makati</a></li>
                              <li><a href="#">Manila</a></li>
                              <li><a href="#">Bonifacio Global City</a></li>
                              <li><a href="#">Pasay</a></li>
                              <li><a href="#">Quezon City</a></li>
                            </ul>
                          </li>
                        </ul>
                        <script src="js/mtree.js"></script>
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/0.2.1/jquery.velocity.min.js"></script> 
                        <script>
                        $(document).ready(function() {
                          var mtree = $('ul.mtree');
                          
                          // Skin selector for demo
                          mtree.wrap('<div class=mtree-demo></div>');
                          var skins = ['bubba','skinny','transit','jet','nix'];
                          mtree.addClass(skins[0]);
                          $('body').prepend('<div class="mtree-skin-selector"><ul class="button-group radius"></ul></div>');
                          var s = $('.mtree-skin-selector');
                          $.each(skins, function(index, val) {
                            s.find('ul').append('<li><button class="small skin">' + val + '</button></li>');
                          });
                          s.find('ul').append('<li><button class="small csl active">Close Same Level</button></li>');
                          s.find('button.skin').each(function(index){
                            $(this).on('click.mtree-skin-selector', function(){
                              s.find('button.skin.active').removeClass('active');
                              $(this).addClass('active');
                              mtree.removeClass(skins.join(' ')).addClass(skins[index]);
                            });
                          })
                          s.find('button:first').addClass('active');
                          s.find('.csl').on('click.mtree-close-same-level', function(){
                            $(this).toggleClass('active'); 
                          });
                        });
                        </script>
                    </div>

               <!--     <div class="filter-title">					
                        <h2><i class="fa fa-filter"></i> Filter</h2>				
                    </div>				
                    <div class="criteria-div">					
                        <form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">        -->                
                            <!--<div class="criteria">                            
                            <p class="criteria-title">School Type
                            </p>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="university">Public</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="polytechnic"> Polytechnic</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="monotechnic"> Monotechnic</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="college of education"> College of Education</label>                       
                            </div>       -->                                   
                  <!--          <div class="criteria">                            
                                <p class="criteria-title">Tuition Fees Range <a href="{{ URL::to('/') }}/schools" >clear</a></p>                       
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 10000") checked @endif value="0 - 10000">Under 10,000
                                </label>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "10000 - 20000") checked @endif value="10000 - 20000">10,000-20,000
                                </label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "20000 - 30000") checked @endif value="20000 - 30000">20,000-30,000
                                </label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "30000 - 40000") checked @endif value="30000 - 40000">30,000-40,000
                                </label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "40000 - 50000") checked @endif value="40000 - 50000">40,000-50,000
                                </label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "50000 - 100000") checked @endif value="50000 - 100000">50,000+
                                </label>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 100000") checked @endif value="0 - 100000">Any college (0-100,000)
                                </label>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Tuition: Sort By</p>                            
                                <select name="sortby" class="tusortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
                                    <option value="">--Select--</option>
                                    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh") selected @endif>Ranking - Lowest to Highest</option>
                                    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow") selected @endif>Ranking - Highest to Lowest</option>
                                </select>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Philippines Board Exam Ranking: Sort By <br><span style="font-size:8pt;">(some schools may not have board exam ranking)</span></p>                            
                                <select name="rankingsortby" class="rnsortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
                                    <option value="">--Select--</option>
                                    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh") selected @endif>Ranking - Lowest to Highest</option>
                                    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow") selected @endif>Ranking - Highest to Lowest</option>
                                </select>
                            </div>
                            <div class="criteria">
                            <p class="criteria-title">Location</p>
                           <input class="form-control" type="text" name="searchLocation" placeholder="eg: Manila" id="location" @if(isset($searchLocation) && !empty($searchLocation)) value="{{$searchLocation}}" @endif>
                           <strong>Popular Locations</strong><br>
                           <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Manila">Manila</label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Bonifacio Global City">Bonifacio Global City</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Makati">Makati</label>
                                <br/>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Status</p> 
                                <label style="font-weight:normal;">
                                    <input type="radio" name="centers_of_excellence" value="{{$searchItem}}">Center of Excellence Recognition</label>
                                <br/> -->
                              <!-- <label style="font-weight:normal;">
                                    <input type="checkbox" name="type" value="ABET">ABET (International) Accreditation</label>
                                <br/>                         -->    
                         <!--       <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="Public">Public</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="Private">Private</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="State">State</label>                       
                            </div> 
                                                        <div class="criteria">                            
                                <p class="criteria-title">Term</p> 
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Semester">Semester</label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Quarter">Quarter</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Trimester">Trimester</label>
                                <br/>                                                   
                            </div>                                              
                            <input type="hidden" name="searchItem" value="{{$searchItem}}"> -->
                        <!--    <input type="hidden" name="searchLocation" @if(isset($searchLocation) && !empty($searchLocation)) value="{{$searchLocation}}" @endif> -->
                   <!--         <input type="hidden" name="criteria" value="{{ $criteria }}">                        
                            <input type="submit" class="btn btn-primary" value="Apply Filter">                     
                        </form>       		
                    </div>		-->

                </div>
                <div class="col-md-7 school-listing">               
                    <h2>SCHOOLS</h2>                                
                    <div class="school">                @if(isset($msg))                
                        <h3 style="text-align:center;">{{$msg}} 
                        </h3>
                        @else
                        @foreach($result as $sch)               
                        <div class="school-wrapper-content col-md-12">                  
                            <div class="school-left col-md-4">                      
                                <div class="school-name">                           
                                    <div class="school-icon">                               
                                        <img src="{{asset('images/'.$sch->logo)}}">                         
                                    </div>                          
                                    <div class="school-title">                              
                                        <a href="{{$sch->url}}">{{strtoupper($sch->name)}}</a>
                                        <br>                                
                                        <span>{{$sch->location}}</span>                         
                                    </div>                          
                                    <div class="school-image">                              
                                        <img src="{{asset('images/'.$sch->thumbnail)}}">                            
                                    </div><br>  
                                    <div class="school-listing-action"> 
                                    <a href="{{$sch->url}}">View Profile</a>                                     
                                    </div>                      
                                </div>                  
                            </div>                  
                            <div class="school-right col-md-8">                                             
                                <strong>Board Exam Ranking (Metro Manila):</strong> @if($sch->ranking_metro_manila)
                                {{$sch->ranking_metro_manila}}
                                @else
                                <span>N/A</span>
                                @endif
                                <strong>Board Exam Ranking (Overall):</strong> @if($sch->ranking_philippines)
                                {{$sch->ranking_philippines}}
                                @else
                                <span>N/A</span>
                                @endif                  
                                <p>{{ str_limit($sch->describtion, 300) }}</p>                        
                                <div class="score-card">                            
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title=""></i> <span class="score"><strong>Type:</strong> @if($sch->type)
                                            {{$sch->type}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>                          
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title=""></i> <span class="score"><strong>Entrance Exam:</strong> @if($sch->entrance_exam)
                                            {{$sch->entrance_exam}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>                          
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="">
                                        </i> <span class="score"><strong>Religious Affiliation:</strong> @if($sch->religious_affiliation)
                                            {{$sch->religious_affiliation}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>      
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="">
                                        </i> <span class="score"><strong>Term Structure:</strong> @if($sch->term_structure)
                                            {{$sch->term_structure}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>  
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="">
                                        </i> <span class="score"><strong>School Starts:</strong> @if($sch->school_year)
                                            {{$sch->school_year}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>                             
                                    <div class="score-div">                             
                                        <i class="fa fa-money" data-toggle="tooltip" data-placement="top" title=""></i><span class="score"><strong>Fees:</strong> PHP {{$sch->fees}}</span>   
                                    </div>                      
                                </div>                  
                            </div>              
                        </div>
                        @endforeach
                        @if(!isset($_REQUEST['fees'])) {{
                            $result->appends($_GET)->links()
                        }}
                        @endif
                        @endif
                    </div>          
                </div>                      
                <div class="col-md-2 ads">                          
                </div>			
                <!--Show on mobile-->			
                <div class="col-md-2 col-md-offset-1 filter hideShowDesktop" style="margin: 10px;">                
                    <div class="filter-title" >                    
                        <a href="#menu" data-toggle="collapse">
                            <h2 style="border:2px solid #3399ff;" class="btn btn-default dropdown-toggle">
                                <i class="fa fa-filter"></i> Filter 
                                <span class="caret">
                                </span>
                            </h2>
                        </a>                
                    </div>                
                    <div class="criteria-div collapse"  id="menu">                    
                 <!--       <form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">      -->                  
                            <!--<div class="criteria">                            
                            <p class="criteria-title">School Type
                            </p>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="university">Public</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="polytechnic"> Polytechnic</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="monotechnic"> Monotechnic</label>
                            <br/>                            
                            <label style="font-weight:normal;">
                            <input type="radio" name="School_type" value="college of education"> College of Education</label>                       
                            </div>       -->                                   
                    <!--        <div class="criteria">                            
                                <p class="criteria-title">Tuition Fees Range <a href="{{ URL::to('/') }}/schools">clear</a></p>                       
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 10000") checked @endif value="0 - 10000">Under 10,000
                                </label>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "10000 - 20000") checked @endif value="10000 - 20000">10,000-20,000
                                </label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "20000 - 30000") checked @endif value="20000 - 30000">20,000-30,000
                                </label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "30000 - 40000") checked @endif value="30000 - 40000">30,000-40,000
                                </label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "40000 - 50000") checked @endif value="40000 - 50000">40,000-50,000
                                </label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "50000 - 100000") checked @endif value="50000 - 100000">50,000+
                                </label>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="FeesRange" @if(isset($returnData['FeesRange']) && $returnData['FeesRange'] == "0 - 100000") checked @endif value="0 - 100000">Any college (0-100,000)
                                </label>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Tuition: Sort By</p>                            
                                <select name="sortby" class="tusortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
                                    <option value="">--Select--</option>
                                    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh") selected @endif>Ranking - Lowest to Highest</option>
                                    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow") selected @endif>Ranking - Highest to Lowest</option>
                                </select>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Philippines Board Exam Ranking: Sort By <br><span style="font-size:8pt;">(some schools may not have board exam ranking)</span></p>                            
                                <select name="rankingsortby" class="rnsortby" style="height: 40px;background: #e6e6e6;width:100%;border: 1px solid #ccc;">
                                    <option value="">--Select--</option>
                                    <option value="LowtoHigh" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "LowtoHigh") selected @endif>Ranking - Lowest to Highest</option>
                                    <option value="HightoLow" @if(isset($returnData['rankingsortby']) && $returnData['rankingsortby'] == "HightoLow") selected @endif>Ranking - Highest to Lowest</option>
                            </div>
                            <div class="criteria">
                            <p class="criteria-title">Location</p>
                           <input class="form-control" type="text" name="searchLocation" placeholder="eg: Manila" id="location" @if(isset($searchLocation) && !empty($searchLocation)) value="{{$searchLocation}}" @endif>
                           <strong>Popular Locations</strong><br>
                           <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Manila">Manila</label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Bonifacio Global City">Bonifacio Global City</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="city" value="Makati">Makati</label>
                                <br/>
                            </div>
                            <div class="criteria">                            
                                <p class="criteria-title">Status</p> 
                                <label style="font-weight:normal;">
                                    <input type="radio" name="centers_of_excellence" value="{{$searchItem}}">Center of Excellence Recognition</label>
                                <br/> -->
                     <!--          <label style="font-weight:normal;">
                              <input type="checkbox" name="type" value="ABET">ABET (International) Accreditation</label>
                                <br/>          -->                  
                     <!--           <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="Public">Public</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="Private">Private</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="type" value="State">State</label>                       
                            </div> 
                             <div class="criteria">                            
                                <p class="criteria-title">Term</p> 
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Semester">Semester</label>
                                <br/>
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Quarter">Quarter</label>
                                <br/>                            
                                <label style="font-weight:normal;">
                                    <input type="radio" name="term_structure" value="Trimester">Trimester</label>
                                <br/>                                                   
                            </div>                                            
                            <input type="hidden" name="searchItem" value="{{$searchItem}}"> -->
                           <!-- <input type="hidden" name="searchLocation" @if(isset($searchLocation) && !empty($searchLocation)) value="{{$searchLocation}}" @endif>-->
                       <!--     <input type="hidden" name="criteria" value="{{ $criteria }}">                        
                            <input type="submit" class="btn btn-primary" value="Apply Filter">                     
                        </form>                
                    </div>            -->
                </div>            
                <!--END OF FILTER SHOW OR HIDE-->			
                <div class="col-md-7 school-listing">				
                    <h2>SCHOOLS</h2>								
                    <div class="school">				@if(isset($msg))				
                        <h3 style="text-align:center;">{{$msg}} 
                        </h3>
                        @else
                        @foreach($result as $sch)				
                        <div class="school-wrapper-content col-md-12">					
                            <div class="school-left col-md-4">						
                                <div class="school-name">							
                                    <div class="school-icon">								
                                        <img src="{{asset('images/'.$sch->logo)}}">							
                                    </div>							
                                    <div class="school-title">								
                                        <a href="{{$sch->url}}">{{strtoupper($sch->name)}}</a>
                                        <br>								
                                        <span>{{$sch->location}}</span>							
                                    </div>							
                                    <div class="school-image">								
                                        <img src="{{asset('images/'.$sch->thumbnail)}}">							
                                    </div><br>	
                                    <div class="school-listing-action"> 
                                    <a href="{{$sch->url}}">View Profile</a>                                     
                                    </div>  					
                                </div>					
                            </div>					
                            <div class="school-right col-md-8">                                             
                                <strong>Board Exam Ranking (Metro Manila):</strong> @if($sch->ranking_metro_manila)
                                {{$sch->ranking_metro_manila}}
                                @else
                                <span>N/A</span>
                                @endif
                                <strong>Board Exam Ranking (Overall):</strong> @if($sch->ranking_philippines)
                                {{$sch->ranking_philippines}}
                                @else
                                <span>N/A</span>
                                @endif                  
                                <p>{{ str_limit($sch->describtion, 300) }}</p>                        
                                <div class="score-card">                            
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title=""></i> <span class="score"><strong>Type:</strong> @if($sch->type)
                                            {{$sch->type}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>                          
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title=""></i> <span class="score"><strong>Entrance Exam:</strong> @if($sch->entrance_exam)
                                            {{$sch->entrance_exam}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>                          
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="">
                                        </i> <span class="score"><strong>Religious Affiliation:</strong> @if($sch->religious_affiliation)
                                            {{$sch->religious_affiliation}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>      
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="">
                                        </i> <span class="score"><strong>Term Structure:</strong> @if($sch->term_structure)
                                            {{$sch->term_structure}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>  
                                    <div class="score-div">                             
                                        <i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="">
                                        </i> <span class="score"><strong>School Starts:</strong> @if($sch->school_year)
                                            {{$sch->school_year}}
                                            @else
                                            <span>N/A</span>
                                            @endif</span>                          
                                    </div>                             
                                    <div class="score-div">                             
                                        <i class="fa fa-money" data-toggle="tooltip" data-placement="top" title=""></i><span class="score"><strong>Fees:</strong> PHP {{$sch->fees}}</span>   
                                    </div>  					
                                </div>					
                            </div>				
                        </div>
                        @endforeach
                        @if(!isset($_REQUEST['fees'])) {{
                            $result->appends($_GET)->links()
                        }}
                        @endif
                        @endif
                    </div>			
                </div>						
                <div class="col-md-2 ads">							
                </div>		
            </div>		
        </div>
        @endif	@include ('footer')
        <script type="text/javascript">                    
                    function opensearchLocation(evt, searchLocationName) {
                            // Declare all variables
                            var i, tabcontent, tablinks;
                            // Get all elements with class="tabcontent" and hide them
                            tabcontent = document.getElementsByClassName("tabcontent");
                            for (i = 0; i < tabcontent.length; i++) {
                                tabcontent[i].style.display = "none";
                            }

                            // Get all elements with class="tablinks" and remove the class "active"
                            tablinks = document.getElementsByClassName("tablinks");
                                    for (i = 0; i < tablinks.length; i++) {
                            tablinks[i].className = tablinks[i].className.replace(" active", "");
                            }

                            // Show the current tab, and add an "active" class to the button that opened the tab
                            document.getElementById(searchLocationName).style.display = "block";
                            evt.currentTarget.className += " active";
                    }
                    
                    jQuery(document).on('change','.tusortby', function(){
                       if(jQuery.trim(jQuery(this).val()) != ''){
                           jQuery('.rnsortby').val("");
                       } 
                    });
                    
                    jQuery(document).on('change','.rnsortby', function(){
                       if(jQuery.trim(jQuery(this).val()) != ''){
                           jQuery('.tusortby').val("");
                       } 
                    });
        </script>   
        <script type="text/javascript">
            document.getElementById("defaultOpen").click();        </script>
    </body>
</html>