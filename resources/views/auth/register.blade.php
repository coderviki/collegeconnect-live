@extends('layouts.app')

@section('content')
<?php
    $server="localhost";
    $user="root";
    $password="B@ctad89";
    $db="collegec_SchoolDir";

    $connect=mysqli_connect($server, $user, $password, $db);
    $sql="select name from schools";
    $check=mysqli_query($connect,$sql);
     ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><center><img src="http://collegeconnect.ph/images/CollegeConnect.png?resolution=1280,1"><br><br><h1>Register Now!</h1></center>
                    <br><br>
                <strong>Our Mission:</strong> To connect Filipinos to the best college anywhere in the Philippines.  We are open to anyone: high school graduates, working professionals, international students.  Education has no limits!<br><br>
                <h2>Use the form below to register and become part of the <span style="font-weight:bold;color:orange;">biggest online community for higher education</span> in the Philippines.</h2>
                <br><br>
                <h3>Find schools that are offering <span style="font-weight:bold;color:orange;">online courses, degree courses, certification courses, and much more!</span></h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                 <!--       <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Other Names</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="fname" value="{{ old('fname') }}">

                                @if ($errors->has('fname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                <!--        <div class="form-group">
                            <label class="col-md-4 control-label">Gender:</label>
                            <div class="col-md-6">
                                <select name="gender" class="form-control">
                                    <option selected="selected" value"">Select Gender</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <label class="col-md-4 control-label">School:</label>
                            <div class="col-md-6">
                                <select name="school" class="form-control">
                                    <option selected="selected" value"">Select School</option>
                                    <?php
                                    while($school=mysqli_fetch_array($check))
                                     {
                                     echo '<option>'.$school['name'].'</option>';   
                                     }
                                    ?>
                                </select>
                                School not listed? Enter it below:<br>
                                <input type="text" class="form-control" name="school" value="">
                            </div>
                        </div>

                 <div class="form-group" style="display:none;">
                            <label class="col-md-4 control-label">Level:</label>
                            <div class="col-md-6">
                                <select name="level" class="form-control">
                                    <option selected="selected" value"">Select Level</option>
                                    <option>400 Level</option>
                                    <option>300 Level</option>
                                    <option selected="selected">200 Level</option>
                                    <option>100Level</option>
                                    <option>HND 2</option>
                                    <option>HND 1</option>
                                    <option>ND 2</option>
                                    <option>ND 1</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
