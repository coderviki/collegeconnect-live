<!DOCTYPE html>

<html>

<head>
	<title>About</title>
@include ('meta')
</head>

<body>

	@include ('header')

		<div class="container-fluid clear">

			<div class="container articles-page">

				<div class="articles-title">

					<h1>About</h1>

				</div>				

				<div class="container">

					

				<div class="row col-md-12">

					<h1>About</h1>

					<p>CollegeConnect.ph is a Philippine-based EdTech startup in Makati, Metro Manila. Founder and CEO, Thomas Watson, came up with the idea in May of 2016 after spending several years in the Philippines studying in different universities and observing the educational system.</p>

					<p>The idea was simple: to create a solution that will help Filipinos, of any age, as well as foreigners find the best college or university anywhere in the Philippines.</p>

					<p>Born and raised in the US and then moving to the Philippines in December of 2010, he quickly realized the differences in culture. Most Filipinos saw a college degree as a one-way ticket to going abroad where, in their minds, success would be achieved. The passion for high quality education was barely existent and it not only affected the academia, but it affected society in general.</p>

					<h2>The Mission and Advocacy</h2>

					<p>CollegeConnect.ph's advocacy is simple: <strong>Every Filipino can and should have access to quality education.</strong>We believe a country is only as good as the people in it. We also believe that community-driven digital solutions is the key to innovation, change, and success. Filipinos have the capability to change and progress.</p>

					<p>Technology has the ability to empower, not only the people but, the society in general. We've seen how the simplest app can connect people to what they need when it comes to traveling, finding a place to stay, or even finding a helper to run simple errands. Why not create an app that empowers higher education?</p>
				</div>

				</div>

			</div>

			<div class="container-fluid newsletter-articles-page">

			<div class="row col-md-12">
			

			</div>

				

			</div>

		</div>

	@include ('footer')



</body>

</html>

