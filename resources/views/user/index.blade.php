<!DOCTYPE html>
<html>
<head>
    <title>CollegeConnect.ph-{{ Auth::user()->fullname }}</title>
    <meta charset="utf-8">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}"/>  
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}"/>  

    <script type="text/javascript" src="{{asset('js/count.js')}}"></script>     

</head>
<body id="profile-body">
    <div class="container-fluid">
     @include ('header')
     <?php
    $server="localhost";
    $user="root";
    $password="B@ctad89";
    $db="collegec_SchoolDir";

    $connect=mysqli_connect($server, $user, $password, $db);
    $sql="select name from schools";
    $check=mysqli_query($connect,$sql);
     ?>
    <div class="container layer1">
        <div class="row prof-content">
            <div class="col-lg-3 col-md-3" style="background:transparent; color:#484848; padding:0px;">
                @if(!empty(Auth::user()->photo))
                <img src="{{asset('images/'.Auth::user()->photo)}}" height="240px" width="240px" style="border-radius:5px; border:1px solid #edefed;"/><br/><br/>
                @else
                <img src="{{asset('images/defaultpic.jpg')}}" height="240px" width="240px" style="border-radius:5px; border:1px solid #edefed;"/><br/><br/>
                @endif
                <form style="background-color:#fff; width:85%; padding:10px;" class="form-inline" role="form" action="{{action('membersController@updatephoto')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{{Auth::user()->id}}">
                    <input type="file" style="width:100%;" name="photo" required>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
                    <span style="padding:9px; width:100%; background-color:#337ab7;" class="btn">
                        <i class="fa fa-user"></i> <input style="background-color:#337ab7; border:0px solid #fff;" type="submit" value="Change Photo">
                    </span>  <br/><br/> 
                    <strong><a data-toggle="tab" href="#menu1"><i class="fa fa-key"></i> Change Password</a></strong><br/> 
                </form>
            </div>

            <div class="col-lg-9 col-md-9">
                <div class="row">
                    <div class="profiletabs" style="border-radius:4px 4px; margin:0px 20px 0px 20px;">
                        <ul class="nav profilenav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#home"><i class="fa fa-user"></i> Profile</a></li>
                          <li><a data-toggle="tab" href="#menu1"><i class="fa fa-edit"></i> Update Profile</a></li>
                        </ul>
                    </div>

                     @if(Session::has('message'))
                        <div style="background-color: #cfe5fa; padding:5px 10px 15px 10px; color: #484848; margin:40px 20px 100px 20px; text-align: center;">
                            <h3>{{ Session::get('message') }}</h3>
                        </div>
                     @elseif(count($errors) > 0)
                        <div style="background-color: #cfe5fa; padding:5px 10px 15px 10px; color: #484848; margin:40px 20px 100px 20px; text-align: center;">
                            @foreach ($errors->all() as $error)
                                <span style="color:#cd7502;"><i class="fa fa-close"></i> {{ $error }}</span><br/>
                            @endforeach
                        </div>
                     @else
                     <div style="background-color: #cfe5fa; padding:5px 10px 15px 10px; color: #484848; margin:40px 20px 100px 20px; text-align: center;">
                        <h3><i class="fa fa-bell"></i> <strong>Welcome! {{ Auth::user()->name }} {{ Auth::user()->fullname }}</strong></h3>
                     </div>
                     @endif
                    
                    <div id="home" class="col-lg-12 col-md-12 tab-pane fade in active clear">
                        <div class="row" style="margin-top:-90px;">
                            <div class="col-lg-5 col-md-5" style="background-color:#fff; padding:0px; color:#484848; margin:10px 10px 10px 20px;">
                                <div style="text-align:center; padding: 10px; color:#484848; background-color: #edefed;"><i class="fa fa-user"></i> Profile Information</div>
                                <div style="padding:20px; color:#444; font-weight:bold;">
                                    <p>
                                        <li class="fa fa-envelope-o" style="color:#222;"></li> Email: {{ Auth::user()->email }}
                                    </p>
                                    <p>
                                        <li class="fa fa-phone" style="color:#222;"></li> Phone: {{ Auth::user()->phone }}
                                    </p>
                                    <p>
                                        <li class="fa fa-graduation-cap" style="color:#222;"></li> School: {{ Auth::user()->school }}
                                    </p>
                                    <p>
                                        <li class="fa fa-level-up" style="color:#222;"></li> Level: {{ Auth::user()->level }}
                                    </p>
                                    <p>
                                        <li class="fa fa-eye" style="color:#222;"></li> Address: {{ Auth::user()->home_address }}
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6" style="background-color:#fff; padding:0px; color:#484848; margin:10px;"> 
                                <div style="text-align:center; padding: 10px; color:#484848; background-color: #edefed;"><i class="fa fa-user"></i> About</div>
                                <div style="padding:20px; color:#444; font-weight:bold;">
                                    @if(!empty(Auth::user()->about))
                                    <div style="border-bottom:1px solid #d0d0d0; padding-bottom:6px;"><li class="fa fa-quote-left"></li> {{ Auth::user()->about }}</div>
                                    <span style="border-bottom:1px solid #d0d0d0; padding-bottom:6px;"></span>
                                    @else
                                    <p style="font-weight:normal; border-bottom:1px solid #d0d0d0; padding-bottom:6px;">Your Professional Summary Goes Here</p>
                                    @endif
                                    <p>
                                        <li class="fa fa-edit" style="color:#222;"></li> Skills: {{ Auth::user()->technical_skills }}
                                    </p>
                                    <p>
                                        <li class="fa fa-edit" style="color:#222;"></li> Hobbies: {{ Auth::user()->hobbies }}
                                    </p>
                                    <p>
                                        <li class="fa fa-quote-left" style="color:#222;"></li> Quote: {{ Auth::user()->quote }}
                                    </p>
                                    <p>
                                        <li class="fa fa-edit" style="color:#222;"></li> Nationality: {{ Auth::user()->nationality }}
                                    </p>
                                    <p>
                                        <li class="fa fa-edit" style="color:#222;"></li> State of Origin: {{ Auth::user()->state_of_origin }}
                                    </p>
                                    <p>
                                        <li class="fa fa-edit" style="color:#222;"></li> Home Town: {{ Auth::user()->hometown }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="menu1" class="col-lg-12 col-md-12 tab-pane fade">
                        <div class="row" style="margin-top:-320px;">
                            <div class="col-lg-7 col-md-7" style="background-color:#fff; padding:0px; color:#484848; margin:10px;"> 
                                <div style="text-align:center; padding: 10px; color:#484848; background-color: #edefed;"><i class="fa fa-edit"></i> Edit Profile Information</div>
                                <div style="padding:20px; color:#444; font-weight:bold;">
                                    <form class="form-horizontal" role="form" action="{{action('membersController@updateprofile')}}" method="post" enctype="multipart/form-data">
                                        <label>About: </label>
                                        <textarea class="form-control" col="5" name="about">{{ Auth::user()->about }}</textarea>
                                        
                                        <label>Phone: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-phone"></i>
                                            </div>
                                        <input class="form-control" name="phone" value="{{ Auth::user()->phone }}" type="text"/>
                                        </div>

                                        <label>Address: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-eye"></i>
                                            </div>
                                        <input class="form-control" name="address" value="{{ Auth::user()->home_address }}" type="text"/>
                                        </div>

                                        <label>Skills: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-edit"></i>
                                            </div>
                                        <input class="form-control" name="skills" value="{{ Auth::user()->technical_skills }}" type="text"/>
                                        </div>

                                        <label>Hobbies: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-edit"></i>
                                            </div>
                                        <input class="form-control" name="hobbies" value="{{ Auth::user()->hobbies }}" type="text"/>
                                        </div>

                                        <label>Quote: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-quote-left"></i>
                                            </div>
                                        <input class="form-control" name="quote" value="{{ Auth::user()->quote }}" type="text"/>
                                        </div>

                                        <label>Nationality: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-edit"></i>
                                            </div>
                                        <input class="form-control" name="nationality" value="{{ Auth::user()->nationality }}" type="text"/>
                                        </div>

                                        <label>State Of Origin: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-edit"></i>
                                            </div>
                                        <input class="form-control" name="state_of_origin" value="{{ Auth::user()->state_of_origin }}" type="text"/>
                                        </div>

                                        <label>Home Town: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-edit"></i>
                                            </div>
                                        <input class="form-control" name="home_town" value="{{ Auth::user()->hometown }}" type="text"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Gender:</label>
                                                <select name="gender" class="form-control" required>
                                                    <option selected="selected" value"">Select Gender</option>
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                </select>
                                        </div>

                                        <div class="form-group">
                                            <label>School:</label>
                                                <select name="school" class="form-control" required>
                                                    <option selected="selected" value"">Select School</option>
                                                    <?php
                                                    while($school=mysqli_fetch_array($check))
                                                    {
                                                     echo '<option>'.$school['name'].'</option>';   
                                                    }
                                                    ?>
                                                </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Level:</label>
                                                <select name="level" class="form-control" required>
                                                    <option selected="selected" value"">Select Level</option>
                                                    <option>400 Level</option>
                                                    <option>300 Level</option>
                                                    <option>200 Level</option>
                                                    <option>100Level</option>
                                                    <option>HND 2</option>
                                                    <option>HND 1</option>
                                                    <option>ND 2</option>
                                                    <option>ND 1</option>
                                                </select>
                                        </div><br/>
                                        
                                        <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                        <input type="submit" class="btn btn-primary" value="Submit" /> 
                                    </form>
                                </div>
                            </div>


                            <div class="col-lg-4 col-md-4" style="background-color:#fff; padding:0px; color:#484848; margin:10px 10px 10px 20px;">
                                <div style="text-align:center; padding: 10px; color:#484848; background-color: #edefed;"><i class="fa fa-key"></i> Change Password</div>
                                <div style="padding:20px; color:#444; font-weight:bold;">
                                    <form class="form-horizontal" role="form" action="{{action('membersController@updatepassword')}}" method="post" enctype="multipart/form-data">
                                        <label for="password">Old password: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-key"></i>
                                            </div>
                                        <input class="form-control input" name="old_password" value="" type="text" required/>
                                        </div>
                                        <label for="password">New password: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-key"></i>
                                            </div>
                                        <input class="form-control input" name="new_password" value="" type="password" required/>
                                        </div>
                                        <label for="password">Comfirm password: </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-key"></i>
                                            </div>
                                        <input class="form-control input" name="comfirm_password" value="" type="password" required/>
                                        </div><br/>
                                        <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                        <input type="hidden" name="current_password" value="{{Auth::user()->password}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                        <input type="submit" class="btn btn-primary" value="Submit" /> 
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
           
                                    
         @include ('footer')
         </div>
                                
    </body>
</html>
