<!DOCTYPE html>

<html>

<head>

	<title>Articles | CollegeConnect.ph</title>

	<meta charset="utf-8">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

        <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}"/> 

        <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}"/> 

        <script type="text/javascript" src="{{asset('js/count.js')}}"></script>  

</head>

<body>

	@include ('header')

		<div class="container-fluid clear">

			<div class="container articles-page">

				<div class="articles-title">

					<h1>ARTICLES</h1>

				</div>

				<div class="row hideShowDesktop">

					<form class="categories" role="form">

						<div class="form-group">

							<select class="form-control">

								<option selected>Categories</option>

							</select>

						</div>

					</form>

				</div>

				<div class="row cat-cloud hide-me">

					<a href="/articles"><button id="default">Show All</button></a>

					@foreach($cat as $cat)

					<?php $category=str_replace('articles/', '', $cat->url);?>

					<a href="{{$cat->url}}"><button>{{$cat->name}}</button></a>

					@endforeach

					<!-- <button>Scholarships</button>

					<button>My Stories</button>

					<button>Visas and Immigration</button>

					<button>Choose a Subjiect</button>

					<button>Living in Nigeria</button>

					<button>Visas and Immigration</button>

					<button>Choose a Subjiect</button>

					<button>Living in Nigeria</button>

					<button>Findng Accomodation</button>

					<button>Polytechnic News</button>

					<button>University News</button>

					<button>Scholarships</button>

					<button>My Stories</button>

					<button>Findng Accomodation</button>

					<button>Polytechnic News</button>

					<button>University News</button> -->

				</div>

				

				<div class="container">

					

				<div class="row col-md-12">

						@if(isset($_REQUEST['searchItem']) && $_REQUEST['searchItem']<>'')

						@if(isset($msg))

						<div class="alert alert-danger">

                             <ul>

                                <li><h3>{{ $msg }}</h3></li>

                             </ul>

                        </div>

						@else

						@foreach($result as $result)

						<div class="col-md-4">

                            <div class="content">

                            <a href="{{$result->url}}" class="latest-heading">{{substr(($result->title),0,88)}}..</a>

                                <img style="padding: 5px; width:100%; height:218px;" src="{{asset('thumbnails/'.$result->thumbnail)}}" class="img-responsive">

                            </div>

                        </div>

                        @endforeach

                        @endif

						@else

                        @foreach($post as $post)

                        <div class="col-md-4">

                            <div class="content">

                            <a href="{{$post->url}}" class="latest-heading">{{substr(($post->title),0,88)}}..</a>

                                <img style="padding: 5px; width:100%; height:218px;" src="{{asset('thumbnails/'.$post->thumbnail)}}" class="img-responsive">

                            </div>

                        </div>

                       @endforeach

                       @endif

				</div>

				</div>

			</div>

			<div class="container-fluid newsletter-articles-page">

			<div class="row col-md-12">

				<div class="col-md-7">

					<h1>Lorem ipsum dolor sit amet, consectetur </h1>

				</div>

				<div class="col-md-5">

					@if(Session::has('message'))

                        <div id="message" style="background-color: #cfe5fa; padding:5px; color: #484848; margin:2px; text-align: center;">{{ Session::get('message') }}</div>

                        @endif

					<form class="form-inline" role="form" action="{{action('PagesController@subscribe')}}">

						<div class="form-group">

							<input class="form-control" name="email" type="email" placeholder="Valid Email Address" required>

						</div>

						<div class="form-group">

							<input class="btn btn-warning" type="submit" value="Subscribe">

						</div>

				</form>

				</div>

				

			</div>

				

			</div>

		</div>

	@include ('footer')



</body>

</html>

