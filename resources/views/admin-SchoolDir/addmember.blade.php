@include("admin-SchoolDir/pagenav")
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Member Registration</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>
                        @endif
                         @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="post" action="{{ action("membersController@savemember") }}">
                            <div class="col-lg-5 col-md-5 col-sm-6" style="background-color:#fff; margin:30px 0px 0px 20px;">
                                
                                <label for="fname">Full Name: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                    </div>
                                <input class="form-control input" name="fname" value="" type="text" required/>
                                </div>
                    
                                <label for="email">Email: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-envelope-o"></i>
                                    </div>
                                <input class="form-control input" name="email" value="" type="email" required/>
                                </div>

                                <label for="password">password: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-key"></i>
                                    </div>
                                <input class="form-control input" name="password" value="" type="password" required/>
                                </div>

                                <label for="cpassword">Comfirm password: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-key"></i>
                                    </div>
                                    <input class="form-control input" name="cpassword" value="" type="password" required/><br/>
                                </div>
                            </div>  
                            <div class="col-lg-4 col-md-4 col-sm-5" style="background-color:#fff; margin:30px 0px 0px 20px;">
                                <label for="gender">Gender:</label><br/>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-question-circle"></i>
                                    </div>
                                    <select name="gender" class="form-control">
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                </div>

                                <label for="school">School:</label><br/>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-mortar-board"></i>
                                    </div>
                                    <select name="school" class="form-control">
                                        <option>UNILAG</option>
                                        <option>UNIBEN</option>
                                        <option>DSPZ</option>
                                    </select>
                                </div>

                                <label for="level">Level:</label><br/>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-level-up"></i>
                                    </div>
                                    <select name="level" class="form-control">
                                        <option>200 Level</option>
                                        <option>100Level</option>
                                        <option>HND 2</option>
                                    </select>
                                </div><br/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-primary" name="Submit" value="Add User" /><br/><br/>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>