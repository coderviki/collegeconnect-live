@include("admin-SchoolDir/pagenav");



        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">

                            Dashboard <small>Add Page</small>

                        </h1>

                        <ol class="breadcrumb">

                            <li class="active">

                                <i class="fa fa-dashboard"></i> Dashboard

                            </li>

                        </ol>

                    </div>

                </div>

                <!-- /.row -->



                <div class="row">

                    <div class="col-lg-12">

                        @if(Session::has('message'))

                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>

                        @endif

                        @if (count($errors) > 0)

                            <div class="alert alert-danger">

                                <ul>

                                    @foreach ($errors->all() as $error)

                                        <li>{{ $error }}</li>

                                    @endforeach

                                </ul>

                            </div>

                        @endif

                        <form class="form-horizontal" role="form" method="post" action="savepage" enctype="multipart/form-data">

                            <div class="row">

                            <div class="col-lg-8 col-md-8" style="padding:5px; margin-top:5px; background-color:#f0f0f0;">

                                <label for="title">Page Title: <span style="color:red;">*</span></label>

                                <input class="form-control input" name="title" value="" type="text" required />

                                <label for="info">Post Body: <span style="color:red;">*</span></label>

                                <textarea name="editor1" class="ckeditor form-control" rows="12"></textarea>

                                <label for="keywords">Keywords: </label>

                                <input value="" class="form-control input" name="keywords" type="text" /><br/>

                            </div>

                            <div class="clear fx">

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding:5px; background-color:#f0f0f0; margin-top:5px; float:right;">

                                <label for="category">Page Category:</label><br/>

                                <select name="category" class="form-control">

                                    <option>None</option>

                                    @foreach($cat as $cat)

                                    <option value="{{$cat->name}}">{{$cat->name}}</option>

                                    @endforeach

                                    

                                </select>

                                <label for="type">Post Type:</label><br/>

                                <select name="type" class="form-control">

                                    <option>News</option>

                                    <option>Article</option>

                                    <option>Page</option>

                                </select>

                                <label for="video">Add video url: </label>

                                <input class="form-control input" name="video" type="text" /><br/>

                                <label for="attachement1">Attach a file: <small style="color:#999;">(type: zip/rar and below 10mb)</small></label>

                                <input type="file" name="file1"/><br/>

                                <label for="snapshot">Snapshot / Thumbnail:</label>

                                <input type="file" name="thumbnail" /><br/>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <input type="submit" class="btn btn-primary" name="Submit" value="Publish" /><br/><br/>

                            </div>

                        </div>

                    </form>

                    </div>

                </div>

                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->

        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

</body>

</html>