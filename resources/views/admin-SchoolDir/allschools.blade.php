@include("admin-SchoolDir/pagenav")
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>SchoolDir Schools</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                        @if(Session::has('message'))
                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped" style="background-color:#fff;">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>School Name</th>
                                <th>School Describtion</th>
                                <th>Options</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($sch as $sch)
                              <tr>
                                <td>{{ $sch->id }}</td>
                                <td>{{ substr(($sch->name),0,70) }}...</td>
                                <td>{{ $sch->describtion }}</td>
                                <td>
                                    <a href="editschool/{{ $sch->id }}">Update</a>&nbsp;&nbsp;&nbsp;
                                    @if($sch->status <>'trashed')
                                    <a href="deleteschool/{{ $sch->id }}">Trash</a>
                                    @else
                                    <a href="restoreschool/{{ $sch->id }}">Restore</a>
                                    @endif
                                </td>
                              </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>