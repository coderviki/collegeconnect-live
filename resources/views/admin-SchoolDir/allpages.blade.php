@include("admin-SchoolDir/pagenav")

        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">

                            Dashboard <small>SchoolDir Pages</small>

                        </h1>

                        <ol class="breadcrumb">

                            <li class="active">

                                <i class="fa fa-dashboard"></i> Dashboard

                            </li>

                        </ol>

                        @if(Session::has('message'))

                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>

                        @endif

                    </div>

                </div>

                <!-- /.row -->



                <div class="row">

                    <div class="col-lg-12">

                        <table class="table table-striped" style="background-color:#fff;">

                            <thead>

                              <tr>

                                <th>ID</th>

                                <th>Title</th>

                                <th>Type</th>

                                <th>Category</th>

                                <th>school</th>

                                <th>Options</th>

                              </tr>

                            </thead>

                            <tbody>

                            @foreach($page as $page)

                              <tr>

                                <td>{{ $page->id }}</td>

                                <td>{{ substr(($page->title),0,70) }}...</td>

                                <td>{{ $page->page_type }}</td>

                                <td>{{ $page->category }}</td>

                                <td>{{ $page->school }}</td>

                                <td>

                                    <a href="editpage/{{ $page->id }}">Update</a>&nbsp;&nbsp;&nbsp;

                                    @if($page->page_status <>'trashed')

                                    <a href="deletepage/{{ $page->id }}">Trash</a>

                                    @else

                                    <a href="restorepage/{{ $page->id }}">Restore</a>

                                    @endif

                                </td>

                              </tr>

                            @endforeach

                            </tbody>

                        </table>

                    </div>

                </div>

                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->

        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

</body>

</html>