<html>
  <head> 
  <title>Save Excel file details to the database</title>
  </head>
  <body>
  	<?php
      error_reporting(E_ALL); 
      ini_set('display_errors', '1');
      ini_set('memory_limit', '1024M');
      require __DIR__ . '/library/php-excel-reader/excel_reader2.php';
      require __DIR__ . '/library/SpreadsheetReader.php';
      require('db_config.php');

      $filePath = __DIR__ . '/collegec_database_digitalocean.xls';

      $Reader = new SpreadsheetReader($filePath);
      $totalSheet = count($Reader->sheets());
      $success = $fail = "";
      $success_count = $fail_count = 0;

      echo "<br />Success! Data Inserted in dababase";
      //$html="<table border='1'>";

      /* For Loop for all sheets */
      for($i=0;$i<$totalSheet;$i++) {

        $Reader->ChangeSheet($i);

        foreach ($Reader as $Row) {
          //$html.="<tr>";

          $id = isset($Row[0]) ? $Row[0] : '';
          $name = isset($Row[1]) ? $Row[1] : '';
          $description = isset($Row[2]) ? $Row[2] : '';
          $email = isset($Row[3]) ? $Row[3] : '';
          $phone = isset($Row[4]) ? $Row[4] : '';
          $website = isset($Row[5]) ? $Row[5] : '';
          $location = isset($Row[6]) ? $Row[6] : '';
          $postgraduatecourses = isset($Row[7]) ? $Row[7] : '';
          $shortcoursesandcertifications = isset($Row[8]) ? $Row[8] : '';
          $technicalandvocationalcourses = isset($Row[9]) ? $Row[9] : '';
          $fees = isset($Row[10]) ? $Row[10] : '';
          $undergraduatecourses = isset($Row[11]) ? $Row[11] : '';
          
          $sch = new schools();
          $sname=$request['sname'];
          $find=array(",","."," ");
          $replace=array("","","-");
          $titleurl = str_replace($find,$replace, $sname);
          $url='schools/'.strtolower($titleurl);
          $logo=strtolower($titleurl) . '.jpg';

         // $html.="<td>".$id."</td>";
         // $html.="<td>".$name."</td>";
          //$html.="<td>".$description."</td>";
         // $html.="<td>".$email."</td>";
         // $html.="<td>".$phone."</td>";
         // $html.="<td>".$website."</td>";
         // $html.="<td>".$location."</td>";
         // $html.="<td>".$postgraduatecourses."</td>";
         // $html.="<td>".$shortcoursesandcertifications."</td>";
         // $html.="<td>".$technicalandvocationalcourses."</td>";
         // $html.="<td>".$fees."</td>";
          //$html.="<td>".$undergraduatecourses."</td>";
         // $html.="</tr>";

          $query = "INSERT INTO schools (
                id,
                name,
                describtion,
                keywords,
                url,
                status,
                location,
                school_type,
                fees,
                ownership,  
                thumbnail,
                logo,
                founded_at,
                courses,
                undergradcourses,
                postgradcourses,
                shortcoursesandcertifications,
                technicalandvocationalcourses,
                website,
                phone,
                email,
                entry_requirement,
                rating_nigeria,
                rating_africa,
                created_at,
                updated_at
            ) VALUES (
            '$id',
            '$name',
            '$description',
            '',
            '',
            '',
            '$location',
            '',
            '$fees',
            '',
            '',
            '',
            '',
            '',
            '$undergraduatecourses',
            '$postgraduatecourses',
            '$shortcoursesandcertifications',
            '$technicalandvocationalcourses',
            '$website',
            '$phone',
            '$email',
            '',
            '',
            '',
            '',
            ''
            )";

            //if ($mysqli->query($query) === TRUE) {
              //echo "<br> New record created successfully <br>";
            if($mysqli->query($query) == 1)
            {
              $query_two = "UPDATE schools SET
                id='$id',
                name='$name',
                describtion='$description',
                keywords='',
                url='$url',
                status='',
                location='$location',
                school_type='',
                fees='$fees',
                ownership='',  
                thumbnail='',
                logo='$logo',
                founded_at='',
                courses='',
                undergradcourses='$undergradcourses',
                postgradcourses='$postgradcourses',
                shortcoursesandcertifications='$shortcoursesandcertifications',
                technicalandvocationalcourses='$technicalandvocationalcourses',
                website='$website',
                phone='$phone',
                email='$email',
                entry_requirement='',
                rating_nigeria='',
                rating_africa='',
                created_at='',
                updated_at=''
            ";
            }
            elseif ($mysqli->query($query_two) == 1) {
                $success .= "<li>" . $name . "</li>";
                $success_count++;
            } else {
              //echo "Error: " . $query . "<br>" . $mysqli->error;
              $fail .= "<li>" . $name . "<br>" . $mysqli->error . "</li>";
              $fail_count++;
            }
         }
      }

      //$html.="</table>";

      //echo $html;
      echo $success_count . " inputted successfully:<br><ul>" . $success . "</ul>";
      echo $fail_count . " not inputted due to errors:<br><ul>" . $fail . "</ul>";


      $mysqli->close();
  	?>


  </body>
</html>