@include("admin-SchoolDir/pagenav")
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Edit Category</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>
                        @endif
                         @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="post" action="{{ action("categoriesController@updatecat") }}">
                            <div class="col-lg-6 col-md-6 col-sm-7" style="background-color:#fff; margin:30px 0px 0px 20px;">
                                
                                <label for="name">Title: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-edit"></i>
                                    </div>
                                <input class="form-control input" name="name" value="{{$cat->name}}" type="text" required/>
                                </div><br/>
                                <label>Description: </label>
                                <textarea class="form-control" rows="2" name="description">{{$cat->description}}</textarea><br/>
                               <input type="hidden" name="id" value="{{$cat->id}}">
                               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                               <input type="submit" class="btn btn-primary" name="Submit" value="Submit" /><br/>
                            </div>  
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>