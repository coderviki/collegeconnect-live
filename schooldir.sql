-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 05, 2017 at 05:32 PM
-- Server version: 5.6.33-cll-lve
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `joemenil_schooldir`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Scholarships', 'mfnmnnnvnjkf jbrfjbvj jbncjbvjmfnm', 'articles/category/scholarships', '', '2016-07-14', '2016-07-24'),
(2, 'Choose a Subject', 'ccjbfhjhjbdjjojorikhfhfj', 'articles/category/choose-a-subject', '', '2016-07-14', '2016-07-24'),
(3, 'Scholarships', 'dbhhgfugugrunjhvhbvh', 'articles/category/scholarships', 'trashed', '2016-07-14', '2016-07-24'),
(4, 'Scholarships', 'dbhhgfugugrumndk', 'articles/category/scholarships', 'trashed', '2016-07-14', '2016-07-24');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `school` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `level` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `marrital_status` varchar(255) NOT NULL,
  `home_address` varchar(255) NOT NULL,
  `school_address` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `state_of_origin` varchar(255) NOT NULL,
  `hometown` varchar(255) NOT NULL,
  `religious` varchar(255) NOT NULL,
  `complexity` varchar(255) NOT NULL,
  `place_of_birth` varchar(255) NOT NULL,
  `other_name` varchar(255) NOT NULL,
  `prof_summary` varchar(255) NOT NULL,
  `education_and_training` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `technical_skills` varchar(255) NOT NULL,
  `hobbies` varchar(255) NOT NULL,
  `quote` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `fullname`, `about`, `school`, `remember_token`, `level`, `age`, `marrital_status`, `home_address`, `school_address`, `gender`, `photo`, `phone`, `email`, `password`, `nationality`, `state_of_origin`, `hometown`, `religious`, `complexity`, `place_of_birth`, `other_name`, `prof_summary`, `education_and_training`, `experience`, `technical_skills`, `hobbies`, `quote`, `created_at`, `updated_at`, `status`) VALUES
(2, 'Amawhe', 'Joel Amawhe', 'You asked, Font Awesome delivers with 30 shiny new icons in version 4.6. Want to request new icons', 'UNILAG', 'TQBI29L2YUcq6LoHE91eKzRwGuKUv4nMswA2zI9q0G7829ZaehjfqPcv8QbZ', '200 Level', '', '', '', '', 'Male', '', '', 'joelamawhe@gmail.com', '$2y$10$hDnE.lkYI.zO7vlZ.8aKZuv4AwQnTfAzzU96lVNYm/pzyTm8V6b9q', '', '', '', '', '', '', '', '', '', '', '', '', '', '2016-06-26', '2017-01-08', ''),
(6, 'Riale', 'Freeborn p', '', 'UNIBEN', 'Biny259eAzbsQUD419tNwfJX69VypL6vzZWBkxykb2Q18SkgAHvhnujVZvyk', '200 Level', '', '', '', '', 'Male', '', '', 'fr@bjb.njj', '$2y$10$hGyfu3mniwVCsBNH2iluB.b0k59vJYQvYXl/xAftEHSDLnlyOzBAO', '', '', '', '', '', '', '', '', '', '', '', '', '', '2016-07-07', '2016-07-07', ''),
(7, 'Demo', 'Admin Demo acct.', 'You asked, Font Awesome delivers with 30 shiny new icons in version 4.6. Want to request new icons Font Awesome delivers with 30 shiny new icons', 'UNILAG', 'pZnRBNBYyBnyzRELB97mXQzz7CImCLqbEgu135htikNmp0OHTJTo2Ndg2jHF', '400 Level', '', '', 'No 1 Emidi Str. Ozoro', '', 'Male', 'me.jpg', '07063031900', 'demo@schooldir.com', '$2y$10$XgRN.0cMwxX9L6X0NcPLgOjbPmgGq.YrvOz8VZRtTh9pv3YDbYRlW', '', '', '', '', '', '', '', '', '', '', 'Web Developer, ', 'Singing, Coding', '', '2016-07-07', '2017-01-17', '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `post_type` varchar(255) NOT NULL,
  `post_status` varchar(255) NOT NULL,
  `comment_status` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `school` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title` (`title`,`content`,`keywords`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category`, `title`, `content`, `keywords`, `post_type`, `post_status`, `comment_status`, `thumbnail`, `video`, `url`, `attachment`, `school`, `created_at`, `updated_at`) VALUES
(1, 'Scholarships', 'Nigeria’s Team Humane makes it to Microsoft Imagine Cup World Finals Nigeria’s Team Humane makes it to Microsoft Imagine Cup World Finals', '&lt;p&gt;The journey to the Imagine Cup World Finals started way back in August, when students began forming their teams at schools all across the globe. Since then, they have poured their hearts and souls into their solutions and competed in dozens of National Finals, taking great projects from just an idea to competition-grade execution.The journey to the Imagine Cup World Finals started way back in August, when students began forming their teams at schools all across the globe. Since then, they have poured their hearts and souls into their solutions and competed in dozens of National Finals, taking great projects from just an idea to competition-grade execution.The journey to the Imagine Cup World Finals started way back in August, when students began forming their teams at schools all across the globe. Since then, they have poured their hearts and souls into their solutions and competed in dozens of National Finals, taking great projects from just an idea to competition-grade execution.&lt;/p&gt;\r\n', 'cjbbjbkbbm,bhgyh,jgyhgv,brbjg,brynamics', 'News', '', '', 'kapsys.PNG', '', 'articles/nigeria’s-team-humane-makes-it-to-microsoft-imagine-cup-world-finals-nigeria’s-team-humane-makes-it-to-microsoft-imagine-cup-world-finals', 'Peter.zip', 'UNIBEN', '2016-06-27', '2016-07-19'),
(2, 'Scholarships', 'cnbhbvgf emifinals where they were judged by a global panel of MVPs', '&lt;p&gt;At the World Finals, the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.At the World Finals, the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.At the World Finals, the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.&lt;/p&gt;\r\n', 'bvcgd,dbjd,bdhgy,ahgy,kjbhd,bfdy,RialkInBj,bhcc', 'News', '', '', 'prx_012.jpg', '', 'articles/cnbhbvgf-emifinals-where-they-were-judged-by-a-global-panel-of-mvps', 'session.zip', 'UNILAG', '2016-06-27', '2016-07-19'),
(3, 'Choose a Subject', 'Then, one team will go on to win the coveted Imagine Cup and the grand prize', '&lt;p&gt;the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.&lt;/p&gt;\r\n', 'ndjghrf,dnygr,djugy', 'Article', '', '', 'domainking3.PNG', '', 'articles/then-one-team-will-go-on-to-win-the-coveted-imagine-cup-and-the-grand-prize', '', 'UNILAG', '2016-06-27', '2016-07-19'),
(4, 'Scholarships', 'remaining teams will compete to be crowned as the champions in their respective categories', '&lt;p&gt;At the World Finals, the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.At the World Finals, the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.At the World Finals, the remaining teams will compete to be crowned as the champions in their respective categories and the chance to win $50,000. Then, one team will go on to win the coveted Imagine Cup and the grand prize, a private mentoring session with Microsoft&amp;rsquo;s Chief Executive Officer, Satya Nadella.&lt;/p&gt;\r\n', 'bjhbfg,dbhgf,dbhgvfg', 'Article', '', '', 'prx_006.jpg', '', 'articles/remaining-teams-will-compete-to-be-crowned-as-the-champions-in-their-respective-categories', '', 'UNILAG', '2016-06-27', '2016-07-19'),
(5, 'Scholarships', 'Prepared statements are very useful against SQL injections', '&lt;p&gt;Prepared statements are very useful against SQL injections, because parameter values, which are transmitted later using a different protocol, need not be correctly escaped. If the original statement template is not derived from external input, SQL injection cannot occur.Prepared statements are very useful against SQL injections, because parameter values, which are transmitted later using a different protocol, need not be correctly escaped. If the original statement template is not derived from external input, SQL injection cannot occur.Prepared statements are very useful against SQL injections, because parameter values, which are transmitted later using a different protocol, need not be correctly escaped. If the original statement template is not derived from external input, SQL injection cannot occur.&lt;/p&gt;\r\n', 'Prepared statements are very useful against SQL injections', 'Article', '', '', 'EduApp-AMIN-Addpost.PNG', '', 'articles/prepared-statements-are-very-useful-against-sql-injections', '', 'UNILAG', '2016-07-09', '2016-07-19'),
(6, 'Scholarships', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.&lt;/p&gt;\r\n', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.', 'Article', '', '', 'domainking3.PNG', '', 'articles/lorem-ipsum-dolor-sit-amet-consectetur-adipisicing-elit', '', 'UNIBEN', '2016-07-23', '2016-07-23'),
(7, 'Scholarships', 'dipisicing elit. Aliquid minima, quae quibusdam expedita pariatur', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.&lt;/p&gt;\r\n', 'dipisicing elit. Aliquid minima, quae quibusdam expedita pariatur', 'Article', '', '', 'domainking3.PNG', '', 'articles/dipisicing-elit-aliquid-minima-quae-quibusdam-expedita-pariatur', '', 'General', '2016-07-23', '2016-07-23');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE IF NOT EXISTS `schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `describtion` text NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `school_type` varchar(255) NOT NULL,
  `fees` varchar(255) NOT NULL,
  `ownership` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `founded_at` varchar(255) NOT NULL,
  `courses` text NOT NULL,
  `website` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `entry_requirement` varchar(255) NOT NULL,
  `rating_nigeria` varchar(255) NOT NULL,
  `rating_africa` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`,`describtion`),
  FULLTEXT KEY `name_2` (`name`,`describtion`),
  FULLTEXT KEY `name_3` (`name`,`describtion`),
  FULLTEXT KEY `name_4` (`name`,`describtion`,`school_type`,`location`,`fees`,`ownership`),
  FULLTEXT KEY `name_5` (`name`,`describtion`,`school_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `name`, `describtion`, `keywords`, `url`, `status`, `location`, `school_type`, `fees`, `ownership`, `thumbnail`, `logo`, `founded_at`, `courses`, `website`, `phone`, `email`, `entry_requirement`, `rating_nigeria`, `rating_africa`, `created_at`, `updated_at`) VALUES
(1, 'UNIBEN', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.', 'UNIBEN, hvyfy,hvtft', 'schools/uniben', '', 'Nigeria', 'University', '50000', 'State', 'test-image.jpg', 'unilag.jpg', '1993', 'mdbnbjfdd,dnjbj,djb', '', '', '', '<p>dnknjdcn nc nj</p>\r\n', '40', '20', '2016-07-06 18:50:25', '2016-07-19 08:21:43'),
(2, 'UNILAG', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa.', 'UNILAG,Hgyhdgf,dhgf,dvy', 'schools/unilag', '', 'Nigeria', 'University', '70000', 'Federal', 'test-image.jpg', 'unilag.jpg', '1987', 'jhfcgh,bhgfg,hgft', 'Unilagint.edu', '+99996567', 'unilagint@gmail.com', '<p>fnjnbjfbkjf,knfjjkf,kjnkf</p>\r\n', '50', '40', '2016-07-06 18:51:25', '2017-01-08 18:45:38'),
(3, 'UNIPORT', 'Lorem ipsum dolor sit ametconsectetur adipisicing university fsd xf university djxg uni u elit.Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque cu', 'UNIPORT,dbhg,sgyfy', 'schools/uniport', '', 'Nigeria', 'University', '50000', 'State', 'test-image.jpg', 'unilag.jpg', '1938', 'rnjnhjf.rjn', '', '', '', '<p>rjhiu</p>\r\n', '80', '80', '2016-07-06 18:51:58', '2016-07-19 08:23:03'),
(4, 'FUTO Owerri', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque culpa. unversity of oweri futo', 'unversity of owerri owerri', 'schools/futo-owerri', '', 'Nigeria', 'University', '40000', 'Federal', 'test-image.jpg', 'unilag.jpg', '1896', 'bfjff,dnjkjk,kmdkjnhi', '', '', '', '<p>enjbj</p>\r\n', '70', '60', '2016-07-08 21:30:44', '2017-01-08 18:48:50'),
(5, 'NEKEDI', 'jnbdjhfvghfvvhv Nekedi nekedi bjchudhy y u tfvfbjvjfnkvnhjkrv njbjvjbjdnbv', 'dbhjdfcc,dhugwy', 'schools/nekedi', '', 'Nigeria', 'Polytechnic', '30000', 'State', 'test-image.jpg', 'unilag.jpg', '1890', 'sdnmbmd.njfkjf.dnjjkn', '', '', '', '<p>dfnjjbjff,djjm</p>\r\n', '70', '60', '2016-07-09 18:11:03', '2017-01-08 18:48:16'),
(6, 'DSPZ', 'fjbj vhv  hvugfcvy gdyu g', 'fbhghf fhvhgv,DSPZ', 'schools/dspz', '', 'Nigeria', 'Polytechnic', '55000', 'State', 'test-image.jpg', 'unilag.jpg', '1881', 'CIT,SLT', '', '', '', '<p>dnjhbjedj hn hvcevdhc hvchv vchhbcjhe</p>\r\n', '50', '30', '2016-07-14 18:20:47', '2017-01-08 18:47:39'),
(7, 'UI', 'Lorem ipsum dolor sit ametconsectetur adipisicing university fsd xf university djxg uni u elit.Aliquid minima, quae quibusdam expedita pariatur non? Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque cu', 'rff,feff,dfe', 'schools/ui', '', 'Nigeria', 'University', '80000', 'Federal', 'test-image.jpg', 'unilag.jpg', '1899', 'vdfff,dnfjvf,dfnjdf,jhjdbjvh', '', '', '', '<p>&nbsp;Dicta autem amet repellat expedita reprehenderit qui et ut, voluptates consectetur quam tempore, cumque cu</p>\r\n', '60', '30', '2016-07-19 07:10:18', '2017-01-08 18:46:55'),
(8, 'Harvard University', 'All text here are for testing sake. not correct. Gmail plugins can make your email management work a snap. Learn which Gmail add-ons will help you work more ', 'Gmail plugins can make your email management work a snap. Learn which Gmail add-ons will help you work more ', 'schools/harvard-university', '', 'Nigeria', 'University', '800,000', 'Private', 'the-unilag-senate-building.jpg', 'unilag.jpg', '1995', 'jgjd,hdgyfy,sghfhtext,test,', 'harevvvbn.hjg', '+233249372042', 'admin@admin.com', '<p>Gmail plugins can make your email management work a snap. Learn which Gmail add-ons will help you work more Gmail plugins can make your email management work a snap. Learn which Gmail add-ons will help you work more Gmail plugins can make your email ma', '69', '56', '2017-01-10 13:40:19', '2017-01-10 13:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'cbjnk@kkj.dhbh', '2016-07-23', '2016-07-23'),
(2, 'bJBhkj@khjk.cjb', '2016-07-23', '2016-07-23'),
(3, 'fnjkjk@kbj.dbj', '2016-07-23', '2016-07-23'),
(4, 'dbjb@Jb.fjb', '2016-07-23', '2016-07-23'),
(5, 'dbjgj@Jgjb.dbh', '2016-07-23', '2016-07-23'),
(6, 'dnjk@kjh.djh', '2016-07-23', '2016-07-23'),
(7, 'rialekingsley@gmail.com', '2016-07-23', '2016-07-23'),
(8, 'kingstech2013@gmail.com', '2016-07-23', '2016-07-23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Riale', 'rialealex@gmail.com', '$2y$10$R.N9bKe/1Q1VBeB9xDli4OmJgabgQ.VUgmRiKrvi/VGNbNyKsEZJi', NULL, '2016-07-07 12:24:16', '2016-07-07 12:24:16'),
(2, 'John', 'john@example.com', 'Doe', NULL, NULL, NULL),
(4, 'Riale', 'jrialencvgh@example.com', 'brume', NULL, NULL, NULL),
(5, 'kin', 'vgg@Hh.dgy', 'br', NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
