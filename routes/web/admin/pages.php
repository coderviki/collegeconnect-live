<?php

Route::get('/', ['middleware' => 'auth', 'uses'=>'PagesController@adminpage']);
Route::get('subscriptions', ['middleware' => 'auth', 'uses'=>'PagesController@subscriptions']);
Route::get('sendmail/{id}', ['middleware' => 'auth', 'uses'=>'PagesController@sendmail']);