<?php

Route::get('allcategories', ['middleware' => 'auth', 'uses'=>'categoriesController@allcategories']);
Route::get('addcategory', ['middleware' => 'auth', 'uses'=>'categoriesController@addcategory']);
Route::get('deletecat/{id}', 'categoriesController@deletecat');
Route::get('restorecat/{id}', 'categoriesController@restorecat');
Route::post('updatecat', ['middleware' => 'auth', 'uses'=>'categoriesController@updatecat','as'=>'updatecat']);
Route::get('editcat/{id}', ['middleware' => 'auth', 'uses'=>'categoriesController@editcat','as'=>'editcat']);
Route::post('savecategory', 'categoriesController@savecategory');
