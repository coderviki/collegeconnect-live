<?php


Route::get('allposts', ['middleware' => 'auth', 'uses'=>'postsController@allposts']);
Route::get('addpost', ['middleware' => 'auth', 'uses'=>'postsController@addpost']);
Route::get('allpages', ['middleware' => 'auth', 'uses'=>'postsController@allpages']);
Route::get('addpage', ['middleware' => 'auth', 'uses'=>'postsController@addpage']);

Route::post('savepost', 'postsController@savepost');
Route::post('savepage', 'postsController@savepage');

Route::get('deletepost/{id}', 'postsController@deletepost');
Route::get('restorepost/{id}', 'postsController@restorepost');

Route::get('editpost/{id}', ['middleware' => 'auth', 'uses'=>'postsController@editpost','as'=>'editpost']);
Route::get('editpage/{id}', ['middleware' => 'auth', 'uses'=>'postsController@editpage','as'=>'editpage']);
Route::post('updatepost', ['middleware' => 'auth', 'uses'=>'postsController@updatepost','as'=>'updatepost']);
Route::post('updatepage', ['middleware' => 'auth', 'uses'=>'postsController@updatepage','as'=>'updatepage']);
Route::post('sendmail', ['middleware' => 'auth', 'uses'=>'postsController@sendmail','as'=>'sendmail']);