<?php

Route::get('addmember', ['middleware' => 'auth', 'uses'=>'membersController@addmember']);
Route::get('allmembers', ['middleware' => 'auth', 'uses'=>'membersController@allmembers']);
Route::get('deletemem/{id}', 'membersController@deletemember');
Route::get('restoremem/{id}', 'membersController@restoremember');
Route::post('savemember', 'membersController@savemember');
Route::post('updatemem', ['middleware' => 'auth', 'uses'=>'membersController@updatemem','as'=>'updatemem']);
Route::get('editmem/{id}', ['middleware' => 'auth', 'uses'=>'membersController@editmember','as'=>'editmember']);