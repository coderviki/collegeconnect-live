<?php

//Route::get('fileimport','schoolsController@fileimport');
Route::get('allschools', ['middleware' => 'auth', 'uses'=>'schoolsController@allschools']);
Route::get('addschool', ['middleware' => 'auth', 'uses'=>'schoolsController@addschool']);
Route::post('updateschool', ['middleware' => 'auth', 'uses'=>'schoolsController@updateschool','as'=>'updateschool']);
Route::get('editschool/{id}', ['middleware' => 'auth', 'uses'=>'schoolsController@editschool','as'=>'editschool']);
Route::post('saveschool', 'schoolsController@saveschool');
Route::get('deleteschool/{id}', 'schoolsController@deleteschool');
Route::get('restoreschool/{id}', 'schoolsController@restoreschool');