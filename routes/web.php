<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::post('loginuser', array('as' => 'loginuser', 'uses' => 'membersController@loginuser'));

Route::group(['middleware' => ['web']], function () {


    Route::get('/logout', 'Auth\LoginController@logout');

    /*
    Route::get('register','membersController@register');
    Route::get('login', ['uses'=>'membersController@login', 'as'=>'login']);
    */
    Route::get('/', 'PagesController@index');

    Route::post('user/updatephoto', 'membersController@updatephoto');
    Route::post('user/updatephoto', 'membersController@updatephoto');
    Route::post('user/updatepassword', 'membersController@updatepassword');
    Route::post('user/updateprofile', 'membersController@updateprofile');

    Route::get('articles','PagesController@posts');
    Route::get('about','PagesController@about');
    Route::get('contact','PagesController@contact');
    Route::get('articles/category/{category}','PagesController@postsort');
    Route::get('schools','PagesController@schools');
    Route::get('schools/{url}','schoolsController@viewschool');
    Route::get('schools/{url}/inquire','schoolsController@inquireschool');
    Route::get('subscribe', ['uses'=>'PagesController@subscribe']);

    //Route::get('user','membersController@user')

    Route::get('schools/university-lagos','PagesController@schoolInfo');
    Route::get('articles/{url}', 'postsController@viewpost');
    //Route::get('{url}', 'postsController@viewpage')->where('url', '/^admin-SchoolDir/');
    Route::get('search', 'searchController@postsearch');
    Route::get('sortsearch', 'searchController@sortsearch');
    Route::get('sortschools', 'PagesController@sortschools');



    Route::group(['prefix' => 'admin-SchoolDir'], function () {

        require __DIR__ . '/web/admin/posts.php';
        require __DIR__ . '/web/admin/schools.php';
        require __DIR__ . '/web/admin/categories.php';
        require __DIR__ . '/web/admin/pages.php';
        require __DIR__ . '/web/admin/members.php';

    });

});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/user', 'HomeController@user');
});
