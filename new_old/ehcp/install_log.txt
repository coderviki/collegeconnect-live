apt-get -y --no-remove --allow-unauthenticated install mariadb-server

apt-get -y --no-remove --allow-unauthenticated install mariadb-client

DEBIAN_FRONTEND=noninteractive apt-get -y --no-remove --allow-unauthenticated install php-mysqlnd

apt-get -y --no-remove --allow-unauthenticated install mc

apt-get -y --no-remove --allow-unauthenticated install lynx

apt-get -y --no-remove --allow-unauthenticated install nmap

apt-get -y --no-remove --allow-unauthenticated install unrar

apt-get -y --no-remove --allow-unauthenticated install rar

apt-get -y --no-remove --allow-unauthenticated install unzip

apt-get -y --no-remove --allow-unauthenticated install zip

apt-get -y --no-remove --allow-unauthenticated install openssh-server

apt-get -y --no-remove --allow-unauthenticated install python-mysqldb

apt-get -y --no-remove --allow-unauthenticated install python-cherrypy3

apt-get -y --no-remove --allow-unauthenticated install apache2

apt-get -y --no-remove --allow-unauthenticated install bind9

apt-get -y --no-remove --allow-unauthenticated install php-gd

apt-get -y --no-remove --allow-unauthenticated install libapache-mod-ssl

apt-get -y --no-remove --allow-unauthenticated install curl

