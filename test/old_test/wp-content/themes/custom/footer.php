<div class="footer">
	<div class="container">
		<div class="footer-main">
			<div class="col-md-3 ftr-grid wow zoomIn animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomIn;">
				<div class="ftr-logo">
				<img src="/assets/Uploads/CollegeConnect.png" width="155px" alt="">
				</div>
				<p>CollegeConnect.ph is a directory that allows you to find the best college or university anywhere in the Philippines.</p>
				<a href="single.html" class="ftr-btn">Read More</a>
			</div>
			<div class="col-md-3 ftr-grid ftr-mid wow zoomIn animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomIn;">
				 <h3>Address</h3>
				 <span class="ftr-line flm"> </span>
				<p></p>
				
			</div>
			<div class="col-md-3 ftr-grid ftr-rit wow zoomIn animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomIn;">
				 <h3>Contact Us</h3>
				 <form lpformnum="1">
				 	<input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
				 	<input type="submit" value="Send">
				 </form>
				  <ul class="ftr-icons">
				 	<li><a href="#"><span class="fa"> </span></a></li>
				 	<li><a href="#"><span class="tw"> </span></a></li>
				 	<li><a href="#"><span class="link"> </span></a></li>
				 	<li><a href="#"><span class="gmail"> </span></a></li>
				 </ul>
			</div>
			<div class="col-md-3 ftr-grid ftr-last-gd ftr-rit wow zoomIn animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomIn;">
				 <h3>Quick Link</h3>
				  <ul class="ftr-nav">
				 	<li><a href="index.html">Home</a></li>
				 	<li><a href="about.html">About</a></li>
				 	<li><a href="services.html">Services </a></li>
				 	<li><a href="room.html">Rooms</a></li>
				 	<li><a href="contact.html">Contact</a></li>
				 </ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>           
</div>

<!-- <footer class="footer" role="contentinfo">
    <div class="top">
        <div class="container">
            <% include FooterNavigation %>
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <span class="copyright">&copy;  2017</span>
        </div>
    </div>
</footer> -->

</body>
</html>