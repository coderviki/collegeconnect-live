<?php get_header(); ?>

<!-- Header with full-height image -->
<header class="bgimg-1 w3-display-container w3-grayscale-min" id="home">
  <div class="w3-display-left w3-padding-xxlarge w3-text-white">
    <span class="w3-jumbo w3-hide-small">Find the best college that fits you</span><br>
    <span class="w3-xxlarge w3-hide-large w3-hide-medium">Find the best college that fits you</span><br>
    <span class="w3-large">Hundreds of colleges to choose from</span>
    <p><a href="/colleges" class="w3-btn w3-white w3-padding-large w3-large w3-margin-top w3-opacity w3-hover-opacity-off">Find a college</a></p>
  </div> 
  <div class="w3-display-bottomleft w3-padding-xxlarge w3-text-grey w3-large">
    <a href="#" class="w3-hover-text-white"><i class="fa fa-facebook-official"></i></a>
    <a href="#" class="w3-hover-text-white"><i class="fa fa-flickr"></i></a>
    <a href="#" class="w3-hover-text-white"><i class="fa fa-instagram"></i></a>
    <a href="#" class="w3-hover-text-white"><i class="fa fa-twitter"></i></a>
    <a href="#" class="w3-hover-text-white"><i class="fa fa-linkedin"></i></a>
  </div>
</header>

<!--information start here-->
<div class="information">
	<div class="container">
		<div class="information-main">
			<div class="col-md-4 information-grid">
				<span class="info-icons hovicon effect-4 sub-b"><img src="/images/find-a-college.png" alt=""></span>
				<h4>Find a college faster</h4>
				<p>Our directory will help narrow down your options.</p>
			</div>
			<div class="col-md-4 information-grid">
				<span class="info-icons hovicon effect-4 sub-b"><img src="/images/search-in-seconds.png" alt=""></span>
				<h4>Turn hours of searching into seconds</h4>
				<p>Find in-depth detail on every college right here.</p>
			</div>
			<div class="col-md-4 information-grid">
				<span class="info-icons hovicon effect-4 sub-b"><img src="/images/access-info-faster.png" alt=""></span>
				<h4>Access information much faster</h4>
				<p>Spend less time finding the information you need.</p>
			</div>
		  <div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--information end here-->


<div class="leaves">
	<div class="container">
		<div class="leaves-main wow zoomIn animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomIn;">
			<h1>Want To Make An Impact On Education?</h1>
			<h6>Show us what you have.</h6>
			<p>We are always looking for contributors to help us in our vision to make higher education better in the Philippines.</p>
		   <a href="single.html" class="hvr-push">Discover More</a>
		</div>
	</div>
</div>

<?php get_footer(); ?>
