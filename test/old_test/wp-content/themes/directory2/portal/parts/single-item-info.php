{if $meta->Accredidations}
<div class="elm-opening-hours-main">
	<h2>{__ 'General Info'}</h2>
	<div class="day-container">
		<div class="day-wrapper">
			<div class="day-title"><h5>{__ 'Accredidations'}</h5></div>
			{var $monday = AitLangs::getCurrentLocaleText($meta->Accredidations)}
			<div class="day-data">
				<p>
					{if $ABET}{$ABET}
					ABET
					{else}-{/if}
				</p>
			</div>
		</div>
			</div>
	{var $note = AitLangs::getCurrentLocaleText($meta->openingHoursNote)}
	{if $note != ""}
	<div class="note-wrapper">
		<p>{$note}</p>
	</div>
	{/if}
</div>
{/if}