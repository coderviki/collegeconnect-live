<?php if (!empty($success)):?>
<div class="sabai-alert sabai-alert-success" style="margin-bottom:10px;">
<?php   foreach ((array)$success as $_success):?>
    <p><?php echo $_success;?></p>
<?php   endforeach;?>
</div>
<?php endif;?>
<?php if (!empty($error)):?>
<div class="sabai-alert sabai-alert-danger" style="margin-bottom:10px;">
<?php   foreach ((array)$error as $_error):?>
    <p><?php echo $_error;?></p>
<?php   endforeach;?>
</div>
<?php endif;?>
<?php if (isset($download_url)):?>
<a style="display:none;" class="sabai-csv-download sabai-btn sabai-btn-primary sabai-btn-lg" href="<?php echo $download_url;?>"><?php Sabai2::_h(__('Download', 'sabai'));?></a>
<?php endif;?>