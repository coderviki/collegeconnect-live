<?php $post = get_post_meta(get_the_ID()); ?>
<div style="border: solid 1px #CCC;padding: 20px 5px 20px 115px;width:100%;height:200px;">
     <div style="width:20%;float:left;">
    <?php if ( $images->thumbnail ): ?>
        <?php echo $images->thumbnail->html; ?>
    <?php endif; ?>
     </div>
     <div style="width:60%;float:left;">
          <div class="excerpt-content">
               <div class="listing-title">
               <h2><?php echo $fields->t_title->value; ?></h2>
               </div>
               <div class="listing-details">
               <?php if ( $fields->_h_address ): ?>
               </div>
                <div class="address-info">
                <label><?php _ex( 'Address', 'themes/default', 'WPBDM' ); ?></label>
                <?php echo $fields->_h_address; ?>
                 </div>
                <?php endif; ?>
		<br>
                <?php echo get_the_excerpt(); ?>
          </div>
       <!-- <?php echo $fields->exclude('t_title,t_address,t_city,t_state,t_country,t_zip')->html; ?> -->
    </div>
     <div style="width:20%;float:left;">
	<?php echo $post['_wpbdp[fields][14]'][0]; ?>
     </div>
</div>
