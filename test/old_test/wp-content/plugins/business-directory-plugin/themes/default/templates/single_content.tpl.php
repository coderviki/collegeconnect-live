<?php $post = get_post_meta(get_the_ID()); ?>
<div style="width:100%;">
     <div style="width:20%;float:left;">
     <?php if ( $images->thumbnail ): ?>
         <?php echo $images->thumbnail->html; ?>
     <?php endif; ?>
     </div>
     <div style="width:80%;float:left;">
	<h2><?php echo $fields->t_title->value; ?></h2>
	<?php echo get_the_excerpt(); ?>
     </div>
</div>
<strong>Overview</strong><br>
<?php echo get_the_content(); ?><br>
<br>
<strong>Tuition:</strong> <?php echo $post['_wpbdp[fields][14]'][0]; ?><br>
<br>
<strong>Partnerships:</strong> <?php echo $post['_wpbdp[fields][15]'][0]; ?><br>
<br>
<strong>Accredidations:</strong> <?php echo $post['_wpbdp[fields][12]'][0]; ?><br>
<br>

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Contact</a></li>
<!--  <li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
  <li><a data-toggle="tab" href="#menu2">Menu 2</a></li> -->
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <?php echo $post['_wpbdp[fields][6]'][0]; ?>
    <?php echo $post['_wpbdp[fields][8]'][0]; ?>
  </div>
 <!-- <div id="menu1" class="tab-pane fade">
    <h3>Menu 1</h3>
    <p>Some content in menu 1.</p>
  </div>
  <div id="menu2" class="tab-pane fade">
    <h3>Menu 2</h3>
    <p>Some content in menu 2.</p>
  </div> -->
</div>

<br>
<strong>Centers of Excellence:</strong><br> <?php echo $post['_wpbdp[fields][17]'][0]; ?><br>
<strong>Centers of Development</strong><br> <?php echo $post['_wpbdp[fields][16]'][0]; ?><br>
<br>
<strong>Courses Offered</strong><br>
<br>

<strong>Undergraduate</strong><br>
<?php echo $post['_wpbdp[fields][18]'][0]; ?><br>
<br>
<strong>Postgraduate</strong><br>
<?php echo $post['_wpbdp[fields][19]'][0]; ?><br>
<br>
<strong>Technical/Vocational Programs</strong><br>
<?php echo $post['_wpbdp[fields][22]'][0]; ?><br>
<br>
<strong>Short Courses and Certifications</strong><br>
<?php echo $post['_wpbdp[fields][21]'][0]; ?><br>
<br>
<strong>Senior High Programs</strong><br>
<?php echo $post['_wpbdp[fields][20]'][0]; ?><br>




<!-- <div class="listing-details cf">
   <?php foreach ( $fields->not( 'social' ) as $field ): ?>
        <?php echo $field->html; ?>
    <?php endforeach; ?>

    <?php $social_fields = $fields->filter( 'social' ); ?>
    <?php if ( $social_fields ): ?>
    <div class="social-fields cf"><?php echo $social_fields->html; ?></div>
    <?php endif; ?>
</div>

<?php if ( $images->extra ): ?>
<div class="extra-images">
    <ul>
        <?php foreach ( $images->extra as $img ): ?>
        <li><?php echo $img->html; ?></li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?> -->

