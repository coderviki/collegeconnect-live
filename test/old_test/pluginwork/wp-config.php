<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'collegec_pluginwork');

/** MySQL database username */
define('DB_USER', 'collegec_admin');

/** MySQL database password */
define('DB_PASSWORD', 'B@ctad89');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D#;,:}#ow<D&3.YYc$z-Y `~ mP9cfLQfvZ)X:i|1lc.k(O^;7?A<p_!*;3U7&a}');
define('SECURE_AUTH_KEY',  'Im1<P^Qe7/VOZP>(wGI(>|#-@r-)O8bE&A,LHZ&;@t <^Y3T(vV,.KIj)oJ#]mp}');
define('LOGGED_IN_KEY',    'zeO6:g7mka&3H[DA7xe{ho#=*cA.UTX5NLrd*aly7&;;Y,jrIycZ]HEZVxAB77dR');
define('NONCE_KEY',        '_ke$a,`RAA9P_BD7@%YrMY$rmJ|(Ww=2;Y-wUMs+_Sk$/{Oe20yeS2/DjKHL0,qX');
define('AUTH_SALT',        'KeO$WI)ByKwd.PQ32;%h5x,r?jJH?W(yPe(i`eQ]]6#W)QsjRK=}WGZuFREjom=O');
define('SECURE_AUTH_SALT', 'F@Qy8!uE.=_iIBkAR<ML.ES2c%*+w!92W5I<Jd)uz/_-^Sh^WLH!C~YJ%OX;Ob1y');
define('LOGGED_IN_SALT',   'QF&0M9Wv)8oVp~>/S@W^r8I?Cmb]S>z]/6d#<R&[SKN#q/{=Lh~2PMcUE1:Hbhcn');
define('NONCE_SALT',       'awW${+b^Jz0tTPP|Y^!PVAwzMgZY3z.x{he@1Ok%g{T3?laK-u{8QS%J)nLNMRnF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
