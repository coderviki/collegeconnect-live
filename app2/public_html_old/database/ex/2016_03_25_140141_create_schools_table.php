<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
       Schema::create('schools', function (Blueprint $table) {
            $table->string('sch-name');
            $table->bigIncrements('sch-id')->index();
            $table->string('found-date');
            $table->bigInteger('rating-nig');
            $table->bigInteger('rating-afr');
            $table->string('type');
            $table->string('ownership');
            $table->text('description');
            $table->string('website');
            $table->string('requirements');
            $table->string('phone');
            $table->string('email');
            $table->string('address');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
