 <div class="container-fluid">
 <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header" style="margin-top:5px;">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
                         <a class="navbar-brand" href="#">SchoolDir</a>
                     </div>
                     <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="/schools">Schools</a></li>
                                <li><a href="/articles">Articles/News</a></li>                                
                            </ul>

                    <!-- Authentication Links -->
                    @if (Auth::guest())
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    </ul>
                    @else
                    <ul class="nav navbar-nav pnavbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               <img src="{{asset('images/'.Auth::user()->photo)}}" style="border-radius:20px; width:25px; height:25px;"></i> {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu" style="padding:0px;">
                                <li><a style="padding:15px 20px;" href="{{ url('/user') }}"><i class="fa fa-btn fa-sign-out"></i>Dashboard</a></li>
                                <li><a style="padding:15px 20px;" href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    @endif
                </div>
                </div>
            </nav>
            </div>