<!DOCTYPE html>
<html>
<head>
	<title>SchoolDir - Schools in Africa</title>
	<meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}"/> 
        <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}"/> 
        <script type="text/javascript" src="{{asset('js/count.js')}}"></script>  
</head>
<body>
	
	@include('header')

	<!-- if there is search query -->
	@if(isset($_REQUEST['searchItem']) && $_REQUEST['searchItem']<>'')

	<div class="container-fluid clear schools-page">
		<div class="row">
			<div class="col-md-12">
				<!--Adverts Placement-->
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-md-offset-1 filter hide-me">
				<div class="filter-title">
					<h2><i class="fa fa-filter"></i> Filter</h2>
				</div>
				<div class="criteria-div">
					<form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">
						<div class="criteria">
                            <p class="criteria-title">School Type</p>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="university"> Universities</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="polytechnic"> Polytechnics</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="ponotechnic"> Monotechnics</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="college of education"> College of Education</label>
                        </div>

                        <div class="criteria">
                            <p class="criteria-title">&#8358; Fees</p>
                            <div class="form-group">
                                <input type="number" name="fees" class="med" id="number" placeholder="ex. 10000">
                            </div>
                        </div>
                        <div class="criteria">
                            <p class="criteria-title">Ownership</p>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="federal"> Federal</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="state"> State</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="private"> Private</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="mission"> Missionary</label>
                        </div>
	  					<div class="criteria">
	  						<p class="criteria-title">Location</p>
	  						<div class="form-group">
								<select name="location" class="med" id="sel1">
								    <option selected="selected">Nigeria</option>
								    <option>Ghana</option>
								    <option>South Africa</option>
								    <option>Cameroon</option>
								  </select>  
							</div>
						</div>
						<input type="hidden" name="searchItem" value="{{$searchItem}}">
                        <input type="hidden" name="criteria" value="{{$criteria}}">
						<input type="submit" class="btn btn-primary" value="Apply Filter"> 
					</form>
				</div>
			</div>
			<!--Show on mobile-->
			<div class="col-md-2 col-md-offset-1 filter hideShowDesktop" style="margin: 10px;">
                <div class="filter-title" >
                    <a href="#menu" data-toggle="collapse"><h2 style="border:2px solid #3399ff;" class="btn btn-default dropdown-toggle"><i class="fa fa-filter"></i> Filter <span class="caret"></span></h2></a>
                </div>
                <div class="criteria-div collapse"  id="menu">
                    <form class="form" role="form" method="get" action="{{ action("searchController@sortsearch") }}">
                        <div class="criteria">
                            <p class="criteria-title">School Type</p>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="university"> University</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="polytechnic"> Polytechnic</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="monotechnic"> Monotechnic</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="college of education"> College of Education</label>
                        </div>

                        <div class="criteria">
                            <p class="criteria-title">&#8358; Fees</p>
                            <div class="form-group">
                                <input type="number" name="fees" class="med" id="number" placeholder="ex. 10000">
                            </div>
                        </div>
                        <div class="criteria">
                            <p class="criteria-title">Ownership</p>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="federal"> Federal</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="state"> State</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="private"> Private</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="mission"> Missionary</label>
                        </div>
                        <div class="criteria">
                            <p class="criteria-title">Location</p>
                            <div class="form-group">
                                <select name="location" class="med" id="sel1">
                                    <option selected="selected">Nigeria</option>
                                    <option>Ghana</option>
                                    <option>South Africa</option>
                                    <option>Cameroon</option>
                                  </select>
                            </div>
                        </div>
                        <input type="hidden" name="searchItem" value="{{$searchItem}}">
                        <input type="hidden" name="criteria" value="{{$criteria}}">
                        <input type="submit" class="btn btn-primary" value="Apply Filter"> 
                    </form>
                </div>
            </div>
            <!--END OF FILTER SHOW OR HIDE-->
			<div class="col-md-7 school-listing">
				<h2>SCHOOLS [ {{$count}} ]</h2>
			<div class="school">
				@if(isset($msg))
				<div class="alert alert-danger">
                    <ul>
                     	 <li><h3>{{ $msg }}</h3></li>
                    </ul>
                </div>
				@else
				@foreach($result as $sch)
				<div class="school-wrapper col-md-12">
					<div class="school-left col-md-4">

						<div class="school-name">
							<div class="school-icon">
								<img src="{{asset('images/'.$sch->logo)}}">
							</div>
							<div class="school-title">
								<a href="{{$sch->url}}">{{strtoupper($sch->name)}}</a><br>
								<span>Nigeria</span>
							</div>
							<div class="school-image">
								<img src="{{asset('images/'.$sch->thumbnail)}}">
							</div>
						</div>
					</div>
					<div class="school-right col-md-8">
						<div class="school-listing-action">
							<a href="">Apply</a>
							<a href="">View Courses</a>
						</div>
						<p>{{$sch->describtion}}</p>
						<div class="score-card">
							<div class="score-div">
								<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Year Founded"></i>
								<p>Found Date</p>
								<p class="score">{{$sch->founded_at}}</p>
							</div>
							<div class="score-div">
								<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Nigeria"></i>
								<p>Nigeria</p>
								<p class="score">{{$sch->rating_nigeria}} %</p>
							</div>
							<div class="score-div">
								<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Africa"></i>
								<p>Africa</p>
								<p class="score">{{$sch->rating_africa}} %</p>
							</div>
							<div class="score-div">
								<i class="fa fa-money" data-toggle="tooltip" data-placement="top" title="Average Fees in Nigerian Naira"></i>
								<p>Fees</p>
								<p class="score">&#8358;{{$sch->fees}}</p>
							</div>
						</div>

					</div>
				</div>
				@endforeach
				@endif
			</div>
			</div>
			
			<div class="col-md-2 ads">
				
			</div>
		</div>
		</div>





	<!-- if search query is empty -->

	@else



		<div class="container-fluid clear schools-page">
		<div class="row">
			<div class="col-md-12">
				<!--Adverts Placement-->
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-md-offset-1 filter hide-me">
				<div class="filter-title">
					<h2><i class="fa fa-filter"></i> Filter</h2>
				</div>
				<div class="criteria-div">
					<form class="form" role="form" method="get" action="{{ action("PagesController@sortschools") }}">
						<div class="criteria">
                            <p class="criteria-title">School Type</p>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="university"> Universities</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="polytechnic"> Polytechnics</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="monotechnic"> Monotechnics</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="college of education"> College of Education</label>
                        </div>

                        <div class="criteria">
                            <p class="criteria-title">&#8358; Fees</p>
                            <div class="form-group">
                                <input type="number" name="fees" class="med" id="number" placeholder="ex. 10000">
                            </div>
                        </div>
                        <div class="criteria">
                            <p class="criteria-title">Ownership</p>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="federal"> Federal</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="state"> State</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="private"> Private</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="mission"> Missionary</label>
                        </div>
	  					<div class="criteria">
	  						<p class="criteria-title">Location</p>
	  						<div class="form-group">
								<select name="location" class="med" id="sel1">
								    <option selected="selected">Nigeria</option>
								    <option>Ghana</option>
								    <option>South Africa</option>
								    <option>Cameroon</option>
								  </select>  
							</div>
						</div>
						<input type="submit" class="btn btn-primary" value="Apply Filter"> 
					</form>
				</div>
			</div>
			<!--Show on mobile-->
			<div class="col-md-2 col-md-offset-1 filter hideShowDesktop" style="margin: 10px;">
                <div class="filter-title" >
                    <a href="#menu" data-toggle="collapse"><h2 style="border:2px solid #3399ff;" class="btn btn-default dropdown-toggle"><i class="fa fa-filter"></i> Filter <span class="caret"></span></h2></a>
                </div>
                <div class="criteria-div collapse"  id="menu">
                    <form class="form" role="form" method="get" action="{{ action("PagesController@sortschools") }}">
                        <div class="criteria">
                            <p class="criteria-title">School Type</p>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="university"> Universities</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="polytechnic"> Polytechnics</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="monotechnic"> Monotechnics</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="School_type" value="college of education"> College of Education</label>
                        </div>

                        <div class="criteria">
                            <p class="criteria-title">&#8358; Fees</p>
                            <div class="form-group">
                                <input type="number" name="fees" class="med" id="number" placeholder="ex. 10000">
                            </div>
                        </div>
                        <div class="criteria">
                            <p class="criteria-title">Ownership</p>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="federal"> Federal</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="state"> State</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="private"> Private</label><br/>
                            <label style="font-weight:normal;"><input type="radio" name="ownership" value="mission"> Missionary</label>
                        </div>
                        <div class="criteria">
                            <p class="criteria-title">Location</p>
                            <div class="form-group">
                                <select name="location" class="med" id="sel1">
                                    <option selected="selected">Nigeria</option>
                                    <option>Ghana</option>
                                    <option>South Africa</option>
                                    <option>Cameroon</option>
                                  </select>  
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Apply Filter"> 
                    </form>
                </div>
            </div>
            <!--END OF FILTER SHOW OR HIDE-->
			<div class="col-md-7 school-listing">
				<h2>SCHOOLS [ {{$count}} ]</h2>
				
				<div class="school">
				@if(isset($msg))
				<h3 style="text-align:center;">{{$msg}}</h3>
				@else
				@foreach($school as $sch)
				<div class="school-wrapper col-md-12">
					<div class="school-left col-md-4">

						<div class="school-name">
							<div class="school-icon">
								<img src="{{asset('images/'.$sch->logo)}}">
							</div>
							<div class="school-title">
								<a href="{{$sch->url}}">{{strtoupper($sch->name)}}</a><br>
								<span>Nigeria</span>
							</div>
							<div class="school-image">
								<img src="{{asset('images/'.$sch->thumbnail)}}">
							</div>
						</div>
					</div>
					<div class="school-right col-md-8">
						<div class="school-listing-action">
							<a href="">Apply</a>
							<a href="">View Courses</a>
						</div>
						<p>{{$sch->describtion}}</p>
						<div class="score-card">
							<div class="score-div">
								<i class="fa fa-calendar" data-toggle="tooltip" data-placement="top" title="Year Founded"></i>
								<p>Found Date</p>
								<p class="score">{{$sch->founded_at}}</p>
							</div>
							<div class="score-div">
								<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Nigeria"></i>
								<p>Nigeria</p>
								<p class="score">{{$sch->rating_nigeria}} %</p>
							</div>
							<div class="score-div">
								<i class="fa fa-star-o" data-toggle="tooltip" data-placement="top" title="Rating in Africa"></i>
								<p>Africa</p>
								<p class="score">{{$sch->rating_africa}} %</p>
							</div>
							<div class="score-div">
								<i class="fa fa-money" data-toggle="tooltip" data-placement="top" title="Average Fees in Nigerian Naira"></i>
								<p>Fees</p>
								<p class="score">&#8358;{{$sch->fees}}</p>
							</div>
						</div>

					</div>
				</div>
				@endforeach
				@endif
				@if(!isset($_REQUEST['fees']))
				{{$school->render()}}
				@endif
			</div>
			</div>
			
			<div class="col-md-2 ads">
				
			</div>
		</div>
		</div>


	@endif
	@include ('footer')

</body>
</html>
