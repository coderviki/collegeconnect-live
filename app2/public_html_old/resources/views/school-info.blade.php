<!DOCTYPE html>
<html>
<head>
	<title>{{$sch->name}} - SchoolDir</title>
	<meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}"/> 
        <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}"/> 
        <script type="text/javascript" src="{{asset('js/count.js')}}"></script> 
</head>
<body>
@include ('header')
	<div class="container-fliud">
		<div class="row clear" style="margin-top: 50px;">
			<div class="col-md-12 school-head" style="background-image: url('{{asset('images/the-unilag-senate-building.jpg')}}');background-position: cover;background-repeat: no-repeat;">
				<div class="school-icon-name">
					<img src="{{asset('images/'.$sch->logo)}}">
					<h1>{{$sch->name}}</h1>
				</div>
			</div>
			<div class="row">
				<div class="tabs">
					<ul class="nav nav-tabs container">
						  <li class="active"><a data-toggle="tab" href="#home">News</a></li>
						  <li><a data-toggle="tab" href="#menu1">Quick Look</a></li>
						  <li><a data-toggle="tab" href="#menu2">Description</a></li>
						  <li><a data-toggle="tab" href="#menu3">Courses</a></li>
						  <li><a data-toggle="tab" href="#menu4">Connect</a></li>
						  <li><a data-toggle="tab" href="#menu5">Articles</a></li>
					</ul>
				</div>
				<div class="tab-content container">
					  <div id="home" class="tab-pane fade in active clear">
					    <div class="school-news">
					    	<div class="row col-md-12">
                       		    @foreach($school_news as $news)
								<div class="col-md-4">
		                            <div class="content">
		                            <a href="../{{$news->url}}" class="latest-heading">{{substr(($news->title),0,88)}}</a>
		                                <img style="padding: 5px; width:100%; height:218px;" src="{{asset('thumbnails/'.$news->thumbnail)}}" class="img-responsive">
		                                <p class="latest-body"><?php echo html_entity_decode(substr(($news->content),0,220)); ?></p>
		                            <div class="stats">
		                                <span><i class="fa fa-eye"></i>20</span><span><i class="fa fa-commenting"></i>20</span>
		                            </div>
		                            </div>
		                        </div> 
		                        @endforeach
							</div>
					    </div>
					     <div class="container-fluid clear" style="margin-top:0px;">
			<div class="row col-md-12 newsletter-articles-page">
				<div class="col-md-7">
					<h1>Lorem ipsum dolor sit amet, consectetur </h1>
				</div>
				<div class="col-md-5">
					@if(Session::has('message'))
                        <div id="message" style="background-color: #cfe5fa; padding:5px; color: #484848; margin:2px; text-align: center;">{{ Session::get('message') }}</div>
                        @endif
					<form class="form-inline" role="form" action="{{action('PagesController@subscribe')}}">
						<div class="form-group">
							<input class="form-control" name="email" type="email" placeholder="Valid Email Address" required>
						</div>
						<div class="form-group">
							<input class="btn btn-warning" type="submit" value="Subscribe">
						</div>
				</form>
				</div>
				
			</div>
				
			</div>
					    <div class="row">
					    	<div class="ads">
					    		
					    	</div>
					    </div>
					    <div class="container-fluid">
					    	
					    <div class="row col-md-12 other-news">
					    		<h3 style="font-weight: bold;">News From Other Schools</h3>
					    		@foreach($others as $others)
					    		<a href="../{{$others->url}}"><div class="col-md-4" style="background-image: url('{{asset('thumbnails/'.$others->thumbnail)}}');background-repeat: no-repeat;">
					    			<div>
					    				<h2><?php echo substr(($others->title),0,70); ?></h2>
					    			</div>
					    		</div></a>
					    		@endforeach
						</div>
					    	
					    </div>
					   
					  </div>
					  <div id="menu1" class="tab-pane fade">
					    <div class="container clear" style="margin:10px 0px;">
					    	<div class="row">
					    		<div class="col-md-12 links">
					    			<a href="">Like</a>
					    			<a href="">Apply</a>
					    		</div>
					    	</div>
						   	<div class="row col-md-12 school-marks">
						   		<div class="col-md-3">
						   			<div>
							   			<i class="fa fa-calendar"></i>
							   			<span>Year Founded</span>
							   			<p class="score">{{$sch->founded_at}}</p>
						   			</div>
						   		</div>
						   		<div class="col-md-3">
						   			<div>
							   			<i class="fa fa-star-o"></i>
							   			<span>Rating in Nigeria</span>
							   			<p class="score">{{$sch->rating_nigeria}} %</p>
						   			</div>
						   		</div>
						   		<div class="col-md-3">
						   			<div>
							   			<i class="fa fa-star-o"></i>
							   			<span>Rating in Africa</span>
							   			<p class="score">{{$sch->rating_africa}} %</p>
						   			</div>
						   		</div>
						   		<div class="col-md-3">
						   			<div>
							   			<i class="fa fa-money"></i>
							   			<span>Average Fees </span>
							   			<p class="score">&#8358;{{$sch->fees}}</p>
						   			</div>
						   		</div><br><br>								<div class="col-md-3">						   			<div>							   			<i class="fa"></i>							   			<span>Undergraduate Courses </span>							   			<p class="score">{{$sch->undergradcourses}}</p>						   			</div>						   		</div>								<div class="col-md-3">						   			<div>							   			<i class="fa"></i>							   			<span>Postgraduate Courses </span>							   			<p class="score">{{$sch->postgradcourses}}</p>						   			</div>						   		</div>								<div class="col-md-3">						   			<div>							   			<i class="fa"></i>							   			<span>Short Course and Certification Courses </span>							   			<p class="score">{{$sch->shortcoursesandcertifications}}</p>						   			</div>						   		</div>								<div class="col-md-3">						   			<div>							   			<i class="fa"></i>							   			<span>Technical and Vocational Courses </span>							   			<p class="score">{{$sch->technicalandvocationalcourses}}</p>						   			</div>						   		</div>
						   	</div>
						   	</div> 
							<div class="row col-md-12">
					    		<div class="academic-strengths">
					    			<h2>Academic Strengths</h2>
					    			<button>Mathematics</button>
					    			<button>Mathematics</button>
					    			<button>Mathematics</button>
					    			<button>Mathematics</button>
					    			<button>Mathematics</button>
									<button>Mathematics</button>
									<button>Mathematics</button>
									<button>Mathematics</button>
									<button>Mathematics</button>
									<button>Mathematics</button>
									<button>Mathematics</button>
					    		</div>
					    	</div>
					    </div>
					  <div id="menu2" class="tab-pane fade">

					    <div class="container">
					    	<div class="row clear descLayer">
					    		<h3>ABOUT</h3>
					    		<p><strong>Established: {{$sch->founded_at}}</strong> <br>
								<p>{{$sch->describtion}}</p>
					    		</p>
					    	</div>
					    	<div class="row descLayer">
					    		<h3>ENTRY REQUIREMENTS</h3>
					    		{!! $sch->entry_requirement !!}
					    	</div>
					    	<div class="row descLayer">
					    		<h3>CONTACT THE <?php echo strtoupper($sch->school_type); ?></h3>
					    		<p><strong>Website:</strong> <a href="http://{{$sch->website}}" target="_blank">{{$sch->website}}</a></p>
								<p><strong>Email:</strong> <a href="mailto:{{$sch->email}}">{{$sch->email}}</a></p>
								<p><strong>Telephone:</strong> <a href="tel:{{$sch->phone}}">{{$sch->phone}}</a></p>
					    	</div>
					    </div>
					  </div>

					  <div id="menu3" class="tab-pane fade">
					    <div class="container">
					    	<div class="row descLayer" style="margin-top:20px;">
					    		<h3>COURSES AT <?php echo strtoupper($sch->name); ?></h3>
					    		<p>{{$sch->courses}}</p>
					    	</div>
					    </div>
					  </div>

					  <div id="menu4" class="tab-pane fade">
					    <div class="container">
					    	<div class="row descLayer" style="margin-top:20px;">
					    		<h3>CONNECT WITH THE <?php echo strtoupper($sch->school_type); ?></h3>
					    		<p><strong>Website:</strong> <a href="http://{{$sch->website}}" target="_blank">{{$sch->website}}</a></p>
								<p><strong>Email:</strong> <a href="mailto:{{$sch->email}}">{{$sch->email}}</a></p>
								<p><strong>Telephone:</strong> <a href="tel:{{$sch->phone}}">{{$sch->phone}}</a></p>
					    	</div>
					    </div>
					  </div> 

					  <div id="menu5" class="tab-pane fade">
					     <div class="school-news" style="margin-top:30px;">
					    	<div class="row col-md-12">
					    		@foreach($school_articles as $article)
								<div class="col-md-4">
		                            <div class="content">
		                            <a href="../{{$article->url}}" class="latest-heading">{{substr(($article->title),0,88)}}</a>
		                                <img style="padding: 5px;width:100%; height:218px;" src="{{asset('thumbnails/'.$article->thumbnail)}}" class="img-responsive">
		                                <p class="latest-body"><?php echo html_entity_decode(substr(($article->content),0,220)); ?></p>
		                            <div class="stats">
		                                <span><i class="fa fa-eye"></i>20</span><span><i class="fa fa-commenting"></i>20</span>
		                            </div>
		                            </div>
		                        </div> 
		                        @endforeach          
							</div>
					    </div>
					  </div>

				</div>
			</div>
		</div>
	</div>

	@include ('footer')
</body>
</html>