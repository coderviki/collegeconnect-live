@include("admin-SchoolDir/pagenav")
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>SchoolDir Subscribed Users</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                        @if(Session::has('message'))
                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <a href="sendmail/all"><i class="fa fa-envelope-o"></i> Mail All</a>
                        <table class="table table-striped" style="background-color:#fff;">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th>Subscribed At</th>
                                <th>Options</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($sub as $sub)
                              <tr>
                                <td>{{ $sub->id }}</td>
                                <td>{{ $sub->email }}</td>
                                <td>{{ date ('M-d-Y',strtotime($sub->created_at)) }}</td>
                                <td>
                                    <a href="sendmail/{{ $sub->id }}"><i class="fa fa-envelope-o"></i></a>
                                </td>
                              </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>