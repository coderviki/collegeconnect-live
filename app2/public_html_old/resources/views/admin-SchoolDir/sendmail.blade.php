@include("admin-SchoolDir/pagenav")
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Send Mail</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>
                        @endif
                        <form class="form-horizontal" role="form" method="post" action="{{ action("postsController@sendmail") }}">
                            <div class="col-lg-6 col-md-6 col-sm-6" style="background-color:#fff; margin:30px 0px 0px 20px;">
                                
                                <label>Message: </label>
                                <textarea name="message" class="form-control"></textarea>
                                <br/>
                                <input type="hidden" name="id" value="{{ $mail }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-primary" name="Submit" value="Send Mail" /><br/><br/>
                            </div>  
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>