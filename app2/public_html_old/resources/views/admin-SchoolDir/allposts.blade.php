@include("admin-SchoolDir/pagenav")
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>SchoolDir Posts</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                        @if(Session::has('message'))
                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped" style="background-color:#fff;">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Type</th>
                                <th>Category</th>
                                <th>school</th>
                                <th>Options</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($post as $post)
                              <tr>
                                <td>{{ $post->id }}</td>
                                <td>{{ substr(($post->title),0,70) }}...</td>
                                <td>{{ $post->post_type }}</td>
                                <td>{{ $post->category }}</td>
                                <td>{{ $post->school }}</td>
                                <td>
                                    <a href="editpost/{{ $post->id }}">Update</a>&nbsp;&nbsp;&nbsp;
                                    @if($post->post_status <>'trashed')
                                    <a href="deletepost/{{ $post->id }}">Trash</a>
                                    @else
                                    <a href="restorepost/{{ $post->id }}">Restore</a>
                                    @endif
                                </td>
                              </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>