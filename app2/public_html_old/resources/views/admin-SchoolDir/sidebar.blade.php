<!-- /header -->
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="{{ action("PagesController@adminpage") }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-arrows-v"></i> Members <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                            <li>
                                <a href="{{ action("membersController@allmembers") }}">All Members</a>
                            </li>
                            <li>
                                <a href="{{ action("membersController@addmember") }}">Add Member</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i> Articles <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li>
                                <a href="{{ action("postsController@allposts") }}">All Posts</a>
                            </li>
                            <li>
                                <a href="{{ action("postsController@addpost") }}">Add Post</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Schools <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="{{ action("schoolsController@allschools") }}">All schools</a>
                            </li>
                            <li>
                                <a href="{{ action("schoolsController@addschool") }}">Add School</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fa fa-fw fa-arrows-v"></i> Categories <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
                            <li>
                                <a href="{{ action("categoriesController@allcategories") }}">All Categories</a>
                            </li>
                            <li>
                                <a href="{{ action("categoriesController@addcategory") }}">Add Category</a>
                            </li>
                        </ul>
                    </li>
		    <li>
			<a href="{{ action("schoolsController@fileimport") }}">File Import</a>
		    </li>                    
		    <li>
                        <a href="{{ action("PagesController@subscriptions") }}"><i class="fa fa-envelope-o"></i> Subscribed Users</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->