@include("admin-SchoolDir/pagenav")
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>SchoolDir Members</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                        @if(Session::has('message'))
                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped" style="background-color:#fff;">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Fullname</th>
                                <th>School</th>
                                <th>Level</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Options</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($user as $user)
                              <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->fullname }}</td>
                                <td>{{ $user->school }}</td>
                                <td>{{ $user->level }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->gender }}</td>
                                <td>
                                    <a href="editmem/{{ $user->id }}">Update</a>&nbsp;&nbsp;&nbsp;
                                    @if($user->status <>'deleted')
                                    <a href="deletemem/{{ $user->id }}">Delete</a>
                                    @else
                                    <a href="restoremem/{{ $user->id }}">Restore</a>
                                    @endif
                                </td>
                              </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>