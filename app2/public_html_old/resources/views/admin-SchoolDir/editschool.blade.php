@include("admin-SchoolDir/pagenav")
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Edit School</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                        <p style="color:#f45a1e; margin-top:3px; text-align:center;">{{ Session::get('message') }}</p>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="post" action="{{ action("schoolsController@updateschool") }}" enctype="multipart/form-data">
                            <div class="col-lg-8 col-md-8 col-sm-7" style="background-color:#fff; margin:30px 0px 0px 20px;">
                                
                                <label for="sname">School Name: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                    </div>
                                <input class="form-control input" name="sname" value="{{ $sch->name }}" type="text" />
                                </div>

                                </div>								<label>Undergraduate Courses: </label>                                <div class="input-group">                                    <div class="input-group-addon">                                      <i class="fa fa-pie-chart"></i>                                    </div>                                <input class="form-control input" name="undergradcourses" value="{{ $sch->undergradcourses }}" type="text" placeholder="Enter courses. seperate with comma (,)" />                                </div>								                                <label>Postgraduate Courses: </label>                                <div class="input-group">                                    <div class="input-group-addon">                                      <i class="fa fa-pie-chart"></i>                                    </div>                                <input class="form-control input" name="postgradcourses" value="{{ $sch->postgradcourses }}" type="text" placeholder="Enter courses. seperate with comma (,)" />                                </div>								<label>Short Courses and Certifications: </label>                                <div class="input-group">                                    <div class="input-group-addon">                                      <i class="fa fa-pie-chart"></i>                                    </div>                                <input class="form-control input" name="shortcoursesandcertifications" value="{{ $sch->shortcoursesandcertifications }}" type="text" placeholder="Enter courses. seperate with comma (,)" />                                </div>								<label>Technical and Vocational Courses: </label>                                <div class="input-group">                                    <div class="input-group-addon">                                      <i class="fa fa-pie-chart"></i>                                    </div>                                <input class="form-control input" name="technicalandvocationalcourses" value="{{ $sch->technicalandvocationalcourses }}" type="text" placeholder="Enter courses. seperate with comma (,)" />                                </div>

                                <label for="password">Describtion: </label>
                                <textarea class="form-control" rows="2" name="describtion">{{ $sch->describtion }}</textarea>
                                
                                <label>Average Fees: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-pie-chart"></i>
                                    </div>
                                <input class="form-control input" name="fees" value="{{ $sch->fees }}" type="text" />
                                </div>

                                <label>Year Founded: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-pie-chart"></i>
                                    </div>
                                <input class="form-control input" name="year" value="{{ $sch->founded_at }}" type="text" />
                                </div>

                                <label for="keywords">Keywords: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-key"></i>
                                    </div>
                                <input class="form-control input" name="keywords" value="{{ $sch->keywords }}" type="text" />
                                </div>

                                <label>Entry_Requirement: </label>
                                <textarea class="form-control ckeditor" rows="2" name="entry_requirement">{{ $sch->entry_requirement }}</textarea>
                                <br/>
                            </div>  
                            
                            <div class="col-lg-3 col-md-3 col-sm-5" style="background-color:#fff; margin:30px 0px 0px 20px;">
                               <label>Rating In Nigeria: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-pie-chart"></i>
                                    </div>
                                <input class="form-control input" value="{{ $sch->rating_nigeria }}" name="rating_nigeria" type="text" placeholder="Rating in Percentage" />
                                </div>

                                <label>Rating In Africa: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-pie-chart"></i>
                                    </div>
                                <input class="form-control input" value="{{ $sch->rating_africa }}" name="rating_africa" type="text" placeholder="Rating in Percentage" />
                                </div>

                                <label>School Type:</label> 
                                <select name="type" class="form-control" >
                                    <option selected="selected" value"">Select Type</option>
                                    <option>University</option>
                                    <option>Polytechnic</option>
                                    <option>Monotechnic</option>
                                    <option>College</option>
                                </select>

                                <label>Ownership:</label>
                                
                                        <select name="ownership" class="form-control" >
                                            <option selected="selected" value"">Ownership</option>
                                            <option>Federal</option>
                                            <option>State</option>
                                            <option>Mision</option>
                                            <option>Private</option>
                                        </select>
                                    

                                <label>School Location:</label>
                                        <select name="location" class="form-control" >
                                            <option selected="selected" value"">Select Location</option>
                                            <option>Nigeria</option>
                                            <option>Ghana</option>
                                            <option>South Africa</option>
                                            <option>Cameroon</option>
                                        </select>

                                            <label>Email: </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                  <i class="fa fa-envelope-o"></i>
                                                </div>
                                            <input class="form-control input" name="email" value="{{ $sch->email }}" type="text"/>
                                            </div>

                                            <label>Phone: </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                  <i class="fa fa-phone"></i>
                                                </div>
                                            <input class="form-control input" name="phone" value="{{ $sch->phone }}" type="text" />
                                            </div>

                                            <label>Website: </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                  <i class="fa fa-edit"></i>
                                                </div>
                                            <input class="form-control input" name="website" value="{{ $sch->website }}" type="text"/>
                                            </div>

                                        <label>School Picture:</label>
                                        <input type="file" name="thumbnail"/>
                                        <label>School Logo:</label>
                                        <input type="file" name="logo"/>
                                    <br/>
                                <input type="hidden" name="id" value="{{ $sch->id }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-primary" name="Submit" value="Update School" /><br/><br/>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>