<!DOCTYPE html>
<html>
<head>
    <title>SchoolDir</title>
    <meta charset="utf-8">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}"/>  
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}"/>  

    <script type="text/javascript" src="{{asset('js/count.js')}}"></script>     

</head>
<body>
    <div class="container-fluid">
     @include ('header')

     <?php
     $redirectUrl = null;
     $searchItem = null;
     if(isset($_GET['search'])){
        $searchItem = $_GET['searchItem'];
        $redirect = $_GET['criteria'];
        switch ($redirect) {
           case 'schools':
           echo '<meta http-equiv="refresh" content="00000000;url=/schools?searchItem='.$searchItem.'">';
           break;
           case 'articles':
           echo '<meta http-equiv="refresh" content="0;url=/articles?searchItem='.$searchItem.'">';
           break;
           case 'courses':
           echo '<meta http-equiv="refresh" content="0;url=/courses?searchItem='.$searchItem.'">';
           break;
           default:
           $redirectUrl = "";
           break;
       }
   }
   ?>
   <div class="container-fluid main-masthead">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center" style="font-family: Montserrat,arial;">Search for Schools information around you</h1>
        </div>
    </div>
    <div class="container"> 
        <div class="row col-md-12 sform">
            <form action="{{ action("searchController@postsearch") }}" method="get" role="form" class="form-inline">
                <div class="form-group col-md-6">
                    <input class="form-control" type="text" name="searchItem" placeholder="School Name or Course" required>
                </div>
                <div class="form-group col-md-4">
                    <select class="form-control" name="criteria" required>
                        <option selected="selected" value="" >Select Criteria</option>
                        <option value="schools">Schools</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="submit" class="btn btn-search" name="search">
                </div>
            </form>
        </div>
    </div>
    <div class="row col-md-12 home-links">
       <a href=""><i class="fa fa-book"></i> Class</a>
       <a href=""><i class="fa fa-pencil-square"></i>
        Exam</a>
        <a href=""><i class="fa fa-forumbee"></i>
            Forum</a>
        </div>
    </div>
    <div class="container layer1">
        <div class="row">
            <div class="col-md-4 latest">
                <div class="latest-wrapper">
                    <h2 class="title">Latest News</h2>
                    <div class="content">
                        <a href="" class="latest-heading">Universities in Scotland: The northern excellence</a>
                        <img style="padding: 5px;width:100%" src="{{asset('images/test-image.jpg')}}" class="img-responsive">
                        <p class="latest-body">Scotland has some of the best universities in the world. According to a recent survey, graduates from Scottish universities find employment or are pursuing further studies within six months.</p>
                        <div class="stats">
                            <span><i class="fa fa-eye"></i>
                                20</span>
                                <span><i class="fa fa-commenting"></i>
                                    20</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 top-schools">
                        <h2 class="title">Recently Added</h2>
                        @foreach($sch as $sch)
                        <div class="sch">
                            <div class="sch-content">
                                <a href="{{$sch->url}}">
                                    <img style="width:60px;vertical-align: middle;" src="{{asset('images/'.$sch->logo)}}">
                                    {{$sch->name}}
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="col-md-4 top-courses">
                        <h2 class="title">Popular Courses</h2>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square red-pos">1<sup>st</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">2<sup>nd</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">3<sup>rd</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">4<sup>th</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">5<sup>th</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">6<sup>th</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">7<sup>th</sup></span>

                        </div>
                        <div class="sch more">
                            <div class="sch-content">
                                <a href="#">
                                    <span class="glyphicon glyphicon-book"></span>
                                    Mathematics
                                </a>
                            </div>
                            <span class="pos-square">8<sup>th</sup></span>

                        </div>
                    </div>

                </div>
            </div>
            <div class="container-fluid layer2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title text-center">Articles</h1>
                                    @foreach($post as $post)
                                    <div class="col-md-4">
                                        <div class="content">
                                        <a href="{{ $post->url }}" class="latest-heading">{{substr(($post->title),0,88)}}..</a>
                                            <img style="padding: 5px; width:100%; height:218px;" src="{{asset('thumbnails/'.$post->thumbnail)}}" class="img-responsive">
                                            <p class="latest-body"><?php echo html_entity_decode(substr(($post->content),0,220)); ?></p>
                                        <div class="stats">
                                            <span><i class="fa fa-eye"></i>20</span><span><i class="fa fa-commenting"></i>20</span>
                                        </div>
                                        </div>
                                    </div>
                                   @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid tag-cloud">
                                        <div class="container ">
                                            <div class="row">
                                                <ul>
                                                    <li class="l2"><a href="">Applying for a postgraduate degree</a></li>
                                                    <li class="l1"><a href="">Making an Application </a></li>
                                                    <li class="l2"><a href="">Visas and Immigration</a></li>
                                                    <li class="l3"><a href="">Boarding Schools</a></li>
                                                    <li class="l1"><a href="">Ranking </a></li>
                                                    <li class="l2"><a href="">Choose a Subject</a></li>
                                                    <li class="l2"><a href="">Living in the UK </a></li>
                                                    <li class="l1"><a href="">Random</a></li>
                                                    <li class="l2"><a href="">Study in the UK</a></li>
                                                    <li class="l3"><a href="">University News</a></li>
                                                    <li class="l1"><a href="">Stories</a></li>
                                                    <li class="l2"><a href="">Living in the UK</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid" style="margin: 10px;">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-book icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Schools</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-book icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Courses</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <i class="fa fa-book icon-custom"></i> 
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Articles</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-user icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Users</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @include ('footer')
                                </div>
                                
                            </body>
                            </html>
