<!DOCTYPE html>
<html>
<head>
    <title>SchoolDir</title>
    <meta charset="utf-8">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}"/>  
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}"/>  

    <script type="text/javascript" src="{{asset('js/count.js')}}"></script>     

</head>
<body>
    <div class="container-fluid">
     @include ('header')
  
    <div class="container layer1">
        <div class="row">
            <div class="reg-container">
                <div class="reg-content">
                    <h2 style="text-align:center;">SchoolDir-REGISTRATION</h2>
                    <form class="form-horizontal" role="form" method="post" action="{{ action("membersController@savemember") }}">
                            <div class="col-lg-5 col-md-5 col-sm-6" style="background-color:#fff; margin:30px 0px 0px 20px;">
                                
                                <label for="fname">Full Name: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                    </div>
                                <input class="form-control input" name="fname" value="" type="text" required/>
                                </div>
                    
                                <label for="email">Email: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-envelope-o"></i>
                                    </div>
                                <input class="form-control input" name="email" value="" type="email" required/>
                                </div>

                                <label for="password">password: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-key"></i>
                                    </div>
                                <input class="form-control input" name="password" value="" type="password" required/>
                                </div>

                                <label for="cpassword">Comfirm password: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-key"></i>
                                    </div>
                                    <input class="form-control input" name="cpassword" value="" type="password" required/><br/>
                                </div><br/>
                            </div>  
                            <div class="col-lg-4 col-md-4 col-sm-5" style="background-color:#fff; margin:30px 0px 0px 20px;">
                                <label for="gender">Gender:</label><br/>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-question-circle"></i>
                                    </div>
                                    <select name="gender" class="form-control">
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                </div>

                                <label for="school">School:</label><br/>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-mortar-board"></i>
                                    </div>
                                    <select name="school" class="form-control">
                                        <option>UNILAG</option>
                                        <option>UNIBEN</option>
                                        <option>DSPZ</option>
                                    </select>
                                </div>

                                <label for="level">Level:</label><br/>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-level-up"></i>
                                    </div>
                                    <select name="level" class="form-control">
                                        <option>200 Level</option>
                                        <option>100Level</option>
                                        <option>HND 2</option>
                                    </select>
                                </div><br/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-primary" name="Submit" value="REGISTER" /><br/><br/>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </div>
           
                                    <div class="container-fluid" style="margin: 10px;">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-book icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Schools</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-book icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Courses</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <i class="fa fa-book icon-custom"></i> 
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Articles</p>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 count-div">
                                                    <span class="glyphicon glyphicon-user icon-custom"></span>
                                                    <p class="count">1000</p>
                                                    <p class="count-label">Users</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @include ('footer')
                                </div>
                                
                            </body>
                            </html>
