<?php



Route::get('/v2/', function() {
/*
    $exitCode = Artisan::call('config:cache');
    print_r($exitCode);
*/
return dd("HELLO WORLD!");
});


/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'PagesController@index');
	

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::post('loginuser', array('as' => 'loginuser', 'uses' => 'membersController@loginuser'));
use Illuminate\Http\Request;
Route::group(['middleware' => ['web']], function () {
	Route::get('register','membersController@register');
	Route::get('login','membersController@login');
	Route::get('/', 'PagesController@index');

	Route::post('admin-SchoolDir/savemember', 'membersController@savemember');
	Route::post('admin-SchoolDir/savepost', 'postsController@savepost');
	Route::post('admin-SchoolDir/savecategory', 'categoriesController@savecategory');
	Route::post('admin-SchoolDir/saveschool', 'schoolsController@saveschool');
	Route::post('user/updatephoto', 'membersController@updatephoto');
	Route::post('user/updatephoto', 'membersController@updatephoto');
	Route::post('user/updatepassword', 'membersController@updatepassword');
	Route::post('user/updateprofile', 'membersController@updateprofile');

	Route::get('articles','PagesController@posts');
	Route::get('articles/category/{category}','PagesController@postsort');
	Route::get('schools','PagesController@schools');
	Route::get('schools/{url}','schoolsController@viewschool');

	Route::get('admin-SchoolDir/fileimport','schoolsController@fileimport');
	//Route::get('user','membersController@user')

	Route::get('schools/university-lagos','PagesController@schoolInfo');
	Route::get('articles/{url}', 'postsController@viewpost');
	Route::get('search', 'searchController@postsearch');
	Route::get('sortsearch', 'searchController@sortsearch');
	Route::get('sortschools', 'PagesController@sortschools');

	Route::get('admin-SchoolDir', ['middleware' => 'auth', 'uses'=>'PagesController@adminpage']);
	Route::get('admin-SchoolDir/allposts', ['middleware' => 'auth', 'uses'=>'postsController@allposts']);
	Route::get('admin-SchoolDir/addpost', ['middleware' => 'auth', 'uses'=>'postsController@addpost']);
	Route::get('admin-SchoolDir/allschools', ['middleware' => 'auth', 'uses'=>'schoolsController@allschools']);
	Route::get('admin-SchoolDir/allcategories', ['middleware' => 'auth', 'uses'=>'categoriesController@allcategories']);
	Route::get('admin-SchoolDir/addcategory', ['middleware' => 'auth', 'uses'=>'categoriesController@addcategory']);
	Route::get('admin-SchoolDir/addschool', ['middleware' => 'auth', 'uses'=>'schoolsController@addschool']);
	Route::get('admin-SchoolDir/addmember', ['middleware' => 'auth', 'uses'=>'membersController@addmember']);
	Route::get('admin-SchoolDir/allmembers', ['middleware' => 'auth', 'uses'=>'membersController@allmembers']);
	Route::get('subscribe', ['middleware' => 'auth', 'uses'=>'PagesController@subscribe']);
	Route::get('admin-SchoolDir/subscriptions', ['middleware' => 'auth', 'uses'=>'PagesController@subscriptions']);
	Route::get('admin-SchoolDir/sendmail/{id}', ['middleware' => 'auth', 'uses'=>'PagesController@sendmail']);
	Route::post('admin-SchoolDir/sendmail', ['middleware' => 'auth', 'uses'=>'postsController@sendmail','as'=>'sendmail']);



	Route::post('admin-SchoolDir/updatemem', ['middleware' => 'auth', 'uses'=>'membersController@updatemem','as'=>'updatemem']);
	Route::get('admin-SchoolDir/editmem/{id}', ['middleware' => 'auth', 'uses'=>'membersController@editmember','as'=>'editmember']);

	Route::post('admin-SchoolDir/updatepost', ['middleware' => 'auth', 'uses'=>'postsController@updatepost','as'=>'updatepost']);
	Route::post('admin-SchoolDir/updateschool', ['middleware' => 'auth', 'uses'=>'schoolsController@updateschool','as'=>'updateschool']);
	Route::post('admin-SchoolDir/updatecat', ['middleware' => 'auth', 'uses'=>'categoriesController@updatecat','as'=>'updatecat']);
	Route::get('admin-SchoolDir/editpost/{id}', ['middleware' => 'auth', 'uses'=>'postsController@editpost','as'=>'editpost']);
	Route::get('admin-SchoolDir/editcat/{id}', ['middleware' => 'auth', 'uses'=>'categoriesController@editcat','as'=>'editcat']);
	Route::get('admin-SchoolDir/editschool/{id}', ['middleware' => 'auth', 'uses'=>'schoolsController@editschool','as'=>'editschool']);

	Route::get('admin-SchoolDir/deletemem/{id}', 'membersController@deletemember');
	Route::get('admin-SchoolDir/deletepost/{id}', 'postsController@deletepost');
	Route::get('admin-SchoolDir/deletecat/{id}', 'categoriesController@deletecat');
	Route::get('admin-SchoolDir/deleteschool/{id}', 'schoolsController@deleteschool');

	Route::get('admin-SchoolDir/restoremem/{id}', 'membersController@restoremember');
	Route::get('admin-SchoolDir/restorepost/{id}', 'postsController@restorepost');
	Route::get('admin-SchoolDir/restorecat/{id}', 'categoriesController@restorecat');
	Route::get('admin-SchoolDir/restoreschool/{id}', 'schoolsController@restoreschool');
    

});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/user', 'HomeController@user');
});
