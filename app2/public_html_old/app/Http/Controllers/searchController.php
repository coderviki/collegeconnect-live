<?php
namespace App\Http\Controllers;

use App\posts;
use App\categories;
use App\schools;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
class searchController extends Controller
{

		public function articlesearch(Request $request){
		$searchItem=$request['searchItem'];
		$criteria=$request['criteria'];
		if($criteria == 'articles'){
		$result=posts::whereRaw("MATCH(title,content,keywords) AGAINST('$searchItem')")->get();
        if(count($result)>0){
        $count=count($result);
		return view('articles', array('result' => $result))->with(array('searchItem'=>$searchItem,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);	
		}
		else{
			return view('articles')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria));
		}
		}
		}

		public function postsearch(Request $request){
		$searchItem=$request['searchItem'];
		$criteria=$request['criteria'];
		if($criteria == 'schools'){
		$result=schools::whereRaw("MATCH(name,describtion) AGAINST('$searchItem')")->where('status','')->get();
        if(count($result)>0){
        $count=count($result);
		return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);	
		}
		else{
			return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria));
		}
		}

		if($criteria == 'articles'){
		$result=posts::whereRaw("MATCH(title,content,keywords) AGAINST('$searchItem')")->get();
        if(count($result)>0){
        $count=count($result);
		return view('articles', array('result' => $result))->with(array('searchItem'=>$searchItem,
			'criteria'=>$criteria,'count'=>$count,'title' => 'SchoolDir Articles',
			'cat' => categories::where('status','')->get()))
		->with('searchItem',$searchItem);	
		}
		else{
			return view('articles')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria,'title' => 'SchoolDir Articles'));
		}
		}

		if($criteria == 'courses'){
		return redirect()->back();	
		}
		}

		//sort search
		public function sortsearch(Request $request){
		$searchItem=$request['searchItem'];
		$criteria=$request['criteria'];
		$ownership=$request['ownership'];
		$fees=$request['fees'];
		$type=$request['School_type'];
		$location=$request['location'];


		if($criteria == 'schools'){
		$first=schools::whereRaw("MATCH(name,describtion,school_type) AGAINST('$type')");

		$result=schools::where([
			['ownership', $ownership],
			['location',$location],
			['school_type',$type],
			])
		->orwhere([
			['ownership', $ownership],
			['location',$location]
			])
		->orwhere('fees',$fees)
		->union($first)
		->get();
        if(count($result)>0){
        $count=count($result);
		return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem,'criteria'=>$criteria,'count'=>$count));	
		}
		else{
			return view('schools')->with(array('msg'=>'No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria));
		}
		}
		}


}
