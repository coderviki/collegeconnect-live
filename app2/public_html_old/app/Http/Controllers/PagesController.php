<?php
namespace App\Http\Controllers;

use App\posts;
use App\categories;
use App\schools;
use App\subscriptions;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
class PagesController extends Controller
{

	


		public function index(){
			$post=posts::where('post_status', '')
	      ->orderBy('id', 'desc')
	      ->take(3)
	      ->get();
	      $school=schools::where('status','')
	      ->orderBy('id', 'desc')
	      ->take(5)
	      ->get();
	      return view('index', array('post' => $post, 'sch'=>$school));
		}

	    public function schools(){
	      $sch=schools::where('status','')->paginate(12);
	      if(count($sch)==0){
	        return redirect()->back()->with('message', 'There are no Schools at this moment');
	      }
	      else{
	      	$count=count($sch);
	        return view ('schools',array('school' => $sch,'searchItem'=>'','criteria'=>'','count'=>$count));
	      }
		}


		//sort schools
		public function sortschools(Request $request){

		$ownership=$request['ownership'];
		$fees=$request['fees'];
		$type=$request['School_type'];
		$location=$request['location'];
		$first=schools::whereRaw("MATCH(name,describtion) AGAINST('$type')");

		$sch=schools::where([
			['ownership', $ownership],
			['location',$location],
			['school_type',$type],
			])
		->orwhere('fees',$fees)
		->orwhere('school_type',$type)
		->orwhere([
			['ownership', $ownership],
			['location',$location]
			])
		//->orwhere('location',$location)
		->where('status','')
		->union($first)
		->get();
        if(count($sch)>0){
        $count=count($sch);
		return view('schools', array('school' => $sch))->with(array('count'=>$count));	
		}
		else{
			return view('schools')->with(array('msg'=>'No result found for this filter','count'=>'0'));
		}
		
		}



		public function articles(){
			return view('articles', array('cat' => categories::where('status','')->get()));
		}
		public function schoolInfo(){
			return view ('school-info');
		}
		public function adminpage(){
			return view ('admin-SchoolDir/index')->with("title","SchoolDir-Admin");
		}

		//articles to view
		public function posts(){
			$post=posts::where('post_status', '')
	      ->orderBy('id', 'desc')
	      ->take(15)
	      ->get();
	      return view('articles', array('post' => $post,'cat' => categories::where('status','')->get()))->with(array('title' => 'SchoolDir Articles', 'page_name'=>'SchoolDir Articles'));
		}
		//articles by category
		public function postsort($category){
		 $bring='articles/category/'.$category;
		 $cat=categories::where('url',$bring)->first();
		 $catName=$cat->name;
		 $post=posts::where('post_status', '')
		  ->where('category',$catName)
	      ->orderBy('id', 'desc')
	      ->take(15)
	      ->get();
	      return view('articles-category', array('post' => $post,'cat' => categories::where('status','')->get()))->with(array('title' => 'SchoolDir Articles', 'page_name'=>'SchoolDir Articles'));
		}

		//user subscribe via email
		public function subscribe(Request $request){
		  $count=subscriptions::where('email',$request['email'])->first();
		  if(count($count)>0){
		  	return redirect()->back()
          ->with('message', 'You already subscribed');
		  }
		  $sub = new subscriptions();
          $sub->email=$request['email'];
          $sub->save();
          return redirect()->back()
          ->with('message', 'Subscription Sucessful');
          //return \response::json(array('success'=>true));
		}

		//subscribed users email
		public function subscriptions(){
		  //prepare email sending
          $emails=subscriptions::All();
          if(count($emails)==0){
	        return redirect()->back()->with('message', 'There are no subscriptions yet');
	      }
	      else{
	        return view ('admin-SchoolDir.subscriptions',array('sub' => $emails))->with("title","Admin-Subscriptions");
	      }
		}

		//send mail
		public function sendmail($id){
		if($id <> 'all'){
		$mailget=subscriptions::where('id', '=', $id)->first();
		$mail=$mailget->id;
		return view('admin-SchoolDir.sendmail')
         ->with(array('mail' => $mail,'title' =>'Send Mail'));
		}
		else{
		return view('admin-SchoolDir.sendmail')->with(array('mail' => 'all','title' =>'Send Mail'));
		}
		}


}
