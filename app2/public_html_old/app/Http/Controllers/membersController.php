<?php
namespace App\Http\Controllers;

use App\members;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class membersController extends Controller
{
		//All Members Route
		public function allmembers(){
      $user=members::All();
      if(count($user)==0){
        return redirect()->back()->with('message', 'There are no members at this moment');
      }
      else{
        return view ('admin-SchoolDir.allmembers',array('user' => $user))->with("title","Admin-Allmembers");
      }
		}

		//New Member Route
		public function addmember(){
			return view ('admin-SchoolDir.addmember')->with("title","Admin-Addmember");
		}

		//save member to DB
		public function savemember(Request $request){
        $this->validate($request, [
        'cpassword' => 'same:password|min:6',
        'password' => 'min:6',
        'email' => 'unique:members,email',
        ]);
          $user = new members();
          $fname=$request['fname'];
          $email=$request['email'];
          $password=$request['password'];
          $cpassword=$request['cpassword'];
          $gender=$request['gender'];
          $school=$request['school'];
          $level=$request['level'];

          $user->fullname=$fname;
          $user->email=$email;
          $user->password=bcrypt($cpassword);
          $user->gender=$gender;
          $user->school=$school;
          $user->level=$level;
          $user->save();
          return redirect()->back()
          ->with('message', 'User created Sucessful');
    }

    //Edit member route
    public function editmember($id){
         return view('admin-SchoolDir.editmem')
         ->with(array('user' => members::where('id', '=', $id)->first(),'title' =>'Update Member'));
    }

    //update member in DB
    public function updatemem(Request $request){
        
          $user = new members();
          $fname=$request['fname'];
          $gender=$request['gender'];
          $school=$request['school'];
          $level=$request['level'];

          members::where('id', $request['id'])
          ->update([
          'fullname' => $fname,
          'gender' =>$gender, 
          'school' =>$school, 
          'level' =>$level, 
          ]);
          return redirect()->back()
          ->with('message', 'User updated Sucessful');
    }

    //delete member
    public function deletemember($id){
         members::where('id', $id)
          ->update(['status' => 'deleted']);
          return redirect()->back()->with('message','Member was deleted successful'); 
    }

    //restore deleted member
    public function restoremember($id){
         members::where('id', $id)
          ->update(['status' => '']);
          return redirect()->back()->with('message','Member restored successful'); 
    }

    public function register(){
      return view ('register')->with("title","SchoolDir-Registration");
    }
    public function login(){
      return view ('login')->with("title","SchoolDir-Login");
    }

    public function user(){
      $user = Auth::user();
        /*if(Auth::check($user)){
        return view('user/index')->with('title', 'SchoolDir-User'); 
        }
        else{
          return view('login')->with(array('msg' => 'Please Login', 'title'=>'User Login'));
        }*/
        return view('user/index')->with('title', 'SchoolDir-User');
    }

    //login user
    public function loginuser(Request $request){
      if( Auth::attempt( ['email'=>$request['email'], 'password'=>$request['password']])){
        return redirect()->intended('user')->with('title', 'SchoolDir-USER PROFILE'); 
      }
      return view('login')->with(array('msg' => 'Wrong User Input', 'title'=>'SchoolDir-Login'));      
    }

    //save Profile photo to DB
    public function updatephoto(Request $request){
        $this->validate($request, [
        'photo' => 'mimes:jpg,jpeg,png|max:5000',
        ]);

          //photo
          $img=$request->file('photo');
          $upload_dir='images';
          if(empty($img)){
            $image="";
          }
          else{
          $image=$img->getClientOriginalName();
          $move=$img->move($upload_dir, $image);
          }
          //end photo process
          $user = new members();
          members::where('id', $request['id'])
          ->update([
          'photo' => $image,
          ]);
          return redirect()->back()
          ->with('message', 'Photo Updated Sucessful');
    }

    //Update Password
    public function updatepassword(Request $request){
        if(!password_verify($request['old_password'],$request['current_password']))
        {
          return redirect()->back()
          ->with('message', 'Incorrect Old Password');
        }
        $this->validate($request, [
        'comfirm_password' => 'same:new_password|min:6',
        'new_password' => 'min:6',
        ]);

          $user = new members();
          members::where('id', $request['id'])
          ->update([
          'password' => bcrypt($request['new_password']),
          ]);
          return redirect()->back()
          ->with('message', 'Password Updated Sucessful');
    }

    //save Profile photo to DB
    public function updateprofile(Request $request){

          $user = new members();
          members::where('id', $request['id'])
          ->update([
          'about' =>$request['about'],
          'phone' =>$request['phone'],
          'home_address' =>$request['address'],
          'technical_skills' =>$request['skills'],
          'hobbies' =>$request['hobbies'],
          'quote' =>$request['quote'],
          'nationality' =>$request['nationality'],
          'state_of_origin' =>$request['state_of_origin'],
          'hometown' =>$request['hometown'],
          'gender' =>$request['gender'],
          'school' =>$request['school'],
          'level' =>$request['level'],
          ]);
          return redirect()->back()
          ->with('message', 'Profile Updated Sucessful');
    }
}