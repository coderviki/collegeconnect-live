<?php

namespace App\Http\Controllers;



use App\posts;

use App\categories;

use App\schools;

use DB;

use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Routing\Controller as BaseController;

use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;

class searchController extends Controller

{

	protected $result = [];



		public function articlesearch(Request $request){

		$searchItem=$request['searchItem'];
		$searchLocation=$request['searchLocation'];

		$criteria=$request['criteria'];

		if($criteria == 'articles'){

		$result=posts::whereRaw("MATCH(title,content,keywords) AGAINST('$searchItem')")->get();

        if(count($result)>0){

        $count=count($result);

		return view('articles', array('result' => $result))->with(array('searchItem'=>$searchItem,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);	

		}

		else{

			return view('articles')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria));

		}

		}

		}



		public function postsearch(Request $request){

		$searchItem=$request['searchItem'];
		$searchLocation=$request['searchLocation'];

		$criteria=$request['criteria'];

		if($criteria == 'schools'){

//		 $result=schools::whereRaw("MATCH(name,describtion) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
            $result=schools::where('name', 'LIKE', '%'.$searchItem.'%')
                 ->where('location', 'LIKE', '%'.$searchLocation.'%')
                 ->where('status','')
                 ->orderBy('name', 'asc')
                 ->get();

            $this->result = $result;

        if(count($result)>0){

        $count=count($result);

		return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);

		}

		else{

			return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria));

		}

		}



		if($criteria == 'articles'){

		$result=posts::whereRaw("MATCH(title,content,keywords) AGAINST('$searchItem')")->get();

        if(count($result)>0){

        $count=count($result);

		return view('articles', array('result' => $result))->with(array('searchItem'=>$searchItem,

			'criteria'=>$criteria,'count'=>$count,'title' => 'SchoolDir Articles',

			'cat' => categories::where('status','')->get()))

		->with('searchItem',$searchItem);	

		}

		else{

			return view('articles')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria,'title' => 'SchoolDir Articles'));

		}

		}



		if($criteria == 'bachelorsdegree'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(undergradcourses) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();

//		$result=schools::where('undergradcourses', 'LIKE', '__'.$searchItem.'__')
//                 ->orWhere('location', 'LIKE', '__'.$searchLocation.'__')
//                 ->where('status','')->get();
            $result=schools::where('undergradcourses', 'LIKE', '%'.$searchItem.'%')
                ->where('location', 'LIKE', '%'.$searchLocation.'%')
                ->where('status','')->orderBy('name', 'asc')->get();

        if(count($result)>0){

        $count=count($result);

		return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);	

		}

		else{

			return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria));
		}

		}

		

if($criteria == 'mastersdegree'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(postgradcourses) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
//		$result=schools::where('postgradcourses', 'LIKE', '___'.$searchItem.'___')
//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
//                 ->where('status','')->get();
    $result=schools::where('postgradcourses', 'LIKE', '%'.$searchItem.'%')
        ->where('location', 'LIKE', '%'.$searchLocation.'%')
        ->where('status','')->orderBy('name', 'asc')->get();

        if(count($result)>0){

        $count=count($result);

		return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);	

		}

		else{

			return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria));
		}

		}

		

if($criteria == 'technicalvocational'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(technicalandvocationalcourses) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
//		$result=schools::where('technicalandvocationalcourses', 'LIKE', '___'.$searchItem.'___')
//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
//                 ->where('status','')->get();
    $result=schools::where('technicalandvocationalcourses', 'LIKE', '%'.$searchItem.'%')
        ->where('location', 'LIKE', '%'.$searchLocation.'%')
        ->where('status','')->orderBy('name', 'asc')->get();

        if(count($result)>0){

        $count=count($result);

		return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);	

		}

		else{

			return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria));
		}

		}

		

if($criteria == 'certificateshortcourse'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(shortcoursesandcertifications) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
//		$result=schools::where('shortcoursesandcertifications', 'LIKE', '___'.$searchItem.'___')
//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
//                 ->where('status','')->get();
    $result=schools::where('shortcoursesandcertifications', 'LIKE', '%'.$searchItem.'%')
        ->where('location', 'LIKE', '%'.$searchLocation.'%')
        ->where('status','')->orderBy('name', 'asc')->get();

        if(count($result)>0){

        $count=count($result);

		return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);	

		}

		else{

			return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria));
		}

		}

		}

		//sort search

		public function sortsearch(Request $request){

		$searchItem=$request['searchItem'];
		$searchLocation=$request['searchLocation'];

		$criteria=$request['criteria'];

		$ownership=$request['ownership'];

		$fees=$request['fees'];

		$type=$request['School_type'];

		$location=$request['location'];





		if($criteria == 'schools'){

//		$first=schools::where("MATCH(name,describtion,school_type,location) AGAINST('$type')");

            $result = schools::where('name', 'LIKE', '%'.$searchItem.'%')
                ->where('school_type', 'LIKE', '%'.$type.'%')
                ->where('ownership', 'LIKE', '%'.$ownership.'%')
                ->where('fees', '<', '%'.$fees.'%')
                ->where('location', 'LIKE', '%'.$location.'%')->get();

//		$result=schools::where([
//
//			['ownership', $ownership],
//
//			['location',$location],
//
//			['school_type',$type],
//
//			])
//
//		->orwhere([
//
//			['ownership', $ownership],
//
//			['location',$location]
//
//			])
//
//		->orwhere('fees',$fees)
//
//		->union($first)
//
//		->get();

        if(count($result)>0){

        $count=count($result);

		return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'criteria'=>$criteria,'count'=>$count, 'searchLocation' => $location));	

		}

		else{

			return view('schools')->with(array('msg'=>'No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria, 'searchLocation' => $location));

		}

		}

		}





}

