<?php

namespace App\Http\Controllers;

use App\schools;
use App\posts;
//use App\categories;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;
//use App\School;

class schoolsController extends Controller
{		
    public function fileimport()
    {
        $schools = Excel::selectSheetsByIndex(0)->load('collegec_database_digitalocean.xls')->get();
        // $schools = Excel::selectSheetsByIndex(0)->load('excel/testerror/collegec_database_digitalocean_test.xlsx')->get();

        $success = $fail = "";
        $success_count = $fail_count = 0;

        foreach ($schools as $school) {

          $slug = str_slug($school->name);
          $url = 'schools/' . $slug;
          $logo = $slug . '.jpg';
          
          try {
            schools::updateOrCreate(
                ['id' => $school->id],
                [
                  'id' => $school->id,
                  'name' => $school->name ?: '',
                  'describtion' => $school->describtion ?: '',
                  'url' => $url,
                  'location' => $school->location ?: '',
                  'fees' => $school->fees ?: '',
                  'logo' => $logo,
                  'undergradcourses' => $school->undergradcourses ?: '',
                  'postgradcourses' => $school->postgradcourses ?: '',
                  'shortcoursesandcertifications' => $school->shortcoursesandcertifications ?: '',
                  'technicalandvocationalcourses' => $school->technicalandvocationalcourses ?: '',
                  'website' => $school->website ?: '',
                  'phone' => $school->phone ?: '',
                  'email' => $school->email ?: ''
                ]
            );

            $success .= "<li>" . $school->name . "</li>";
            $success_count++;
          } catch (\Exception $e) {
            $fail .= "<li>" . $school->name . "<br>" . $e->getMessage() . "</li>";
            $fail_count++;
          }
        }

        echo $success_count . " inputted successfully:<br><ul>" . $success . "</ul>";
        echo $fail_count . " not inputted due to errors:<br><ul>" . $fail . "</ul>";

        // return view('admin-SchoolDir/fileimport', compact('schools'))
        //      ->with("title","Admin-Fileimport");
    }

		//all posts route
		public function allschools(){
		$sch=schools::All();
	      if(count($sch)==0){
	        return redirect()->back()->with('message', 'There are no posts at this moment');
	      }
	      else{
	        return view ('admin-SchoolDir.allschools',array('sch' => $sch))->with("title","Admin-Allschools");
	      }
		}

		//add post route		
		public function addschool(){			
		    return view ('admin-SchoolDir/addschool')->with("title","Admin-Addschool");		
		}		
			//save post to DB route
		public function saveschool(Request $request){

          $this->validate($request, [
        'thumbnail' => 'mimes:jpg,jpeg,png|max:5000',
        'logo' => 'mimes:jpg,jpeg,png|max:5000',
        ]);

          //thumbnail
          $img=$request->file('thumbnail');
          $upload_dir='images';
          if(empty($img)){
            $image="";
          }
          else{
          $image=$img->getClientOriginalName();
          $move=$img->move($upload_dir, $image);
          }
          //end thumbnail process

          //logo
          $log=$request->file('logo');
          $upload_dir='images';
          if(empty($log)){
            $logo="";
          }
          else{
          $logo=$log->getClientOriginalName();
          $move=$log->move($upload_dir, $logo);
          }
          //end logo process

	    		$sch = new schools();
		    	$sname=$request['sname'];
          $describtion=$request['describtion'];
          $keywords=$request['keywords'];
          $fees=$request['fees'];
          $type=$request['type'];
          $ownership=$request['ownership'];
          $location=$request['location'];
		  
		  $sch->undergradcourses=$request['undergradcourses'];
		  $sch->postgradcourses=$request['postgradcourses'];
		  $sch->shortcoursesandcertifications=$request['shortcoursesandcertifications'];
		  $sch->technicalandvocationalcourses=$request['technicalandvocationalcourses'];          
		  $sch->email=$request['email'];          
		  $sch->phone=$request['phone'];          
		  $sch->website=$request['website'];          
		  $sch->founded_at=$request['year'];          
		  $sch->rating_africa=$request['rating_africa'];          
		  $sch->rating_nigeria=$request['rating_nigeria'];		    	
		  $sch->entry_requirement=$request['entry_requirement'];		    	
		  $sch->save();		    	
		  return redirect()->back()		    	
		  ->with('message', 'School Added Sucessful');    	    
		  }    

    //Edit school route
    public function editschool($id){
         return view('admin-SchoolDir.editschool')
         ->with(array('sch' => schools::where('id', '=', $id)->first(),'title' =>'Update Scool'));
    }

    //update school in DB
    public function updateschool(Request $request){
    
      $this->validate($request, [
        'thumbnail' => 'mimes:jpg,jpeg,png|max:5000',
        'logo' => 'mimes:jpg,jpeg,png|max:5000',
        ]);

        if(is_null($request->file('thumbnail'))){
          $p=schools::where('id', '=', $request['id'])->first();
          $image=$p->thumbnail;
        }
        else
        {
          $img=$request->file('thumbnail');
          $upload_dir='images';
          $image=$img->getClientOriginalName();
          $move=$img->move($upload_dir, $image);
          //end thumbnail process 
        }
        //process logo
        if(is_null($request->file('logo'))){
          $p=schools::where('id', '=', $request['id'])->first();
          $logo=$p->logo;
        }
        else
        {
          $log=$request->file('logo');
          $upload_dir='images';
          $logo=$log->getClientOriginalName();
          $move=$log->move($upload_dir, $logo);
          //end logo process 
        }

    $sch = new schools();
		$sname=$request['sname'];
		$describtion=$request['describtion'];
    $keywords=$request['keywords'];
    $fees=$request['fees'];
    $type=$request['type'];
    $ownership=$request['ownership'];
		$location=$request['location'];

        //process url
        $find=array(",","."," ");
        $replace=array("","","-");
        $titleurl = str_replace($find,$replace, $sname);
        $url='schools/'.strtolower($titleurl);
        $check=schools::where('url', '=', $url)->first();
        if(count($check)>1){
          return redirect()->back()
            ->with('message', 'Looks like there is a similar school. please review post');
        }
        //end url process

        $check=schools::where('id', $request['id'])
        ->update([
          'name' => $sname,
          'thumbnail' =>$image, 
          'logo' =>$logo, 
          'describtion' =>$describtion, 
          'keywords' =>$keywords,  
          'location' =>$location,  
          'school_type' =>$type,  
          'ownership' =>$ownership,  
          'fees' =>$fees,  
          'url' =>$url,              
          'undergradcourses' =>$request['undergradcourses'],  
		  'postgradcourses' =>$request['postgradcourses'], 
		  'shortcoursesandcertifications' =>$request['shortcoursesandcertifications'], 
		  'technicalandvocationalcourses' =>$request['technicalandvocationalcourses'],           
		  'email' =>$request['email'],            
		  'phone' =>$request['phone'],            
		  'website' =>$request['website'],            
		  'founded_at' =>$request['year'],            
		  'entry_requirement' =>$request['entry_requirement'],            
		  'rating_nigeria' =>$request['rating_nigeria'],            
		  'rating_africa' =>$request['rating_africa'],            
		  ]);        
		  return redirect()->back()        
		  ->with('message', 'School updated Sucessful');    
		  }    
//delete post
    public function deleteschool($id){
         schools::where('id', $id)
          ->update(['status' => 'trashed']);
          return redirect()->back()->with('message','School was trashed successful'); 
    }

    //restore deleted post
    public function restoreschool($id){
         schools::where('id', $id)
          ->update(['status' => '']);
          return redirect()->back()->with('message','School restored successful'); 
    }

    //view school
    public function viewschool($url){
      $url ='schools/'.$url;
    	if(count(schools::where('url', '=', $url)->first())<>1){
    		return redirect()->back();
    	}
    $sch=schools::where('url', '=', $url)->first();
    $articles=posts::where('post_type','article')->where('school',$sch->name)->take(3)->get();
    $news=posts::where('post_type','news')->where('school',$sch->name)->take(3)->get();
    $others=posts::where('post_type','news')->take(3)->get();
		return view('school-info')
    	->with(array('sch' => $sch,'school_articles'=>$articles,'school_news'=>$news,'others'=>$others));
    }
}