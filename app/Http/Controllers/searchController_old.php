<?php

namespace App\Http\Controllers;

use App\posts;

use App\categories;

use App\schools;

use DB;

use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Routing\Controller as BaseController;

use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;

class searchController extends Controller
{
	protected $result = [];

	public function articlesearch(Request $request){

		$searchItem=$request['searchItem'];
		$searchLocation=$request['searchLocation'];

		$criteria=$request['criteria'];

		if($criteria == 'articles'){

			$result=posts::whereRaw("MATCH(title,content,keywords) AGAINST('$searchItem')")->get();

			if(count($result)>0){

				$count=count($result);

				return view('articles', array('result' => $result))->with(array('searchItem'=>$searchItem,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);	

			}

			else{

				return view('articles')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria));

			}

		}

	}

	public function postsearch(Request $request)
	{
		$searchItem=$request['searchItem'];
		$searchLocation=$request['searchLocation'];

		$criteria=$request['criteria'];
		$search = "";
		if($request['search'] == "Search Colleges"){
			$search = "SearchColleges";
		}else{
			$search = "SearchCourses";
		}

		$fees = schools::where('status','')
		->orderBy('fees', 'asc')
		->get()
		->pluck('fees')
		->toArray();
		
		$fees = array_unique($fees);
		$priceRanges = array();

		$minPrice = 0;
		$maxPrice = 0;
		if(count($fees) > 0){
			foreach ($fees as $key => $value) {
				$range = explode(' - ', $value);
				$priceRanges[] = $range['0'];
				$priceRanges[] = $range['1'];
			}
			$priceRanges = array_unique($priceRanges);
			$minPrice = (int) min($priceRanges);
			$maxPrice = (int) max($priceRanges);
		}

		if($criteria == 'schools'){

		//		 $result=schools::whereRaw("MATCH(name,describtion) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
			$result=schools::where('name', 'LIKE', '%'.$searchItem.'%')
			->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')
			->orderBy('name', 'asc')
			->get();


			$this->result = $result;

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria,'count'=>$count,'search'=>$search))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));

			}

		}



		if($criteria == 'articles'){

			$result=posts::whereRaw("MATCH(title,content,keywords) AGAINST('$searchItem')")->get();

			if(count($result)>0){

				$count=count($result);

				return view('articles', array('result' => $result))->with(array('searchItem'=>$searchItem,

					'criteria'=>$criteria,'count'=>$count,'title' => 'SchoolDir Articles','search'=>$search,

					'cat' => categories::where('status','')->get()))

				->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('articles')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria,'search'=>$search,'title' => 'SchoolDir Articles'))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));

			}

		}



		if($criteria == 'bachelorsdegree'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(undergradcourses) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();

		//		$result=schools::where('undergradcourses', 'LIKE', '__'.$searchItem.'__')
		//                 ->orWhere('location', 'LIKE', '__'.$searchLocation.'__')
		//                 ->where('status','')->get();
			$result=schools::where('undergradcourses', 'LIKE', '%'.$searchItem.'%')
			->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')->orderBy('name', 'asc')->get();

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search,'count'=>$count))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'search'=>$search,'criteria'=>$criteria))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));
			}

		}



		if($criteria == 'mastersdegree'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(postgradcourses) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
		//		$result=schools::where('postgradcourses', 'LIKE', '___'.$searchItem.'___')
		//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
		//                 ->where('status','')->get();
			$result=schools::where('postgradcourses', 'LIKE', '%'.$searchItem.'%')
			->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')->orderBy('name', 'asc')->get();

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search,'count'=>$count))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'search'=>$search,'criteria'=>$criteria))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));
			}

		}



		if($criteria == 'technicalvocational'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(technicalandvocationalcourses) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
		//		$result=schools::where('technicalandvocationalcourses', 'LIKE', '___'.$searchItem.'___')
		//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
		//                 ->where('status','')->get();
			$result=schools::where('technicalandvocationalcourses', 'LIKE', '%'.$searchItem.'%')
			->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')->orderBy('name', 'asc')->get();

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search,'count'=>$count))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'search'=>$search,'criteria'=>$criteria))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));
			}

		}



		if($criteria == 'certificateshortcourse'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(shortcoursesandcertifications) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
		//		$result=schools::where('shortcoursesandcertifications', 'LIKE', '___'.$searchItem.'___')
		//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
		//                 ->where('status','')->get();
			$result=schools::where('shortcoursesandcertifications', 'LIKE', '%'.$searchItem.'%')
			->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')->orderBy('name', 'asc')->get();

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search,'count'=>$count))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'search'=>$search,'criteria'=>$criteria))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));
			}

		}

	}	

	//sort search

	public function sortsearch(Request $request){

		$input = $request->all(); 
		
		$fees = schools::where('status','')
		->orderBy('fees', 'asc')
		->get()
		->pluck('fees')
		->toArray();

		 
		$ranking_philippines = schools::where('status','')
		->orderBy('ranking_philippines', 'asc')
		->get()
		->pluck('ranking_philippines')
		->toArray();

		$fees = array_unique($fees);
		$priceRanges = array();
		$minPrice = 0;
		$maxPrice = 0;
		if(count($fees) > 0){
			foreach ($fees as $key => $value) {
				$range = explode(' - ', $value);
				$priceRanges[] = $range['0'];
				$priceRanges[] = $range['1'];
			}
			$priceRanges = array_unique($priceRanges);
			$minPrice = (int) min($priceRanges) ?? 0;
			$maxPrice = (int) max($priceRanges) ?? 0;
			// dd($maxPrice);
		}

		$ranking_philippines = array_unique($ranking_philippines);
		$rankings = array();
		$minRank = 0;
		$maxRank = 0;
		if(count($ranking_philippines) > 0){
			foreach ($ranking_philippines as $key => $value) {
				$range = explode("  ", $value);
				$rankings[] = $range['0'];
				$rankings[] = isset($range['1']) ? $range['1'] : null;
			}
			$rankings = array_unique($rankings);
			$minRank = (int) min($rankings) ?? 0;
			$maxRank = (int) max($rankings) ?? 0;
			//dd($maxPrice);
		}

		$searchItem=$request['searchItem'];
		$searchLocation=$request['searchLocation'];

		$criteria=$request['criteria'];

		$ownership=$request['ownership'];

		$Ranges = array();
		if (isset($request['FeesRange']) && !empty($request['FeesRange'])) {
			$Ranges = explode(' - ',$request['FeesRange']);
		}else{
			$Ranges = explode(' - ',$request['fees']);
		}

		$Ranking = array();
		if (isset($request['PhilippinesRanking']) && !empty($request['PhilippinesRanking'])) {
			$Ranking = explode(" ",$request['PhilippinesRanking']);
		}else{
			$Ranking = explode(" ",$request['ranking_philippines']);
		}

		$returnData['Ranges'] = $Ranges;
		$returnData['Ranking'] = $Ranking;
		$returnData['sortby'] = $request['sortby'];
		$returnData['rankingsortby'] = $request['rankingsortby'];
		$returnData['PhilippinesRanking'] = $request['PhilippinesRanking'];
		$returnData['FeesRange'] = $request['FeesRange'];
		//dd($Rfees);
		$sortby=$request['sortby'];
		$rankingsortby=$request['rankingsortby'];
		$search=$request['search'];
		//dd($fees);
		//$type=$request['School_type'];

		$location=$request['location'];


		//	$first=schools::where("MATCH(name,describtion,school_type,location) AGAINST('$type')");
		$result = schools::where('location', 'LIKE', '%'.$searchLocation.'%');
		
		if($criteria == 'schools'){
			$result = $result->where('name', 'LIKE', '%'.$searchItem.'%');
		}elseif($criteria == 'bachelorsdegree'){
			$result = $result->where('undergradcourses', 'LIKE', '%'.$searchItem.'%');
		}elseif($criteria == 'mastersdegree'){
			$result = $result->where('postgradcourses', 'LIKE', '%'.$searchItem.'%');
		}elseif($criteria == 'technicalvocational'){
			$result = $result->where('technicalandvocationalcourses', 'LIKE', '%'.$searchItem.'%');
		}elseif($criteria == 'certificateshortcourse'){
			$result = $result->where('shortcoursesandcertifications', 'LIKE', '%'.$searchItem.'%');
		}

        if (sizeof($Ranges) > 1) {
		    $result = $result->where(DB::raw("SUBSTRING_INDEX(fees, ' - ', 1)"), '>=', (int) $Ranges[0])
                             ->where(DB::raw("SUBSTRING_INDEX(fees, ' - ', -1)"), '<=', (int) $Ranges[1]);
        }

		if (sizeof($Ranking) > 1) {
		    $result = $result->where(DB::raw("SUBSTRING_INDEX(fees, '  ', 1)"), '>=', (int) $Ranking[0])
                             ->where(DB::raw("SUBSTRING_INDEX(fees, '  ', -1)"), '<=', (int) $Ranking[1]);
		}

		if($ownership){
			$result = $result->whereIn('ownership',$ownership);
		}

		if($sortby == "LowtoHigh"){
			$result = $result->orderBy('fees', 'asc');
		}else{
			$result = $result->orderBy('fees', 'desc');
		}

		if($rankingsortby == "LowtoHigh"){
			$result = $result->orderBy('ranking_philippines', 'asc');
		}else{
			$result = $result->orderBy('ranking_philippines', 'desc');
		}

		$result = $result->paginate();


		if(count($result)>0){

			$count=count($result);

			return view('schools', [
		     	'result' => $result,
		     	'searchItem' => $searchItem, 
		     	'criteria' => $criteria,
		     	'count' => $count, 
		     	'searchLocation' => $location, 
		     	'search' => $search,
		     	'minPrice' => $minPrice,
		     	'maxPrice' => $maxPrice,
		     	'minRank' => $minRank,
		     	'maxRank' => $maxRank,
		     	'returnData' => $returnData
		    ]);

		}

		else{

			return view('schools', [
			 	'msg' => 'No result found',
			 	'count' => '0',
			 	'searchItem' => $searchItem,
			 	'criteria' => $criteria, 
			 	'searchLocation' => $location,
			 	'search' => $search,
		     	'minRank' => $minRank,
		     	'maxRank' => $maxRank,
			 	'returnData' => $returnData
			]);

		}
	}





}

