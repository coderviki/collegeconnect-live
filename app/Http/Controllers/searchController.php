<?php

namespace App\Http\Controllers;

use App\posts;

use App\categories;

use App\schools;

use DB;

use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Routing\Controller as BaseController;

use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;

class searchController extends Controller
{
	protected $result = [];

	public function articlesearch(Request $request){

		$searchItem=$request['searchItem'];
		$searchLocation=$request['searchLocation'];

		$criteria=$request['criteria'];

		if($criteria == 'articles'){

			$result=posts::whereRaw("MATCH(title,content,keywords) AGAINST('$searchItem')")->get();

			if(count($result)>0){

				$count=count($result);

				return view('articles', array('result' => $result))->with(array('searchItem'=>$searchItem,'criteria'=>$criteria,'count'=>$count))->with('searchItem',$searchItem);	

			}

			else{

				return view('articles')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria));

			}

		}

	}

	public function postsearch(Request $request)
	{
		$searchItem=$request['searchItem'];
		$searchLocation=$request['searchLocation'];

		$criteria=$request['criteria'];
		$search = "";
		if($request['search'] == "Search Colleges"){
			$search = "SearchColleges";
		}else{
			$search = "SearchCourses";
		}

		$fees = schools::where('status','')
		->orderBy('fees', 'asc')
		->get()
		->pluck('fees')
		->toArray();
		
		$fees = array_unique($fees);
		$priceRanges = array();

		$minPrice = 0;
		$maxPrice = 0;
		if(count($fees) > 0){
			foreach ($fees as $key => $value) {
				$range = explode(' - ', $value);
				$priceRanges[] = $range['0'];
				//$priceRanges[] = $range['1'];
			}
			$priceRanges = array_unique($priceRanges);
			$minPrice = (int) min($priceRanges);
			$maxPrice = (int) max($priceRanges);
		}

		if($criteria == 'schools'){

		//		 $result=schools::whereRaw("MATCH(name,describtion) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
			$result=schools::where('name', 'LIKE', '%'.$searchItem.'%')
		//	->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')
			->orderBy('name', 'asc')
			->get();


			$this->result = $result;

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria,'count'=>$count,'search'=>$search))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));

			}

		}



		if($criteria == 'articles'){

			$result=posts::whereRaw("MATCH(title,content,keywords) AGAINST('$searchItem')")->get();

			if(count($result)>0){

				$count=count($result);

				return view('articles', array('result' => $result))->with(array('searchItem'=>$searchItem,

					'criteria'=>$criteria,'count'=>$count,'title' => 'SchoolDir Articles','search'=>$search,

					'cat' => categories::where('status','')->get()))

				->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('articles')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'criteria'=>$criteria,'search'=>$search,'title' => 'SchoolDir Articles'))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));

			}

		}



		if($criteria == 'bachelorsdegree'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(undergradcourses) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();

		//		$result=schools::where('undergradcourses', 'LIKE', '__'.$searchItem.'__')
		//                 ->orWhere('location', 'LIKE', '__'.$searchLocation.'__')
		//                 ->where('status','')->get();
			$result=schools::where('undergradcourses', 'LIKE', '%'.$searchItem.'%')
		//	->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')->orderBy('name', 'asc')->get();

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search,'count'=>$count))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'search'=>$search,'criteria'=>$criteria))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));
			}

		}



		if($criteria == 'mastersdegree'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(postgradcourses) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
		//		$result=schools::where('postgradcourses', 'LIKE', '___'.$searchItem.'___')
		//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
		//                 ->where('status','')->get();
			$result=schools::where('postgradcourses', 'LIKE', '%'.$searchItem.'%')
		//	->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')->orderBy('name', 'asc')->get();

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search,'count'=>$count))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'search'=>$search,'criteria'=>$criteria))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));
			}

		}



		if($criteria == 'technicalvocational'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(technicalandvocationalcourses) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
		//		$result=schools::where('technicalandvocationalcourses', 'LIKE', '___'.$searchItem.'___')
		//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
		//                 ->where('status','')->get();
			$result=schools::where('technicalandvocationalcourses', 'LIKE', '%'.$searchItem.'%')
		//	->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')->orderBy('name', 'asc')->get();

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search,'count'=>$count))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'search'=>$search,'criteria'=>$criteria))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));
			}

		}



		if($criteria == 'certificateshortcourse'){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(shortcoursesandcertifications) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
		//		$result=schools::where('shortcoursesandcertifications', 'LIKE', '___'.$searchItem.'___')
		//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
		//                 ->where('status','')->get();
			$result=schools::where('shortcoursesandcertifications', 'LIKE', '%'.$searchItem.'%')
		//	->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')->orderBy('name', 'asc')->get();

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search,'count'=>$count))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'search'=>$search,'criteria'=>$criteria))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));
			}

		}

		if($criteria == ''){

		//return redirect()->back();	
		//$result=schools::whereRaw("MATCH(shortcoursesandcertifications) AGAINST('$searchItem') AND MATCH(location) AGAINST('$searchLocation')")->where('status','')->get();
		//		$result=schools::where('shortcoursesandcertifications', 'LIKE', '___'.$searchItem.'___')
		//                 ->orWhere('location', 'LIKE', '___'.$searchLocation.'___')
		//                 ->where('status','')->get();
			$result=schools::where()
		//	->where('location', 'LIKE', '%'.$searchLocation.'%')
			->where('status','')->orderBy('name', 'asc')->get();

			if(count($result)>0){

				$count=count($result);

				return view('schools', array('result' => $result))->with(array('searchItem'=>$searchItem, 'searchLocation'=>$searchLocation,'criteria'=>$criteria,'search'=>$search,'count'=>$count))->with('searchItem',$searchItem)->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));	

			}

			else{

				return view('schools')->with(array('msg'=>'Oops, No result found','count'=>'0','searchItem'=>$searchItem,'searchLocation'=>$searchLocation,'search'=>$search,'criteria'=>$criteria))->with(array('minPrice' =>$minPrice,'maxPrice'=>$maxPrice));
			}

		}

	}	

	//sort search

	public function sortsearch(Request $request){

		

		$input = $request->all();
		
		$fees = schools::where('status','')
		->orderBy('fees', 'asc')
		->get()
		->pluck('fees')
		->toArray();

		 
		$ranking_philippines = schools::where('status','')
		->orderBy('ranking_philippines', 'asc')
		->get()
		->pluck('ranking_philippines')
		->toArray();

		$fees = array_unique($fees);
		$priceRanges = array();
		$minPrice = 0;
		$maxPrice = 0;

		if(count($fees) > 0){
			foreach ($fees as $key => $value) {
				$range = explode(' - ', $value);
				$priceRanges[] = $range['0'];
				//$priceRanges[] = $range['1'];
			}
			$priceRanges = array_unique($priceRanges);
			$minPrice = (int) (min($priceRanges)?min($priceRanges):0);
			$maxPrice = (int) (max($priceRanges)?max($priceRanges):0);			
		}

		$ranking_philippines = array_unique($ranking_philippines);
		$rankings = array();
		$minRank = 0;
		$maxRank = 0;

		if(count($ranking_philippines) > 0){
			foreach ($ranking_philippines as $key => $value) {
				$range = explode("  ", $value);
				$rankings[] = $range['0'];
				$rankings[] = isset($range['1']) ? $range['1'] : null;
			}
			$rankings = array_unique($rankings);
			$minRank = (int) (min($rankings)?min($rankings):0);
			$maxRank = (int) (max($rankings)?max($rankings):0);                        						
		}



		##--- Search begins
		$searchItem = str_replace("%", " ", $request['searchItem']);

		$searchLocation = $request['searchLocation'];

		$criteria = $request['criteria'];

		$ownership = $request['ownership'];
		//$centers_of_excellence=str_replace(" ", "%", $request['centers_of_excellence']);
				// changed by Suraj k 3-4-18;
		// center of ex is an array in view/ filters; please see the view file;
		$coe = $request['centers_of_excellence'];
		$centers_of_excellence = "";
		if(is_array($coe) && isset($coe[0])){
			$centers_of_excellence = str_replace(" ", "%", $coe[0]);
		}
		##--
		

		$city = $request['city']; // array in view file
		$type = $request['type']; // array in view file
		$term_structure = $request['term_structure']; // array in view file



		$Ranges = array();
		if (isset($request['FeesRange']) && !empty($request['FeesRange'])) {
			$Ranges = explode(' - ',$request['FeesRange']);
		}else{
			$Ranges = explode(' - ',$request['fees']);
		}

		$Ranking = array();
		if (isset($request['PhilippinesRanking']) && !empty($request['PhilippinesRanking'])) {
			$Ranking = explode(" ",$request['PhilippinesRanking']);
		}else{
			$Ranking = explode(" ",$request['ranking_philippines']);
		}

		$returnData['Ranges'] = $Ranges;
		$returnData['Ranking'] = $Ranking;
		$returnData['sortby'] = $request['sortby'];
		$returnData['rankingsortby'] = $request['rankingsortby'];
		$returnData['PhilippinesRanking'] = $request['PhilippinesRanking'];
		$returnData['FeesRange'] = $request['FeesRange'];
		//dd($Rfees);
		$sortby=isset($request['sortby'])?trim($request['sortby']):'';
		$rankingsortby=isset($request['rankingsortby'])?trim($request['rankingsortby']):'';
		$search=$request['search'];
		//dd($fees);
		//$type=$request['School_type'];

		//$location=$request['location'];		
		//$result = schools::where('city', 'LIKE', '%'.$searchLocation.'%');
		//dd($result);
		//$result = schools::where('location', 'LIKE', '%' . $city . '%')->orwhere('name', 'LIKE', '%' . $searchItem . '%')->orwhere('describtion', 'LIKE', '%' . $searchItem . '%');

		// changed by Suraj k. 4 Apr 2018
		$result = NULL;

		if ($criteria == 'schools') {
			$result = schools::where('name', 'LIKE', '%' . $searchItem . '%');
		} elseif ($criteria == 'bachelorsdegree') {
			$result = schools::where('undergradcourses', 'LIKE', '%' . $searchItem . '%');
		} elseif ($criteria == 'mastersdegree') {
			$result = schools::where('postgradcourses', 'LIKE', '%' . $searchItem . '%');
		} elseif ($criteria == 'technicalvocational') {
			$result = schools::where('technicalandvocationalcourses', 'LIKE', '%' . $searchItem . '%');
		} elseif ($criteria == 'certificateshortcourse') {
			$result = schools::where('shortcoursesandcertifications', 'LIKE', '%' . $searchItem . '%');
		} elseif ($criteria == '') {
			$result = schools::where();
		}


		if (!empty($searchLocation)) {

			$result = $result->where('city', 'LIKE', '%' . $searchLocation . '%');

		}
		elseif(!empty($city)) {

			$result = $result->where('city', 'LIKE', '%' . $city . '%');

		}


		if (!empty($type)) {

			$result = $result->where('type', 'LIKE', '%' . $type . '%');

		}

		if (!empty($term_structure)) {

			$result = $result->where('term_structure', 'LIKE', '%' . $term_structure . '%');

		}

		##-- changed by Suraj k. 4 Apr 2018

                if (sizeof($Ranges) > 1) {
                            $result = $result->where(DB::raw("SUBSTRING_INDEX(fees, ' - ', 1)"), '>=', (int) $Ranges[0])
                                     ->where(DB::raw("SUBSTRING_INDEX(fees, ' - ', -1)"), '<=', (int) $Ranges[1]);
                }

		if (sizeof($Ranking) > 1) {
		    $result = $result->where(DB::raw("SUBSTRING_INDEX(fees, '  ', 1)"), '>=', (int) $Ranking[0])
                             ->where(DB::raw("SUBSTRING_INDEX(fees, '  ', -1)"), '<=', (int) $Ranking[1]);
		}

		if($ownership){

			$result = $result->whereIn('ownership', $ownership);

		}
		//if($city){
		//	$result = $result->whereIn('city',$city);
		//}
		// changed by suraj k 4 apr 18
		if($criteria == $centers_of_excellence && !empty($centers_of_excellence) ){

			$result = $result->whereRaw("( centers_of_excellence LIKE '%$centers_of_excellence%' ) ");

		}
		//if($type){
		//	$result = $result->whereIn('type',$type);
		//}
		//if($criteria == $term_structure){
		//	$result = $result->where('term_structure', '=', $term_structure);
		//}
		//if($term_structure){
		//	$result = $result->whereIn('term_structure',$term_structure);
		//}

		/*
		// changed by suraj k. 4 Apr 18
		if (is_array($city) && count($city) > 0) {

			
			$tq = array();

			foreach ($city as $c) {

				$tq[] = "city LIKE '%$c%'";
				break;
			}

			$c_str = implode(' OR ', $tq);

			if (!empty($c_str)) {

				$c_str = " (" . $c_str . ")";

				$result->whereRaw("$c_str");

			}

		}

		
		// changed by suraj k. 4 Apr 18
		if (is_array($type) && count($type) > 0) {

			$tq = array();

			foreach ($type as $ty) {

				$tq[] = "term_structure LIKE '%$ty%'";
				break;
			}

			$type_str = implode(' OR ', $tq);

			//dd($type_str);

			if (!empty($type_str)) {

				$type_str = " (" . $type_str . ")";

				$result->whereRaw("$type_str");

			}

		}

		// changed by suraj k. 4 Apr 18
		if (is_array($term_structure) && count($term_structure) > 0) {

			$tq = array();

			foreach ($term_structure as $ts) {

				$tq[] = "term_structure LIKE '%$ts%'";
				break;

			}

			$term_str = implode(' OR ', $tq);

			//dd($type_str);

			if(!empty($term_str)) {

				$term_str = " (". $term_str .")";

				$result->whereRaw("$term_str");

			}

		}
		*/
                
                if($sortby != ""){
                    if($sortby == "HightoLow"){
                            $result = $result->orderBy('fees', 'asc');
                    }else{
                            $result = $result->orderBy('fees', 'desc');
                    }
                }
                
                if($rankingsortby != ""){
                    if($rankingsortby == "HightoLow"){
                            //$result = $result->orderByRaw('convert(ranking_philippines,unsigned) asc');
                            $result = $result->orderByRaw('convert((CASE WHEN ranking_philippines IS NULL then 129999999999999 ELSE ranking_philippines END),unsigned) asc');
                    }else{
                            //$result = $result->orderByRaw('convert(ranking_philippines,unsigned) desc');
                            $result = $result->orderByRaw('convert((CASE WHEN ranking_philippines IS NULL then 0 ELSE ranking_philippines END),unsigned) desc');
                    }
                }
                
		$result = $result->get();



		if(count($result)>0){

			$count=count($result);

			return view('schools', [
		     	'result' => $result,
		     	'searchItem' => $searchItem,
		     	'criteria' => $criteria,
		     	'count' => $count, 
		     	'searchLocation' => $searchLocation,
		     	'search' => $search,
		     	'minPrice' => $minPrice,
		     	'maxPrice' => $maxPrice,
		     	'minRank' => $minRank,
		     	'maxRank' => $maxRank,
		     	'returnData' => $returnData
		    ]);

		}

		else{

			return view('schools', [
			 	'msg' => 'No result found',
			 	'count' => '0',
			 	'searchItem' => str_replace("%", " ", $searchItem),
			 	'criteria' => $criteria, 
			 	'searchLocation' => $searchLocation,
			 	'search' => $search,
		     	'minRank' => $minRank,
		     	'maxRank' => $maxRank,
			 	'returnData' => $returnData
			]);

		}
	}





}