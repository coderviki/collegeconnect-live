<?php



namespace App\Http\Controllers;



use App\posts;

use App\pages;

use App\categories;

use App\schools;

use App\subscriptions;

use DB;

use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Routing\Controller as BaseController;

use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;



use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class postsController extends Controller

{

		//all posts route

		public function allposts(){

		$post=posts::All();

	      if(count($post)==0){

	        return redirect()->back()->with('message', 'There are no posts at this moment');

	      }

	      else{

	        return view ('admin-SchoolDir.allposts',array('post' => $post))->with("title","Admin-Allposts");

	      }

		}



		//add post route

		public function addpost(){

			return view ('admin-SchoolDir/addpost')->with(array("title"=>"Admin-Addpost",

        'cat'=>categories::where('status','')->get(),

        'sch'=>schools::where('status','')->get()));

		}



		//save post to DB route

		public function savepost(Request $request){

    	$this->validate($request, [

    		'thumbnail' => 'mimes:jpg,jpeg,png|max:800',

    		'file1' => 'mimes:rar,zip|max:10000',

    		]);



    		//process thumbnail and files

    		//attachment1

    		$file1=$request->file('file1');

    		if(!empty($file1)){

    			$upload_dir='uploads';

    			$attmt1=$file1->getClientOriginalName();

    			$move=$file1->move($upload_dir, $attmt1);

    		}

        else{

          $attmt1="";

        }

    		//thumbnail

          $img=$request->file('thumbnail');

    		  $upload_dir='thumbnails';

          if(empty($img)){

            $image="";

          }

          else{

    		  $image=$img->getClientOriginalName();

    		  $move=$img->move($upload_dir, $image);

          }

    		  //end thumbnail process



	    		$posts = new posts();

		    	$title=$request['title'];

		    	$content=htmlentities($request['editor1']);

		    	$keywords=$request['keywords'];

		    	$video=$request['video'];

		    	$category=$request['category'];

		    	$type=$request['type'];

		    	$school=$request['school'];



		    	//process url

		    	$find=array(",","."," ");

		    	$replace=array("","","-");

		    	$titleurl = str_replace($find,$replace, $title);

				  $url='articles/'.strtolower($titleurl);

				  $check=posts::where('url', '=', $url)->first();

				  if(count($check)>0){

					   return redirect()->back()

			    	->with('message', 'Looks like there is a similar topic. please review post');

				  }

				  //end url process

		    	$posts->thumbnail=$image;

		    	$posts->title=$title;

		    	$posts->content=$content;

		    	$posts->url=$url;

		    	$posts->keywords=$keywords;

		    	$posts->video=$video;

		    	$posts->category=$category;

		    	$posts->post_type=$type;

		    	$posts->attachment=$attmt1;

		    	$posts->school=$school;

		    	$posts->save();

}

    //all pages route

    public function allpages(){

    $page=pages::All();

        if(count($page)==0){

          return redirect()->back()->with('message', 'There are no pages at this moment');

        }

        else{

          return view ('admin-SchoolDir.allpages',array('page' => $page))->with("title","Admin-Allpages");

        }

    }



    //add page route

    public function addpage(){

      return view ('admin-SchoolDir/addpage')->with(array("title"=>"Admin-Addpage",

        'cat'=>categories::where('status','')->get(),

        //'sch'=>schools::where('status','')->get()
      ));

    }



    //save page to DB route

    public function savepage(Request $request){

      $this->validate($request, [

        'thumbnail' => 'mimes:jpg,jpeg,png|max:800',

        'file1' => 'mimes:rar,zip|max:10000',

        ]);



        //process thumbnail and files

        //attachment1

        $file1=$request->file('file1');

        if(!empty($file1)){

          $upload_dir='uploads';

          $attmt1=$file1->getClientOriginalName();

          $move=$file1->move($upload_dir, $attmt1);

        }

        else{

          $attmt1="";

        }

        //thumbnail

          $img=$request->file('thumbnail');

          $upload_dir='thumbnails';

          if(empty($img)){

            $image="";

          }

          else{

          $image=$img->getClientOriginalName();

          $move=$img->move($upload_dir, $image);

          }

          //end thumbnail process



          $pages = new pages();

          $title=$request['title'];

          $content=htmlentities($request['editor1']);

          $keywords=$request['keywords'];

          $video=$request['video'];

          $category=$request['category'];

          $type=$request['type'];

          //$school=$request['school'];



          //process url

          $find=array(",","."," ");

          $replace=array("","","-");

          $titleurl = str_replace($find,$replace, $title);

          //$url='articles/'.strtolower($titleurl);
          $url='/'.strtolower($titleurl);

          $check=pages::where('url', '=', $url)->first();

          if(count($check)>0){

             return redirect()->back()

            ->with('message', 'Looks like there is a similar topic. please review page');

          }

          //end url process

          $pages->thumbnail=$image;

          $pages->title=$title;

          $pages->content=$content;

          $pages->url=$url;

          $pages->keywords=$keywords;

          $pages->video=$video;

          $pages->category=$category;

          $pages->page_type=$type;

          $pages->attachment=$attmt1;

          //$pages->school=$school;

          $pages->save();



          //prepare email sending

          $emails=subscriptions::All();

          foreach ($emails as $email) {

            $email=$email->email;

            $to = $email;

            $subject = "SchoolDir Update";

            $msg = substr(wordwrap($content,70),0,350);

            $headers = "From: SchoolDir"."\r\n";

            //send email

            mail($to,$subject,$msg,$headers);

          }



		    	return redirect()->back()

		    	->with('message', 'Post created Sucessful');

    	

    }



    //Edit post route

    public function editpost($id){

         return view('admin-SchoolDir.editpost')

         ->with(array('post' => posts::where('id', '=', $id)->first(),'title' =>'Update Post',

          'cat'=>categories::where('status','')->get(),

          'sch'=>schools::where('status','')->get()));

    }



    //update post in DB

    public function updatepost(Request $request){

        $this->validate($request, [

        'thumbnail' => 'mimes:jpg,jpeg,png|max:800',

        'file1' => 'mimes:rar,zip|max:10000',

        ]);



        $file1=$request->file('file1');

        if(is_null($request->file('file1'))){

          $p=posts::where('id', '=', $request['id'])->first();

          $attmt1=$p->attachment;

        }

        else

        {

        $upload_dir='uploads';

    	$attmt1=$file1->getClientOriginalName();

    	$move=$file1->move($upload_dir, $attmt1);

        }



        if(is_null($request->file('thumbnail'))){

          $p=posts::where('id', '=', $request['id'])->first();

          $image=$p->thumbnail;

        }

        else

        {

          $img=$request->file('thumbnail');

          $upload_dir='thumbnails';

          $image=$img->getClientOriginalName();

          $move=$img->move($upload_dir, $image);

          //end thumbnail process 

        }

        $posts = new posts();

		$title=$request['title'];

		$content=htmlentities($request['editor1']);

		$keywords=$request['keywords'];

		$video=$request['video'];

		$category=$request['category'];

		$type=$request['type'];

		$school=$request['school'];



        //process url

        $find=array(",","."," ");

        $replace=array("","","-");

        $titleurl = str_replace($find,$replace, $title);

       $url='articles/'.strtolower($titleurl);

        $check=posts::where('url', '=', $url)->first();

        if(count($check)>1){

          return redirect()->back()

            ->with('message', 'Looks like there is a similar topic. please review post');

        }

        //end url process



        $check=posts::where('id', $request['id'])

        ->update([

          'title' => $title,

          'content' =>$content, 

          'keywords' =>$keywords, 

          'video' =>$video, 

          'category' =>$category, 

          'thumbnail' =>$image, 

          'post_type' =>$type, 

          'url' =>$url, 

          'school' =>$school, 

          'attachment' =>$attmt1, 

          ]);

        return redirect()->back()

        ->with('message', 'Post updated Sucessful');

    }



    //delete post

    public function deletepost($id){

         posts::where('id', $id)

          ->update(['post_status' => 'trashed']);

          return redirect()->back()->with('message','Post was trashed successful'); 

    }



    //restore deleted post

    public function restorepost($id){

         posts::where('id', $id)

          ->update(['post_status' => '']);

          return redirect()->back()->with('message','Post restored successful'); 

    }



    //view post

    public function viewpost($url){

      $url ='articles/'.$url;

    	if(count(posts::where('url', '=', $url)->first())<>1){

    		return redirect()->back();

    	}

		return view('viewpost')

    	->with('post', posts::where('url', '=', $url)->first());

    }





//Edit page route

    public function editpage($id){

         return view('admin-SchoolDir.editpage')

         ->with(array('page' => pages::where('id', '=', $id)->first(),'title' =>'Update Page',

          'cat'=>categories::where('status','')->get(),

         // 'sch'=>schools::where('status','')->get()
        ));

    }



    //update page in DB

    public function updatepage(Request $request){

        $this->validate($request, [

        'thumbnail' => 'mimes:jpg,jpeg,png|max:800',

        'file1' => 'mimes:rar,zip|max:10000',

        ]);



        $file1=$request->file('file1');

        if(is_null($request->file('file1'))){

          $p=pages::where('id', '=', $request['id'])->first();

          $attmt1=$p->attachment;

        }

        else

        {

        $upload_dir='uploads';

      $attmt1=$file1->getClientOriginalName();

      $move=$file1->move($upload_dir, $attmt1);

        }



        if(is_null($request->file('thumbnail'))){

          $p=pages::where('id', '=', $request['id'])->first();

          $image=$p->thumbnail;

        }

        else

        {

          $img=$request->file('thumbnail');

          $upload_dir='thumbnails';

          $image=$img->getClientOriginalName();

          $move=$img->move($upload_dir, $image);

          //end thumbnail process 

        }

        $pages = new pages();

    $title=$request['title'];

    $content=htmlentities($request['editor1']);

    $keywords=$request['keywords'];

    $video=$request['video'];

    $category=$request['category'];

    $type=$request['type'];

    //$school=$request['school'];



        //process url

        $find=array(",","."," ");

        $replace=array("","","-");

        $titleurl = str_replace($find,$replace, $title);

        //$url='articles/'.strtolower($titleurl);
        $url= '/'.strtolower($titleurl);

        $check=pages::where('url', '=', $url)->first();

        if(count($check)>1){

          return redirect()->back()

            ->with('message', 'Looks like there is a similar topic. please review page');

        }

        //end url process



        $check=pages::where('id', $request['id'])

        ->update([

          'title' => $title,

          'content' =>$content, 

          'keywords' =>$keywords, 

          'video' =>$video, 

          'category' =>$category, 

          'thumbnail' =>$image, 

          'page_type' =>$type, 

          'url' =>$url, 

        //  'school' =>$school, 

          'attachment' =>$attmt1, 

          ]);

        return redirect()->back()

        ->with('message', 'Page updated Sucessful');

    }



    //delete page

    public function deletepage($id){

         pages::where('id', $id)

          ->update(['page_status' => 'trashed']);

          return redirect()->back()->with('message','Page was trashed successful'); 

    }



    //restore deleted page

    public function restorepage($id){

         pages::where('id', $id)

          ->update(['page_status' => '']);

          return redirect()->back()->with('message','Page restored successful'); 

    }



    //view page

    public function viewpage($url){

      //$url ='articles/'.$url;
      $url = '/'.$url;

      if(count(pages::where('url', '=', $url)->first())<>1){

        return redirect()->back();

      }

    return view('viewpage')

      ->with('page', pages::where('url', '=', $url)->first());

    }


    //sendmail

    public function sendmail(Request $request){

      if($request['id']<>'all'){

          //prepare email sending

          $content=$request['message'];

          $mail=subscriptions::where('id',$request['id'])->first();

            $email=$mail->email;

            $to = $email;

            $subject = "SchoolDir Update";

            $msg = substr(wordwrap($content,70),0,350);

            $headers = "From: SchoolDir"."\r\n";

            //send email

            mail($to,$subject,$msg,$headers);

            return redirect()->back()

          ->with('message', 'Message Sent Sucessful');

      }else{

          //prepare email sending

          $content=$request['message'];

          $emails=subscriptions::All();

          foreach ($emails as $email) {

            $email=$email->email;

            $to = $email;

            $subject = "SchoolDir Update";

            $msg = substr(wordwrap($content,70),0,350);

            $headers = "From: SchoolDir"."\r\n";

            //send email

            mail($to,$subject,$msg,$headers);

            return redirect()->back()

          ->with('message', 'Message Sent Sucessful');

          }

      }

    } 

}