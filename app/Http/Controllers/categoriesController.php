<?php
namespace App\Http\Controllers;

use App\posts;
use App\schools;
use App\categories;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
class categoriesController extends Controller
{

	public function allcategories(){
		$cat=categories::All();
	    if(count($cat)==0){
	       return redirect()->back()->with('message', 'There are no categories at this moment');
	    }
	    else{
	       return view ('admin-SchoolDir.allcategories',array('cat' => $cat))->with("title","Admin-Allcategories");
	    }
	}

	public function addcategory(){
		return view ('admin-SchoolDir/addcategory')->with("title","Admin-Addcategory");
	}

	public function savecategory(Request $request){
          $cat = new categories();

          $find=array(",","."," ");
		  $replace=array("","","-");
		  $titleurl = str_replace($find,$replace, $request['name']);
		  $url='articles/category/'.strtolower($titleurl);

          $cat->name=$request['name'];
          $cat->description=$request['description'];
          $cat->url=$url;
          $cat->save();
          return redirect()->back()
          ->with('message', 'Category created Sucessful');
    }

    //delete category
    public function deletecat($id){
         categories::where('id', $id)
          ->update(['status' => 'trashed']);
          return redirect()->back()->with('message','Category was trashed successful'); 
    }

    //restore deleted category
    public function restorecat($id){
         categories::where('id', $id)
          ->update(['status' => '']);
          return redirect()->back()->with('message','Category restored successful'); 
    }

    //Edit category route
    public function editcat($id){
         return view('admin-SchoolDir.editcategory')
         ->with(array('cat' => categories::where('id', '=', $id)->first(),'title' =>'Update Category'));
    }

    //update category in DB
    public function updatecat(Request $request){

   		$cat = new categories();
        //process url
        $find=array(",","."," ");
        $replace=array("","","-");
        $titleurl = str_replace($find,$replace, $request['name']);
        $url='articles/category/'.strtolower($titleurl);
        $check=categories::where('url', '==', $url)->first();
        if(count($check)>1){
          return redirect()->back()
            ->with('message', 'Looks like there is a similar category. please review post');
        }
        //end url process

        $check=categories::where('id', $request['id'])
        ->update([
          'name' => $request['name'],
          'description' => $request['description'], 
          'url' =>$url,  
          ]);
        return redirect()->back()
        ->with('message', 'Category updated Sucessful');
    }

}
