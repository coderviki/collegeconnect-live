<?php
define("DATABASE_SERVER", 'localhost');
define("DATABASE_USERNAME", 'univers1_ss');
define("DATABASE_PASSWORD", 'cZp&*kKIp]ML');
define("DATABASE_NAME", 'univers1_ss');

class UniHubDB extends PDO
{

	public function __construct()
	{
		$dsn = 'mysql:host='.DATABASE_SERVER.';dbname='.DATABASE_NAME.';charset=utf8mb4';
		parent::__construct($dsn, DATABASE_USERNAME, DATABASE_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	}

	public function GetRootQualificationTypes()
	{
		$result		 = array();
		$statement	 = $this->prepare("
					SELECT `QualificationType`.ID, `Parent`.`Name` AS `ParentName`, `QualificationType`.`Name` AS `Name`
					FROM  `QualificationType`
					LEFT JOIN  `QualificationType` AS  `Parent` ON  `QualificationType`.`ParentID` =  `Parent`.`ID`
					WHERE  `Parent`.`ParentID` =0
					ORDER BY  `Parent`.`Name` ,  `QualificationType`.`Name`
			");
		if ($statement->execute()) {
			if ($statement->rowCount() > 0) {
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
					$result[$row['ParentName']][$row['ID']] = $row['Name'];
				}
			}
		}
		return $result;
	}

	public function FindQualificationChildren($id)
	{
		$params		 = array(
			':ID' => $id
		);
		$result		 = array();
		$statement	 = $this->prepare("
			SELECT TopType.ID as L1ID, Child1.ID as L2ID, Child2.ID as L3ID, Child3.ID as L4ID, Child4.ID as L5ID

			FROM `QualificationType` AS TopType

			LEFT JOIN QualificationType AS Child1 ON Child1.`ParentID` = TopType.`ID`
			LEFT JOIN QualificationType AS Child2 ON Child2.`ParentID` = Child1.`ID`
			LEFT JOIN QualificationType AS Child3 ON Child3.`ParentID` = Child2.`ID`
			LEFT JOIN QualificationType AS Child4 ON Child4.`ParentID` = Child3.`ID`

			WHERE `TopType`.`ID` = :ID
			");
		if ($statement->execute($params)) {
			if ($statement->rowCount() > 0) {
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
					foreach (array("L1ID", "L2ID", "L3ID", "L4ID", "L5ID") as $key) {
						if (isset($row[$key]) && $row[$key] && !in_array($row[$key], $result)) {
							$result[] = $row[$key];
						}
					}
				}
			}
		}
		return $result;
	}

	public function GetIDArray(PDOStatement $statement)
	{
		$result = array();
		if ($statement) {
			if ($statement->rowCount() > 0) {
				while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
					if (isset($row["ID"]) && !in_array($row["ID"], $result)) {
						$result[] = $row["ID"];
					}
				}
			}
		}
		return $result;
	}
	private $_QualificationFullNameCache = array();

	public function GetQualificationTypeByID($qualificationTypeID)
	{
		$params		 = array(
			':ID' => $qualificationTypeID
		);
		$result		 = array();
		$statement	 = $this->prepare("
			SELECT *
			FROM `QualificationType`
			WHERE `QualificationType`.ID = :ID
			");
		if ($statement->execute($params)) {
			$result = $statement->fetch();
		}
		return $result;
	}

	public function GetQualificationByID($qualificationID)
	{
		$params		 = array(
			':ID' => $qualificationID
		);
		$result		 = array();
		$statement	 = $this->prepare("
			SELECT *
			FROM `Qualification`
			WHERE `Qualification`.ID = :ID
			");
		if ($statement->execute($params)) {
			$result = $statement->fetch();
		}
		return $result;
	}

	public function GetQualificationFullName($qualificationID)
	{
		$result = isset($this->_QualificationFullNameCache[$qualificationID]) ? $this->_QualificationFullNameCache[$qualificationID] : null;
		if (!$result) {
			$params		 = array(
				':ID' => $qualificationID
			);
			$result		 = array();
			$statement	 = $this->prepare("
			SELECT
				`L1`.ID as L1ID,
				`L2`.ID as L2ID,
				`L3`.ID as L3ID,
				`L4`.ID as L4ID,
				`L5`.ID as L5ID,
				`Qualification`.*,
				 CONCAT_WS(' &gt; ',`L1`.`Name`,`L2`.`Name`,`L3`.`Name`,`L4`.`Name`,`L5`.`Name`,`Qualification`.`Name`) as FullName

			FROM `Qualification`

            LEFT JOIN QualificationType AS L5 ON `Qualification`.`TypeID` = L5.`ID`
			LEFT JOIN QualificationType AS L4 ON L5.`ParentID` = L4.`ID`
			LEFT JOIN QualificationType AS L3 ON L4.`ParentID` = L3.`ID`
			LEFT JOIN QualificationType AS L2 ON L3.`ParentID` = L2.`ID`
			LEFT JOIN QualificationType AS L1 ON L2.`ParentID` = L1.`ID`

			WHERE `Qualification`.ID = :ID

			ORDER BY `L1`.`Name`,`L2`.`Name`,`L2`.`Name`,`L4`.`Name`,`Qualification`.`Name`

			");
			if ($statement->execute($params)) {
				$row = $statement->fetch();
				if (isset($row['FullName'])) {
					$result = $row['FullName'];
				}
			}
		}
		return $result;
	}

	public function FindInstitutions($cityName, $qualificationTypeID = null, $qualificationName = null)
	{
		$qualificationTypeIDs = array();
		if ($qualificationTypeID) {
			$qualificationTypeIDs = $this->FindQualificationChildren($qualificationTypeID);
		}
		$params	 = array(
			':CityName' => "%".str_replace("%", "", $cityName)."%"
		);
		$query	 = '
						SELECT
						`City`.`Name` AS "CityName",
						GROUP_CONCAT(`Qualification`.`Name`) as "Qualifications",
						GROUP_CONCAT(`Qualification`.`ID`) as "QualificationIDs",
						`Institution`.*

						FROM  `Institution`

						LEFT JOIN  `City` ON  `Institution`.`CityID` =  `City`.`ID`
						LEFT JOIN  `Institution_Qualifications` ON  `Institution`.`ID` =  `Institution_Qualifications`.`InstitutionID`
						LEFT JOIN  `Qualification` ON  `Qualification`.`ID` =  `Institution_Qualifications`.`QualificationID`

						WHERE `City`.`Name` LIKE :CityName';
		if ($qualificationTypeIDs) {
			$query .='
						AND `Qualification`.`TypeID` IN ('.implode(',', $qualificationTypeIDs).') ';
		}
		if ($qualificationName) {
			$params[':QualificationName'] = "%".str_replace("%", "", $qualificationName)."%";
			$query .='
						AND `Qualification`.`Name` LIKE :QualificationName';
		}


		$query .= '
						GROUP BY `Institution`.`ID`';
		$statement = $this->prepare($query);
		return $statement->execute($params) ? $statement : false;
	}
}