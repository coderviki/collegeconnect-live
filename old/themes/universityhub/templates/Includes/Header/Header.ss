<header class="site-header">

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false" aria-controls="main-navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="$BaseHref" class="logo brand" rel="home">

                <img src="<% if SiteConfig.Logo %>$SiteConfig.Logo.SetWidth(250).URL<% else %>$ThemeDir/images/logo.png<% end_if %>" alt="$SiteConfig.Title" />
            </a>
            <% if SiteConfig.ShowTitleInHeader %>$SiteConfig.Title<% end_if %>
        </div>
        <div id="main-navbar" class="navbar-collapse collapse">
            <% include Navigation %>
            <% if $SearchForm %>
            <div class="navbar-form navbar-right">
                $SearchForm
            </div>
            <% end_if %>
        </div>
    </div>
</header>
