<% if $CarouselItems.Filter(Archived, 0).Count > 0 %>
<div id="$CarouselID" class="carousel slide large-carousel" data-ride="carousel">
    <% if $CarouselItems.Filter(Archived, 0).Count > 1 %>
    <ol class="carousel-indicators">
        <% loop CarouselItems.Filter(Archived, 0).Sort(SortID) %>
        <li data-target="#$CarouselID" data-slide-to="$Pos(0)" class="<%if $First%>active<%end_if%>"></li>
        <% end_loop %>
    </ol>
    <% end_if %>

    <div class="carousel-inner">
        <% loop CarouselItems.Filter(Archived, 0).Sort('SortID') %>
        <div class="item <%if $First%>active<%end_if%>">
            <% if Link %><a href="$Link.Link"><% end_if %>$Image.CroppedImage(690,310)<% if Link %></a><% end_if %>
            <% if Title || Caption %>
            <div class="container">
                <div class="carousel-caption large">
                    <% if Title %>
                    <h1>$Title</h1>
                    <% end_if %>
                    <p>$Caption</p>
                </div>
            </div>
            <% end_if %>
        </div>
        <% end_loop %>
    </div>
    <% if $CarouselItems.Filter(Archived, 0).Count > 1 %>
    <a class="left carousel-control" href="#$CarouselID" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
    <a class="right carousel-control" href="#$CarouselID" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <% end_if %>
</div>
<% end_if %>
