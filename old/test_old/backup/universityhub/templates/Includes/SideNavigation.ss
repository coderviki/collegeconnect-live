<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">List by</h3>
    </div>
    <ul class="nav nav-pills nav-stacked">
        <% loop SideNavigationItems %>
                <li class="$CssClass"><a href='$Link'>$Title</a></li>
        <% end_loop %>

    </ul>
</div>