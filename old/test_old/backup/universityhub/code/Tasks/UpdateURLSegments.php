<?php

class UpdateURLSegments extends UniHubTask
{
	protected $title		 = "Update URL Segments";
	protected $description	 = "Update URLSegments from names on Provinces/Cities/Institutions";

	public function run($request)
	{
		$result = true;
		if (!Director::isDev() && !$this->isCLI && !Permission::check("ADMIN")) {
			$result = Security::permissionFailure();
		} else {
			$this->renderHeader();
			$this->doUpdateURLSegments();
			$this->renderFooter();
		}
		return $result;
	}

	public function doUpdateURLSegments()
	{
		foreach (Province::get() as $province) {
			$province->URLSegment = \UniHub\Utils\String::slugify($province->Name);
			$province->write();
		}
		foreach (City::get() as $city) {
			$city->URLSegment = \UniHub\Utils\String::slugify($city->Name);
			$city->write();
		}
		foreach (Institution::get() as $institution) {
			$institution->URLSegment = \UniHub\Utils\String::slugify($institution->Name);
			$institution->write();
		}
	}
}