<?php

/**
 * @property string $Name
 * @property int $TypeID
 * @method Institution[] Institutions()
 * @method QualificationType[] Type()
 */
class Qualification extends DataObject
{
	private static $db					 = array(
		"Name"		 => "Text",
	);
	private static $belongs_many_many	 = array(
		"Institutions" => "Institution",
	);
//    private static $many_many = array(
//        "Institutions" => "Institution",
//    );
	private static $has_one				 = array(
		"Type" => "QualificationType"
	);

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();
		$fields->addFieldToTab("Root.Main", TreeDropdownField::create("TypeID", "Type", "QualificationType"));

		return $fields;
	}
}