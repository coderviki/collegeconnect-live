<?php

/**
 * @property string $Name
 * @property string URLSegment
 */
class Province extends DataObject
{
	private static $db		 = array(
		"Name"		 => "Varchar(255)",
		"URLSegment" => "Varchar(255)",
	);
	private static $has_many = array(
		"Cities" => "City"
	);

	/**
	 *
	 * @return City[]|ArrayList
	 */
	public static function getProvincesWithInstitutions()
	{
		$rows = static::get()
			->leftJoin("City", "Province.ID = City.ProvinceID")
			->leftJoin("Institution", "City.ID = Institution.CityID")
			->alterDataQuery(function(DataQuery $query) {
			$query
			->groupby("Province.ID")
			->having("Count(Institution.ID) > 0");
		});
		return $rows;
	}

	/**
	 *
	 * @return City
	 */
	public static function getByURLSegment($URLSegment)
	{
		return static::get()->filter(array("URLSegment" => $URLSegment))->first();
	}

	/**
	 *
	 * @return City
	 */
	public function Institutions()
	{
		$provinceID	 = $this->ID;
		$rows		 = Institution::get()->leftJoin("City", "City.ID = Institution.CityID")
			->alterDataQuery(function(DataQuery $query) use ($provinceID) {
			return $query
				->groupby("Institution.ID")
				->having("Count(Institution.ID) > 0")
				->where("City.ProvinceID = $provinceID");
		});
		return $rows;
	}
}