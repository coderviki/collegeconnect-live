<?php

class InstitutionAdmin extends ModelAdmin
{
    public static $managed_models = array(
        'Institution'
    );
    public static $url_segment    = 'institutions';
    public static $menu_title     = 'Institutions';

//    public function getEditForm($id = null, $fields = null)
//    {
//        $form                  = parent::getEditForm($id, $fields);
//        /* @var $missingTaleoFieldGrid GridField */
//        $missingTaleoFieldGrid = $form->Fields()->fieldByName("MissingTaleoFieldMapper");
//        if ($missingTaleoFieldGrid && $missingTaleoFieldGrid instanceof GridField) {
//            $missingTaleoFieldGrid->getConfig()->removeComponentsByType("GridFieldEditButton");
//            $missingTaleoFieldGrid->getConfig()->addComponent(new MissingTaleoFieldMapperGridFieldEditButton(), "GridFieldDeleteAction");
//        }
//        return $form;
//    }
//
//    public function getList()
//    {
//        $list = ArrayList::create();
//        if ($this->modelClass == 'MissingTaleoFieldMapper') {
//            $sqlQuery = new SQLQuery();
//            $sqlQuery->setFrom('SSTasExpertise');
//            $sqlQuery->setSelect(array('TaleoFieldMapper.TasCategoryID', 'SSTasExpertise.ID'));
//            $sqlQuery->addLeftJoin('TaleoFieldMapper', '"TaleoFieldMapper"."TasCategoryID" = "SSTasExpertise"."ID"');
//            $sqlQuery->setWhere("TaleoFieldMapper.ID IS NULL");
//            foreach ($sqlQuery->execute() as $row) {
//                $map                = TaleoFieldMapper::create();
//                $map->TasCategoryID = $row['ID'];
//                $list->push($map);
//            }
//        } else {
//            $list = parent::getList();                         //<-- takes care of custom filters, but gets all members
//        }
//        return $list;
//    }
}