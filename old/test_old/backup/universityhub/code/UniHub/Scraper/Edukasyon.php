<?php

namespace UniHub\Scraper;

use Symfony\Component\DomCrawler\Crawler;

class Edukasyon extends Crawler
{
    protected static $SITE = "http://edukasyon.ph";
    protected static $PATH = "/en/institutions/colleges?region=|NCR|&search_type=college&page=%PAGE%";

    /**
     *
     * @return Crawler
     */
    public static function create()
    {
        $url = str_replace("%PAGE%", 1, self::$SITE.self::$PATH);
        $res = self::fetchURL($url);
        return $res;
    }

    public function run()
    {
        $unis = $this->filter(".insti-list h3 a")->each(function (Crawler $a, $i) {
            $detailsURL = $a->link();
            return $detailsURL->getUri();
        });
    }

    
}