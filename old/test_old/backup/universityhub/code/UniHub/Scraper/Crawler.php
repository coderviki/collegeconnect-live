<?php

namespace UniHub\Scraper;

class Crawler extends \Symfony\Component\DomCrawler\Crawler
{

    /**
     *
     * @param type $url
     * @return Crawler
     */
    public static function fetchURL($url)
    {
        $cacheKey   = md5($url);
        $cache      = \SS_Cache::factory("Crawler", 'Output', array(
//                'lifetime' => 7200, // cache lifetime of 2 hours
                )
        );
        if (!($resultBody = $cache->load($cacheKey))) {
            $client = new \GuzzleHttp\Client();
            $res    = $client->request('GET', $url);
            if ($res->getStatusCode() == 200) {
                $resultBody = (string) $res->getBody();
                $cache->save($resultBody, $cacheKey);
            }
        }
        $dom = new static($resultBody, $url);
        return $dom;
    }

    public function directText()
    {
        $text = implode(" ",
                        array_map(
                function($node) {
                if ($node->nodeType === XML_TEXT_NODE) {
                    return $node->textContent;
                }
            }, iterator_to_array($this->getNode(0)->childNodes))
        );

        return \UniHub\Utils\String::clean($text);
    }
}