<?php

namespace UniHub\Scraper\Edukasyon;

use Symfony\Component\DomCrawler\Crawler;

class EdukasyonList extends \UniHub\Scraper\Crawler
{
	protected static $SITE	 = "http://edukasyon.ph";
	protected static $PATH	 = "/en/institutions/colleges?region=|NCR|&search_type=college";

	/**
	 *
	 * @return EdukasyonList
	 */
	public static function create()
	{
		$page = 1;
		if (\Controller::has_curr() && \Controller::curr()->getRequest()->getVar("page")) {
			$page = \Controller::curr()->getRequest()->getVar("page");
		}
		$res = self::fetchURL(self::$SITE.self::$PATH."&page=".$page);
		return $res;
	}

	/**
	 *
	 * @return EdukasyonDetails[]
	 */
	public function getDetails()
	{
//            return $this->filter(".insti-list h3 a")->first()->each(function (Crawler $a, $i) {
		return $this->filter(".insti-list h3 a")->each(function (Crawler $a, $i) {
				/* @var $detailsURL \Symfony\Component\DomCrawler\Link */
				$detailsURL = $a->link();
				return EdukasyonDetails::create($detailsURL->getUri());
			});
	}
}