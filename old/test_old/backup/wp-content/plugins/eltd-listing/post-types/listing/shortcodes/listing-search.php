<?php
namespace ElatedListing\Listing\Shortcodes;

use ElatedListing\Lib;

/**
 * Class ListingAdvancedSearch
 * @package ElatedListing\Listing\Shortcodes
 */
class ListingSearch implements Lib\ShortcodeInterface {


	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'eltd_listing_search';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 *
	 * @see vc_map
	 */
	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map( array(
					'name' => 'Listing Search',
					'base' => $this->getBase(),
					'category' => 'by ELATED',
					'icon' => 'icon-wpb-listing-search extended-custom-icon',
					'allowed_container_element' => 'vc_row',
					'params' => array(
					)
				)
			);
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render( $atts, $content = null ) {

		$params['top_categories'] = $this->getTopCategories();
		$params['locations'] = $this->getLocations();
		$params['categories'] = $this->getCategories();
		$params['button_hover_color'] = $this->getButtonHoverColor();

		$html = eltd_listing_get_shortcode_module_template_part('listing', 'listing-search-template', '', $params);
		return $html;

	}

	/**
	 * Sort listing categories by listing item count and take maximum 5 categories
	 *
	 * @return array
	 */
	private function getTopCategories() {

		$listing_types = get_posts(array(
			'post_type' => 'listing-type-item',
			'posts_per_page' => '5'
		));
		return $listing_types;

	}

	private function getLocations() {

		$locations = array();
		$taxonomies = get_terms('listing-item-location');
		foreach ($taxonomies as $location) {
			$locations[$location->term_id] = $location->name;
		}
		return $locations;

	}

	private function getCategories() {

		$listing_types = array();
		$args = array(
			'post_type' => 'listing-type-item',
			'posts_per_page' => '-1'
		);
		$types = get_posts($args);

		foreach ($types as $type) {
			$listing_types[$type->ID] = $type->post_title;
		}
		return $listing_types;

	}

	private function getButtonHoverColor(){

		$button_hover_color = '#1ab5c1';
		if(search_and_go_elated_options()->getOptionValue('first_color') !== ''){
			$button_hover_color = search_and_go_elated_options()->getOptionValue('first_color');
		}
		return $button_hover_color;
	}

}