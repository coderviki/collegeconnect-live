<?php
ob_start();
if (filter_input(INPUT_POST, "action") == "search") {
	$qualificationTypeID = filter_input(INPUT_POST, "QualificationType", FILTER_VALIDATE_INT); // Get the selection sent by the visutor
	$qualificationName	 = filter_input(INPUT_POST, "QualificationName"); // Get the selection sent by the visutor
	$cityName			 = filter_input(INPUT_POST, "City"); // Get the selection sent by the visutor


	require './UniHubDB.corey.php';
	$db					 = new UniHubDB();
	$qualificationType	 = $db->GetQualificationTypeByID($qualificationTypeID);

	$result = $db->FindInstitutions($cityName, $qualificationTypeID, $qualificationName);

	if ($result && $result->rowCount() > 0) {
		?>
		<table class="table table-bordered table-striped">
			<tr>
				<th>University</th>
				<th>City</th>
				<th>Courses</th>
			</tr>
			<?php
			while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
				$qualificationNames = array();
				foreach (explode(",", $row['QualificationIDs']) as $matchedQualId) {
					$qualificationNames[] = $db->GetQualificationFullName($matchedQualId);
				}
				$qualification = implode("<br/>", $qualificationNames);
				if ($qualificationName) {
					$qualification = preg_replace("/(".preg_quote($qualificationName).")/i", "<mark>$1</mark>", $qualification);
				}
				$city = $row['CityName'];
				if ($cityName) {
					$city = preg_replace("/(".preg_quote($cityName).")/i", "<mark>$1</mark>", $city);
				}
				?>
				<tr>

					<td><a href="$Top.LinkBase/$URLSegment"><?= $row['Name'] ?></a></<td>
					<td><?= $city ?></<td>
					<td><?= $qualification ?></<td>
				</tr>
				<?php
			}
			?>
		</table>
		<?php
	} else {
		echo "No Institions matched your query";
	}
} else {

}

$content = ob_get_contents();
ob_end_clean();
?>

<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en-GB" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html lang="en-GB" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html lang="en-GB" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en-GB" class="no-js"> <!--<![endif]-->
    <head>
        <base href="http://corey.universityhub.ph/"><!--[if lte IE 6]></base><![endif]-->
		<title>Home &raquo; UniversityHub.ph</title>
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge" /><![endif]-->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="generator" content="SilverStripe - http://silverstripe.org" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta name="x-page-id" content="1" />
		<meta name="x-cms-edit-link" content="admin/pages/edit/show/1" />
		<link rel="shortcut icon" href="themes/universityhub/images/favicon.ico" />
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="http://corey.universityhub.ph/themes/universityhub/css/layout.css?m=1470195831" />
		<link rel="stylesheet" type="text/css" href="http://corey.universityhub.ph/themes/universityhub/css/typography.css?m=1470195831" />
		<link rel="stylesheet" type="text/css" media="print" href="http://corey.universityhub.ph/themes/universityhub/css/print.css?m=1470195830" />
		<link rel="stylesheet" type="text/css" href="http://corey.universityhub.ph/userforms/css/UserForm.css?m=1470186267" />
		<style>
			td{
				white-space: nowrap;
				line-height: 1.5;
				overflow: hidden;
				text-overflow: ellipsis;
			}
		</style>
	</head>
    <body role="document" class="holding-page" dir="ltr" data-site-mode="dev">
		<div class="container">
			<br/>
			I want
			a  <em><?= $qualificationType ? $qualificationType['Name'] : "..." ?></em>
			in <em><?= $qualificationName ? $qualificationName : " any course " ?></em>
			in <em> <?= $cityName ? $cityName : " any city" ?></em>
			<br/>
			<?= $content ?>
		</div>

	</body>
</html>