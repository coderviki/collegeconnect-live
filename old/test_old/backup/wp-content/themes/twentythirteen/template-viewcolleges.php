 <?php

/*

Template Name: View Schools



*/



get_header(); ?>

<br><br>

<h1>Add Your School Below:</h1>

<form method="post" action="template-insertdataprocessor.php" enctype="multipart/form-data" style="width: 80%; background: lightblue none repeat scroll 0% 0%; border: 2px solid darkcyan; margin-right: 55px; padding: 35px; margin-left: 55px;">

	<p> Upload your school logo in gif or jpeg format. The file name should be named after the school's name. If the same file name is uploaded twice it will be overwritten! </p>



	<p> Logo: </p>

	<input type="hidden" name="size" value="350000">

	<input type="file" name="schoollogo"><br>

	<br><table><tr><td style="width: 15%;"><p> Name of School:</td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="Name"/> </td><td style="width: 42.5%;">eg: University of the Philippines</p></td></tr></table>



	<table><tr><td style="width: 15%;"><p>School Motto?

		</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="Motto"> </textarea></td><td style="width: 42.5%;">eg: "Great Moral Excellence......."</p></td></tr></table>

 <script type="text/javascript">

        CKEDITOR.replace( 'Motto' );

      </script>



	<br><br><table><tr><td style="width: 15%;"><p>Brief description/history of college

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="Description"> </textarea> </td><td style="width: 42.5%;">eg: "Built in 1880, UP was constructed by Engineer John Smith....It was recognized as an educational institution on.....It remains to be a leader in......"</p></td></tr></table>

 <script type="text/javascript">

        CKEDITOR.replace( 'Description' );

      </script>



	<br><br><table><tr><td style="width: 15%;"><p>Type of School? (eg: Public, Private) </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="Type"/> </td><td style="width: 42.5%;">eg: Public</p></td></tr></table>

	

	<table><tr><td style="width: 15%;"><p>Any religious affiliation? </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="ReligiousAffiliation"/> </td><td style="width: 42.5%;">eg: Catholic</p></td></tr></table>

	

	<table><tr><td style="width: 15%;"><p>Partnerships and Non-Religious Affiliations? </td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="Certifications"> </textarea> </td><td style="width: 42.5%;">eg: Association of Education in Western Europe<br>Organization of Great People<br>Association of Leadership in Fine Arts</p></td></tr></table><script type="text/javascript">

        CKEDITOR.replace( 'OtherPartnerships' );

      </script>

	

	<table><tr><td style="width: 15%;"><p>Gender admission? (eg: Men and Women, All Men, etc.) </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="GenderAdmission"/> </td><td style="width: 42.5%;">eg: Male and Female</p></td></tr></table>

	

	<table><tr><td style="width: 15%;"><p>Term Type? (eg: Quarter, Semester) </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="TermType"/> </td><td style="width: 42.5%;">eg: Quarter</p></td></tr></table>

	

	<table><tr><td style="width: 15%;"><p>Entrance Exam Required? </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="EntranceExam"/> </td><td style="width: 42.5%;">eg: No</p></td></tr></table>

	

	<table><tr><td style="width: 15%;"><p>When can students take the entrance exam? </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="EntranceExamDate"/> </td><td style="width: 42.5%;">eg: March, May, September, December (see our website for more details)</p></td></tr></table>

	

 	<table><tr><td style="width: 15%;"><p>How many students overall are enrolled in your school? </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="TotalEnrolled"/> </td><td style="width: 42.5%;">eg: 50,000</p></td></tr></table>

        

	<table><tr><td style="width: 15%;"><p>What is your college's website URL? (Please make sure your input is correct) </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="collegewebsite"/> </td><td style="width: 42.5%;">eg: http://www.up.edu.ph</p></td></tr></table>

	

	<table><tr><td style="width: 15%;"><p>What is the best way for a student or parent to contact the school? Any contact info?

	</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="ContactInfo"> </textarea> </td><td style="width: 42.5%;">eg: Email and Phone<br><br>Email: name@up.edu.ph<br>Phone: 123-4567</p></td></tr></table>

 <script type="text/javascript">

        CKEDITOR.replace( 'ContactInfo' );

      </script>

	<br><br><table><tr><td style="width: 15%;"><p> Campus Locations (please make sure your input is correct): </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="CampusLocations"/> </td><td style="width: 42.5%;">eg: Diliman, Baguio, Visayas, Los Banos....</p></td></tr></table>

	

	<table><tr><td style="width: 15%;"><p> Certifications Offered:

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="Certifications"> </textarea> </td><td style="width: 42.5%;">eg: Computer Repair<br>Health Tech<br>Electrician</p></td></tr></table>

	 <script type="text/javascript">

        CKEDITOR.replace( 'Certifications' );

      </script>

	<br><br><table><tr><td style="width: 15%;"><p> Diploma Courses Offered:

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="Diplomas"> </textarea> </td><td style="width: 42.5%;">eg: Computer Tech<br>Advanced Health Tech<br>Advanced Electrician</p></td></tr></table>

	 <script type="text/javascript">

        CKEDITOR.replace( 'Diplomas' );

     </script>

	<br><br><table><tr><td style="width: 15%;"><p> Associate (2-Year) Degrees Offered:

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="AssociateDegrees"> </textarea> </td><td style="width: 42.5%;">eg: Nursing<br>IT<br>Web development</p></td></tr></table>

	 <script type="text/javascript">

        CKEDITOR.replace( 'AssociateDegrees' );

      </script>

	<br><br><table><tr><td style="width: 15%;"><p> Bachelors (4-Year) Degrees Offered: 

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="BachelorsDegrees"> </textarea> </td><td style="width: 42.5%;">eg: Nursing<br>Computer Engineering<br>Math<br>Marketing</p></td></tr></table>

	 <script type="text/javascript">

        CKEDITOR.replace( 'BachelorsDegrees' );

      </script>

	<br><br><table><tr><td style="width: 15%;"><p> Masters (5-6 Year) Degrees Offered:

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="MastersDegrees"> </textarea> </td><td style="width: 42.5%;">eg: Business Administration<br>Nursing<br>Math</p></td></tr></table>

	 <script type="text/javascript">

        CKEDITOR.replace( 'MastersDegrees' );

      </script>

	<br><br><table><tr><td style="width: 15%;"><p>Doctorate Degrees Offered

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="DoctorateDegrees"> </textarea> </td><td style="width: 42.5%;">eg: Theology<br>Math<br>Philosophy<br>Medicine</p></td></tr></table>

	 <script type="text/javascript">

        CKEDITOR.replace( 'DoctorateDegrees' );

      </script>

	<br><br><table><tr><td style="width: 15%;"><p>Does your college offer any special degree programs?

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="SpecialDegreePrograms"> </textarea> </td><td style="width: 42.5%;">eg: ETEEAP</p></td></tr></table>

	 <script type="text/javascript">

        CKEDITOR.replace( 'SpecialDegreePrograms' );

      </script>

	<br><br><table><tr><td style="width: 15%;"><p> Accreditations:

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="Accreditation"> </textarea> </td><td style="width: 42.5%;">eg: ABET<br>PAASCU</p></td></tr></table>

		<script type="text/javascript">

        CKEDITOR.replace( 'Accreditation' );

      </script>

	 <br> Cost of Tuition:<br><br> <table><tr><td style="width: 15%;"><p>

Local Students:

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="CostofTuitionLocal"> </textarea> </td><td style="width: 42.5%;">eg: 22,000.00</p></td></tr></table>

 <script type="text/javascript">

        CKEDITOR.replace( 'CostofTuitionLocal' );

      </script>

<table><tr><td style="width: 15%;"><p>Foreign Students:

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="CostofTuitionLocal"> </textarea> </td><td style="width: 42.5%;">eg: 32,000.00</p></td></tr></table>

 <script type="text/javascript">

        CKEDITOR.replace( 'CostofTuitionForeign' );

      </script>



	<br><br><table><tr><td style="width: 15%;"><p>Does your college offer nearby housing and accommodation?

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="Housing"> </textarea> </td><td style="width: 42.5%;">eg: Yes, local boarding houses</p></td></tr></table>

 <script type="text/javascript">

        CKEDITOR.replace( 'Housing' );

      </script>

	<br><br><table><tr><td style="width: 15%;"><p>Acceptance Rate? How hard is it to get in? (eg: 95% - Easy ...amount of people who get accepted for admissions / amount of people who apply) </td><td style="width: 42.5%;"><input style="width: 330px;" type="text" name="AcceptanceRate"/> </td><td style="width: 42.5%;">eg: 75% - Challenging</p></td></tr></table>

	

	<br><br><table><tr><td style="width: 15%;"><p>What Centers of Excellence has your college been awarded? (eg: Nursing)

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="CoE"> </textarea> </td><td style="width: 42.5%;">eg: Nursing<br>Education<br>IT</p></td></tr></table>

 <script type="text/javascript">

        CKEDITOR.replace( 'CoE' );

      </script>

	<br><br><table><tr><td style="width: 15%;"><p>What Centers of Development has your college been awarded? (eg: Nursing)

</td><td style="width: 42.5%;"><textarea rows="10" cols="35" name="CoD"> </textarea> </td><td style="width: 42.5%;">eg: Nursing<br>Education<br>IT</p></td></tr></table>

 <script type="text/javascript">

        CKEDITOR.replace( 'CoD' );

      </script>

	<br><br><p> Upload images of your school in gif or jpeg format.</p>

	<p> Images: </p>

	<input  type="hidden" name="size" value="350000">

	<input type="file" name="screenshots">

	<br/> <br/>

	<input  TYPE="submit" name="upload" title="Add data to the Database" value="Add College"/>

</form>

<?php get_sidebar(); ?>

<?php get_footer(); ?>