<?php
/*** Functions ***/
/**
 * mb_stripos all occurences
 * based on http://www.php.net/manual/en/function.strpos.php#87061
 *
 * Find all occurrences of a needle in a haystack
 *
 * @param string $haystack
 * @param string $needle
 * @return array or false
 */
function mb_stripos_all($haystack, $needle) {
 
  $s = 0;
  $i = 0;
 
  while(is_integer($i)) {
 
    $i = mb_stripos($haystack, $needle, $s);
 
    if(is_integer($i)) {
      $aStrPos[] = $i;
      $s = $i + mb_strlen($needle);
    }
  }
 
  if(isset($aStrPos)) {
    return $aStrPos;
  } else {
    return false;
  }
}
 
/**
 * Apply highlight to row label
 *
 * @param string $a_json json data
 * @param array $parts strings to search
 * @return array
 */
function apply_highlight($a_json, $parts) {
 
  $p = count($parts);
  $rows = count($a_json);
  
  for($row = 0; $row < $rows; $row++) {
  	$patterns = array();
	$replacements = array();
	foreach($parts as $value) {
		preg_match_all('/'.$value.'(?![^<]*>)/i', $a_json[$row]["label"], $matches);
		foreach ($matches[0] as $value2) {
			$patterns[] = '/'.$value2.'(?![^<]*>)/';
			$replacements[] = '<span class="hl_results">' . $value2 . '</span>';
		}
	}
	$a_json[$row]["label"] =  preg_replace($patterns, $replacements, $a_json[$row]["label"]);
  }
  
  return $a_json;
 
}
/*****************/

// prevent direct access
$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
if(!$isAjax) {
  $user_error = 'Access denied - not an AJAX request...';
  trigger_error($user_error, E_USER_ERROR);
}

// get what user typed in autocomplete input
$term = trim($_GET['term']);
 
$a_json = array();
$a_json_row = array();
 
$a_json_invalid = array(array("id" => "#", "value" => $term, "label" => "Only letters and digits are permitted..."));
$json_invalid = json_encode($a_json_invalid);
 
// replace multiple spaces with one
$term = preg_replace('/\s+/', ' ', $term);
 
// SECURITY HOLE ***************************************************************
// allow space, any unicode letter and digit, underscore and dash
if(preg_match("/[^\040\pL\pN_-]/u", $term)) {
  print $json_invalid;
  exit;
}
// *****************************************************************************
$parts = explode(' ', $term);
$p = count($parts);


/**** Your Database code here ********/

/***** Use Loop function here ********/
/*$a_json_row["id"] = 'An ID';
$a_json_row["value"] = 'What ends up in input value';
$a_json_row["label"] = 'What in the list';
array_push($a_json, $a_json_row);*/

/*** FOR DEMO ONLY ***/
if(preg_match('/'.$term.'/i', 'Help')) {
	$a_json_row["id"] = '01';
	$a_json_row["value"] = 'Help';
	$a_json_row["label"] = '<img src="https://www.google.co.uk/images/srpr/logo11w.png" height="20" alt="help"> - <span class="Help">Help How Can I help</span>';
	array_push($a_json, $a_json_row);
	$a_json_row["id"] = '02';
	$a_json_row["value"] = 'Help Me Pelase';
	$a_json_row["label"] = '<span class="Help">Help Me Please</span>';
	array_push($a_json, $a_json_row);
}

if(preg_match('/'.$term.'/i', 'Free')) {
	$a_json_row["id"] = '03';
	$a_json_row["value"] = 'Free';
	$a_json_row["label"] = '<span class="Free">Free This is Free</span>';
	array_push($a_json, $a_json_row);
}
/*** END OF DEMO ONLY ***/

/***** End Of  Loop function ********/

/*************************************/
	
	// highlight search results
	$a_json = apply_highlight($a_json, $parts);
	 
	$json = json_encode($a_json);
	print $json;
 ?>
