<?php
/*
  Template Name: Homepage Template
 */
get_header();

require __DIR__.'/UniHubDB.php';
$db = new UniHubDB();
?>
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main" style="height: 500px;">
		<div class="bg"></div>
		<form method="post" action="wp-content/themes/twentythirteen/findcolleges.corey.php" id="searchform">
			<input type="hidden" name="action" value="search"/>
			<div style="width:100%;">
				<div style="width:100%;float:left;"><h2 style="padding-top: 100px; position: relative; float: right; margin-right: 40px;">I want a
						<select name="QualificationType" style="    max-width: 252px;">
							<?php
							foreach ($db->GetRootQualificationTypes() as $parent => $children) {
								?><optgroup label="<?= $parent ?>"><?php
									foreach ($children as $id => $label) {
										?><option value="<?= $id ?>"><?= $label ?></option><?php
									}
								?></optgroup><?php
							}
							?>
						</select>
						in  <input type="text" name="QualificationName" placeholder="Insert Course here" id="topic_title">
						in  <input type="text" name="City" placeholder="Insert City here" id="topic_title2" >
						<div id="button-wrap-inner" style="/*float: right; width: 25%; padding-top: 130px; position: relative;*/ padding-left: 40%;/* position: absolute; */padding-top: 180px;">
							<input type="submit" name="submit" value="Find a College" class="btn">
						</div>
					</h2>
					<br>
				</div>
			</div>
		</form>
	</div><!-- #content -->

</div><!-- #primary -->
<script type="text/javascript">
    $(function () {
        $("#topic_title").autocomplete({
            source: "wp-content/themes/twentythirteen/ajax.php",
            minLength: 2,
            select: function (event, ui) {
                var value = ui.item.id;
                if (value != '#') {
                    alert(value);
                }
            },
            html: true, // optional (jquery.ui.autocomplete.html.js required)

            // optional (if other layers overlap autocomplete list)
            open: function (event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#topic_title2").autocomplete({
            source: "wp-content/themes/twentythirteen/ajax.php",
            minLength: 2,
            select: function (event, ui) {
                var value = ui.item.id;
                if (value != '#') {
                    alert(value);
                }
            },
            html: true, // optional (jquery.ui.autocomplete.html.js required)

            // optional (if other layers overlap autocomplete list)
            open: function (event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    });
</script>