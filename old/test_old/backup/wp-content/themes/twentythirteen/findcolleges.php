<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);


//$query = $_GET['submit'];
//$query = $_POST['degreetype'];
//$query = htmlspecialchars($query);
//$query = mysql_real_escape_string($query);
//$degreetype=trim($_POST['degreetype']);
//$degreetype = $_POST['degreetype'];
//city       = $_POST['city'];
//$course     = $_POST['course'];

$validTypes = array("Bachelors degree", "graduate", "associate"); // Create a list of valid selections

$userSelection	 = filter_input(INPUT_POST, "degreetype"); // Get the selection sent by the visutor
$selectedCity	 = filter_input(INPUT_POST, "city"); // Get the selection sent by the visutor

$selectedType = in_array($userSelection, $validTypes) ? $userSelection : "Bachelors degree"; // Check if the user selection is in our validTypes list. Otherswise selce a default

$query = "";

$params = array();

switch ($selectedType) {  //Construct the right query for the selection type
	case "Bachelors degree":
		$query	 = "SELECT BachelorsDegrees.Name, BachelorsDegrees.BachelorsDegrees, College_Data.City
FROM BachelorsDegrees
INNER JOIN College_Data
ON College_Data.City=BachelorsDegrees.Name
WHERE BachelorsDegrees.BachelorsDegrees = :SelectedDegree && College_Data.City = :SelectedCity";
		$params	 = array(
			':SelectedDegree'	 => $userSelection,
			':SelectedCity'		 => $selectedCity
		);
		break;
}

try {
	$db = new PDO('mysql:host=localhost;dbname=univers1_test;charset=utf8mb4', 'univers1_admin', 'B@ctad89', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
} catch (PDOException $ex) {
	echo "Unable to connect to the database!: ".$ex->getMessage()."<br/>";
	die();
}

try {
	$statement = $db->prepare($query);
	if ($statement->execute($params)) {
		$foundRows = $statement->rowCount();
		if ($foundRows > 0) {
			$result = $statement->fetch(PDO::FETCH_ASSOC);
			foreach ($result as $row) {
				echo 'College Name: '.$row['Name'];
				echo '<br /> Accreditation: '.$row['Accreditation'];
				echo '<br /> Term Type: '.$row['TermType'];
				echo $userSelection;
				echo $selectedCity;
			}
		} else {
			echo "No colleges found matching your criteria ";
			echo "<pre>".print_r($params, true)."</pre>";
		}
	} else {
		echo "Failed to run query ".print_r($query->errorInfo(), true);
	}
} catch (PDOException $ex) {
	echo "Unable to fetch Degrees!: ".$ex->getMessage()."<br/>";
}
?>