<?php
// TELL THE CORE THIS IS A CHILD THEME
define("WLT_CHILDTHEME", true);

// CHILD THEME LAYOUT SETTINGS
function childtheme_designchanges(){
				
				// LOAD IN CORE STYLES AND UNSET THE LAYOUT ONES SO OUR CHILD THEME DEFAULT OPTIONS CAN WORK
				$core_admin_values = get_option("core_admin_values"); 
			 
					// SET HEADER
					$core_admin_values["layout_header"] = "";
					// SET MENU
					$core_admin_values["layout_menu"] = "2";
					// SET RESPONISVE DESIGN
					$core_admin_values["responsive"] = "1";
					// SET COLUMN LAYOUTS
					$core_admin_values["layout_columns"] = array('homepage' => '3', 'search' => '1', 'single' => '1', 'page' => '1', 'footer' => '', '2columns' => '0', 'style' => 'fixed', '3columns' => '');
					// SET WELCOME TEXT
					$core_admin_values["header_welcometext"] = "Your own text here.";        
					// SET RATING
					$core_admin_values["rating"] 		= "1";
					$core_admin_values["rating_type"] 	= "1";
					// BREADCRUMBS
					$core_admin_values["breadcrumbs_inner"] 	= "1";
					$core_admin_values["breadcrumbs_home"] 		= "0"; 
					// TURN OFF CATEGORY DESCRIPTION
					$core_admin_values["category_descrition"] 	= "1";	
					// GEO LOCATION
					$core_admin_values["geolocation"] 	= "1";
					$core_admin_values["geolocation_flag"] 	= "AF";
					// FOOTER SOCIAL ICONS
					$core_admin_values["social"] 	= array(
					'twitter' => '##', 'twitter_icon' => 'fa-twitter', 
					'facebook' => '##', 'facebook_icon' => 'fa-facebook', 
					'dribbble' => '', 'dribbble_icon' => 'fa-google-plus', 
					'linkedin' => '##', 'linkedin_icon' => 'fa-linkedin', 
					'youtube' => '##', 'youtube_icon' => 'fa-youtube', 
					'rss' => '##', 'rss_icon' => 'fa-rss',         
					);
					// FOOTER COPYRIGHT TEXT
					$core_admin_values["copyright"] 	= "© Copyright 2014 - http://localhost/WP";
					// HOME PAGE OBJECT SETUP
					$core_admin_values["homepage"]["widgetblock1"] = "new8_12,categoryblock_1,carsousel_0,";	
					
$core_admin_values['widgetobject']['new8']['12'] = array(
'text' => "<section><div class=\"wlt_object_head_1\"><div class=\"content_bottom\"><div id=\"banner1\" class=\"banner\"><div class=\"col-md-4 col-sm-4 col-xs-12\"><div class=\"s-desc\"><h3>Stuff to sell?</h3><h4>Get listed today</h4>List your classifieds here today</div></div><div class=\"col-md-4 col-sm-4 col-xs-12\"><div class=\"s-desc\"><h5>Call us now toll free:</h5><h3>0123-456-789</h3>Our team are standing by to help you</div></div><div class=\"col-md-4 col-sm-4 col-xs-12\"><div class=\"s-desc\"><h3>Join Now</h3><h5>REGISTRATION is free</h5>manage your classifieds and favourite items</div></div></div></div></div></section>",
'fullw' => "yes",
);
$core_admin_values['widgetobject']['categoryblock']['1'] = array(
'title' => "Popular Website Categories",
'btnview' => "yes",
'image' => "yes-side",
'desc' => "yes",
'iconsize' => "yes",
'catcount' => "yes",
'pcats' => "",
'subcats' => "3",
'subcatcount' => "yes",
'subcatempty' => "yes",
'fullw' => "yes",
);
$core_admin_values['widgetobject']['carsousel']['0'] = array(
'title' => "Recently Added Classifieds",
'query' => "",
'tax' => "",
'arrows' => "default",
'fullw' => "yes",
);	
					// SET ITEMCODE
					$core_admin_values["itemcode"] 	= "[IMAGE]<span class='ftext'>[price]</span>[/IMAGE][RATING]<div class='caption'><h1>[TITLE]</h1><div class='hidden_details'><ul><li>[ICON id='fa fa-group' fa=1] [hits] user views [ICON id='fa fa-comments' fa=1] [COMMENT_COUNT] reviews <i class='fa fa-clock-o'></i> Added [DATE] </li> <li> [ICON id='map-marker'] [LOCATION]  </li></ul>  <hr />[EXCERPT size=260]... <div class='clearfix'></div><hr />[BUTTON] <span class='right hidden-xs hidden-sm' style='padding-right:20px; line-height:40px;'>[FAVS]</span>[ICON id='fa fa-tags' fa=true] [CATEGORY] [TAGS]  [DISTANCE] </div></div>";
					// SET LISTING PAGE CODE
					$core_admin_values["listingcode"] 	= "<div class='block'><div class='pricestricker'>[price]</div><div class='block-title'><h1><span>[TITLE]</span></h1></div><div class='block-content'>[IMAGES]<hr /> [CONTACT button=1] [ICON id='map-marker'] [LOCATION]<hr /><div class='clearfix'></div>[DISTANCE]<ul class='nav nav-tabs' id='Tabs'><li class='active'><a href='#t1' data-toggle='tab'>Description</a></li><li><a href='#t2' data-toggle='tab'>Details</a></li>   <li><a href='#t4' data-toggle='tab'>Comments</a></li></ul><div class='tab-content'><div class='tab-pane active' id='t1'>[TOOLBOX]<h5>[TITLE]</h5>[CONTENT] [GOOGLEMAP] </div><div class='tab-pane' id='t2'>[FIELDS hide='map']</div> <div class='tab-pane' id='t4'>[COMMENTS]</div></div></div></div>";
					// SET PRINT PAGE CODE
					$core_admin_values["printcode"]  = "<div class='center'>            <p id='postTitle'>[TITLE-NOLINK]</p>            <p id='postMeta'>Date:<strong>[DATE]</strong>  </p>            <p id='postLink'>[LINK]</p>               <div id='postContent'>[CONTENT]</div>                 <div id='postFields'>[FIELDS]</div>            <p id='printNow'><a href='#print' onClick='window.print(); return false;' title='Click to print'>Print</a></p>            </div>";						
					// RETURN VALUES
					return $core_admin_values;
}
// FUNCTION EXECUTED WHEN THE THEME IS CHANGED
function _after_switch_theme(){
	// SAVE VALUES
	update_option("core_admin_values",childtheme_designchanges());		
}
add_action("after_switch_theme","_after_switch_theme");
// DEMO MODE
if(defined("WLT_DEMOMODE")){ 
	$GLOBALS["CORE_THEME"] = childtheme_designchanges();
}?>