<?php
/* 
* Theme: PREMIUMPRESS CORE FRAMEWORK FILE
* Url: www.premiumpress.com
* Author: Mark Fail
*
* THIS FILE WILL BE UPDATED WITH EVERY UPDATE
* IF YOU WANT TO MODIFY THIS FILE, CREATE A CHILD THEME
*
* http://codex.wordpress.org/Child_Themes
*/
if (!defined('THEME_VERSION')) {	header('HTTP/1.0 403 Forbidden'); exit; }
?>

<?php global $CORE, $post; if(isset($GLOBALS['flag-search']) ){ $canShowMap = get_post_meta($post->ID,'showgooglemap',true); }else{ $canShowMap = false; }

ob_start(); ?>

<div class="itemdata icons itemid<?php echo $post->ID; ?> <?php hook_item_class(); ?>" <?php echo $CORE->ITEMSCOPE('itemtype'); ?>>

<div class="thumbnail clearfix">
 
	<div class="col-md-11">
    
    [IMAGE hover=1 hover_content=5] 
    
        <div class="content"> 
        
            <h2>[TITLE] [RATING small=1 style=1]</h2>
         
            <p>[LOCATION] [DISTANCE] [phone] </p>
         
           
           
            <div class="hidden_grid"> [EXCERPT size=180]  </div>
            
        </div>  

    </div>
    
    
    <div class="col-md-1 hidden_grid">
    
    <ul>
    <?php if($GLOBALS['CORE_THEME']['show_account_favs'] == '1'){ ?>
    <li>[FAVS]</li>
	<?php } ?>
    <?php if( $canShowMap == "yes"){ ?>
    <li><a href="#top" class="mapbtn"><?php echo $CORE->_e(array('button','52')); ?></a></li>
    <?php } ?>
    <li><a href="[LINK]" class="detbtn"><?php echo $CORE->_e(array('button','13')); ?></a></li>
    </ul>
    
       
    </div> 
 
</div>
 
</div>
<?php if( $canShowMap == "yes"){ ?>
<script>
jQuery(document).ready(function(){ 

	jQuery('.itemid<?php echo $post->ID; ?> .mapbtn').on('click', function(){ 		
		
		loadGoogleMapsApi();
		if(MapTriggered != "yes"){
			setTimeout(function(){ zoomItemMarker(<?php echo $post->ID; ?>); }, 2000); 
		}else{
			zoomItemMarker(<?php echo $post->ID; ?>);
 		}
		
	});

});
</script>
<?php } ?>
<?php 
$SavedContent = ob_get_clean(); 
?>
<?php echo hook_item_cleanup($CORE->ITEM_CONTENT($post, hook_content_listing($SavedContent))); ?>  