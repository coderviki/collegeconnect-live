<li id="dropdown-$ID" class="$LinkingMode<%if $LinkingMode == "current" || $LinkingMode == "section" %> active<%end_if%><% if $Children %><% if not $Parent %> dropdown<% else %> dropdown-submenu<% end_if %><% end_if %>">
    <a href="$Link" title="$Title.XML">$MenuTitle.XML<% if $Children %> <span class="caret"  data-toggle="dropdown" data-target="#dropdown-$ID" aria-expanded="false"><i></i></span><% end_if %></a><% if $Children %>
    <ul class="dropdown-menu" role="menu">
        <% loop $Children %><% include NavigationItem %><% if not $Last%><li class="divider"></li><% end_if %><% end_loop %></ul><% end_if %>
</li>
