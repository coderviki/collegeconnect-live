<% include HeaderBar %>
<div class="container">
    <div class="row">
        <% if OtherAlbums %>
        $setHasLeft(true)
        <div class="$LeftCssClass">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Other Albums</h3>
                </div>
                <ul class="nav nav-pills nav-stacked">
                    <% loop OtherAlbums %>
                    <li role="presentation"  class="<%if $LinkingMode == "current" %>active<%end_if%> $LinkingMode">
                        <a href="$Link" class="$LinkingMode" title="Go to the $Name.XML page">
                            <span class="text">$Name</span>
                        </a>
                    </li>
                    <% end_loop %>
                </ul>
            </div>
            <% loop $WidgetArea(LeftSideBar) %>
            $WidgetHolder
            <% end_loop %>
        </div>
        <% else %>
        <% include LeftSideBar %>
        <% end_if %>
        <div class="$CenterCssClass">
            <article class="typography">
                <% include PageHeader %>
                <article class="content typography">
                    $Content
                    <% if PhotoAlbum.Description %><p>$PhotoAlbum.Description</p><% end_if %>
                    <% if $PaginatedPhotos %>
                    <ul id="album-photos" class="thumbnail-list">
                        <% loop $PaginatedPhotos %>
                        <li>
                            <div class="thumbnail-container">
                                <% if $Type == "Photo" %>
                                <a href="$PhotoSized(800,800).URL" rel="shadowbox[Gallery]" title="$Caption"  style="$ThumbStyle(125,125)">
                                    <img src="$PhotoCropped(125,125).URL" alt="$Caption" />
                                    <span></span>
                                </a>
                                <% else_if $Type == "Video" %>
                                <a href="$VideoItem.GetEmbedUrl(800,800,1)" rel="shadowbox[Gallery];$VideoRel(800,800)" title="$Caption" style="$ThumbStyle(125,125)">
                                    <img src="$VideoItem.ThumbURL" alt="$Caption" style="width: 100%; height: auto; max-width: 100%; " />
                                    <span></span>
                                </a>
                                <% end_if %>
                            </div>
                        </li>
                        <% end_loop %>
                    </ul>
                    <% with $PaginatedPhotos %>
                    <% include Pagination %>
                    <% end_with %>
                    <% else %>
                    <p>There are no photos in this album.</p>
                    <% end_if %>
                </article>
                $Form
                $PageComments
            </article>
        </div>
        <% include RightSideBar %>
    </div>
</div>
<% include FooterBar %>



