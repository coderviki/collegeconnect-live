"use strict";
var gulp = require("gulp");
var path = require("path");
var plugins = require("gulp-load-plugins")({pattern: ["gulp-*", "gulp.*", "lazypipe", "run-sequence", "through2", "sprity"]});
var merge = require('merge-stream');
var fs = require('fs');
var escRegex = require("escape-string-regexp");
var argv = require('yargs').argv;
var spawn = require('cross-spawn');
var process = require('process');
var options = {
    debug: true,
    scss: {
        src: {
            pattern: "/**/*.scss",
            folder: "css/scss"
        },
        dest: "css",
        options: {
            style: "compressed",
            loadPath: [
                "/bower_components/bootstrap-sass/assets/stylesheets",
                "/bower_components/bootstrap-sass/fontawesome/scss",
                "/bower_components/bourbon/app/assets/stylesheets"
            ]
        }
    },
    js: {
        src: ["javascript/theme/**/*.js", "!javascript/theme/**/*min.js"],
        dest: "javascript/theme",
        lint: {
            options: {
                "lookup": false,
                "boss": true,
                "curly": true,
                "eqeqeq": true,
                "eqnull": true,
                "expr": true,
                "immed": true,
                "noarg": true,
                "quotmark": "double",
                "onevar": false,
                "smarttabs": true,
                "trailing": true,
                "unused": true,
                "strict": true,
                "node": false,
                "predef": ["window", "dateFormat", "moment", "responsiveHelper", "jQuery", "debugHelper"]
            },
            reporter: "jshint-stylish"
        }
    },
    vendor: {
        bowerFile: "./bower.json",
        src: {
            javascript: ["**/*.js"],
            css: ["**/*.css"],
            fonts: ["**/*.eot", "**/*.svg", "**/*.ttf", "**/*.woff", "**/*.woff2"]
        },
        dest: {
            javascript: "javascript/vendor",
            css: "css/vendor",
            fonts: "fonts/vendor"
        }

    },
    templates: {
        src: "templates/**/*.ss"
    },
    php: {
        src: "../../mysite/**/*.php"
    },
    sprites: {
        src: "images/sprites/",
        dest: "images/sprites/",
        scssDir: "includes/_generated_sprites" // Relative to options.scss.src
    }
};
var processors = [
    require('autoprefixer')({browsers: ['last 2 version']}),
    require('css-mqpacker'),
    require('cssnano')
];

var minifyJS = plugins.lazypipe()
        .pipe(plugins.ignore.exclude, "*.min.js")
        //send notification that Uglify has started
        .pipe(plugins.notify, {message: "Uglify minify: <%= file.relative %>", sound: false})
        // Initialise source maps for uglify
        .pipe(plugins.sourcemaps.init)
        // Add the .min suffix to javascript files
        .pipe(plugins.rename, {suffix: ".min"})
        // Minify javascript files
        .pipe(plugins.uglify, {preserveComments: "license"})
        // Save javascript source maps to the pipe
        .pipe(plugins.sourcemaps.write, ".");

var minifyCSS = plugins.lazypipe()
        .pipe(plugins.ignore.exclude, "*.min.css")
        //send notification that CSSNano has started
        .pipe(plugins.notify, {message: "CSSNano minify: <%= file.relative %>", sound: false})
        // Initialise source maps for minifyCss
        .pipe(plugins.sourcemaps.init, {identityMap: true})
        // Add the .min suffix to CSS files
        .pipe(plugins.rename, {suffix: ".min"})
        // Minify CSS files
        .pipe(plugins.postcss, processors)
        // Save CSS source maps to the pipe
        .pipe(plugins.sourcemaps.write, ".", {includeContent: false, sourceRoot: 'scss'});

var compileSCSS = plugins.lazypipe()
        .pipe(plugins.plumber)
        .pipe(plugins.sassGraphAbs, [path.join(__dirname, options.scss.src.folder)])
        //filter out partials (folders and files starting with "_" )
        .pipe(plugins.filter, function (file) {
            var hasPartialInPath = /[\/|\\]_/.test(file.path);
            var isPartialFileName = /^_/.test(file.relative);
            var include = !hasPartialInPath && !isPartialFileName;
            return include;
        })
        //send notification that scss compiling has started
        .pipe(plugins.notify, {message: "SCSS Compile: <%= file.relative %>", sound: false})
        // Initialise source maps for scss
        .pipe(plugins.sourcemaps.init, {identityMap: true})
        //process scss files
        .pipe(plugins.sass, options.scss.options)
        .pipe(plugins.postcss, [
            require('autoprefixer')({browsers: ['last 2 version']}),
            require('css-mqpacker')])
        // Save scss source maps to the pipe
        .pipe(plugins.sourcemaps.write, ".", {includeContent: false, sourceRoot: 'scss'})
        //save all the files
        .pipe(gulp.dest, options.scss.dest)
        //send notification that scss compiling has ended (First filtering out the map file)
        .pipe(plugins.filter, function (file) {
            return /css$/.test(file.relative);
        })
        .pipe(plugins.notify, {message: "SCSS Save: <%= file.relative %>", sound: false})
        // Trigger live reload with the destination CSS files
        .pipe(plugins.livereload)
        .pipe(function () {
            return plugins.through2.obj(function (file, enc, cb) {
                cb();
                spawn('gulp', ["minify-css", "--silent", "--css", file.path], {stdio: 'inherit', stderr: 'inherit'});
            });
        });


var checkJS = plugins.lazypipe()
        .pipe(plugins.jshint, options.js.lint.options)
        .pipe(plugins.jshint.reporter, options.js.lint.reporter);

gulp.task("config-sync", function () {
    gulp.src(["bower.json", "composer.json"])
            .pipe(plugins.configSync({
                fields: [
                    "name",
                    "version",
                    "description"
                ]
            }))
            .pipe(gulp.dest(".")); // write it to the same dir
});
gulp.task("vendor-install", function (cb) {
    return plugins.runSequence(
            "vendor-download",
            "vendor-copy",
            cb);
});
gulp.task("vendor-download", function () {
    return plugins.bower();
});
gulp.task("vendor-copy", function () {
    var jsFilter = plugins.filter(options.vendor.src.javascript, {restore: true});
    var cssFilter = plugins.filter(options.vendor.src.css, {restore: true});
    var fontFilter = plugins.filter(options.vendor.src.fonts, {restore: true});
    var bowerConfig = JSON.parse(fs.readFileSync(options.vendor.bowerFile, "utf-8"));
    var overrides = bowerConfig && bowerConfig.overrides;
    return gulp.src(options.vendor.bowerFile)
            // Get all main bower files from bower.json
            .pipe(plugins.mainBowerFiles(
                    {
                        "includeDev": true,
                        "debugging": options.debug
                    }
            ))
            // Flatten filenames to vendor-name/file.ext (e.g. bootstrap/assets/javascripts/bootstrap.js becomes bootstrap/bootstrap.js)
            .pipe(plugins.rename(function (filePath) {
                var pathParts = filePath.dirname.split(path.sep);
                var name = pathParts[0];
                var base = overrides[name] && overrides[name].base;
                var dest = overrides[name] && overrides[name].dest;
                if (base) {
                    var replaceRegex = new RegExp(".*" + escRegex(base.replace("/", path.sep)));
                    filePath.dirname = path.join(name, filePath.dirname.replace(replaceRegex, ""));
                } else if (dest) {
                    filePath.dirname = path.dirname(dest);
                    filePath.basename = path.basename(dest);
                } else {
                    filePath.dirname = name;
                }
                // Strip -sass suffix from vendor-name (e.g. boostrap-sass becomes boostrap)
                filePath.dirname = filePath.dirname.replace(/-sass$/, "");
            }))
            /**
             * Javascsript
             */
            // Filter out javascript files
            .pipe(jsFilter)
            //Create a mirror of the pipe so we are able to save the originals plus the minified
            .pipe(plugins.mirror(
                    //Save original vendor javascript files
                    gulp.dest(options.vendor.dest.javascript),
                    // Minify and create source maps of vendor javascript files
                    minifyJS()
                    ))
            //Save minified and maps of vendor javascript files
            .pipe(gulp.dest(options.vendor.dest.javascript))
            //Reset the js filter
            .pipe(jsFilter.restore)
            /**
             * CSS
             */
            // Filter out CSS files
            .pipe(cssFilter)
            .pipe(plugins.mirror(
                    //Save original vendor CSS files
                    gulp.dest(options.vendor.dest.css),
                    // Minify and create source maps of vendor CSS files
                    minifyCSS()
                    ))
            //Save minified and maps of vendor CSS files
            .pipe(gulp.dest(options.vendor.dest.css))
            //Reset the CSS filter
            .pipe(cssFilter.restore)
            /**
             * Fonts
             */
            // Filter out font files
            .pipe(fontFilter)
            // Save font file
            .pipe(gulp.dest(options.vendor.dest.fonts))
            .pipe(fontFilter.restore);
});
gulp.task("compile-scss", function (cb) {
    return gulp.src(options.scss.src.folder + "/*.scss")
            .pipe(compileSCSS())
            // Catch errors and send a notification
            .on("error", plugins.notify.onError({message: "Error: <%= error.message %>", sound: false}));
});

gulp.task("watch-scss", function (cb) {
    if (!plugins.livereloadserver) {
        plugins.livereload.listen();
    }
    return  plugins.watch(options.scss.src.folder + options.scss.src.pattern)
            // Catch errors and send a notification
            .pipe(plugins.plumberNotifier())
            .pipe(compileSCSS());
});
gulp.task("minify-css", function () {
    var files = argv.css ? argv.css : [options.scss.dest + "/*.css", "!" + options.scss.dest + "/*min.css"];
    return gulp.src(files)
            .pipe(minifyCSS())
            .pipe(gulp.dest(options.scss.dest))
            .pipe(plugins.filter(function (file) {
                return /css$/.test(file.relative);
            }))
            .pipe(plugins.notify({message: "CSSNano Save: <%= file.relative %>", sound: false}));
    ;
});
gulp.task("watch-css", function () {
    return plugins.watch([options.scss.dest + "/*.css", "!" + options.scss.dest + "/*min.css"])
            .pipe(plugins.notify({message: "Minnify: <%= file.relative %>", sound: false}))
            .pipe(minifyCSS())
            .pipe(gulp.dest(options.scss.dest));
});
gulp.task("check-js", function () {
    return gulp.src(options.js.src)
            .pipe(plugins.notify({message: "Check JS: <%= file.relative %>", sound: false}))
            .pipe(checkJS());
    ;
});
gulp.task("minify-js", function () {
    return gulp.src(options.js.src)
            .pipe(checkJS())
            .pipe(minifyJS())
            .pipe(plugins.notify({message: "Uglify Save: <%= file.relative %>", sound: false}))
            .pipe(gulp.dest(options.js.dest));
});
gulp.task("watch-js", function () {
    if (!plugins.livereloadserver) {
        plugins.livereload.listen();
    }
    return plugins.watch(options.js.src)
            .pipe(checkJS())
            .pipe(plugins.livereload())
            .pipe(minifyJS())
            .pipe(gulp.dest(options.js.dest));
});
gulp.task("watch-templates", function () {
    if (!plugins.livereloadserver) {
        plugins.livereload.listen();
    }
    return plugins.watch(options.templates.src)
            .pipe(plugins.notify({message: "Template changed: <%= file.relative %>", sound: false}))
            .pipe(plugins.livereload());
});
gulp.task("watch-php", function () {
    if (!plugins.livereloadserver) {
        plugins.livereload.listen();
    }
    return plugins.watch(options.php.src)
            .pipe(plugins.notify({message: "PHP file changed: <%= file.relative %>", sound: false}))
            .pipe(plugins.livereload());
});

function getSpriteNames() {
    return fs
            .readdirSync(options.sprites.src)
            .filter(function (file) {
                return fs.statSync(path.join(options.sprites.src, file)).isDirectory();
            });
}

function createSprites(folder) {
    var spriteName = path.basename(folder);
    var spriteData = gulp.src(path.join(options.sprites.src, folder, '*.png'))
            .pipe(plugins.spritesmith({
                imgPath: "../" + options.sprites.dest + spriteName + '.png',
                imgName: spriteName + '.png',
                cssName: "_" + spriteName + '.scss',
                cssSpritesheetName: spriteName,
                cssVarMap: function (sprite) {
                    sprite.name = spriteName + '-' + sprite.name;
                }
            }));
    var imgStream = spriteData.img
            .pipe(gulp.dest(options.sprites.dest))
            .pipe(plugins.notify({message: "Sprite image: <%= file.relative%>", sound: false}));
    var sassStream = spriteData.css
            .pipe(plugins.replace(', );', ');'))
            .pipe(plugins.replace(/url\((#\{\$[a-z\-]+\})\)/i, 'url("$1")'))
            .pipe(plugins.stripCssComments())
            .pipe(plugins.replace(/\r?\n\s*\n\r?/g, "\n"))
            .pipe(plugins.trim())
            .pipe(gulp.dest(path.join(options.scss.src.folder, options.sprites.scssDir)))
            .pipe(plugins.notify({message: "Sprite sass: <%= file.relative %>", sound: false}));
    return merge(imgStream, sassStream);
}
function createAllSpritesSCSS() {
    var spriteNames = getSpriteNames();
    var allScssImports = spriteNames.map(function (spriteName) {
        return "@import \"_" + spriteName + ".scss\";";
    });
    var allSpriteVars = spriteNames.map(function (spriteName) {
        return "$" + spriteName + "-sprites";
    });
    var allScssTxt = allScssImports.join("\n") + "\n\n" + "$all-sprites: (" + allSpriteVars.join(",") + ");" + "\n";
    return plugins.file('_all.scss', allScssTxt, {src: true})
            .pipe(gulp.dest(path.join(options.scss.src.folder, options.sprites.scssDir)))
            .pipe(plugins.notify({message: "Sprite main: <%= file.relative %>", sound: false}));
}
gulp.task('compile-sprites', function () {
    var spriteNames = getSpriteNames();
    var tasks = spriteNames.map(createSprites);
    return merge(tasks, createAllSpritesSCSS());
});

gulp.task("watch-sprites", function () {
    if (!plugins.livereloadserver) {
        plugins.livereload.listen();
    }
    return plugins.watch(path.join(options.sprites.src, "*/*.png"), function (file) {
        gulp.src(file.path).pipe(plugins.notify({message: "Sprite changed: <%= file.relative %>", sound: false}));
        return merge(createSprites(path.basename(path.dirname(file.path))), createAllSpritesSCSS());
    });
});

gulp.task("watch-all", ["watch-js", "watch-scss", "watch-sprites", "watch-templates", "watch-php"]);
gulp.task("compile-all", ["compile-scss", "minify-js"]);
gulp.task('design-watch-scss', function () {
    plugins.watch(options.scss.src.folder + options.scss.src.pattern)
            // Catch errors and send a notification
            .pipe(plugins.plumberNotifier())
            .pipe(compileSCSS());
});
gulp.task('design-watch-html', function () {
    plugins.watch("index.html")
            .pipe(plugins.notify({message: "HTML changed: <%= file.relative %>", sound: false}))
            .pipe(plugins.livereload());
});
gulp.task('design-webserver', function () {

    gulp.src('.')
            .pipe(plugins.serverLivereload({
                livereload: {
                    enable: true,
                    filter: function (filePath, cb) {
                        cb((!(/\.min\.css$/.test(filePath))) && (/(\.css|\.html)$/.test(filePath)));
                    }
                },
                open: true
            }));
});
gulp.task("design-mode", ["design-webserver", "watch-scss", "design-watch-html"], function () {
});
gulp.task('auto-reload', function () {
    var taskProcess;
    gulp.watch('gulpfile.js', restartTask);
    restartTask();
    function restartTask(e) {
        // kill previous spawned process
        if (taskProcess) {
            if (process.platform === "win32") {
                spawn("taskkill", ["/pid", taskProcess.pid, '/f', '/t']);
            } else {
                taskProcess.kill("SIGINT");
            }
        }
        // `spawn` a child `gulp` process linked to the parent `stdio`
        taskProcess = spawn('gulp', [argv.task], {stdio: 'inherit', stderr: 'inherit'});
    }
});

gulp.task("default", plugins.taskListing.withFilters(function (task) {
    return !task.match(/(\-all$|design\-mode)/);
}, function (task) {
    return task.match(/(^design\-w|^default$)/);
}));
