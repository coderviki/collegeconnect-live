# Bootstrap Silverstripe boilerplate

## Dev requirements
* Node.js:            http://nodejs.org/download/ (For Gulp, bower and nodesass)

## Dev dependencies
3rd party tools ([Node-sass](https://github.com/sass/node-sass),[Bower](http://bower.io/),[Gulp](http://http://gulpjs.com//)) and SASS utilities ([Bootstrap](https://github.com/twbs/bootstrap-sass), [FontAwesome](https://github.com/FortAwesome/font-awesome-sass), [Bourbon](http://bourbon.io/)) are required to compile this theme.
To install these, ensure you have [Node.js](http://nodejs.org/download/) installed, cd into the theme directory and run
```npm install```

## Gulp
.... To be documented

## Bower
Bower is a front end package manager (think of it as the [composer](https://getcomposer.org/) or [nuget](https://www.nuget.org/) for your 3rd party CSS, JavaScript and Font requirements).

Bower is not used directly, but via the `gulp vendor-install` task.

To add/edit packages edit the [bower.json](https://git2.gdmedia.tv/guru-digital-media/ss-theme-gurutemplate/blob/master/bower.json#L17) file.

3rd party libraries need to be copied from the `bower_components` folder and into the themes css/javascript/font folders. This can be automated with Gulp, look at [gulpfile.js](https://git2.gdmedia.tv/guru-digital-media/ss-theme-gurutemplate/blob/master/gulpfile.js#L54) for examples.
Some 3rd parties do not come with minified files, you can use gulp/uglify to minify these. Look at [gulpfile.js](https://git2.gdmedia.tv/guru-digital-media/ss-theme-gurutemplate/blob/master/gulpfile.js#L174) for examples.

Running `gulp vendor-install` will update bower dependencies as configured in [bower.json](https://git2.gdmedia.tv/guru-digital-media/ss-theme-gurutemplate/blob/master/bower.json#L17), copy them to the theme folder and minify any configured files.

## SCSS Structure
```
.
+-- css\scss
    |
    +-- config - Contains variables and settings, no files under config should emit output
    |   +-- _bootstrap-variables.scss - Variables for boostrap, See http://getbootstrap.com/customize/#less-variables
    |   +-- _theme-variables.scss - Theme specific variables
    |   +-- _variables.scss - Imports all variable files
    |
    +-- includes - Contains mixings and other helpers, no files under includes should emit output
    |   +-- _mixins.scss - Imports 3rd part mixins and contains default helper mixins
    |
    +-- partials - Main location of them files
    |   +-- components - Contains partials that relate to specific items
    |   |   +-- _breadcrumbs.scss - Helpers/additions for Bootstrap Breadcrumbs (http://getbootstrap.com/components/#breadcrumbs)
    |   |   +-- _collapse.scss - Helpers/additions for Bootstrap Collapse (http://getbootstrap.com/javascript/#collapse)
    |   |   +-- _date-time-picker.scss - Styling for the date time picker (https://eonasdan.github.io/bootstrap-datetimepicker/)
    |   |   +-- _dropdowns.scss - Helpers/additions for Bootstrap Dropdown (http://getbootstrap.com/components/#dropdowns)
    |   |   +-- _fonts.scss - Imports 3rd party fonts and contains default theme declarations
    |   |   +-- _forms.scss - Stying for form elements (https://docs.silverstripe.org/en/3.1/developer_guides/forms/introduction/)
    |   |   +-- _pagination.scss - Stying the pagination element (https://docs.silverstripe.org/en/3.1/developer_guides/templates/how_tos/pagination/)
    |   |   +-- _photo-gallery.scss - Styling photo gallery type elements
    |   |   +-- _results.scss - Styling result lists. E.g Search results, news listings
    |   |   +-- _side-navigation.scss - Styling side navigation panels and widgets
    |   |   +-- _social-links.scss - Styling for Social Network icons and links
    |   |   +-- _top-navigation.scss - Styling for sites main navigation bar
    |   |
    |   +-- layout - Contains partials for main layout elements
    |   |   +-- _footer.scss - Styling for global site footer
    |   |   +-- _header.scss - Styling for global site header ( masthead and _top-navigation )
    |   |   +-- _main.scss - Styling for sites main navigation bar
    |   |
    |   +-- pages - Contains partials that relate to specific page types
    |
    +-- vendor - Contains imports and helpers for 3rd party components
    |   +-- _boostrap.scss - Imports all required scss files for the Bootstrap framework (http://getbootstrap.com/)
    |   +-- _bourbon.scss - Imports all required scss files for the Bourbon toolset (http://bourbon.io/)
    |   +-- _font-awesome.scss - Imports all required scss files for the Font Awesome iconic font and CSS toolkit (http://fontawesome.io/)
    |
    +-- editor.scss - Used to add styles to the SilverStripe WYSIWYG (https://docs.silverstripe.org/en/3.1/developer_guides/customising_the_admin_interface/typography/)
    +-- layout.scss - Imports most partials to build the main theme CSS file
    +-- print.scss - Used for print mode styles
    +-- typography.scss - Global styling for typography items, such as heading, body copy, images etc
```


## Netbeans
~~Netbeans does not currently support node-sass. To work around this, save the bat file below, and set it as your sass compiler in Netbeans.~~
~~Some paths may need updating.~~
~~https://gist.github.com/cjsewell/deedba7269c532fc33fb~~
Replaced in favour of `gulp watch-scss`
