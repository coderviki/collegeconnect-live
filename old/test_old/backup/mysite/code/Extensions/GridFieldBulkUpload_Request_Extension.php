<?php

class GridFieldBulkUpload_Request_Extension extends Extension
{

    public function getdisplayfoldername()
    {
        $uploadField = $this->owner->getUploadField();
        return $uploadField->getFolderName();
    }
}