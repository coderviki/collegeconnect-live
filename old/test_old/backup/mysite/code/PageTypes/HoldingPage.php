<?php

class HoldingPage extends UserDefinedForm
{
    public static $icon = 'silverstripe-gdm-extensions/assets/images/sitetree-images/home.png';

    /**
     * @var array Fields on the user defined form page.
     */
    private static $db = array(
        "BeforeFormContent" => "HTMLText",
        "AfterFormContent"  => "HTMLText",
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab("Root.Main", HtmlEditorField::create("BeforeFormContent", "Content before Form"), "Metadata");
        $fields->addFieldToTab("Root.Main", HtmlEditorField::create("AfterFormContent", "Content after Form"), "Metadata");
        return $fields;
    }
//
}

class HoldingPage_Controller extends UserDefinedForm_Controller
{
    private static $allowed_actions = array(
        'index',
        'ping',
        'Form',
        'finished'
    );

    public function finished()
    {
        $submission = Session::get('userformssubmission'.$this->ID);

        if ($submission) {
            $submission = SubmittedForm::get()->byId($submission);
        }

        $referrer = isset($_GET['referrer']) ? urldecode($_GET['referrer']) : null;
        parent::finished();
        return $this->customise(array(
                'BeforeFormContent' => "",
                'AfterFormContent'  => "",
                'Form'              => $this->customise(array(
                    'Submission' => $submission,
                    'Link'       => $referrer
                ))->renderWith('ReceivedFormSubmission')
        ));
    }
}