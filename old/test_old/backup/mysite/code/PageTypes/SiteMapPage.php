<?php

class SitemapPage extends Page
{
    static $icon = "silverstripe-gdm-extensions/assets/images/sitetree-images/sitemap.png";

}

class SitemapPage_Controller extends Page_Controller
{

    public function init()
    {
        parent::init();
        Requirements::customCSS($this->generatePageIconsCss());
    }

    public function someTest($somevar)
    {
        Debug::dump($somevar);
    }
}