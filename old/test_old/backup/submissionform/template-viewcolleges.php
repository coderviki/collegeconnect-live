<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>View Colleges</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" type="text/css" href="TableFilter/filtergrid.css">
<script type="text/javascript" src="TableFilter/jquery-latest.js"></script> 
<script type="text/javascript" src="TableFilter/jquery.tablesorter.js"></script> 
<script type="text/javascript">
$(document).ready(function() 
    { 
        $("#table4").tablesorter(); 
    } 
); 
</script>

</head>
<body>

<h1>View Colleges</h1>

<?php
// Connects to your Database

mysql_connect("localhost:3306", "univers1_admin", "B@ctad89") or die(mysql_error()) ;

mysql_select_db("univers1_test") or die(mysql_error()) ;
$query1=mysql_query("SELECT * FROM College_Data ORDER BY ID");

// display data in table
echo "<a href=http://www.universityhub.ph/test/submissionform/template-addcollege-basicform-withdropdownmenu.php>Add a College</a> - Number of Colleges in Database: ";
$numberofcolleges = mysql_query("SELECT COUNT(*) from College_Data");
$row = mysql_fetch_array($numberofcolleges);

$total = $row[0];
echo $total;

echo "<br><br>";
echo "<table id='table4' class='mytable' border='1' cellpadding='0' cellspacing='0'>";
echo "<thead><tr> <th>ID</th> <th>College Name</th> <th>Logo</th> <th>City</th> <th>Description</th> <th>Term Type</th> <th>Entrance Exam</th> <th>College Website</th> <th>Contact Info</th> <th>Campus Locations</th> <th>Accredidation</th> <th>Tuition Local</th> <th>Tuition Foreign</th><th></th> <th></th></tr></thead>";
echo "<tfoot><tr> <td></td>   <td></td>             <td></td>     <td></td>     <td id='sum1' align='right'></td>        <td id='sum2' align='right'></td>        <td></td>        <td></td>        <td></td>        <td></td><td></td><td></td> <td><div align='center'></div></td>       </tr>    </tfoot>";
echo "<tbody>";

while($query2=mysql_fetch_array($query1))
{

// echo out the contents of each row into a table
echo "<tr>";
echo "<td>" . $query2['ID'] . "</td>";
echo "<td><img src=http://www.universityhub.ph/test/submissionform/images/logo/" . $query2['schoollogo'] . "></td>";
echo "<td>" . $query2['Name'] . "</td>";
echo "<td>" . $query2['City'] . "</td>";
echo "<td>" . $query2['Province'] . "</td>";
echo "<td>" . $query2['TermType'] . "</td>";
echo "<td>" . $query2['EntranceExam'] . "</td>";
echo "<td>" . $query2['collegewebsite'] . "</td>";
echo "<td>" . $query2['ContactInfo'] . "</td>";
echo "<td>" . $query2['CampusLocations'] . "</td>";
echo "<td>" . $query2['Accreditation'] . "</td>";
echo "<td>" . $query2['CostofTuitionLocal'] . "</td>";
echo "<td>" . $query2['CostofTuitionForeign'] . "</td>";
echo "<td><a href='template-editcollege.php?ID=" . $query2['ID'] . "'>Edit</a></td>";
echo "<td><a href='delete.php?id=" . $query2['ID'] . "'>Delete</a></td>";
echo "</tr>";
}

// close table>
echo "</tbody></table></body></html>";
?>
<script src="TableFilter/tablefilter_all.js" language="javascript" type="text/javascript"></script>
<script src="TableFilter/tablefilter_all_min.js" language="javascript" type="text/javascript"></script>
<script src="TableFilter/tablefilter.js" language="javascript" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//<![CDATA[
	var table4_Props = 	{
					on_change: true,
					btn: false,
					enter_key: true,
					 sort: true,   
    					/* sort_config: { sort_col: [1,false]  },  */
    					filters_row_index: 1,  
    					alternate_rows: true,

     					/* btn_reset: true,  
    					btn_reset_text: "Clear", */  
    					loader: true,  
    					/* loader_html: '<img src="loader.gif" alt="" '="" +="" 'style="vertical-align:middle; margin:0 5px 0 5px">' +  
                    '<span id="lblStatus"></span>', */
    					 loader_css_class: 'myLoader',   
    					/* status_bar: true,   */
    					/* status_bar_target_id: 'lblStatus',  */

    					/* rows_always_visible:[totRowIndex],  */
     					on_filters_loaded: function(o){ o.SetAlternateRows(); },  
    					custom_cell_data_cols: [0,13],  
    					custom_cell_data: function(o,c,i){    
        					if(i==0)  
        					{  
            						var chk = c.getElementsByTagName('input')[0];  
            						if(!chk || c.parentNode.parentNode.parentNode.nodeName=='TFOOT') return '';  
            						if(chk.checked) return 'yes';  
            						else return 'no';  
       						 } else {  
          						var slc = c.getElementsByTagName('select')[0];  
            						if(!slc) return 'none';  
           						else return (tf_isIE) ? slc.options[slc.options.selectedIndex].firstChild.data : slc.value;  
        					}  
   						 } ,  
				}
 
	var tf = setFilterGrid( "table4",table4_Props );
//]]>
</script>
</body>
</html>
