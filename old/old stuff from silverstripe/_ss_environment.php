<?php
/**
 * See http://doc.silverstripe.org/framework/en/topics/environment-management for options
 */
/* @var $enviromentConfig An array of configurations to use for each environment type */
$enviromentConfigs = array(
//    'dev'  => array(
  //      "SS_DATABASE_CLASS"       => 'MySQLPDODatabase',
    //    "SS_DATABASE_SERVER"      => 'localhost',
      //  "SS_DATABASE_USERNAME"    => 'root',
        //"SS_DATABASE_PASSWORD"    => '',
        //"SS_DATABASE_CHOOSE_NAME" => 1,
        //"SS_ENVIRONMENT_TYPE"     => 'dev',
        //"http_path"               => 'http://localhost/'.basename(dirname(__FILE__))
//    ),
    'prod' => array(
        "SS_DATABASE_CLASS"    => 'MySQLPDODatabase',
        "SS_DATABASE_SERVER"   => 'localhost',
        "SS_DATABASE_USERNAME" => 'collegec_admin',
        "SS_DATABASE_PASSWORD" => 'B@ctad89',
        "SS_DATABASE_NAME"     => 'collegec_ss',
        "SS_ENVIRONMENT_TYPE"  => 'live',
        "http_path"            => 'http://collegeconnect.ph'
    ),
    'prod' => array(
        "SS_DATABASE_CLASS"    => 'MySQLPDODatabase',
        "SS_DATABASE_SERVER"   => 'localhost',
        "SS_DATABASE_USERNAME" => 'collegec_admin',
        "SS_DATABASE_PASSWORD" => 'B@ctad89',
        "SS_DATABASE_NAME"     => 'collegec_ss',
        "SS_ENVIRONMENT_TYPE"  => 'dev',
        "http_path"            => 'http://collegeconnect.ph'
    ),
);

/* @var $enviroments An array of IP address / Host names to help config determin the current enviroment type */
$enviroments = array(
    'dev'  => array("::1", "127.0.0.1", "119.81.160.220", "COREY-LAPTOP", "192.168.2.15"),
    'uat'  => array(),
    'prod' => array("119.81.160.220"),
);

$currentServer = array();
if (filter_input(INPUT_SERVER, 'SERVER_ADDR')) {
    $currentServer[] = filter_input(INPUT_SERVER, 'SERVER_ADDR');
} else if (filter_input(INPUT_SERVER, 'LOCAL_ADDR')) {
    $currentServer[] = filter_input(INPUT_SERVER, 'LOCAL_ADDR');
}
if (filter_input(INPUT_SERVER, 'HTTP_HOST')) {
    $currentServer[] = filter_input(INPUT_SERVER, 'HTTP_HOST');
} else if (gethostbyname(php_uname("n")) != "127.0.0.1") {
    $currentServer[] = gethostbyname(php_uname("n"));
}
$currentServer[] = php_uname("n");

$enviromentConfig = null;
foreach ($enviroments as $enviroment => $addresses) {
    if (isset($enviromentConfigs[$enviroment]) && count(array_intersect($currentServer, $addresses))) { // Current enviroment is PROD
        $enviromentConfig = $enviromentConfigs[$enviroment];
        break;
    }
}

if ($enviromentConfig) {
    foreach ($enviromentConfig as $key => $value) {
        if (strpos($key, "SS_") === 0 && !defined($key)) {
            define($key, $value);
        } else if ($key == "http_path") {
            global $_FILE_TO_URL_MAPPING;
            $_FILE_TO_URL_MAPPING[dirname(SS_ENVIRONMENT_FILE)] = $enviromentConfig['http_path'];
        }
    }
} else { // Unable to determin enviroment type
    die("Unable to determin server type (dev,uat,prod)? '".'<br/><pre class="debug">'.PHP_EOL.print_r($currentServer, true).PHP_EOL.'</pre>');
}

if (defined("SS_ENVIRONMENT_TYPE") && SS_ENVIRONMENT_TYPE == "dev") {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else {
    error_reporting(E_ALL & ~E_WARNING);
    ini_set('display_errors', 'Off');
}

/**
 * Ensure we always have login to the CMS
 */
define('SS_DEFAULT_ADMIN_USERNAME', 'admin');
define('SS_DEFAULT_ADMIN_PASSWORD', 'TeIWLvDT');


define('SS_ERROR_LOG', "ss.error.log");
