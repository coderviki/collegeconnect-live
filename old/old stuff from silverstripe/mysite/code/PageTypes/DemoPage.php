<?php

class DemoPage extends Page {

}

class DemoPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
		'FormHorizontal', 'FormNormal', 'FormInline'
	);

	public function FormHorizontal() {
		$fields		 = new FieldList(array(
			EmailField::create("Email", "Email"),
			PasswordField::create("Password", "Password"),
			FieldGroup::create(
					CheckboxField::create("Checkbox", "Checkbox")
			)->setTitle("Checkboxes"),
			CheckboxField::create("Checkbox2", "Checkbox"),
			TextareaField::create("Textarea", " Textarea")->setDescription("A longer block of help text that breaks onto a new line and may extend beyond one line."),
			OptionsetField::create("Radios", "Radios", array("1" => "Option one is this", "2" => "Option two can be something else")),
			DropdownField::create("DropdownField", "Selects", array("1" => "1", "2" => "2", "3" => "3", "4" => "4", "5" => "5")),
			ListboxField::create("ListboxField", "Selects", array("1" => "1", "2" => "2", "3" => "3", "4" => "4", "5" => "5"), 1, 4, 1)->setTitle(false),
			DateField::create("Date", "Date"),
			TimeField::create("Time", "Time"),
			DatetimeField::create("Datetime", "Date and time"),
			FileField::create("Upload", "Uploading"),
			CreditCardField::create("CreditCardField", "Credit Card"),
			CurrencyField::create("CurrencyField", "Currency"),
			NumericField::create("NumericField", "Numeric"),
			PhoneNumberField::create("PhoneNumberField", "Phone Number"),
			CountryDropdownField::create("CountryDropdownField", "Countries", "NZ"),
		));
		$actions	 = new FieldList(array(
			FormAction::create("Submit")->setTitle("Submit"),
			ResetFormAction::create("Cancel")->setTitle("Cancel")
		));
		$validator	 = ZenValidator::create();
		$validator->addRequiredFields(array(
			'Email'				 => 'Please enter your rmail',
			'Password'			 => 'Please enter your password',
			'Checkbox'			 => 'Please check this box',
			'CreditCardField'	 => '4 digits required',
		));
		$validator->setConstraint('Email', Constraint_type::create(Constraint_type::EMAIL)->setMessage("Please enter a valid email address"));
		$validator->setConstraint('CreditCardField', Constraint_type::create(Constraint_type::DIGITS)->setMessage("4 digits required"));
		$validator->setConstraint('CreditCardField', Constraint_length::create(Constraint_length::RANGE, 4, 4)->setMessage("4 digits required"));

		$form = new Form($this, 'Form', $fields, $actions, $validator);
		$form->setLegend("Legend")->addExtraClass("form-horizontal")->setAttribute("data-toggle", "validator");

		// Load the form with previously sent data
		$form->loadDataFrom($this->request->postVars());
		return $form;
	}

	public function FormNormal() {
		$fields		 = new FieldList(array(
			EmailField::create("FormNormalEmail", "Email"),
			PasswordField::create("FormNormalPassword", "Password"),
			CheckboxField::create("FormNormalCheckbox", "Checkbox"),
		));
		$actions	 = new FieldList(array(
			FormAction::create("FormNormalSubmit")->setUseButtonTag(true)->setButtonContent("Submit"),
			ResetFormAction::create("FormNormalCancel")->setUseButtonTag(true)->setButtonContent("Cancel")
		));
		$validator	 = ZenValidator::create();
		$validator->addRequiredFields(array(
			'FormNormalEmail'	 => 'Please enter your rmail',
			'FormNormalPassword' => 'Please enter your password',
			'FormNormalCheckbox' => 'Please check this box',
		));
		$validator->setConstraint('FormNormalEmail', Constraint_type::create('email')->setMessage("Please enter a valid email address"));

		$form = new Form($this, 'FormNormal', $fields, $actions, $validator);
		$form->setLegend("Legend")->setAttribute("data-toggle", "validator");

		// Load the form with previously sent data
		$form->loadDataFrom($this->request->postVars());
		return $form;
	}

	public function FormInline() {
		$fields		 = new FieldList(array(
			EmailField::create("FormInlineEmail", "Email"),
			PasswordField::create("FormInlinePassword", "Password"),
			CheckboxField::create("FormInlineCheckbox", "Checkbox"),
		));
		$actions	 = new FieldList(array(
			FormAction::create("Submit")->setTitle("Submit"),
		));
		$validator	 = ZenValidator::create();
		$validator->addRequiredFields(array(
			'FormInlineEmail'	 => 'Please enter your rmail',
			'FormInlinePassword' => 'Please enter your password',
			'FormInlineCheckbox' => 'Please check this box',
		));
		$validator->setConstraint('FormInlineCheckbox', Constraint_type::create('email')->setMessage("Please enter a valid email address"));
		$form		 = new Form($this, 'Form', $fields, $actions, $validator);
		$form->setLegend("Legend")->addExtraClass("form-inline")->setAttribute("data-toggle", "validator");

		// Load the form with previously sent data
		$form->loadDataFrom($this->request->postVars());
		return $form;
	}

	public function Submit() {
		return array();
	}

	/**
	 * Returns a paginated list of all pages in the site.
	 */
	public function PaginatedPages() {
		return new PaginatedList(Page::get(), $this->request);
	}

}
