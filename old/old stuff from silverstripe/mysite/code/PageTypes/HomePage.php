<?php

class HomePage extends SSGuru_HomePage
{
    static $hide_ancestor = 'SSGuru_HomePage';

}

class HomePage_Controller extends Page_Controller
{
    private static $allowed_actions = array(
        "SignUp"
    );

    public function Form()
    {
        $fields    = new FieldList(array(
            TextField::create("FullName", "Full Name"),
            EmailField::create("Email", "Email")
        ));
        $actions   = new FieldList(array(
            FormAction::create("SignUp")->setTitle("Notify Me"),
        ));
        $validator = ZenValidator::create();
        $validator->addRequiredFields(array(
            'FullName' => 'Please enter your password',
            'Email'    => 'Please enter your email',
        ));
        $validator->setConstraint('Email', Constraint_type::create(Constraint_type::EMAIL)->setMessage("Please enter a valid email address"));
        $form      = new Form($this, 'Form', $fields, $actions, $validator);
        // Load the form with previously sent data
        $form->loadDataFrom($this->request->postVars());
        return $form;
    }
}