<?php

/**
 * Adds new global settings.
 */
class CustomSiteConfig_Extension extends DataExtension
{
    static $db        = array(
        'GACode'                => 'Varchar(16)',
        'Copyright'             => 'HTMLText',
        'ShowTitleInHeader'     => 'Boolean',
        'FooterLogoDescription' => 'Text',
    );
    static $has_one   = array(
        'Logo'       => 'Image',
        'FooterLogo' => 'Image'
    );
    static $many_many = array(
        'FooterLogoLink' => 'Link',
        'FooterLinks'    => 'SiteTree',
    );

    function updateCMSFields(FieldList $fields)
    {
        // Main
        $fields->addFieldToTab('Root.Main',
                               $gaCode    = new TextField('GACode',
                                                          'Google Analytics account'));
        $gaCode->setRightTitle('Account number to be used all across the site (in the format <strong>UA-XXXXX-X</strong>)');
        $fields->addFieldToTab('Root.Main',
                               $showTitle = new CheckboxField('ShowTitleInHeader',
                                                              'Show title in header'),
                                                              'Tagline');
        /* @var $logoField UploadField */
        $logoField = new UploadField('Logo',
                                     'Large logo, to appear in the header.');
        $logoField->setAllowedFileCategories('image');
        $logoField->setConfig('allowedMaxFileNumber', 1);
        $fields->addFieldToTab('Root.Main', $logoField);

        //Footer
        $fields->addFieldToTab('Root.Footer',
                               $footerLogoField = new UploadField('FooterLogo'));
        $footerLogoField->setAllowedFileCategories('image');
        $footerLogoField->setConfig('allowedMaxFileNumber', 1);
        $fields->addFieldToTab('Root.Footer',
                               $footerLink      = new LinkField('FooterLogoLink',
                                                                'Footer Logo link'));
        $fields->addFieldToTab('Root.Footer',
                               new TextField('FooterLogoDescription',
                                             'Footer Logo description'));
        $fields->addFieldToTab('Root.Footer',
                               new TreeMultiselectField('FooterLinks',
                                                        'Footer Links',
                                                        'SiteTree'));
        $fields->addFieldToTab('Root.Footer',
                               new TextField('Copyright', 'Copyright'));
    }
}