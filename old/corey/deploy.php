<?php

/* @var $uatPath string Where to deploy this project on the UAT server */
$uatPath  = '';
/* @var $prodPath string Where to deploy this project on the Production server */
$prodPath = '';

/**
 * Standard practice is to have your keys in a directory named .ssh within your home dir.
 * Deployer will automatically find them there. If you keep your keys some where else,
 * uncomment and update the below
 */
//$privKey = 'c:\path\to\id_rsa';
//$pubKey  = 'c:\path\to\id_rsa.pub';


$recipieFile = dirname(__FILE__) . './vendor/gdmedia/deployer-recipes/silverstripe.php';
if (!is_file($recipieFile)) {
    echo PHP_EOL . "Guru recipes not found!" . PHP_EOL . "Please add gdmedia/deployer-recipes to your project by running" . PHP_EOL . PHP_EOL . "composer require gdmedia/deployer-recipes". PHP_EOL;
    exit(1);
}
require $recipieFile;

server('uat-server', 'uatlinux.gdmedia.tv')
        ->env('deploy_path', $uatPath)
        ->env('branch', "master")
        ->user('root')
        ->stage('uat')
        ->identityFile($pubKey, $privKey);

server('prod-server', 'vs1.gdmedia.tv')
        ->env('deploy_path', $prodPath)
        ->env('branch', "master")
        ->user('root')
        ->stage('prod')
        ->identityFile($pubKey, $privKey);

set('repository', lookupRepository());
set('default_stage', 'uat');