<?php

class InstitutionSearchPage extends Page
{

}

class InstitutionSearchPage_Controller extends Page_Controller
{
	private static $allowed_actions	 = array(
		'Form'
	);
	private static $url_handlers	 = array(
		'findthem' => 'Form'
	);

	public function Form()
	{

		$QualificationTypes = array();
		foreach (QualificationType::get()->filter("ParentID", 0)->sort("Name") as $QualificationType) {
			$QualificationTypes[$QualificationType->Name] = $QualificationType->Children()->sort("Name")->map()->toArray();
		}

		$fields		 = new FieldList(array(
			GroupedDropdownField::create("QualificationTypeID", "Degree", $QualificationTypes),
			TextField::create("CourseName", "Course"),
			DropdownField::create("CityID", "City", City::getCitiesWithInstitutions()->sort("Name")->map())->setEmptyString("Any city")
		));
		$actions	 = new FieldList(array(
			FormAction::create("doSearch")->setTitle("Find a College")
		));
		$validator	 = ZenValidator::create();
		$validator->addRequiredFields(array(
			'QualificationTypeID' => 'Please select a Degree'
		));
		$form		 = new Form($this, 'findthem', $fields, $actions, $validator);
		$form->setLegend("Criteria")->addExtraClass("form-horizontal")->setAttribute("data-toggle", "validator");

		$form->loadDataFrom($this->request->postVars());
		return $form;
	}

	public function doSearch()
	{
		$searchedForQualificationType	 = "any degree";
		$searchedForCourseName			 = "any course";
		$searchedForCity				 = "any city";

		$form			 = $this->Form();
		$redirectBack	 = false;
		$dataFilterd	 = filter_var_array($this->getRequest()->requestVars(),
								   array(
			"QualificationTypeID"	 => FILTER_SANITIZE_NUMBER_INT,
			"CourseName"			 => FILTER_SANITIZE_STRING,
			"CityID"				 => FILTER_SANITIZE_NUMBER_INT
		));
		$filters		 = array();

		if ($dataFilterd["QualificationTypeID"]) {
			$QualificationType = QualificationType::get()->byID($dataFilterd["QualificationTypeID"]);
			if ($QualificationType) {
				$searchedForQualificationType	 = $QualificationType->Name;
				$filters["Qualifications.ID"]	 = Qualification::get()->filter(array("TypeID" => $QualificationType->getDescendantIDList()))->column();
			} else {
				$form->addErrorMessage("QualificationTypeID", "Invalid Degree", "bad");
				$redirectBack = true;
			}
		}
		if ($dataFilterd["CourseName"]) {
			$searchedForQualificationType				 = $searchedForCourseName;
			$filters["Qualifications.Name:PartialMatch"] = $dataFilterd["CourseName"];
		}
		if ($dataFilterd["CityID"]) {
			$City = City::get()->byID($dataFilterd["CityID"]);
			if ($QualificationType) {
				$searchedForCity	 = $City->Name;
				$filters["CityID"]	 = $dataFilterd["CityID"];
			} else {
				$form->addErrorMessage("CityID", "Invalid City", "bad");
				$redirectBack = true;
			}
		}

		if ($redirectBack) {
			$results = $this->redirectBack();
		} else {
			$results = array(
				'IsSearching'	 => true,
				'DegreeType'	 => $searchedForQualificationType,
				'Course'		 => $searchedForCourseName,
				'City'			 => $searchedForCity,
				"Content"		 => "",
				"Form"			 => "",
				"Results"		 => new PaginatedList(Institution::get()->filter($filters), $this->getRequest())
			);
		}
		return $results;
	}
}