<?php

class ScrapeEdukasyon extends UniHubTask
{
	protected $title						 = "Scrape http://edukasyon.ph/";
	protected $description					 = "Import data from http://edukasyon.ph/";
	protected static $SITE					 = "http://edukasyon.ph";
	protected static $PATH					 = "/en/institutions/colleges?region=|NCR|&search_type=college&page=%PAGE%";
	protected static $LOGO_DIR				 = "institution/logos/";
	protected static $CITY_CACHE			 = array();
	protected static $QUALIFICATION_CACHE	 = array();

	/**
	 *
	 * @var PDO
	 */
	protected $db;

	public function run($request)
	{
		$result = true;
		if (!Director::isDev() && !$this->isCLI && !Permission::check("ADMIN")) {
			$result = Security::permissionFailure();
		} else {
			$this->renderHeader();
//			foreach (array("Institution", "Qualification", "QualificationType", "Qualification_Institutions", "Institution_Qualifications") as $table) {
//				DB::alteration_message("Truncated ".$table, "bad");
//				DB::query("TRUNCATE TABLE \"$table\"");
//			}
			$this->doImport();
			$this->renderFooter();
		}
		return $result;
	}

	public function doImport($page = 1)
	{
		$s = UniHub\Scraper\Edukasyon\EdukasyonList::create();
		foreach ($s->getDetails() as $details) {
			$Name = $details->getName();
			DB::alteration_message("Processing ".$Name, "created");

			$LogoURL	 = $details->getLogoURL();
			$Address	 = $details->getAddress();
			$Cost		 = $details->getCost();
			$Phones		 = $details->getPhoneNumbers();
			$Emails		 = $details->getEmail();
			$Websites	 = $details->getWebsite();
			$Courses	 = $details->getCourses();
			$logo		 = Image::create();
			if ($LogoURL) {
				$logoExt = pathinfo($LogoURL, PATHINFO_EXTENSION);
				$logo	 = $this->fetchLogo($LogoURL, \UniHub\Utils\String::slugify($Name)."-logo.".$logoExt);
			}

			$city = City::create();
			if ($Address) {
				$city = $this->findCityByAddress($Address);
			}
			$institution = Institution::get()->filter(array('Name' => $Name))->first();
			$isNew		 = !$institution;
			if ($isNew) {
				$institution = Institution::create();
			}
			$institution->Name			 = $Name;
			$institution->URLSegment	 = \UniHub\Utils\String::slugify($Name);
//            $institution->Motto          = "";
//            $institution->Description    = "";
//            $institution->TermType       = "";
//            $institution->EntranceExam   = "";
			$institution->PhoneNumbers	 = is_array($Phones) ? $Phones : array($Website);
			$institution->Websites		 = is_array($Websites) ? $Websites : array($Websites);
			$institution->EmailAddresses = is_array($Emails) ? $Emails : array($Emails);
			$institution->Address		 = $Address;
//            $institution->Locations      = "";
//            $institution->Accreditation  = "";
			$institution->CostMin		 = $Cost["Min"];
			$institution->CostMax		 = $Cost["Max"];
			$institution->CostType		 = $Cost["Type"];

			if ($logo && $logo->exists()) {
				$institution->LogoID = $logo->ID;
			}
			if ($city && $city->exists()) {
				$institution->CityID = $city->ID;
			}
			if ($Courses) {
				foreach ($Courses as $l1Course => $l2Courses) {
					$L1QualificationType = $this->findQualificationTypeByName($l1Course);
					foreach ($l2Courses as $l2Course => $l3Courses) {
						$L2QualificationType = $this->findQualificationTypeByName($l2Course, $L1QualificationType->ID);
						foreach ($l3Courses as $l3Course => $qualifications) {
							$L3QualificationType = $this->findQualificationTypeByName($l3Course, $L2QualificationType->ID);
							foreach ($qualifications as $qualificationName) {
								$qualification = $this->findQualificationByName($qualificationName, $L3QualificationType->ID);
								$institution->Qualifications()->add($qualification);
							}
						}
					}
				}
			}
			$institution->write();
			if ($institution->exists()) {
				DB::alteration_message(($isNew ? "Created" : "Updated")." ".$institution->Name, "created");
			} else {
				DB::alteration_message("Failed to save ".$institution->Name, "error");
			}
		};
	}

	public function findQualificationTypeByName($name, $parentID = 0)
	{
		$result = isset(self::$QUALIFICATION_CACHE[$name.$parentID]) ? self::$QUALIFICATION_CACHE[$name.$parentID] : null;
		if (!$result) {
			$result = QualificationType::get()->filter(array("Name" => $name, "ParentID" => $parentID))->first();
			if (!$result || !$result->exists()) {

				$result				 = QualificationType::create();
				$result->Name		 = $name;
				$result->ParentID	 = $parentID;
				$result->write();
			}
			self::$QUALIFICATION_CACHE[$name.$parentID] = $result;
		}
		return $result;
	}

	public function findQualificationByName($name, $typeID)
	{
		$result = isset(self::$QUALIFICATION_CACHE[$name.$typeID]) ? self::$QUALIFICATION_CACHE[$name.$typeID] : null;
		if (!$result) {
			$result = Qualification::get()->filter(array("Name" => $name, "TypeID" => $typeID))->first();
			if (!$result || !$result->exists()) {
				$result			 = Qualification::create();
				$result->Name	 = $name;
				$result->TypeID	 = $typeID;
				$result->write();
			}
			self::$QUALIFICATION_CACHE[$name.$typeID] = $result;
		}
		return $result;
	}

	public function findCityByAddress($address)
	{
		$addressParts	 = explode(", ", $address);
		$name			 = end($addressParts);

		$result = isset(self::$CITY_CACHE[$name]) ? self::$CITY_CACHE[$name] : null;
		if (!$result) {
			$regexes = array(
				'city$',
				'[0-9]+\s+',
				('city$|[0-9]+\s+')
			);

			$names = array(
				trim($name),
				trim(str_ireplace("metro", "", $name)),
			);
			foreach ($names as $name) {
				foreach ($regexes as $regex) {
					$names[] = trim(preg_replace('/'.$regex.'/i', "", trim($name)));
				}
			}
			$tried = array();
			foreach ($names as $cityName) {
				if (!in_array($cityName, $tried)) {
					$result	 = City::get()->find("Name", $cityName);
					$tried[] = $cityName;
					if ($result) {
						self::$CITY_CACHE[$name] = $result;
						break;
					}
				}
			}
		}

		return $result;
	}

	protected function fetchLogo($url, $filename = null)
	{
		$file			 = null;
		$path			 = self::$LOGO_DIR;
		$folder			 = Folder::find_or_make($path);
		$folderRelPath	 = $folder->getRelativePath();
		$folderAbsPath	 = Director::getAbsFile($folderRelPath);
		if (!$filename) {
			$filename = basename($url);
		}
		$relPath = $folderRelPath.$filename;
		$absPath = Director::getAbsFile($relPath);
		if (!file_exists($absPath)) {
			file_put_contents($absPath, file_get_contents($url));
		}

		$file = Image::find($relPath);
		if (!$file) {
			$file			 = Image::create();
			$file->ParentID	 = $folder->ID;
			$file->Filename	 = $relPath;
			$file->Name		 = $filename;
			$file->write();
		}

		return $file;
	}
}
//
//class EdukasonHelper
//{
//
//    public static function cleanString($string)
//    {
//        return trim(str_replace("–", "-", strip_tags(html_entity_decode($string, ENT_QUOTES))));
//    }
//
//    /**
//     *
//     * @param int $page
//     * @return PHPHtmlParser\Dom
//     */
//    public function fetchPage($page = 1)
//    {
//        $url = str_replace("%PAGE%", $page, self::$SITE.self::$PATH);
//        return $this->fetchURL($url);
//    }
//
//    /**
//     *
//     * @param string $path
//     * @return PHPHtmlParser\Dom
//     */
//    public function fetchDetails($path)
//    {
//        return $this->fetchURL(self::$SITE.$path);
//    }
//
//    /**
//     *
//     * @param string $url
//     * @return PHPHtmlParser\Dom
//     */
//    public function fetchURL($url)
//    {
//        $cacheKey   = md5($url);
//        $cache      = SS_Cache::factory(__CLASS__);
//        if (!($resultBody = $cache->load($cacheKey))) {
//            $client = new \GuzzleHttp\Client();
//            $res    = $client->request('GET', $url);
//            if ($res->getStatusCode() == 200) {
//                DB::alteration_message("Fetched $url", "created");
//                $resultBody = (string) $res->getBody();
//                $cache->save($resultBody, $cacheKey);
//            }
//        }
//        $dom = new PHPHtmlParser\Dom;
//        $dom->loadStr($resultBody, []);
//        return $dom;
//    }
//
//    public function slugify($text)
//    {
//        // replace non letter or digits by -
//        $text = preg_replace('~[^\pL\d]+~u', '-', str_replace(array('"', "'", chr(145), chr(146), chr(147), chr(148), chr(151)), "", $text));
//
//        // transliterate
//        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
//
//        // remove unwanted characters
//        $text = preg_replace('~[^-\w]+~', '', $text);
//
//        // trim
//        $text = trim($text, '-');
//
//        // remove duplicate -
//        $text = preg_replace('~-+~', '-', $text);
//
//        // lowercase
//        $text = strtolower($text);
//
//        if (empty($text)) {
//            return 'n-a';
//        }
//
//        return $text;
//    }
//}
//
//class EdukasonOverviewParser
//{
//    /**
//     *
//     * @var AbstractNode
//     */
//    protected $node;
//
//    public function __construct($node)
//    {
//        $this->node = $node;
//
//        $logoURL = $this->node->find(".img-responsive")->getAttribute("src");
//        $logoExt = pathinfo($logoURL, PATHINFO_EXTENSION);
//        $logo    = EdukasonHelper::fetchLogo($logoURL, EdukasonHelper::slugify($name)."-logo.".$logoExt);
////        $detailsHTML = EdukasonHelper::fetchDetails($detailsPath);
//    }
//
//    public function getName()
//    {
//        $el = $this->node->find("h3");
//        return $el->count() ? EdukasonHelper::cleanString($el->innerHtml()) : null;
//    }
//
//    public function getAddress()
//    {
//        $el = $this->node->find(".icons-list .fa-map-marker");
//        if ($el->count()) {
//            $el = $el->count() ? $el->getParent() : $el;
//        }
//        return $el->count() ? EdukasonHelper::cleanString($el->text()) : null;
//    }
//
//    public function getCost()
//    {
//        $result = array("Min" => 0, "Max" => 0);
//        $el     = $this->node->find(".icons-list .fa-money");
//        if ($el->count()) {
//            $el = $el->count() ? $el->getParent() : $el;
//            if ($el->count()) {
//                $cost = $el->count() ? EdukasonHelper::cleanString($el->text()) : null;
//                if ($cost) {
//                    $costEx        = explode(" - ", $cost);
//                    $result["Min"] = preg_replace("/\D/", "", current($costEx));
//                    if (isset($costEx[1])) {
//                        $result["Max"] = preg_replace("/\D/", "", $costEx[1]);
//                    }
//                }
//            }
//        }
//        return $result;
//    }
//}
//
//class EdukasonDetailsParser
//{
//    /**
//     *
//     * @var AbstractNode
//     */
//    protected $node;
//
//    public function __construct($node)
//    {
//        $this->node = $node;
//    }
//
//    public function getPhoneNumbers()
//    {
//        $result = array();
//        $el     = $this->node->find(".icons-list .fa-phone");
//        if ($el->count()) {
//            $el = $el->count() ? $el->getParent() : $el;
//            if ($el->count()) {
//                $phoneString = $el->count() ? EdukasonHelper::cleanString($el->text()) : null;
//                $phones      = explode(";", $phoneString);
//                $result      = array_map('trim', $phones);
//            }
//        }
//        return $result;
//    }
//
//    public function getEmail()
//    {
//        $el = $this->node->find(".container .icons-list .fa-envelope");
//        if ($el->count()) {
//            $el = $el->count() ? $el->getParent() : $el;
//        }
//        return $el->count() ? EdukasonHelper::cleanString($el->text()) : null;
//    }
//
//    public function getWebsite()
//    {
//        $el = $this->node->find(".container .icons-list .fa-globe");
//        if ($el->count()) {
//            $el = $el->count() ? $el->getParent() : $el;
//        }
//        return $el->count() ? EdukasonHelper::cleanString($el->text()) : null;
//    }
//}