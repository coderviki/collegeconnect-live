<?php

/**
 * @property string $Name
 * @property int $ParentID
 * @method QualificationType[] Children()
 * @method QualificationType Parent()
 * @method Qualification[] Qualifications()
 */
class QualificationType extends DataObject
{
    private static $db       = array(
        "Name" => "Text"
    );
    private static $has_many = array(
        "Children"       => "QualificationType.Parent",
        "Qualifications" => "Qualification"
    );
    private static $has_one  = array(
        "Parent" => "QualificationType"
    );

    public function getTreeTitle()
    {
        return $this->Name;
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab("Root.Main", TreeDropdownField::create("ParentID", "Parent", "QualificationType"));

        return $fields;
    }
}