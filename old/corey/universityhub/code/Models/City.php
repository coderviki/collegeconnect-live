<?php

/**
 * @property string $Name
 * @property string URLSegment
 * @method Province Province()
 * @method Institution[]|HasManyList Institutions()
 */
class City extends DataObject
{
	private static $db		 = array(
		"Name"		 => "Varchar(255)",
		"URLSegment" => "Varchar(255)",
	);
	private static $has_one	 = array(
		"Province" => "Province"
	);
	private static $has_many = array(
		"Institutions" => "Institution"
	);

	/**
	 *
	 * @return City[]|ArrayList
	 */
	public static function getCitiesWithInstitutions()
	{
		$rows = static::get()->leftJoin("Institution", "City.ID = Institution.CityID")
			->alterDataQuery(function(DataQuery $query) {
			$query
			->groupby("City.ID")
			->having("Count(Institution.ID) > 0");
		});
		return $rows;
	}

	/**
	 *
	 * @return City
	 */
	public static function getByURLSegment($URLSegment)
	{
		return static::get()->filter(array("URLSegment" => $URLSegment))->first();
	}
}