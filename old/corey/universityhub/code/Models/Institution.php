<?php

/**
 * @property string $Name
 * @property string $URLSegment
 * @property string $Motto
 * @property string $Description
 * @property string $Website
 * @property string $TermType
 * @property string $EntranceExam
 * @property array $PhoneNumbers
 * @property array $Websites
 * @property array $EmailAddresses
 * @property string $Address
 * @property string $Locations
 * @property string $Accreditation
 * @property float $CostMin
 * @property float $CostMax
 * @property string $CostType
 * @method City City()
 * @property int $CityID
 * @method Image Logo()
 * @property int $LogoID
 * @method Qualification[]|DataList Qualifications()
 */
class Institution extends DataObject
{
	private static $urlBase		 = null;
	private static $db			 = array(
		"URLSegment"	 => "Varchar(255)",
		"Name"			 => "Varchar(255)",
		"Motto"			 => "Text",
		"Description"	 => "HTMLText",
		"TermType"		 => "Enum('Trimester,Semester,Quarter','Semester')",
		"EntranceExam"	 => "Boolean",
		"PhoneNumbers"	 => "MultiValueField",
		"Websites"		 => "MultiValueField",
		"EmailAddresses" => "MultiValueField",
		"Address"		 => "Text",
		"Locations"		 => "HTMLText",
		"Accreditation"	 => "HTMLText",
		"CostMin"		 => "Currency",
		"CostMax"		 => "Currency",
		"CostType"		 => "Text",
	);
	private static $has_one		 = array(
		"City"	 => "City",
		"Logo"	 => "Image",
	);
	private static $many_many	 = array(
		"Qualifications" => "Qualification"
	);

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();
		$fields->addFieldToTab("Root.Main", DropdownField::create("CityID", "City", City::get()->map()), "Locations");

		return $fields;
	}

	/**
	 *
	 * @return Institution
	 */
	public static function getByURLSegment($URLSegment)
	{
		return static::get()->filter(array("URLSegment" => $URLSegment))->first();
	}

	public function Link()
	{
		if (!self::$urlBase) {
			/* @var $listingPage InstitutionListingPage */
			$listingPage = InstitutionListingPage::get()->first();
			if ($listingPage && $listingPage->exists()) {
				self::$urlBase = $listingPage->Link("details");
			}
		}
		return Controller::join_links(self::$urlBase,$this->URLSegment);
	}
}