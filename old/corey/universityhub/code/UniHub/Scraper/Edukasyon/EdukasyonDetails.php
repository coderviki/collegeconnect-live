<?php

namespace UniHub\Scraper\Edukasyon;

use Symfony\Component\DomCrawler\Crawler;

class EdukasyonDetails extends \UniHub\Scraper\Crawler
{

    /**
     *
     * @return Crawler
     */
    public static function create($url)
    {
        $res = self::fetchURL($url);
        return $res;
    }

    public function getName()
    {
        $el = $this->filter(".institution-contact-details h1");
        return \UniHub\Utils\String::clean($el);
    }

    public function getAddress()
    {
        $el = $this->filter(".institution-contact-details .fa-map-marker")->parents()->first();
        return \UniHub\Utils\String::clean($el->directText());
    }

    public function getCost()
    {
        $result     = array("Min" => 0, "Max" => 0, "Type" => "");
        $el         = $this->filter(".institution-contact-details .fa-money")->parents()->first();
        $costString = \UniHub\Utils\String::clean($el);
        if ($costString) {
            $costEx        = explode(" - ", $costString);
            $result["Min"] = floatval(preg_replace("/\D/", "", current($costEx)));
            if (isset($costEx[1])) {
                $result["Max"] = floatval(preg_replace("/\D/", "", $costEx[1]));
            }
            $typeMatches = array();
            preg_match('/(?<=\()(?<type>.+)(?=\))/is', $costString, $typeMatches);
            if (isset($typeMatches["type"])) {
                $result["Type"] = $typeMatches["type"];
            }
        }
        return array_map("trim", $result);
    }

    public function getPhoneNumbers()
    {
        $el = $this->filter(".institution-contact-details .fa-phone");
        if ($el->count()) {
            $el = $el->parents()->first();
        }
        return array_map("trim", explode(";", \UniHub\Utils\String::clean($el)));
    }

    public function getEmail()
    {
        $el = $this->filter(".institution-contact-details .fa-envelope");
        if ($el->count()) {
            $el = $el->parents()->first();
        }
        return \UniHub\Utils\String::clean($el);
    }

    public function getWebsite()
    {
        $el = $this->filter(".institution-contact-details .fa-globe");
        if ($el->count()) {
            $el = $el->parents()->first();
        }
        return \UniHub\Utils\String::clean($el);
    }

    public function getLogoURL()
    {
        $url = null;
        $el  = $this->filter(".institutions-show-heading img.img-responsive");
        if ($el->count()) {
            $url = $el->image()->getUri();
        }
        return $url;
    }

    public function getCourses()
    {
        $result = array();
        $el     = $this->filter("#courses-tab .course-list-view .panel-heading.course-heading-1 h3.panel-title");
        if ($el->count()) {
            $result = $el->each(function(EdukasyonDetails $heading) {
                $title            = \UniHub\Utils\String::clean($heading);
                $result           = new Course();
                $result->name     = $title;
                $result->children = $heading->parents()->first()->siblings()->filter("h4.panel-title")->each(function(EdukasyonDetails $heading) {
                    $title            = \UniHub\Utils\String::clean($heading);
                    $result           = new Course();
                    $result->name     = $title;
                    $result->children = $heading->parents()->first()->siblings()->filter("h5.panel-title")->each(function(EdukasyonDetails $heading) {
                        $title            = \UniHub\Utils\String::clean($heading);
                        $result           = new Course();
                        $result->name     = $title;
                        $result->children = $heading->parents()->first()->siblings()->filter("h6.panel-title")->each(function(EdukasyonDetails $heading) {
                            $title        = \UniHub\Utils\String::clean($heading);
                            $result       = new Course();
                            $result->name = $title;
                            return $result;
                        });
                        return $result;
                    });
                    return $result;
                });
                return $result;
            });
        }
        $courses = array();
        foreach ($result as $l1) {
            $courses[$l1->name] = array();
            foreach ($l1->children as $l2) {
                $courses[$l1->name][$l2->name] = array();
                foreach ($l2->children as $l3) {
                    $courses[$l1->name][$l2->name][$l3->name] = array();
                    foreach ($l3->children as $l4) {
                        $courses[$l1->name][$l2->name][$l3->name][] = $l4->name;
                    }
                }
            }
        }
        return $courses;
    }
}

class Course
{
    public $name;
    public $children;

}