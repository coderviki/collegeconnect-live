<?php

namespace UniHub\Utils;

class String
{

    public static function clean($string)
    {
        if ($string instanceof \DOMElement) {
            $string = $string->textContent;
        } else if ($string instanceof \Symfony\Component\DomCrawler\Crawler) {
            $string = $string->count() ? $string->text() : "";
        }
        return $string ? trim(
                self::SingleSpaces(
                    html_entity_decode(
                        strip_tags(
                            self::Normalize(
                                self::FixUTF8(
                                    $string
                                )
                            )
                        )
                        , ENT_QUOTES
                    )
                )
            ) : "";
    }

    protected static function SingleSpaces($string)
    {
        return preg_replace('/\s+/', ' ', $string);
    }

    protected static function FixUTF8($string)
    {
        return preg_replace('/[^\x0A\x20-\x7E]/', '', \ForceUTF8\Encoding::UTF8FixWin1252Chars($string));
    }

    protected static function Normalize($string)
    {
        $search = array(
            "\xC2\xAB", // « (U+00AB) in UTF-8
            "\xC2\xBB", // » (U+00BB) in UTF-8
            "\xE2\x80\x98", // ‘ (U+2018) in UTF-8
            "\xE2\x80\x99", // ’ (U+2019) in UTF-8
            "\xE2\x80\x9A", // ‚ (U+201A) in UTF-8
            "\xE2\x80\x9B", // ‛ (U+201B) in UTF-8
            "\xE2\x80\x9C", // “ (U+201C) in UTF-8
            "\xE2\x80\x9D", // ” (U+201D) in UTF-8
            "\xE2\x80\x9E", // „ (U+201E) in UTF-8
            "\xE2\x80\x9F", // ‟ (U+201F) in UTF-8
            "\xE2\x80\xB9", // ‹ (U+2039) in UTF-8
            "\xE2\x80\xBA", // › (U+203A) in UTF-8
            "\xE2\x80\x93", // – (U+2013) in UTF-8
            "\xE2\x80\x94", // — (U+2014) in UTF-8
            "\xE2\x80\xA6"  // … (U+2026) in UTF-8
        );

        $replace = array(
            "<<",
            ">>",
            "'",
            "'",
            "'",
            "'",
            '"',
            '"',
            '"',
            '"',
            "<",
            ">",
            "-",
            "-",
            "..."
        );

        return str_replace($search, $replace, $string);
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', str_replace(array('"', "'", chr(145), chr(146), chr(147), chr(148), chr(151)), "", $text));

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }


}