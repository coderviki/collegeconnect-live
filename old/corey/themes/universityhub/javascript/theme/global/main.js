/* global jQuery */
/* global dateFormat */
/* global moment, responsiveHelper */

(function ($) {
    "use strict";
    /**
     * Date and Datetime fields setup
     */
    $("input.time").datetimepicker({
//        pickDate: false
    });
    $("input.date").each(function () {
        $(this).attr("data-date-format", dateFormat.convert($(this).attr("data-jquerydateformat"), dateFormat.datepicker, dateFormat.momentJs));
    }).datetimepicker({
//        pickTime: false
    });
    $(".field.datetime.fieldgroup").each(function () {
        var
                $dateField = $("input.date", this),
                $timeField = $("input.time", this),
                $dateTimeField = $dateField.clone().attr("name", "datetimetemp").removeClass("date").addClass("datetime");
        $(".field", this).hide();
        $("> .middleColumn", this).append($dateTimeField);
        $dateTimeField.attr("data-date-format", $dateTimeField.attr("data-date-format") + " HH:mm:ss").datetimepicker({
//            pickDate: true,
//            pickTime: true
        }).on("dp.change", function (e) {
            $dateField.data("DateTimePicker").setDate(e.date);
            $timeField.data("DateTimePicker").setDate(e.date);
        });
    });
    /**
     * Multilevel bootstrap dropdowns setup
     */
    $(".dropdown-submenu")
            .on("click.bs.dropdown.data-api", function () {
                $(this).parents(".open").addClass("force-open");
            })
            .on("show.bs.dropdown", function () {
                $(this).parents(".force-open").addClass("open").removeClass("force-open");
            })
            .on("hide.bs.dropdown", function () {
                $(this).parents(".force-open").addClass("open").removeClass("force-open");
            });

    moment.locale("en-NZ"); // 'en'

    if (typeof debugHelper !== 'undefined' && debugHelper.isDev()) {
        responsiveHelper.init();
        responsiveHelper.showDebugBox();
    }

})(jQuery);