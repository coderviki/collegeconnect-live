<?php

class Page extends SiteTree
{
    private static $db              = array(
    );
    private static $has_one         = array(
    );
    private static $allowed_widgets = array(
    );

}

class Page_Controller extends ContentController
{
    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
    );

    public function init()
    {
        parent::init();
        // You can include any CSS or JS required by your project here.
        // See: http://doc.silverstripe.org/framework/en/reference/requirements
        $this->setRequirements();
    }

    public function setRequirements()
    {
        if (Director::isDev()) {
            Requirements::themedCSS("debug");
        }
        Requirements::set_force_js_to_bottom(true);

        $currentTheme = Config::inst()->get('SSViewer', 'theme');
        Requirements::combine_files($currentTheme."-main.css", array(
            "layout",
            "typography",
        ));
        Requirements::combine_files($currentTheme."-print.css", array(
            "print",
            ), "print");
        Requirements::combine_files($currentTheme."-lib.js", array(
            "vendor/guru-js-lib/lib",
            "vendor/bootstrap/ie10-viewport-bug-workaround",
            "vendor/bootstrap/bootstrap",
            "vendor/jquery-validation/jquery.validate",
            "vendor/moment/moment",
            "vendor/moment/moment-round",
            "vendor/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker",
        ));
        Requirements::combine_files($currentTheme."-main.js", array(
            "theme/global/main",
        ));
    }
}