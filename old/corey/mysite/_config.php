<?php

global $project;
$project = 'mysite';

global $database;
$database = '';

require_once('conf/ConfigureFromEnv.php');

// Set the site locale
i18n::set_locale('en_GB');

// Set the default timezone
date_default_timezone_set("Pacific/Auckland");

// Enable HTTP cache if this is a production environment (86400 seconds = one day)
if (defined("SS_ENVIRONMENT_TYPE") && SS_ENVIRONMENT_TYPE == "live") {
    HTTP::set_cache_age(86400);
}
