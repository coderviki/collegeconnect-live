<head>
<title>UniversityHub.ph | Philippine College Rankings, Reviews, Admissions</title>
<link href="http://www.universityhub.ph/wp-content/themes/splashpage/style.css" rel="stylesheet" />
</head>
<body>
<div class="background" style="">
	<div class="content">
		<img src="http://www.universityhub.ph/wp-content/themes/splashpage/emblemmatic-universityhub.ph-logo-11.png">
			<h1 style="font-size: 32px;">PHILIPPINE COLLEGE RANKINGS IN A WHOLE NEW WAY</h1>
			<h2 style="font-size: 18px;">Finding the right college just got easier.</h2>	
		<div style="width:100%;overflow: auto;padding-top: 50px;padding-bottom: 50px;">
			<div style="width:33%;float:left;"><strong>Rankings and Reviews</strong><br>Reduce the amount of time looking for the right college.</div>
			<div style="width:33%;float:left;"><strong>More detail</strong><br>Find all of the information you need in one place.</div>
			<div style="width:33%;float:left;"><strong>A whole new way</strong><br>Preparing for your future just got clearer.
</div>
	</div>
<div><strong>Yes! I want to know when your site opens! (enter your contact info below):</strong><br><br><?php echo do_shortcode('[contact-form-7 id="9" title="Lead Capture"]'); ?></div>
</div>
</body>