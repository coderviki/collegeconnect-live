<head>
<title>UniversityHub.ph | Philippine College Rankings, Reviews, Admissions</title>
<link href="http://www.universityhub.ph/wp-content/themes/splashpage/style.css" rel="stylesheet" />
</head>
<body>
<div class="background" style="">
<div style="padding-left: 80px; padding-right: 80px; padding-top: 40px;">
	<div style="width: 50%; float: left;">
<!-- <img src="http://www.universityhub.ph/wp-content/themes/splashpage/emblemmatic-universityhub.ph-logo-11-cropped.png"> -->
<!-- <span style="color:#ce3939;font-size:44px;">UniversityHub.ph</span> -->
<img src="http://www.universityhub.ph/wp-content/themes/splashpage/UniversityHub-noslogan-final.png" alt="Philippine College Rankings and Reviews - University Hub" style="height: 150px;">
	</div>
	<div style="width: 44%; float: left;text-align: right;">
<a href="https://www.facebook.com/UniversityHubph-753217828147399"><img src="http://www.universityhub.ph/wp-content/themes/splashpage/flat-social-icons_0002_facebook.png" style="height: 35px;" alt="UniversityHubPH Facebook"></a>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/universityhubph"><img src="http://www.universityhub.ph/wp-content/themes/splashpage/flat-social-icons_0008_twitter.png" style="height: 35px;" alt="UniversityHubPH Twitter"></a>
	</div>
</div>
<div style="padding-left: 80px;">
	<div style="float:left;width: 58.33333333%;text-shadow: 0px 1px 1px rgb(0, 0, 0);font-size: 21px;">
<br><h1 style="font-size: 50px;margin-bottom: 55px;line-height:0em;font-family: TimesNewRoman;"><i>Philippine College Rankings</i></h1>
<h1 style="font-size: 40px;margin-bottom: 30px;line-height:0em;font-family: TimesNewRoman;">in a Whole New Way</h1><br>
<h4 style="font-size: 33px;margin: 0px;">Rankings and Reviews</h4>
Reduce the amount of time looking for the right college.<br><br>
<h4 style="font-size: 33px;margin: 0px;">More Detail</h4>
Find all of the information you need in one place.<br><br>
<h4 style="font-size: 33px;margin: 0px;">A Whole New Way</h4>
Preparing for your future just got clearer.<br><br>
	</div>
<div style="float: left; color: #ce3939; border-radius: 5px; background-color: rgba(255, 255, 255, 0.6); padding: 15px;">
<strong style="font-size: 37px;/*text-shadow: 0px 1px 2px #ffffff;*/">Yes!</strong><br> <span style="/*text-shadow: 0px 1px 2px #ffffff;*/">I want to know when your site opens.</style><br><br><?php echo do_shortcode('[contact-form-7 id="9" title="Lead Capture"]'); ?>
</div>
</div>
</div>
</body>