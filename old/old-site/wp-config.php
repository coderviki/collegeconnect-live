<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'univers1_wp');

/** MySQL database username */
define('DB_USER', 'univers1_admin');

/** MySQL database password */
define('DB_PASSWORD', 'B@ctad89');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

$locale='en_US';

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3qoA1n3=Sq,B&Poz2rbScR1=ihk;1T2HhVPex@rZ&gv14%CnG:fq&fJjWxdk8zWb');
define('SECURE_AUTH_KEY',  '3CC$,ZePm0@ #^i[?~[I2$fFtW^K_u[APb#q@x[@g9lpu5Ww.)&KwV(Htctl{b$%');
define('LOGGED_IN_KEY',    'fokR]oI8FmO;MtW-i)vD~l8X%{jX M}{EVDQg[)tKkpds~#5@pKoi%!gQFmE)hKj');
define('NONCE_KEY',        'R|%C_^>#ilQ9Hd>A<>@,NZB=~x{:#S<&7}BOZh2@k:_NW]k0>ssC@Ioev#u-JfW>');
define('AUTH_SALT',        'i11Zovz,U`B0|Qe`2@&O]^YxA>wPa3>8*E}E3dL$ivRFulvhspHS[wl1:pn%[3hX');
define('SECURE_AUTH_SALT', '_j&KTtk??R5Z,ochS1ds{y PA,jWf8cWo^wn[Yw?b(yWk`P!`3h?f O*m3L@2KJ2');
define('LOGGED_IN_SALT',   '+dHH!p]_}b. R6nm1H^fWNl?o[DMy}.]yx:GfQNvCk?M;Y(9cT>Qw{H^(YpL/1LZ');
define('NONCE_SALT',       'zM7$#5W+$%uqC.33)0^]nn{ ny9R8R#]})K!Y5>?-^XHv`u9aTUQGSW3F$r_O10r');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
