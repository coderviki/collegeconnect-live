<?php

class InstitutionCoursesPage extends Page
{

}

class InstitutionCoursesPage_Controller extends Page_Controller
{

	public function index()
	{
		return $this->Listings();
	}

	public function Details()
	{
		$response				 = null;
		$institutionURLSegment	 = $this->getRequest()->param("InstitutionURLSegment");
		/* @var $institution Institution */
		$institution			 = $institutionURLSegment ? Institution::getByURLSegment($institutionURLSegment) : null;
		if ($institution && $institution->exists()) {
			$response = $this->customise(array(
				"Institution" => $institution,
				 "MetaTitle"    => $institution->Name 
			));
		} else {
			$response = $this->NotFound();
		}
		return $response;
	}

	public function Listings()
	{
		$filterForm		 = new InstitutionFilterForm($this, 'filter');
		$filterForm->setFormAction($this->Link("listings"));
		$filterForm->loadDataFrom($this->getRequest()->requestVars());
		$institutions	 = $filterForm->filterInstitutions(Institution::get());
		return $this->customise(array(
				"FilterForm"	 => $filterForm,
				"LinkBase"		 => $this->Link("details"),
				"Title"			 => $this->Title,
				"Institutions"	 => new PaginatedList($institutions, $this->getRequest())
		));
	}

	public function DisplayCourses()
	{
		return Qualification::get();
	}

}