<?php

class InstitutionListingPage extends Page
{

}

class InstitutionListingPage_Controller extends Page_Controller
{
	private static $allowed_actions	 = array(
		'Listings',
		'ByProvince',
		'ByCity',
		'Details',
	);
	private static $url_handlers	 = array(
		'details//$InstitutionURLSegment'							 => 'Details',
		'listings//$subAction'										 => 'Listings',
		'by-province//$ProvinceURLSegment/$InstitutionURLSegment'	 => 'ByProvince',
		'by-city//$CityURLSegment/$InstitutionURLSegment'			 => 'ByCity',
	);



	public function index()
	{
		return $this->Listings();
	}

	public function SideNavigationItems()
	{
		$nav			 = array(
			"All"				 => "listings",
			"List by Province"	 => "by-province",
			"List by City"		 => "by-city"
		);
		$result			 = ArrayList::create();
		$urlParams		 = $this->getURLParams();
		$currentAction	 = isset($urlParams["Action"]) ? $urlParams["Action"] : "listings";

		foreach ($nav as $title => $action) {
			$cssClass = $currentAction == $action ? "active" : "";
			$result->add(ArrayData::create(array("Link" => $this->Link($action), "Title" => $title, "CssClass" => $cssClass)));
		}
		return $result;
	}

	public function Details()
	{
		$response				 = null;
		$institutionURLSegment	 = $this->getRequest()->param("InstitutionURLSegment");
		/* @var $institution Institution */
		$institution			 = $institutionURLSegment ? Institution::getByURLSegment($institutionURLSegment) : null;
		if ($institution && $institution->exists()) {
			$response = $this->customise(array(
				"Institution" => $institution,
				 "MetaTitle"    => $institution->Name 
			));
		} else {
			$response = $this->NotFound();
		}
		return $response;
	}

	public function Listings()
	{
		$filterForm		 = new InstitutionFilterForm($this, 'filter');
		$filterForm->setFormAction($this->Link("listings"));
		$filterForm->loadDataFrom($this->getRequest()->requestVars());
		$institutions	 = $filterForm->filterInstitutions(Institution::get());
		return $this->customise(array(
				"FilterForm"	 => $filterForm,
				"LinkBase"		 => $this->Link("details"),
				"Title"			 => $this->Title,
				"Institutions"	 => new PaginatedList($institutions, $this->getRequest())
		));
	}

	public function NotFound()
	{
		$response = ErrorPage::response_for(404);
		$this->httpError(404, $response ? $response : 'The requested page could not be found.');
		return$response;
	}

	public function ByCity()
	{
		$response				 = null;
		$cityURLSegment			 = $this->getRequest()->param("CityURLSegment");
		$institutionURLSegment	 = $this->getRequest()->param("InstitutionURLSegment");
		if ($cityURLSegment) {
			$city = City::getByURLSegment($cityURLSegment);
			if ($institutionURLSegment && $city && $city->exists()) {
				$institution = $city->Institutions()->filter(array("URLSegment" => $institutionURLSegment))->first();
				if ($institution && $institution->exists()) {
					$response = $this->redirect($this->Link("details/".$institution->URLSegment));
				} else {
					$response = $this->NotFound();
				}
			} else if ($city && $city->exists()) {
				$response = $this->customise(array(
					"LinkBase"		 => $this->Link("by-city/".$city->URLSegment),
					"Title"			 => "Institutions in ".$city->Title,
					"Institutions"	 => new PaginatedList($city->Institutions()->sort("Name"), $this->getRequest())
				));
			} else {
				$response = $this->NotFound();
			}
		} else {
			$response = $this->customise(array(
				"LinkBase"	 => $this->Link("by-city"),
				"Title"		 => $this->Title." by City",
				"Cities"	 => new PaginatedList(City::getCitiesWithInstitutions()->sort("Name"), $this->getRequest())
			));
		}

		return $response;
	}

	public function ByProvince()
	{
		$response				 = null;
		$province				 = $this->getRequest()->param("ProvinceURLSegment");
		$institutionURLSegment	 = $this->getRequest()->param("InstitutionURLSegment");
		if ($province) {
			$province = Province::getByURLSegment($province);
			if ($institutionURLSegment && $province && $province->exists()) {
				$institution = $province->Institutions()->filter(array("URLSegment" => $institutionURLSegment))->first();
				if ($institution && $institution->exists()) {
					$response = $this->redirect($this->Link("details/".$institution->URLSegment));
				} else {
					$response = $this->NotFound();
				}
			} else if ($province && $province->exists()) {
				$response = $this->customise(array(
					"LinkBase"		 => $this->Link("by-province/".$province->URLSegment),
					"Title"			 => "Institutions in ".$province->Title,
					"Institutions"	 => new PaginatedList($province->Institutions()->sort("Name"), $this->getRequest())
				));
			} else {
				$response = $this->NotFound();
			}
		} else {
			$response = $this->customise(array(
				"LinkBase"	 => $this->Link("by-province"),
				"Title"		 => $this->Title." by Province",
				"Provinces"	 => new PaginatedList(Province::getProvincesWithInstitutions()->sort("Name"), $this->getRequest())
			));
		}

		return $response;
	}
}