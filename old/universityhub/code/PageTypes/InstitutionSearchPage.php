<?php

class InstitutionSearchPage extends Page
{

}

class LookupDataObject extends DataObject {
    static $db = array(
        'LookupQualification'               => 'Varchar',
    );
}

class InstitutionSearchPage_Controller extends Page_Controller
{
	private static $allowed_actions	 = array(
		'Form',
		'Details'
	);
	private static $url_handlers	 = array(
		'findthem' => 'Form',
		'details//$InstitutionURLSegment'							 => 'Details'
	);

	public function Form()
	{
		$QualificationTypes = array("Any degree");
		foreach (QualificationType::get()->filter("ParentID", 0)->sort("Name") as $QualificationType) {
			$QualificationTypes[$QualificationType->Name] = $QualificationType->Children()->sort("Name")->map()->toArray();
		}


		$fields		 = new FieldList(array(
			GroupedDropdownField::create("QualificationTypeID", "", $QualificationTypes)->setAttribute('placeholder','Type of degree')->setEmptyString("Any degree")->addExtraClass("chosen-select"),
			AutoCompleteField::create('CourseName','Course Name','',null,null,'LookupDataObject','LookupQualification'),
			DropdownField::create("CityID", "", City::getCitiesWithInstitutions()->sort("Name")->map())->setAttribute('placeholder','City')->setEmptyString("Any city")->addExtraClass("chosen-select")
		));
		$actions	 = new FieldList(array(
			FormAction::create("doSearch")->setTitle("Find a College")
		));
		$validator	 = ZenValidator::create();
		$validator->addRequiredFields(array(
			'QualificationTypeID' => 'Please select a Degree'
		));
		$form		 = new Form($this, 'findthem', $fields, $actions, $validator);
		$form->addExtraClass("form-inline college-search")->setAttribute("data-toggle", "validator");
		$form->loadDataFrom($this->request->postVars());
		$form->disableSecurityToken();
		return $form;
	}

	public function Details()
	{
		$response				 = null;
		$institutionURLSegment	 = $this->getRequest()->param("InstitutionURLSegment");
		/* @var $institution Institution */
		$institution			 = $institutionURLSegment ? Institution::getByURLSegment($institutionURLSegment) : null;
		if ($institution && $institution->exists()) {
			$response = $this->customise(array(
				"Institution" => $institution
			));
		} else {
			$response = $this->NotFound();
		}
		return $response;
	}

	public function Listings()
	{
		$filterForm		 = new InstitutionFilterForm($this, 'filter');
		$filterForm->setFormAction($this->Link("listings"));
		$filterForm->loadDataFrom($this->getRequest()->requestVars());
		$institutions	 = $filterForm->filterInstitutions(Institution::get());
		return $this->customise(array(
				"FilterForm"	 => $filterForm,
				"LinkBase"		 => $this->Link("details"),
				"Title"			 => $this->Title,
				"Institutions"	 => new PaginatedList($institutions, $this->getRequest())
		));
	}


	public function FilterForm()
	{
		$filterForm = new InstitutionFilterForm($this, 'filter');
		$filterForm->Fields()->push(HiddenField::create("QualificationTypeID", $this->getRequest()->postVar("QualificationTypeID")));
		$filterForm->Fields()->push(HiddenField::create("CourseName", $this->getRequest()->postVar("CourseName")));
		$filterForm->Fields()->push(HiddenField::create("CityID", $this->getRequest()->postVar("CityID")));
		$filterForm->loadDataFrom($this->getRequest()->requestVars());
		$filterForm->setFormAction($this->Link("findthem"));
		$filterForm->disableSecurityToken();
		$filterForm->Actions()->removeByName("action_filter");
		$filterForm->Actions()->push(FormAction::create("doSearch")->setTitle("Filter"));
		$filterForm->loadDataFrom($this->getRequest()->requestVars());
		return $filterForm;
	}

	public function doSearch()
	{
		$searchedForQualificationType	 = "any degree";
		$searchedForCourseName			 = "any course";
		$searchedForCity				 = "any city";

		$form			 = $this->Form();
		$redirectBack	 = false;
		$dataFilterd	 = filter_var_array($this->getRequest()->requestVars(),
								   array(
			"QualificationTypeID"	 => FILTER_SANITIZE_NUMBER_INT,
			"CourseName"			 => FILTER_SANITIZE_STRING,
			"CityID"				 => FILTER_SANITIZE_NUMBER_INT
		));
		$filters		 = array();

		if ($dataFilterd["QualificationTypeID"]) {
			$QualificationType = QualificationType::get()->byID($dataFilterd["QualificationTypeID"]);
			if ($QualificationType) {
				$searchedForQualificationType	 = $QualificationType->Name;
				$filters["Qualifications.ID"]	 = Qualification::get()->filter(array("TypeID" => $QualificationType->getDescendantIDList()))->column();
			} else {
				$form->addErrorMessage("QualificationTypeID", "Invalid Degree", "bad");
				$redirectBack = true;
			}
		}
		if ($dataFilterd["CourseName"]) {
			$searchedForQualificationType				 = $searchedForCourseName;
			$filters["Qualifications.Name:PartialMatch"] = $dataFilterd["CourseName"];
		}
		if ($dataFilterd["CityID"]) {
			$City = City::get()->byID($dataFilterd["CityID"]);
			if ($QualificationType) {
				$searchedForCity	 = $City->Name;
				$filters["CityID"]	 = $dataFilterd["CityID"];
			} else {
				$form->addErrorMessage("CityID", "Invalid City", "bad");
				$redirectBack = true;
			}
		}

		if ($redirectBack) {
			$results = $this->redirectBack();
		} else {
			$institutions	 = $this->FilterForm()->filterInstitutions(Institution::get()->filter($filters));
			$results		 = array(
				'IsSearching'	 => true,
				'DegreeType'	 => $searchedForQualificationType,
				'Course'		 => $searchedForCourseName,
				'City'			 => $searchedForCity,
				"Content"		 => "",
				"Form"			 => "",
				"Results"		 => new PaginatedList($institutions, $this->getRequest())
			);
		}
		return $results;
	}
}