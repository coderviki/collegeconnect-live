<?php

class InstitutionFilterForm extends Form
{

	public function __construct($controller, $name)
	{
		$fields	 = new FieldList(array(
			ListboxField::create('TermType', 'TermType', singleton('Institution')->dbObject('TermType')->enumValues(), "", null, true)->addExtraClass("chosen-select"),
			DropdownField::create("EntranceExam", "EntranceExam", array("0" => "No", "1" => "Yes"))->setEmptyString("Any"),
			ListboxField::create('Accreditations', 'Accreditations', InstitutionAccreditation::get()->map("ID", "Name")->toArray(), "", null, true)->addExtraClass("chosen-select"),
			ListboxField::create("CostMin", "CostMin", range(0, 100000, 10000)),
			ListboxField::create("CostMax", "CostMax", range(0, 100000, 10000)),
		));
		$actions = new FieldList(array(
			FormAction::create("filter")->setTitle("Filter")
		));
		parent::__construct($controller, $name, $fields, $actions);
		$this->addExtraClass("form-inline");
	}

	public function filterInstitutions(DataList $institutions)
	{
		$filter	 = array();
		$request = $this->getController()->getRequest();
		if ($request->requestVar("TermType")) {
			$filter["TermType"] = $request->requestVar("TermType");
		}
		if (in_array($request->requestVar("EntranceExam"), array("0", "1"))) {
			$filter["EntranceExam"] = $request->requestVar("EntranceExam");
		}
		if ($request->requestVar("Accreditations")) {
			$filter["Accreditations.ID"] = $request->requestVar("Accreditations");
		}
		if ($request->requestVar("CostMin")) {
			$filter["CostMin:GreaterThan"] = $request->requestVar("CostMin");
		}
		if ($request->requestVar("CostMax")) {
			$filter["CostMax:LessThan"] = $request->requestVar("CostMax");
		}
		if ($filter) {
			$institutions = $institutions->filter($filter);
		}
		return $institutions;
	}
}