<?php

/**
 * @property string $Name
 * @property string $URLSegment
 * @property string $Motto
 * @property string $Description
 * @property string $Website
 * @property string $TermType
 * @property string $EntranceExam
 * @property array $PhoneNumbers
 * @property array $Websites
 * @property array $EmailAddresses
 * @property string $Address
 * @property string $Locations
 * @property string $Accreditation
 * @property float $CostMin
 * @property float $CostMax
 * @property string $CostType
 * @method City City()
 * @property int $CityID
 * @method Image Logo()
 * @property int $LogoID
 * @method Qualification[]|DataList Qualifications()
 */
class InstitutionAccreditation extends DataObject
{
	private static $urlBase				 = null;
	private static $db					 = array(
		"Name"		 => "Varchar(255)",
		"URLSegment" => "Varchar(255)",
	);
	private static $belongs_many_many	 = array(
		"Institutions" => "Institution"
	);

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();
		$fields->removeByName("URLSegment");

		return $fields;
	}

	/**
	 *
	 * @return Institution
	 */
	public static function getByURLSegment($URLSegment)
	{
		return static::get()->filter(array("URLSegment" => $URLSegment))->first();
	}

	public function Link()
	{
		if (!self::$urlBase) {
			/* @var $listingPage InstitutionListingPage */
			$listingPage = InstitutionListingPage::get()->first();
			if ($listingPage && $listingPage->exists()) {
				self::$urlBase = $listingPage->Link("details");
			}
		}
		return Controller::join_links(self::$urlBase, $this->URLSegment);
	}
}