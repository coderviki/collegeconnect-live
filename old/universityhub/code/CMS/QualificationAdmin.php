<?php

class QualificationAdmin extends ModelAdmin
{
    public static $managed_models = array(
        'Qualification','QualificationType'
    );
    public static $url_segment    = 'qualification';
    public static $menu_title     = 'Qualifications';

}