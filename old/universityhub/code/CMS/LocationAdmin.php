<?php

class LocationAdmin extends ModelAdmin
{
    public static $managed_models = array(
        'City','Province'
    );
    public static $url_segment    = 'locations';
    public static $menu_title     = 'Locations';

}