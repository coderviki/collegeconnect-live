<?php

class DebugContorller extends Page_Controller
{
	private static $allowed_actions	 = array(
		'test',
		'colleges'
	);
	private static $url_handlers	 = array(
		'colleges/$City/$College' => 'colleges'
	);
	public $templates				 = array('index' => 'Page');


	public function NewHomePage()
	{

require __DIR__.'/UniHubDB.php';
$db = new UniHubDB();
?>
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main" style="height: 500px;">
		<div class="bg"></div>
		<form method="post" action="wp-content/themes/twentythirteen/findcolleges.corey.php" id="searchform">
			<input type="hidden" name="action" value="search"/>
			<div style="width:100%;">
				<div style="width:100%;float:left;"><h2 style="padding-top: 100px; position: relative; float: right; margin-right: 40px;">I want a
						<select name="QualificationType" style="    max-width: 252px;">
							<?php
							foreach ($db->GetRootQualificationTypes() as $parent => $children) {
								?><optgroup label="<?= $parent ?>"><?php
									foreach ($children as $id => $label) {
										?><option value="<?= $id ?>"><?= $label ?></option><?php
									}
								?></optgroup><?php
							}
							?>
						</select>
						<input type="text" name="QualificationName" placeholder="Insert Course here" id="topic_title">
						<input type="text" name="City" placeholder="Insert City here" id="topic_title2" >
						<div id="button-wrap-inner" style="/*float: right; width: 25%; padding-top: 130px; position: relative;*/ padding-left: 40%;/* position: absolute; */padding-top: 180px;">
							<input type="submit" name="submit" value="Find a College" class="btn">
						</div>
					</h2>
					<br>
				</div>
			</div>
		</form>
	</div><!-- #content -->

</div><!-- #primary -->
<script type="text/javascript">
    $(function () {
        $("#topic_title").autocomplete({
            source: "wp-content/themes/twentythirteen/ajax.php",
            minLength: 2,
            select: function (event, ui) {
                var value = ui.item.id;
                if (value != '#') {
                    alert(value);
                }
            },
            html: true, // optional (jquery.ui.autocomplete.html.js required)

            // optional (if other layers overlap autocomplete list)
            open: function (event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#topic_title2").autocomplete({
            source: "wp-content/themes/twentythirteen/ajax.php",
            minLength: 2,
            select: function (event, ui) {
                var value = ui.item.id;
                if (value != '#') {
                    alert(value);
                }
            },
            html: true, // optional (jquery.ui.autocomplete.html.js required)

            // optional (if other layers overlap autocomplete list)
            open: function (event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    });
</script>
	}


	public function index()
	{
		foreach (QualificationType::get()->where(array("ParentID" => 0)) as $qualificationType) {
			$this->printQualificationTypeAsUL($qualificationType);
		}
	}

	public function printQualificationTypeAsUL($qualificationType)
	{
		echo "<ul>";
		foreach ($qualificationType as $qualificationType) {
			/* @var $qualificationType Hierarchy */
			echo "<li>".$qualificationType->Title;
			if ($qualificationType->Children()->count()) {
				foreach ($qualificationType->Children() as $childQualificationType) {
					$this->printQualificationTypeAsUL($childQualificationType);
				}
			}
			$this->printQualifications($qualificationType);

			echo "</li>";
		}
		echo "</ul>";
	}

	public function printQualifications($qualificationType)
	{
		$qualifications = $qualificationType->Qualifications();
		if ($qualifications->count()) {
			echo "<ul>";
			foreach ($qualifications as $qualificationType) {
				/* @var $qualificationType Hierarchy */
				echo "<li><em>".$qualificationType->Title;
				echo "</em></li>";
			}
			echo "</ul>";
		}
	}

	public function test()
	{
		$fs = Institution::get()->filter(array('Qualifications.Name:PartialMatch' => 'Bach'));
		foreach ($fs as $f) {
			echo '<pre class="debug">'.__FILE__.'::'.__LINE__.' '.__FUNCTION__.PHP_EOL.''.print_r($f->toArray(), true).PHP_EOL.'</pre>';
		}
	}

	public function colleges()
	{
		$citySlug	 = $this->getRequest()->param("City");
		$collegeSlug = $this->getRequest()->param("College");
		$content	 = "";
		$title		 = "";
		if ($citySlug) {
			$city = City::get()->filter(array("Name:PartialMatch" => str_replace("-", "%", $citySlug)))->first();
			if ($city) {
				if ($collegeSlug) {
					$college = $city->Institutions()->filter(array("Name:PartialMatch" => str_replace("-", "%", $collegeSlug)))->first();
					if ($college) {
						$title = $college->Name;
						$content .= "<h2>".$college->Qualifications()->Count()." courses</h2>";
						$content .= $college->Logo()->ForTemplate();
					} else {
						$title = "Not found?";
					}
					$content .= "</br><a href='/testing/colleges/".\UniHub\Utils\String::slugify($city->Name)."'>Go back</a>";
				} else {
					$title = $city->Name;
					$content .= "<h2>".$city->Institutions()->Count()." Colleges</h2>";
					$content .= "<ul>";
					foreach ($city->Institutions() as $institution) {
						$content .= "<li>";
						$content .= "<a href='/testing/colleges/".\UniHub\Utils\String::slugify($city->Name)."/".\UniHub\Utils\String::slugify($institution->Name)."'>".$institution->Name." (".$institution->Qualifications()->Count()." courses listed)</a>";
						$content .= "</li>";
					}
					$content .= "</ul>";
					$content .= "</br><a href='/testing/colleges/'>Go back</a>";
				}
			} else {
				$title = "Not found?";
			}
		} else {
			$content .= "<ul>";
			foreach (City::get()->sort("Name") as $city) {
				if ($this->getRequest()->getVar("all") || $city->Institutions()->Count() > 0) {
					$content .= "<li>";
					$content .= "<a href='/testing/colleges/".\UniHub\Utils\String::slugify($city->Name)."'>".$city->Name." (".$city->Institutions()->Count()." Colleges listed)</a>";
					$content .= "</li>";
				}
			}
			$content .= "</ul>";
		}
		return array("Title" => $title, "Content" => $content);
	}

}