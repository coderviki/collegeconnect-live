<div class="results-list list-group">
    <header class="results-header alt">
        <h3 class="title">Provinces </h3>
        <span class="page-count"><% _t('SS_Theme.DISPLAYINGPAGE', 'Displaying Page') %> $ProvincesCurrentPage of $Provinces.TotalPages</span>
    </header>
    <% loop Provinces %>
    <article class="$EvenOdd list-group-item">
        <a href="$Top.LinkBase/$URLSegment">
            $Name
        </a>
    </article>
    <% end_loop %>
    <% with Provinces %>
    <% include Pagination %>
    <% end_with %>
</div>